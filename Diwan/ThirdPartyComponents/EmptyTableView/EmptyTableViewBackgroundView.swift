

import UIKit
import ViewAnimator
class EmptyTableViewBackgroundView: UIView {

    @IBOutlet weak var imageView: UIImageView!
   
    @IBOutlet weak var messageLabel: BaseUILabel!
    
    @IBOutlet weak var noResultLabel: BaseUILabel!
    
    @IBOutlet weak var actionBtn2: FlashButton!
    @IBOutlet weak var actionBtn: FlashButton!
    
    class func instanceFromNib() -> EmptyTableViewBackgroundView {
        return UINib(nibName: "EmptyTableViewBackgroundView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! EmptyTableViewBackgroundView
    }

}
class  UtilityEmptyListView {
    static func emptyTableViewMessage(message:String,image: String? = nil, tableView: UITableView, listType : ListViewType) {
        
        let emptyView = EmptyTableViewBackgroundView.instanceFromNib()
        emptyView.messageLabel.text = ""
        emptyView.noResultLabel.text = ""
        emptyView.actionBtn2.isHidden = true
        if let _ = image{
            emptyView.imageView.image = UIImage(named: image!)}
        switch listType {
        case .address:
            emptyView.messageLabel.isHidden = true
            emptyView.noResultLabel.isHidden = true
            emptyView.actionBtn.isHidden = false
            emptyView.actionBtn.setTitle("Add a New Address".localized, for: .normal)
            //UtilityEmptyListView.zoomAnimation(view: emptyView.imageView)
        case .store:
            emptyView.messageLabel.isHidden = true
            emptyView.noResultLabel.isHidden = false
            emptyView.imageView.isHidden = true
            emptyView.actionBtn.isHidden = true
            emptyView.noResultLabel.text = "No Record Found".localized
            emptyView.imageView.isHidden = false
            UtilityEmptyListView.zoomAnimation(view: emptyView.imageView)
        case .brand:
            emptyView.messageLabel.isHidden = true
            emptyView.noResultLabel.isHidden = false
            emptyView.imageView.isHidden = true
            emptyView.actionBtn.isHidden = true
            emptyView.noResultLabel.text = "No Record Found".localized
            emptyView.imageView.isHidden = false
            UtilityEmptyListView.zoomAnimation(view: emptyView.imageView)
        case .celebrity:
            emptyView.messageLabel.isHidden = true
            emptyView.noResultLabel.isHidden = false
            emptyView.imageView.isHidden = true
            emptyView.actionBtn.isHidden = true
            emptyView.noResultLabel.text = "No Record Found".localized
            emptyView.imageView.isHidden = true
            UtilityEmptyListView.zoomAnimation(view: emptyView.imageView)
        case .cart:
            emptyView.messageLabel.isHidden = true
            emptyView.noResultLabel.isHidden = false
            emptyView.imageView.isHidden = false
            emptyView.actionBtn.isHidden = false
            emptyView.actionBtn2.isHidden = false
            emptyView.noResultLabel.fontColorTheme = "themeBlue"
            emptyView.noResultLabel.fontNameTheme = "fontSFMedium"
            emptyView.noResultLabel.fontSizeTheme = "sizeLarge"
            emptyView.noResultLabel.text = "Cart is empty!".localized
            emptyView.actionBtn.fillThemeColor = "themeBlue"
            emptyView.actionBtn.setTitle("Add from your Wishlist".localized, for: .normal)
            emptyView.actionBtn2.fillThemeColor = "themeSkyBlue2"
            emptyView.actionBtn2.setTitle("Shop More".localized, for: .normal)
            UtilityEmptyListView.zoomAnimation(view: emptyView.imageView)
            
        default:
            print("")
        }
        //        emptyView.messageLabel.text = message
        //        emptyView.noResultLabel.text = message
        emptyView.backgroundColor = UIColor.clear
        tableView.backgroundView = emptyView
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.clear
        
    }
    
    class func hideEmptyTableViewMessage(tableView:UITableView) {
        
        tableView.backgroundView = nil
        
    }
    class func hideEmptyCollectionViewMessage(collectionView:UICollectionView) {
        
        collectionView.backgroundView = nil
        
    }
    static func emptycollectionViewMessage(message:String, image: String? = nil, collectionView: UICollectionView, listType : ListViewType) {
        let emptyView = EmptyTableViewBackgroundView.instanceFromNib()
        emptyView.messageLabel.isHidden = true
        emptyView.noResultLabel.isHidden = false
        emptyView.actionBtn.isHidden = true
        emptyView.imageView.isHidden = true
        emptyView.actionBtn2.isHidden = true
        switch listType {
        case .address:
            emptyView.messageLabel.text = message
            print("")
        case .store:
            emptyView.messageLabel.text = message
            print("")
        case .wishlist:
            emptyView.messageLabel.isHidden = false
            emptyView.noResultLabel.isHidden = false
            emptyView.actionBtn.isHidden = false
            emptyView.actionBtn.fillThemeColor = "themeSkyBlue2"
            emptyView.noResultLabel.fontColorTheme = "themeBlue"
            emptyView.noResultLabel.fontNameTheme = "fontSFMedium"
            emptyView.noResultLabel.fontSizeTheme = "sizeLarge"
            emptyView.messageLabel.fontColorTheme = "grayplaceholder"
            emptyView.messageLabel.fontNameTheme = "fontRegular"
            emptyView.messageLabel.fontSizeTheme = "sizeLarge"
            emptyView.actionBtn.setTitle("Start Shopping".localized, for: .normal)
            emptyView.noResultLabel.text = "Your Wishlist is empty".localized
            emptyView.messageLabel.text = "Add your favorite products here and buy them whenever you like!".localized
            UtilityEmptyListView.zoomAnimation(view: emptyView.imageView)
            print("")
        case .pharmacy:
            emptyView.messageLabel.isHidden = true
            emptyView.noResultLabel.isHidden = false
            emptyView.imageView.isHidden = true
            emptyView.actionBtn.isHidden = true
            emptyView.noResultLabel.text = "No Record Found".localized
            emptyView.imageView.isHidden = false
            emptyView.actionBtn.setTitle("Refresh".localized, for: .normal)
            UtilityEmptyListView.zoomAnimation(view: emptyView.imageView)
            print("")
        case .author:
            emptyView.messageLabel.text = message
            UtilityEmptyListView.zoomAnimation(view: emptyView.imageView)
            print("")
        case .normal:
            emptyView.messageLabel.text = message
            print("")
        default:
            emptyView.messageLabel.text = message
            print("")
        }
        if let _ = image{
            emptyView.imageView.isHidden = false
            emptyView.imageView.image = UIImage(named: image!)
        }
        
        emptyView.backgroundColor = .clear
        collectionView.backgroundView = emptyView
    }
    static func zoomAnimation(view : UIView) {
        let zoomAnimation = AnimationType.zoom(scale: 0.2)
        UIView.animate(views: [view],
        animations: [zoomAnimation],
        duration: 0.5)
    }
}
