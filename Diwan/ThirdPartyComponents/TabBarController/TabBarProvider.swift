//
//  TabBarProvider.swift
//  Diwan
//
//  Created by Muzamil Hassan on 01/07/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
//import ESTabBarController
import ESTabBarController_swift

enum TabBarProvider {

    static func customStyle() -> ESTabBarController {
        let tabBarController = ESTabBarController()
        let v1 = StoreCategoryListController.loadVC()
        let v2 = StoreCategoryListController.loadVC()
        let v3 = StoreCategoryListController.loadVC()
        let v4 = StoreCategoryListController.loadVC()
        let v5 = StoreCategoryListController.loadVC()
        
        v1.tabBarItem = ESTabBarItem.init(title: "Home", image: UIImage(named: "home"), selectedImage: UIImage(named: "home_1"))
        v2.tabBarItem = ESTabBarItem.init(title: "Find", image: UIImage(named: "find"), selectedImage: UIImage(named: "find_1"))
        v3.tabBarItem = ESTabBarItem.init(title: "Photo", image: UIImage(named: "photo"), selectedImage: UIImage(named: "photo_1"))
        v4.tabBarItem = ESTabBarItem.init(title: "Favor", image: UIImage(named: "favor"), selectedImage: UIImage(named: "favor_1"))
        v5.tabBarItem = ESTabBarItem.init(title: "Me", image: UIImage(named: "me"), selectedImage: UIImage(named: "me_1"))
        
        tabBarController.viewControllers = [v1, v2, v3, v4, v5]
        tabBarController.selectedIndex = 1
        return tabBarController
    }
    
    static func navigationWithTabbarStyle() -> TabBarNavigationController {
        let tabBarController = TabBarProvider.customStyle()
        let navigationController = TabBarNavigationController.init(rootViewController: tabBarController)
        tabBarController.title = "Example"
        return navigationController
    }
    static func tabbarWithNavigationStyle(index : Int) -> ESTabBarController {
        let tabBarController = ESTabBarController()
        let v1 = HomeViewController.loadVC()
        let v2 = PharmacyMainCategoryController.loadVC()
        let v3 = CelebritiesMainListController.loadVC()
        let v4 = LibraryMainListController.loadVC()
        let v5 = StoreCategoryListController.loadVC()
        
        v1.tabBarItem = ESTabBarItem.init(ExampleBouncesContentView(), title: "Home".localized, image: UIImage(named: "tabbar_home"), selectedImage: UIImage(named: "tabbar_home_sel"))
        v2.tabBarItem = ESTabBarItem.init(ExampleBouncesContentView(), title: "Pharmacy".localized, image: UIImage(named: "tabbar_pharmacy"), selectedImage: UIImage(named: "tabbar_pharmacy_sel"))
        v3.tabBarItem = ESTabBarItem.init(ExampleBouncesContentView(), title: "Celebrities".localized, image: UIImage(named: "tabbar_celebrities"), selectedImage: UIImage(named: "tabbar_celebrities_sel"))
        v4.tabBarItem = ESTabBarItem.init(ExampleBouncesContentView(), title: "Library".localized, image: UIImage(named: "tabbar_library"), selectedImage: UIImage(named: "tabbar_library_sel"))
        v5.tabBarItem = ESTabBarItem.init(ExampleBouncesContentView(), title: "Store".localized, image: UIImage(named: "tabbar_store"), selectedImage: UIImage(named: "tabbar_store_sel"))
        let n1 = BaseNavigationController.init(rootViewController: v1)
        let n2 = BaseNavigationController.init(rootViewController: v2)
        let n3 = BaseNavigationController.init(rootViewController: v3)
        let n4 = BaseNavigationController.init(rootViewController: v4)
        let n5 = BaseNavigationController.init(rootViewController: v5)
        
        v1.title = "Home".localized
        v2.title = "Pharmacy".localized
        v3.title = "Celebrities".localized
        v4.title = "Library".localized
        v5.title = "Store".localized
        
        /*UITabBar.appearance().barTintColor = UIColor.clear
        UITabBar.appearance().shadowImage = UIImage()
        UITabBar.appearance().backgroundImage = UIImage()*/
        
        tabBarController.viewControllers = [n1, n2, n3, n4, n5]
        //tabBarController.tabBar.barTintColor = ColorManager.color(forKey: "themeBlue")!
        tabBarController.selectedIndex = index
        tabBarController.tabBar.barTintColor = UIColor.clear
        tabBarController.tabBar.shadowImage = UIImage()
        tabBarController.tabBar.backgroundImage = UIImage()
        tabBarController.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: FontManager.constant(forKey: "fontRegular")!, size: FontManager.constant(forKey: "sizeXSmall")!)!], for: .normal)
        tabBarController.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: FontManager.constant(forKey: "fontRegular")!, size: FontManager.constant(forKey: "sizeXSmall")!)!], for: .selected)

        let vv = UIView(frame: CGRect(x: 0, y: 0, width: tabBarController.view.frame.size.width, height: tabBarController.view.frame.size.height))
        vv.backgroundColor = ColorManager.color(forKey: "themeBlue")!
        vv.layer.cornerRadius = 10
        vv.layer.masksToBounds = true
        tabBarController.tabBar.insertSubview(vv, at: 0)
        return tabBarController
    }
}
class TabBarNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appearance = UIBarButtonItem.appearance()
        appearance.setBackButtonTitlePositionAdjustment(UIOffset.init(horizontal: 0.0, vertical: -60), for: .default)
        self.navigationBar.isTranslucent = true
        self.navigationBar.barTintColor = UIColor.init(red: 250/255.0, green: 250/255.0, blue: 250/255.0, alpha: 0.8)
        #if swift(>=4.0)
            self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.init(red: 38/255.0, green: 38/255.0, blue: 38/255.0, alpha: 1.0), NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16.0)]
        #elseif swift(>=3.0)
            self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.init(red: 38/255.0, green: 38/255.0, blue: 38/255.0, alpha: 1.0), NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)];
        #endif
        self.navigationBar.tintColor = UIColor.init(red: 38/255.0, green: 38/255.0, blue: 38/255.0, alpha: 1.0)
        self.navigationItem.title = "Example"
    }
    
}
class ExampleBouncesContentView: ExampleBasicContentView {

    public var duration = 0.3

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func selectAnimation(animated: Bool, completion: (() -> ())?) {
        self.bounceAnimationTab()
        completion?()
    }

    override func reselectAnimation(animated: Bool, completion: (() -> ())?) {
        self.bounceAnimationTab()
        completion?()
    }
    
    func bounceAnimationTab() {
//        let impliesAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
//        impliesAnimation.values = [1.0 ,1.4, 0.9, 1.15, 0.95, 1.02, 1.0]
//        impliesAnimation.duration = duration * 2
//        impliesAnimation.calculationMode = CAAnimationCalculationMode.cubic
//        imageView.layer.add(impliesAnimation, forKey: nil)
        
        let impliesAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        impliesAnimation.values = [1.15, 0.8, 1.15]
        impliesAnimation.duration = 0.3
        impliesAnimation.calculationMode = CAAnimationCalculationMode.cubic
        impliesAnimation.isRemovedOnCompletion = true
        imageView.layer.add(impliesAnimation, forKey: nil)
    }
}
class ExampleBasicContentView: ESTabBarItemContentView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        textColor = ColorManager.color(forKey: "white")!//UIColor.init(white: 175.0 / 255.0, alpha: 1.0)
        highlightTextColor = ColorManager.color(forKey: "themeSkyBlue2")!
        iconColor = ColorManager.color(forKey: "white")!
        highlightIconColor = ColorManager.color(forKey: "themeSkyBlue2")!
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
