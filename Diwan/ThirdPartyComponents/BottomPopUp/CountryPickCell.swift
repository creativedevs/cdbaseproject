//
//  CountryPickCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 16/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class CountryPickCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(dataList : CountryStateCityProtocol?, type : PopUpListType?){
    
        if let country = dataList as? Country {
            titleLbl.text = country.title
        }
        if let state = dataList as? State {
            titleLbl.text = state.title
        }
        if let city = dataList as? City {
            titleLbl.text = city.title
        }
        
    }
    
}
