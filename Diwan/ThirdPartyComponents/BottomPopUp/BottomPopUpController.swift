//
//  BottomPopUpController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import BottomPopup

enum PopUpListType {
    case country
    case state
    case city
}

class BottomPopUpController: BaseViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var topView: UIView!
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    var searching = false
    var dataList : [CountryStateCityProtocol]?
    var searchDataList : [CountryStateCityProtocol]?
    var popupListType : PopUpListType?
    var didTapOnCell : (((_ data : CountryStateCityProtocol) -> Void))?
    let searchController = UISearchController(searchResultsController: nil)
        @IBAction func dismissButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    // Bottom popup attribute variables
    // You can override the desired variable to change appearance
    
//    override var popupHeight: CGFloat { return height ?? CGFloat(300) }
//    
//    override var popupTopCornerRadius: CGFloat { return topCornerRadius ?? CGFloat(10) }
//    
//    override var popupPresentDuration: Double { return presentDuration ?? 1.0 }
//    
//    override var popupDismissDuration: Double { return dismissDuration ?? 1.0 }
//    
//    override var popupShouldDismissInteractivelty: Bool { return shouldDismissInteractivelty ?? true }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.view.frame.height)
     //   setSearhBarInitial()
       // tblView.registerCell(CountryPickCell.self)
        tblView.registerCell(CountryPickCell.self)
        searchBar.delegate = self
        setupAppDefaultNavigationBar()
        createCrossBtn()
    }
    func createCrossBtn() {
        let img = UIImage.init(named: "crossbtn")?.flipIfNeeded()
        self.addBarButtonItemWithImage(img ?? UIImage(), .BarButtonItemPositionLeft, self, #selector(self.popViewController))
    }
    override func popViewController() {
        self.dismiss(animated: true) {
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       // tblView.contentInsetAdjustmentBehavior = .never
    }
    func setInitialData(dataList : [CountryStateCityProtocol], type : PopUpListType) {
        
        self.dataList = dataList
        self.searchDataList = dataList
        self.popupListType = type
        print(self.dataList)
        switch type {
        case .country:
            self.navigationItem.title = "Countries".localized
        case .city:
            self.navigationItem.title = "Cities".localized
        case .state:
            self.navigationItem.title = "Sates".localized
        }
        
    }
    func reloadData() {
        
        tblView.reloadData()
    }
    func setSearhBarInitial() {
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tblView.tableHeaderView = searchController.searchBar
        // 4
       // navigationItem.searchController = searchController
        // 5
        //topView.addSubview(searchController.searchBar) //= searchController.searchBar
        definesPresentationContext = false
    }
    //override var popupDimmingViewAlpha: CGFloat { return BottomPopupConstants.kDimmingViewDefaultAlphaValue }

}
extension BottomPopUpController : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        
        print(searchController)
    }
//    func filterContentForSearchText(_ searchText: String,
//                                    category: Candy.Category? = nil) {
//      filteredCandies = candies.filter { (candy: Candy) -> Bool in
//        return candy.name.lowercased().contains(searchText.lowercased())
//      }
//
//      tableView.reloadData()
//    }

    
    
}
extension BottomPopUpController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchDataList?.count ?? 0
//        if searching {
//            return searchDataList?.count ?? 0
//        } else {
//            return dataList?.count ?? 0
//        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryPickCell") as? CountryPickCell
        cell?.selectionStyle = .none
        cell?.setData(dataList: searchDataList?[indexPath.row], type: popupListType)
//        if searching {
//            cell?.textLabel?.text = searchedCountry[indexPath.row]
//        } else {
//            cell?.textLabel?.text = countryNameArr[indexPath.row]
//        }
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didTapOnCell?((searchDataList?[indexPath.row])!)
        dismiss(animated: true, completion: nil)
    }
    
}
extension BottomPopUpController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        switch self.popupListType {
        case .country:
            let data = dataList as? [Country]
            searchDataList = data?.filter({$0.title.lowercased().prefix(searchText.count) == searchText.lowercased()})
        case .state:
            let data = dataList as? [State]
            searchDataList = data?.filter({$0.title.lowercased().prefix(searchText.count) == searchText.lowercased()})
        case .city:
            let data = dataList as? [City]
            searchDataList = data?.filter({$0.title.lowercased().prefix(searchText.count) == searchText.lowercased()})
        default:
            print("")
        }
        
        searching = true
        tblView.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        tblView.reloadData()
    }
    
}
