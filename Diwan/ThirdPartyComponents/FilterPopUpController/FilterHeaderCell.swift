//
//  FilterHeaderCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 19/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import ExpyTableView
class FilterHeaderCell: UITableViewCell {

    @IBOutlet weak var titleLbl: BaseUILabel!
    @IBOutlet weak var radioSelected: UIImageView!
    @IBOutlet weak var radioBtn: UIButton!
    var mainFilter : FilterModel?
    var didTapOnEnd : ((() -> Void))?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(dataList : SortFilterProtocol?, type : MainPopUpListType?){
    
        if let sort = dataList as? SortModel {
            titleLbl.text = sort.title
            radioBtn.isHidden = true
            radioSelected.image = sort.isSelected ? UIImage(named: "filteradioselected") : UIImage(named: "filteradiounselected")
        }
        if let filter = dataList as? FilterModel {
            self.mainFilter = filter
            titleLbl.text = filter.filterEntityName
            radioSelected.image = filter.isSelected ? UIImage(named: "login_boxtick1") : UIImage(named: "login_boxwithouttick1")
        }
//        if let state = dataList as? State {
//            titleLbl.text = state.title
//        }
//        if let city = dataList as? City {
//            titleLbl.text = city.title
//        }
        
    }
        func canExpandCell(dataList : SortFilterProtocol?) ->  Bool{
        
            if let sort = dataList as? SortModel {
                return sort.hasChild
            }
            return false
        }
    @IBAction func radioBtnPressed(_ sender: Any) {
        self.didTapOnEnd?()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension FilterHeaderCell : ExpyTableViewHeaderCell {
    
    func changeState(_ state: ExpyState, cellReuseStatus cellReuse: Bool) {
        
//        if !canAnimate {
//            return
//        }
        
        switch state {
        case .willExpand:
            print("WILL EXPAND")
           // arrowDown(animated: !cellReuse)
            
        case .willCollapse:
            print("WILL COLLAPSE")
           // arrowRight(animated: !cellReuse)
            
        case .didExpand:
            print("DID EXPAND")
            
        case .didCollapse:
            print("DID COLLAPSE")
        }
    }
    
}
