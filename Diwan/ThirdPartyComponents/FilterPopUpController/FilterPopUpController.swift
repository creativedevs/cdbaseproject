//
//  BottomPopUpController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import BottomPopup
import ExpyTableView
enum MainPopUpListType {
    case filter(type : FilterScreenListType)
    case sort
}
enum FilterScreenListType : Int {
    case none = 0
    case bookPharmacy = 1
    case librarySection = 2
    case libraryGenre = 3
    case store = 4
    case author = 5
    case brand = 6
    case favorite = 9
}
enum FilterParamKey : Int {
    case none = 0
    case celebrities = 1
    case brands = 2
    case author = 3
    case genre = 4
    case sections = 5
    case avgCustomerReview = 6
    case subjects = 7
    case tagsByBrands = 8
    case avgStartRating = 9
    case storeCategories = 10
}
enum FilterSelectionType : Int {
    case single = 1     //"single"
    case multi = 2      //"multi"
    case starRating = 4 //"star-single"
    
}
class FilterPopUpController: UIViewController {

    @IBOutlet weak var crossBtn: BounceButton!
    @IBOutlet weak var applyBtn: FlashButton!
    @IBOutlet weak var clearBtn: FlashButton!
    @IBOutlet weak var doneBtn: FlashButton!
    @IBOutlet weak var filterBtn: BounceButton!
    @IBOutlet weak var sortBtn: BounceButton!
    @IBOutlet weak var expendTableView: ExpyTableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var topView: BaseUIView!
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    var dataList : [SortFilterProtocol]?
    var popupListType : MainPopUpListType?
    var didTapOnCell : (((_ data : SortFilterProtocol) -> Void))?
    var didTapOnSortDone : (((_ data : [SortModel]) -> Void))?
    var didTapOnFilterDone : (((_ data : [FilterModel]) -> Void))?
    var didTapOnFilterClear : (((_ data : [FilterModel]) -> Void))?
    override func viewDidLoad() {
        
        super.viewDidLoad()
        topView.backgroundColor = ColorManager.color(forKey: "rounderbordercolor")!
        setUpTable()
        setupButton()
        //tblView.registerCell(CountryPickCell.self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    func setupButton() {
        
        if (CurrentUser.languageId == .arabic) {
            sortBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -20)
            filterBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -20)
        }
        else {
            sortBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
            filterBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
        }
        crossBtn.didTapOnBounceEnd = { [weak self] in
                self?.dismiss(animated: true) {
                }
        }
        doneBtn.didTapOnBounceEnd = {[weak self] in
            print("DONE")
            self?.dismiss(animated: true) {
                self?.didTapOnSortDone?(self?.dataList as! [SortModel])
            }
        }
        clearBtn.didTapOnBounceEnd = {[weak self] in
            print("CLEAR")
            let list = self?.dataList as! [FilterModel]
            list.forEach { (model) in
                   model.isSelected = false
                    model.filters.forEach { (subModel) in
                        subModel.isSelected = false
                    }
            }
            self?.dismiss(animated: true) {
                self?.didTapOnFilterClear?(list)
            }
            
        }
        applyBtn.didTapOnBounceEnd = {[weak self] in
            print("APPLY")
            self?.dismiss(animated: true) {
                self?.didTapOnFilterDone?(self?.dataList as! [FilterModel])
            }
        }
        
    }
    func setUpTable(){
        
        expendTableView.registerCell(FilterHeaderCell.self)
        expendTableView.registerCell(FilterOptionsCell.self)
        //expendTableView.layer.borderWidth = 1
        //expendTableView.layer.borderColor = ColorManager.color(forKey: "grayplaceholder")!.cgColor

    }
    
    func setData(dataList : [SortFilterProtocol], type : MainPopUpListType) {
        
        self.dataList = dataList
        self.popupListType = type
        switch type {
        case .sort:
            applyBtn.isHidden = true
            clearBtn.isHidden = true
            filterBtn.isHidden = true
            crossBtn.isHidden = true
        case .filter(type: .store):
            doneBtn.isHidden = true
            sortBtn.isHidden = true
        default:
            print("")
        }
        expendTableView.reloadData()
    }
    
    
    
    //override var popupDimmingViewAlpha: CGFloat { return BottomPopupConstants.kDimmingViewDefaultAlphaValue }

}
// MARK: - Expending TableView Delegate
extension FilterPopUpController: ExpyTableViewDataSource , ExpyTableViewDelegate {
    
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
        
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterHeaderCell.identifier()) as! FilterHeaderCell
            cell.selectionStyle = .none
        cell.setData(dataList: dataList![section], type: self.popupListType)
        cell.didTapOnEnd = {[weak self] in
            print(section)
            let data = self?.dataList![section]
            let filterList = self?.dataList as! [FilterModel]
            if let filter = data as? FilterModel {
                filter.isSelected = !filter.isSelected
                cell.radioSelected.image = filter.isSelected ? UIImage(named: "login_boxtick1") : UIImage(named: "login_boxwithouttick1")
                if filter.isSelected == false {
                    filter.filters.forEach { (subModel) in
                        subModel.isSelected = false
                        subModel.starRating = 0
                    }
                    
                    tableView.collapse(section)
                }
                else {
                    tableView.expand(section)
                    
                }
            }
            //tableView.reloadData()
            //tableView.reloadSections([section], with: .none)
        }
 //           cell.lblQuestion.text = self.faqList[section].question
//            if faqData[section].question == nil {
//                cell.canAnimate = false
//            }
            return cell
    //    }

    }
    
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
//        print(faqData[section].guidelineId)
        
//        let data = dataList![section]
//        if let sort = data as? SortModel {
//            return sort.hasChild
//        }
        
        return true
    }
    
    func canExpand(section: Int, inTableView tableView: ExpyTableView) -> Bool {

        let data = dataList![section]
              if let sort = data as? SortModel {
                  return sort.hasChild
              }
        return false
    }
    
   /* func expandableCell(forSection section: Int, inTableView tableView: ExpyTableView) -> UITableViewCell {
        
        if section == faqData.count && shouldShowLoadingCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell") as! LoadMoreCell
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FAQSectionCell") as! FAQSectionCell
            
            cell.lblQuestion.text = faqData[section].name
            cell.lblQuesNo.text = String(section+1)+"."
            return cell
        }

    }
    */
    func numberOfSections(in tableView: UITableView) -> Int {

        return self.dataList!.count//self.faqList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            let data = dataList![section]
            if let sort = data as? SortModel {
                return sort.hasChild == true ? 2 : 1
            }
        if let filter = data as? FilterModel {
            if(filter.filterSelectionTypeId == FilterSelectionType.starRating.rawValue) {
                return 2
            }
            return filter.filters.count + 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: FilterOptionsCell.identifier()) as! FilterOptionsCell
        cell.selectionStyle = .none
        switch popupListType {
        case .sort:
            print("")
        case .filter(type: .store):
            let data = dataList![indexPath.section] as! FilterModel
            cell.setData(dataList: data.filters[indexPath.row - 1], type: self.popupListType, selection: FilterSelectionType(rawValue: data.filterSelectionTypeId)!)
            
        default:
            print("")
        }
        
        //cell.lblAns.text = self.faqList[indexPath.section].answer
        return cell


        
    }
    

//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//        if indexPath.section == faqData.count && shouldShowLoadingCell {
//            currentPage += 1
//            callFaqService(searchText: "")
//        }
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        switch popupListType {
        case .sort:
            let data = dataList![indexPath.section]
            let sortList = dataList as! [SortModel]
            if let oldSelected = sortList.filter({$0.isSelected}).first {
                oldSelected.isSelected = false
            }
            if let sort = data as? SortModel {
                sort.isSelected = true
                //selectedSort = sort
            }
        case .filter(type: .store):
            let data = dataList![indexPath.section]
            let filterList = dataList as! [FilterModel]
            if let filter = data as? FilterModel {
//                if (indexPath.row == 0) {
//                    filter.isSelected = !filter.isSelected
//                    if filter.isSelected == false {
//                        filter.filters.forEach { (subModel) in
//                            subModel.isSelected = false
//                        }
//                    }
//                }
                if (indexPath.row != 0) {
                    
                    let cell = tableView.cellForRow(at: indexPath) as! FilterOptionsCell
                    
                    let item = filter.filters[indexPath.row - 1]
                    
                    
                    if(filter.filterSelectionTypeId == FilterSelectionType.single.rawValue) {
                        for data in filter.filters {
                            data.isSelected = false
                        }
                        filter.isSelected = true
                        item.isSelected = !item.isSelected
                    }
                    else if(filter.filterSelectionTypeId == FilterSelectionType.starRating.rawValue) {
                        item.starRating = Int(cell.ratingView.rating)
                        filter.isSelected = true
                        item.isSelected = !item.isSelected
                    }
                    else if(filter.filterSelectionTypeId == FilterSelectionType.multi.rawValue) {
                        filter.isSelected = true
                        item.isSelected = !item.isSelected
                    }
                }
            }
            
           
        default:
            print("")
        }
        expendTableView.reloadData()
        
        
        
        
        //If you don't deselect the row here, seperator of the above cell of the selected cell disappears.
        //Check here for detail: https://stackoverflow.com/questions/18924589/uitableviewcell-separator-disappearing-in-ios7
        
    /*    tableView.deselectRow(at: indexPath, animated: false)
        
        if faqData[indexPath.section].descriptionValue == nil {
            let eduGuideDetailVC = AppConstants.APP_STORYBOARD.HOME.instantiateViewController(withIdentifier: "EducationGuideDetailController") as! EducationGuideDetailController
            eduGuideDetailVC.eduGuidelineId = faqData[indexPath.section].guidelineId
            self.navigationController?.pushViewController(eduGuideDetailVC, animated:true)
        }
        //This solution obviously has side effects, you can implement your own solution from the given link.
        //This is not a bug of ExpyTableView hence, I think, you should solve it with the proper way for your implementation.
        //If you have a generic solution for this, please submit a pull request or open an issue.
        
        print("DID SELECT row: \(indexPath.row), section: \(indexPath.section)")  */
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
