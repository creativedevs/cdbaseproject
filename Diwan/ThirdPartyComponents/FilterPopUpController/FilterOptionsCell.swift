//
//  FilterOptionsCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 19/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import Cosmos
class FilterOptionsCell: UITableViewCell {

    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var titleLbl: BaseUILabel!
    @IBOutlet weak var radioSelected: UIImageView!
    @IBOutlet weak var radioBtn: UIButton!
    var selectionType : FilterSelectionType?
    override func awakeFromNib() {
        super.awakeFromNib()
        ratingView.settings.minTouchRating = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(dataList : SortFilterProtocol?, type : MainPopUpListType?, selection : FilterSelectionType?){
            
        
        
        self.selectionType = selection
            if let filter = dataList as? SubFilterModel {
                titleLbl.text = filter.title
                switch self.selectionType {
                case .single:
                    print("")
                    ratingView.isHidden = true
                    titleLbl.isHidden = false
                    radioSelected.isHidden = false
                    radioBtn.isHidden = true
                    radioSelected.image = filter.isSelected ? UIImage(named: "filteradioskyselected") : UIImage(named: "filteradiounselected")
                case .multi:
                    print("")
                    ratingView.isHidden = true
                    titleLbl.isHidden = false
                    radioSelected.isHidden = false
                    radioBtn.isHidden = true
                    radioSelected.image = filter.isSelected ? UIImage(named: "filterselected") : UIImage(named: "filterunselected")
                case .starRating:
                    ratingView.rating = Double(filter.starRating)
                    ratingView.isHidden = false
                    titleLbl.isHidden = true
                    radioSelected.isHidden = true
                    radioBtn.isHidden = true
                    print("")
                default:
                    print("")
                }
                
            }
            
        }
}
