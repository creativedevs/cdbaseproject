//
//  HorizontalDynamicCollectionView.swift
//  Diwan
//
//  Created by Muzamil Hassan on 27/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit


class HorizontalDynamicCollectionView: UIView {

    private var collectionView : UICollectionView!
    private var alphabetArray  = ["All","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    var didSelectItem : (((String) -> Void))?
    var isSelectedIndex = 0
    //MARK: Init and awake
    override init(frame: CGRect) {
        super.init(frame: frame)
       // postInitialize()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        postInitialize()
    }
    
    override open func prepareForInterfaceBuilder() {
           postInitialize()
       }
       
       private func postInitialize() {
           updateView()
       }
       
       //MARK: Overrides
       override open func layoutSubviews() {
         //  layoutCharactersAndPlaceholders()
           super.layoutSubviews()
       }
    //MARK: Private
    private func updateView() {
        createCollectionView() 
        setNeedsLayout()
    }
    private func createCollectionView() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        //layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        //layout.itemSize = CGSize(width: 60, height: 60)
        let frame = CGRect(x: 0, y: 0, width: Constants.kWINDOW_WIDTH, height: self.frame.size.height)
        collectionView = UICollectionView(frame: frame, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.register(HorizontalDynamicCollectionCell.self, forCellWithReuseIdentifier: "HorizontalDynamicCollectionCell")
        self.addSubview(collectionView)
        
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
//MARK:- Collection Datasource and Delegate
extension HorizontalDynamicCollectionView: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return alphabetArray.count
        
        }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HorizontalDynamicCollectionCell.identifier(), for: indexPath) as! HorizontalDynamicCollectionCell
        let isSelected = isSelectedIndex == indexPath.row ? true : false
        cell.setData(word: alphabetArray[indexPath.item], isSelected: isSelected)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let data = self.alphabetArray[indexPath.row]
        isSelectedIndex = indexPath.row
        self.collectionView.reloadData()
        self.didSelectItem?(data)
    }
    func calculateWidhtCell(word : String) -> CGSize {
        let font = UIFont(name: FontManager.constant(forKey: "fontSFRegular")!,size: CGFloat(FontManager.style(forKey: "sizeSmall")))!
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let size = CGSize(width: 250, height: 1500)

        let estimatedFrame = NSString(string: word).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font : font], context: nil)
        let attributes = [NSAttributedString.Key.font : font]

        let yourLabelSize: CGSize = word.size(withAttributes:attributes )
        return yourLabelSize
        //var width1 = yourLabelSize.width + 30
//        if width1 < 30 {
//            width1 = 30
//        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //let width = calculateWidhtCell(word: alphabetArray[indexPath.item])
        //print(width)
        return CGSize(width: 25, height: self.frame.size.height)
        //return CGSize(width: collectionView.frame.size.width/2, height: DesignUtility.getValueFromRatio(298))
        //return CGSize(width: DesignUtility.getValueFromRatio(188), height: DesignUtility.getValueFromRatio(198))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
class HorizontalDynamicCollectionCell: UICollectionViewCell {
    
    var titleLbl : BaseUILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        createLabel()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        createLabel()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.backgroundColor = .clear
        createLabel()
    }
    func createLabel() {
         
        guard titleLbl != nil else {
            titleLbl = BaseUILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
            ///titleLbl.font = UIFont(name: FontManager.constant(forKey: "fontSFRegular")!,size: CGFloat(FontManager.style(forKey: "sizeSmall")))!
            titleLbl.fontNameTheme = "fontSFRegular"
            titleLbl.fontSizeTheme = "sizeSmall"
            titleLbl.fontColorTheme = "grayplaceholder"
            titleLbl.textAlignment = .center
            self.addSubview(titleLbl)
            titleLbl.text = ""
            return
        }
        
    }
    func setData(word : String, isSelected : Bool) {
        titleLbl.text = word
        var fontColor = isSelected == true ? "themeSkyBlue2" : "grayplaceholder"
        titleLbl.fontColorTheme = fontColor
//        titleLbl.backgroundColor = .random
    }
}

