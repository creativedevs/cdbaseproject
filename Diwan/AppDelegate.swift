//
//  AppDelegate.swift
//  Diwan
//
//  Created by Waqas Ali on 17/04/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import MOLH
import Firebase
import GoogleSignIn
import TwitterKit
import FBSDKCoreKit
import FBSDKLoginKit
import Realm
import RealmSwift
import BMPlayer
var router = Router()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var configService: ConfigService?
    internal var shouldRotate = false
    func application(_ application: UIApplication,
                     supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return shouldRotate ? .allButUpsideDown : .portrait
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
          
        self.configService = ConfigService(configEnv: ConfigHelper.configEnv)
        print(self.configService)
        FirebaseApp.configure()
        
        basicSetup()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any])
        -> Bool {
            print(url.path)
            if TWTRTwitter.sharedInstance().application(application, open: url, options: options){
                return true
            }
           // return TWTRTwitter.sharedInstance().application(application, open: url, options: options)
            return GIDSignIn.sharedInstance().handle(url)
    }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        print(url.path)
        
        if TWTRTwitter.sharedInstance().application(application, open: url, options: [:]){
            return true
        }
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    func basicSetup()  {
        
        BMPlayerConf.topBarShowInCase = .none
        BMPlayerConf.enableVolumeGestures = true
        BMPlayerConf.enablePlaytimeGestures = true
//        MOLH.setLanguageTo("ar")
//        let lang = MOLHLanguage.currentAppleLanguage() == "ar" ? "ar" : "en"
//        CurrentUser.languageId = lang == "en" ? .english : .arabic
//        print(MOLHLanguage.currentAppleLanguage())
        print(CurrentUser.languageId)
        print(Realm.Configuration.defaultConfiguration.fileURL)
        TWTRTwitter.sharedInstance().start(withConsumerKey: "Y4C1ynYlXMdgf7xjinEj41ecU", consumerSecret: "cq4iBrgNTWbDNKd9ETwuJraRydqB8auGny6z8QlMVHRfFCklwI")
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        MOLH.shared.activate(true)
        IQKeyboardManager.shared.enable = true
        getFontNames()
//        IQKeyboardManager.shared.disabledToolbarClasses = [BottomPopUpController.self]
        //AFNetwork.shared.baseURL = "www.google.com/api/"
    }
    
    func getFontNames()  {
        for fontFamilyName in UIFont.familyNames{
            for fontName in UIFont.fontNames(forFamilyName: fontFamilyName){
                print("Family: \(fontFamilyName)     Font: \(fontName)")
            }
        }
    }
}

