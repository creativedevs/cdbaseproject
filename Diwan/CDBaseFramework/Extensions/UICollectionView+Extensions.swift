//
//  UICollectionView+Extensions.swift
//  Diwan
//
//  Created by Muzamil Hassan on 18/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//
import ViewAnimator
import UIKit
extension UICollectionViewCell{
    static func identifier() -> String{
        return "\(self)"
    }
    
    
    static func nib() -> UINib{
        return UINib.init(nibName: "\(self)", bundle: nil)
    }
    func setupSelfSizingForiOS12(contentView: UIView) {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        let leftConstraint = contentView.leftAnchor.constraint(equalTo: leftAnchor)
        let rightConstraint = contentView.rightAnchor.constraint(equalTo: rightAnchor)
        let topConstraint = contentView.topAnchor.constraint(equalTo: topAnchor)
        let bottomConstraint = contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        NSLayoutConstraint.activate([leftConstraint, rightConstraint, topConstraint, bottomConstraint])
    }
}



extension UICollectionView{
    
    func registerCell<T: UICollectionViewCell>(_: T.Type) {
        register(T.nib(), forCellWithReuseIdentifier: T.identifier())
    }
    
    func animationBottomToTop() {
        let animations = [AnimationType.from(direction: .bottom, offset: 230.0)]
        /*
         UIView.animate(views: self.orderedVisibleCells,
        animations: animations, completion: {
        })
         */
        
        self.performBatchUpdates({
            UIView.animate(views: self.orderedVisibleCells,
            animations: animations, delay: 0.0, completion: {
            })
        }, completion: nil)

    }
    
}
