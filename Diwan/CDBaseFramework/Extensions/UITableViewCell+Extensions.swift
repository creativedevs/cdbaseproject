//
//  UITableViewCell+Extensions.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//
import ViewAnimator
import UIKit
extension UITableViewCell{
    static func identifier() -> String{
        return "\(self)"
    }
    
    
    static func nib() -> UINib{
        return UINib.init(nibName: "\(self)", bundle: nil)
    }
}



extension UITableView{
    
    func registerCell<T: UITableViewCell>(_: T.Type) {
        register(T.nib(), forCellReuseIdentifier: T.identifier())
    }
    func animateTableViewFromZoom() {
        let fromAnimation = AnimationType.from(direction: .right, offset: 30.0)
        let zoomAnimation = AnimationType.zoom(scale: 0.2)
        UIView.animate(views: self.visibleCells,
                       animations: [fromAnimation, zoomAnimation], delay: 0.2)
    }
    func animationTableViewFromBottom() {
        let animations = [AnimationType.from(direction: .bottom, offset: 30.0)]
        UIView.animate(views: self.visibleCells, animations: animations, completion: {
        })

    }
    func animationTableViewFromLeft() {
        let animations = [AnimationType.from(direction: .left, offset: 30.0)]
        UIView.animate(views: self.visibleCells, animations: animations, completion: {
        })

    }
}

