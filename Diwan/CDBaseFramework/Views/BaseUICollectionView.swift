//
//  BaseUICollectionView.swift


import UIKit

open class BaseUICollectionView: UICollectionView, FillDesignable {

    open var collectionSubTag = 0
    // MARK: - FillDesignable
    @IBInspectable open var fillThemeColor: String? {
        didSet {
            configureFillColor();
        }
    }
    
    
    @IBInspectable open var opacity: CGFloat = CGFloat.nan {
        didSet {
            configureOpacity();
        }
    }
    

}
