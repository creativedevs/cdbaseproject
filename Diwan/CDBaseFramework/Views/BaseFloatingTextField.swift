//
//  BaseFloatingTextField.swift
//  Diwan
//
//  Created by Muzamil Hassan on 05/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import MOLH

extension SideImageDesignable where Self: BaseFloatingTextField {
    
    func configureLeftImage() {
        
        guard let leftImage = leftImage else {
            leftView = nil
            return
        }
        
        let sideView = makeSideView(with: leftImage,
                                    leftPadding: leftImageLeftPadding,
                                    rightPadding: leftImageRightPadding,
                                    topPadding: leftImageTopPadding)
        leftViewMode = .always
        leftView = sideView
    }
    
    func configureRightImage() {
        
        guard let rightImage = rightImage else {
            rightView = nil
            return
        }
        
        let sideView = makeSideView(with: rightImage,
                                    leftPadding: rightImageLeftPadding,
                                    rightPadding: rightImageRightPadding,
                                    topPadding: rightImageTopPadding)
        rightViewMode = .always
        rightView = sideView
    }
    
    func makeSideView(with image: UIImage, leftPadding: CGFloat, rightPadding: CGFloat, topPadding: CGFloat) -> UIView {
        
        let imageView = UIImageView(image: image)
       //--ww imageView.contentMode = .center
        
        // If not set, use 0 as default value
        var leftPaddingValue: CGFloat = 0.0
        if !leftPadding.isNaN {
            leftPaddingValue = leftPadding
        }
        
        // If not set, use 0 as default value
        var rightPaddingValue: CGFloat = 0.0
        if !rightPadding.isNaN {
            rightPaddingValue = rightPadding
        }
        if UIApplication.isRTL() {
            let rPadding = rightPaddingValue;
            rightPaddingValue = leftPaddingValue;
            leftPaddingValue = rPadding;
        }
        // If does not specify `topPadding`, then center it in the middle
        if topPadding.isNaN {
            imageView.frame.origin = CGPoint(x: leftPaddingValue, y: (bounds.height - imageView.bounds.height) / 2)
        } else {
            imageView.frame.origin = CGPoint(x: leftPaddingValue, y: topPadding)
        }
        
        let padding = rightPaddingValue + imageView.bounds.size.width + leftPaddingValue
        let sideView = UIView(frame: CGRect(x: 0, y: 0, width: padding, height: bounds.height))
        sideView.addSubview(imageView)
        return sideView
    }
}

extension SideImageDesignable where Self: BaseFloatingTextField {
    
     func configureImages() {
        configureLeftImage()
        configureRightImage()
    }
}

extension PlaceholderDesignable where Self: BaseFloatingTextField {
    
     var placeholderTxt: String? { get { return "" } set {} }
    
     func configurePlaceholderColor() {
        if placeholderThemeColor == nil {
            placeholderThemeColor = "black";
        }
        
        let placeholderColor:UIColor? = UIColor.color(forKey: placeholderThemeColor);
        
        let text = placeholder ?? placeholderTxt
        if let placeholderColor = placeholderColor, let placeholder = text {
            attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor: placeholderColor])
        }
    }
}

class BaseFloatingTextField: JVFloatLabeledTextField, FontDesignable, CornerDesignable, BorderDesignable, PlaceholderDesignable, SideImageDesignable, FillDesignable , ShadowDesignable {
    
    
    func setInitialUI() {
        
        let titleFont = UIFont.init(name: FontManager.constant(forKey: "fontSFRegular")!, size: CGFloat(FontManager.style(forKey: "sizeMedium")))
        if let font = titleFont {
            self.font = font
        }
        self.floatingLabelFont = UIFont.init(name: FontManager.constant(forKey: "fontSFLight")!, size: CGFloat(FontManager.style(forKey: "sizeXSmall")))
        self.textColor = ColorManager.color(forKey: "themeBlue")
        self.floatingLabelTextColor = ColorManager.color(forKey: "grayplaceholder")
        self.floatingLabelYPadding = -8
        self.floatingLabelActiveTextColor = ColorManager.color(forKey: "grayplaceholder")
    }
    // MARK: - ShadowDesignable
    @IBInspectable open var shadowRadius: CGFloat = DefaultConfig.shared.defaultShadowRadius {
        
        didSet {
            configureDropShadow();
        }
    }
    
    @IBInspectable open var shadowOpacity: CGFloat = DefaultConfig.shared.defaultShadowOpacity {
        
        didSet {
            configureDropShadow();
        }
    }
    
    @IBInspectable open var hasDropShadow:Bool = false {
        didSet {
            configureDropShadow();
        }
    }
    
    @IBInspectable open var hasBaseWidthConstraint:Bool = false {
        didSet {
            configureDropShadow();
        }
    }
    
    // MARK: - FontDesignable
    @IBInspectable open var fontNameTheme:String? = DefaultConfig.shared.defaultFontName {
        
        didSet {
            configureFont();
        }
    }
    
    @IBInspectable open var fontColorTheme:String? = DefaultConfig.shared.defaultFontColor  {
        
        didSet {
            configureFont();
        }
    }
    
    @IBInspectable open var fontSizeTheme:String? = DefaultConfig.shared.defaultFontSize  {
        didSet {
            configureFont();
        }
    }
    
    // MARK: - CornerDesignable
    @IBInspectable open var isRounded: Bool = false {
        didSet {
            configureCornerRadius();
        }
    }
    
    @IBInspectable open var cornerRadius: CGFloat = CGFloat.nan {
        didSet {
            configureCornerRadius();
        }
    }
    
    open var cornerSides: CornerSides  = .allSides {
        didSet {
            configureCornerRadius();
        }
    }
    
    //NOTE:- Possible values topleft, topright, bottomleft, bottomright
    @IBInspectable var _cornerSides: String? {
        didSet {
            cornerSides = CornerSides(rawValue: _cornerSides);
        }
    }
    
    // MARK: - BorderDesignable
    open var borderType: BorderType  = .solid {
        didSet {
            configureBorder();
        }
    }
    
    //NOTE:- Possible values solid, dash
    @IBInspectable var _borderType: String? {
        didSet {
            borderType = BorderType(string: _borderType);
        }
    }
    
    @IBInspectable open var borderThemeColor: String? {
        didSet {
            configureBorder();
        }
    }
    
    @IBInspectable open var borderWidth: CGFloat = CGFloat.nan {
        didSet {
            configureBorder();
        }
    }
    
    open var borderSides: BorderSides  = .AllSides {
        didSet {
            configureBorder();
        }
    }
    
    //NOTE:- Possible values Top, Right, Bottom, Left
    @IBInspectable var _borderSides: String? {
        didSet {
            borderSides = BorderSides(rawValue: _borderSides);
        }
    }
    
    // MARK: - FillDesignable
    @IBInspectable open var fillThemeColor: String? {
        didSet {
            configureFillColor();
        }
    }
    
    
    @IBInspectable open var opacity: CGFloat = CGFloat.nan {
        didSet {
            configureOpacity();
        }
    }
    
    // MARK: - PlaceholderDesignable
    @IBInspectable open var placeholderThemeColor: String? {
        didSet {
            configurePlaceholderColor();
        }
    }
    
    // MARK: - SideImageDesignable
    @IBInspectable open var leftImage: UIImage? {
        didSet {
            configureImages();
        }
    }
    
    @IBInspectable open var leftImageLeftPadding: CGFloat = CGFloat.nan {
        didSet {
            configureImages();
        }
    }
    
    @IBInspectable open var leftImageRightPadding: CGFloat = CGFloat.nan {
        didSet {
            configureImages();
        }
    }
    
    @IBInspectable open var leftImageTopPadding: CGFloat = CGFloat.nan {
        didSet {
            configureImages();
        }
    }
    
    @IBInspectable open var rightImage: UIImage? {
        didSet {
            configureImages();
        }
    }
    
    @IBInspectable open var rightImageLeftPadding: CGFloat = CGFloat.nan {
        didSet {
            configureImages();
        }
    }
    
    @IBInspectable open var rightImageRightPadding: CGFloat = CGFloat.nan {
        didSet {
            configureImages();
        }
    }
    
    @IBInspectable open var rightImageTopPadding: CGFloat = CGFloat.nan {
        didSet {
            configureImages();
        }
    }
    
    
     @IBInspectable open var maxCharacterCount: Int = 30
    
    override open var placeholder: String? {
        get {
            return super.placeholder;
        }
        
        set {
            super.placeholder = newValue;
            configurePlaceholderColor();
        }
    }
    
    //MARK: - Initializers
    open override func awakeFromNib() {
        super.awakeFromNib();
        self.autocorrectionType = .no
        configureInspectableProperties();
        
         self.addTarget(self, action: #selector(txtEditing), for: .editingChanged)
        if MOLHLanguage.currentAppleLanguage() == "en" {
            self.textAlignment = .left
        }else{
            self.textAlignment = .right
        }
    }
    
    
    @objc func txtEditing(_ txt : UITextField){
        
        if let typedText = self.text {
            if typedText.count > maxCharacterCount{
              
                txt.deleteBackward()
            }
           
        }
    }
    open override func layoutSubviews() {
        super.layoutSubviews();
        configureAfterLayoutSubviews();
        
    }
    
    // MARK: - Private
    fileprivate func configureInspectableProperties() {
        configureCornerRadius();
        configureImages();
    }
    
    fileprivate func configureAfterLayoutSubviews() {
        configureCornerRadius();
        configureBorder();
    }
    
}
