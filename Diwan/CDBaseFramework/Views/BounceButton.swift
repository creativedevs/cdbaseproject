//
//  CheckBoxButton.swift


import UIKit

public class BounceButton : BaseUIButton {
    
    //var simpleClosure:() -> ()
    var isClick = false
    var didTapOnBounceEnd : ((() -> Void))?
    var didTapOnBounceStart : ((() -> Void))?
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        //self.isUserInteractionEnabled = false
        if self.isClick == true {
            return
        }
        self.isClick = true
        self.didTapOnBounceStart?()
        self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 6, options: .allowUserInteraction, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: { (complete) in
            self.didTapOnBounceEnd?()
            self.isClick = false
         //   self.isUserInteractionEnabled = true
        })
        
        super.touchesBegan(touches, with: event)
            
    }
}

extension BounceButton{
    
//    func setTitleInset() {
//        self.titleEdgeInsets = UIEdgeInsets(top: 20,left: 0,bottom: 0,right: 0)
//
//    }
}
