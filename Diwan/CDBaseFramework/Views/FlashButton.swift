//
//  FlashButton.swift
//  Diwan
//
//  Created by Muzamil Hassan on 08/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
public class FlashButton : BaseUIButton {
    
    //var simpleClosure:() -> ()
    
    var didTapOnBounceEnd : ((() -> Void))?
    var didTapOnBounceStart : ((() -> Void))?
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.didTapOnBounceStart?()
        self.isUserInteractionEnabled = false
        CATransaction.begin()
        CATransaction.setCompletionBlock({
            self.isUserInteractionEnabled = true
            self.didTapOnBounceEnd?()
        })
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = 0.2
        flash.fromValue = 1
        flash.toValue = 0.5
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        flash.autoreverses = true
        flash.repeatCount = 1
        
        layer.add(flash, forKey: nil)
        CATransaction.commit()
        /*
        self.alpha = 1.0
        UIView.animate(withDuration: 0.5, delay: 1.0, options: UIView.AnimationOptions.curveEaseOut, animations: {

            self.alpha = 0.0

        }, completion: { (complete) in
            self.didTapOnBounceEnd?()
        })*/
        super.touchesBegan(touches, with: event)
            
    }
}

