//
//  AFNetwork.swift


import UIKit
import Alamofire
import MOLH
import UserNotifications
import FCAlertView
import SVProgressHUD
//main class
public class AFNetwork: NSObject {
    
    //MARK: constant and variable
    //manager
    public var alamoFireManager: Alamofire.Session!
    public var failureMessage = "Unable to connect to the internet"
    var err: Error?
    //network
    public var baseURL = (Constants.APP_DELEGATE.configService!.apiUrl)!
    public var commonHeaders: Dictionary<String, String> = [
        "Authorization" : "",
        "Accept" : "application/json",
        "Accept-Language" : ""
    ]
    
    //spinner
    struct spinnerViewConfig {
        static let tag: Int = 98272
        static let color = UIColor.white
    }
    
    //progress view
    public var progressLabel: UILabel?
    var progressView: UIProgressView?
    struct progressViewConfig {
        static let tag: Int = 98273
        static let color = UIColor.white
        static let labelColor = UIColor.red
        static let trackTintColor = UIColor.red
        static let progressTintColor = UIColor.green
    }
    
    //shared Instance
    public static let shared: AFNetwork = {
        let instance = AFNetwork()
        return instance
    }()
    
    // MARK: - : override
    override init() {
        
        alamoFireManager = Alamofire.Session(
            configuration: URLSessionConfiguration.default
        )
        alamoFireManager.session.configuration.timeoutIntervalForRequest = 120
    }
    
    //setupSSL
    public func setupSSLPinning(_ fileNameInBundle: String) {
        
        // Set up certificates
        let pathToCert = Bundle.main.path(forResource: fileNameInBundle, ofType: "crt")
        let localCertificate = NSData(contentsOfFile: pathToCert!)
        let certificates = [SecCertificateCreateWithData(nil, localCertificate!)!]
        
        // Configure the trust policy manager
//        let serverTrustPolicy = ServerTrustPolicy.pinCertificates(
//            certificates: certificates,
//            validateCertificateChain: true,
//            validateHost: true
//        )
//
//        let serverTrustPolicies = [
//            AFNetwork.shared.baseURL.getDomain() ?? AFNetwork.shared.baseURL : serverTrustPolicy
//        ]
//
//        alamoFireManager =
//            Alamofire.Session(
//                configuration: URLSessionConfiguration.default,
//                serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
//        )
    }
//    func getAuthorizationHeader () -> Dictionary<String,String>{
//        
//        if(AppStateManager.sharedInstance.isUserLoggedIn()){
//            if let token = APIManager.instance.serverToken {
//                
//                var str = "bearer "
//                str.append(token)
//                return ["Authorization":str,"Accept":"application/json"]
//            }
//        }
//        return otherRequestHeader
//    }
}

// MARK: - Request
extension AFNetwork {
    
    //general request
    public func apiRequest(_ info: AFParam, isSpinnerNeeded: Bool, success:@escaping (Data?) -> Void, failure:@escaping (Error?) -> Void) {
        
        //if spinner needed
        if isSpinnerNeeded {
            DispatchQueue.main.async {
                AFNetwork.shared.showSpinner(nil)
            }
        }
        //URLEncoding(destination: .methodDependent)
        //request
        print(mergeWithCommonHeaders(CurrentUser.headers))
        alamoFireManager.request(self.baseURL + info.endpoint, method: info.method, parameters: info.params, encoding: info.parameterEncoding, headers: mergeWithCommonHeaders(CurrentUser.headers)).responseJSON { (response) -> Void in
            
            
            //remove spinner
            if isSpinnerNeeded {
                DispatchQueue.main.async {
                    AFNetwork.shared.hideSpinner()
                }
            }
            
            //check response result case
            switch response.result {
            case .success(let value):
                debugPrint(response)
                debugPrint(value)
                guard let data = response.data else { return }
                
                do
                {
                    
                    let decoder = JSONDecoder()
                    let result = try decoder.decode(MainResponse.self, from: data)
                    switch (result.statusCode) {
                        case ServiceCodes.successCode:
                            //self.isSuccess = true
                            success(response.data)
                        case ServiceCodes.authExpireCode:
                            self.userUnAuthorize()
                        case ServiceCodes.userBlockCode:
                            self.userBlocked()
                        case ServiceCodes.errorCode:
                            if let error = result.errorInternal {
                                Alert.showErrorMessage(msg: error)
                                failure(self.err)
                            }
                            
                        
                        default: break
                    
                        }

                    /*else
                     {
                     AlertClass.showError(msg : result.message ?? "Server Not responding")
                     
                     
                     }*/
                    
                }
                catch let error
                {
                    print("Error: ", error)
                }
            case .failure(let error):
               // let error : Error = response.result.error!
                debugPrint("responseError: \(error)")
                Alert.showMsg(msg: error.localizedDescription)
                failure(error)
            }
        }
    }
    //file upload
    public  func apiRequestUpload(_ info: AFParam, isSpinnerNeeded: Bool, success:@escaping (Data?) -> Void, failure:@escaping (Error) -> Void) {
        
        //if spinner needed
        if isSpinnerNeeded {
            DispatchQueue.main.async {
                AFNetwork.shared.showSpinner(nil)
            }
        }
        
        let URL = try! URLRequest(url: self.baseURL + info.endpoint, method: info.method, headers: mergeWithCommonHeaders(CurrentUser.headers))
        
        print(URL)
        alamoFireManager.upload(multipartFormData: { multipartFormData in
                            //multipart images
                if info.images != nil {
                    if info.images!.count > 0 {
                        for value in info.images! {
                            let imageData = value.pngData() as Data?
                            if imageData != nil {
                                
                                let imgName = "Image\(Int64(Date().timeIntervalSince1970 * 1000)).png"
                                //multipartFormData.append(imageData!, withName: "images[]",  mimeType: "image/png")
                                multipartFormData.append(imageData!, withName: "file",fileName: imgName, mimeType: "image/png")
                            }
                        }
                    }
                }
                //multipart params
                if info.params != nil {
                    
                    for (key, value) in info.params! {
                        if let data = (value as AnyObject).data(using: String.Encoding.utf8.rawValue) {
                            multipartFormData.append(data, withName: key)
                        }
                    }
                }
            }, with: URL)
        .uploadProgress(queue: .main, closure: { progress in
            //Current upload progress of file
            print("Upload Progress: \(progress.fractionCompleted)")
            self.showSpinner(nil, percentage: progress.fractionCompleted, isShowProgress: true)
            //self.setProgressProgress(Float(progress.fractionCompleted))
        })
        .responseJSON(completionHandler: { data in
            //Do what ever you want to do with response
            if isSpinnerNeeded {
                DispatchQueue.main.async {
                    AFNetwork.shared.hideSpinner()
                }
            }
            
            //set progress view
            self.setProgressProgress(1.0)
            switch data.result {
            case .success(let value):
              print("SUCCESS")
                debugPrint(value)
                debugPrint(data.data)
                success(data.data)
            case .failure(let error):
                  print("FAIL")
                debugPrint("responseError: \(error)")
                Alert.showMsg(msg: error.localizedDescription)
                failure(error)
            }
            
        })
    }
    
    
   /*
    func apiGeneric<T : Decodable> (_ param: AFParam , modelType : T.Type ,
                                    isShowSpinner : Bool = true, completion: @escaping (T) -> Void){
        
        
        //request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: isShowSpinner, success: { (response) in
            
            guard let data = response else { return }
            
            do
            {
                let decoder = JSONDecoder()
                let result = try decoder.decode(modelType.self, from: data)
                completion(result)
                
            }
            catch let error
            {
                print("Error: ", error)
            }
            
            
        }) { (error) in
            
        }
    }*/
}

// MARK: - Progress and spinner methods
extension AFNetwork {
    
    func userUnAuthorize() {
        self.showAlert(title: nil, subTitle: "Your authorization is expired please login".localized, doneBtnTitle: NSLocalizedString("OK", comment: ""), completionAction: {
            try! Constants.realm.write {
                Constants.realm.delete(CurrentUser.data!)
                CurrentUser.data = nil
                CurrentUser.userType = .registered
                UNUserNotificationCenter.current().removeAllDeliveredNotifications()
                UIApplication.shared.applicationIconBadgeNumber = 0
               // router.goToLoginAsRoot()

            }
        })
        
        
    }
    func userBlocked() {
        self.showAlert(title: nil, subTitle: "Your are blocked by admin".localized, doneBtnTitle: NSLocalizedString("OK", comment: ""), completionAction: {
            try! Constants.realm.write {
                Constants.realm.delete(CurrentUser.data!)
                CurrentUser.data = nil
                CurrentUser.userType = .registered
                UNUserNotificationCenter.current().removeAllDeliveredNotifications()
                UIApplication.shared.applicationIconBadgeNumber = 0
               // router.goToLoginAsRoot()

            }
        })
        
        
    }
    func showAlert(title:String? , subTitle:String? , doneBtnTitle:String? = NSLocalizedString("OK", comment: ""), completionAction: (() -> Void)? = nil) {
        let subtitleFont = UIFont(name: FontManager.constant(forKey: "fontRegular")!,size: CGFloat(FontManager.style(forKey: "sizeSmall")))!
        
        let alert = FCAlertView()
        //withCustomImage:UIImage(named: "logo"),
        alert.showAlert(in: returnTopWindow(),
                        withTitle:title,
                        withSubtitle:subTitle,
                        withCustomImage:nil,
                        withDoneButtonTitle:doneBtnTitle,
                        andButtons:nil)
        
        alert.customImageScale = 1.2
        alert.cornerRadius = 10
        alert.subtitleFont = subtitleFont
        
        //alert.alertBackgroundColor = UIColor.init(hexString: "1D1D1D")
        alert.colorScheme = ColorManager.color(forKey: "themeBlue")
        alert.hideSeparatorLineView = true
        alert.detachButtons = true
        alert.subTitleColor = ColorManager.color(forKey: "darkGreyText")
        //alert.subTitleColor = ColorManager.color(forKey: "themeText")
        alert.titleColor = ColorManager.color(forKey: "themeBlue")
        
        alert.doneButtonCustomFont = subtitleFont
        
        alert.doneActionBlock {
            completionAction?()
        }
        
    }
    public func showProgressView(_ customView: UIView?) {
        
        var window = customView
        
        if (window == nil) {
            window = returnTopWindow()
        }
        if window?.viewWithTag(progressViewConfig.tag) != nil {
            return
        }
        
        let backgroundView = UIView(frame: CGRect.zero)
        backgroundView.tag = progressViewConfig.tag
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        backgroundView.backgroundColor = UIColor.clear.withAlphaComponent(0.7)
        
        let progressContainer = UIView()
        progressContainer.translatesAutoresizingMaskIntoConstraints = false
        progressContainer.backgroundColor = UIColor.clear
        
        progressLabel = UILabel()
        progressLabel?.translatesAutoresizingMaskIntoConstraints = false
        progressLabel?.textColor = progressViewConfig.labelColor
        progressLabel?.text = "Upload progress 0%"
        progressLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: UIFont.Weight.bold)
        progressLabel?.adjustsFontSizeToFitWidth = true
        progressLabel?.textAlignment = .center
        progressContainer.addSubview(progressLabel!)
        
        progressView = UIProgressView(progressViewStyle: .default)
        progressView?.translatesAutoresizingMaskIntoConstraints = false
        progressView?.progressTintColor = progressViewConfig.progressTintColor
        progressView?.trackTintColor = progressViewConfig.trackTintColor
        progressView?.progress = 0.0
        progressContainer.addSubview(progressView!)
        
        progressContainer.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[progressView]-(10)-[progressLabel]|", options: [], metrics: nil, views: ["progressLabel" : progressLabel!, "progressView" : progressView!]))
        progressContainer.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[progressView(200)]|", options: [], metrics: nil, views: ["progressView" : progressView!]))
        progressContainer.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[progressLabel]|", options: [], metrics: nil, views: ["progressLabel" : progressLabel!]))
        backgroundView.addSubview(progressContainer)
        
        backgroundView.addConstraint(NSLayoutConstraint(item: backgroundView, attribute: .centerY, relatedBy: .equal, toItem: progressContainer, attribute: .centerY, multiplier: 1.0, constant: 0.0))
        backgroundView.addConstraint(NSLayoutConstraint(item: backgroundView, attribute: .centerX, relatedBy: .equal, toItem: progressContainer, attribute: .centerX, multiplier: 1.0, constant: 0.0))
        
        window?.addSubview(backgroundView)
        window?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[backgroundView]|", options: [], metrics: nil, views: ["backgroundView" : backgroundView]))
        window?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[backgroundView]|", options: [], metrics: nil, views: ["backgroundView" : backgroundView]))
    }
    
    //hide progress view
    public func hideProgressView() {
        
        let window: UIWindow? = returnTopWindow()
        window?.viewWithTag(progressViewConfig.tag)?.removeFromSuperview()
        progressLabel = nil
        progressView = nil
    }
    
    //show spinner
    public func showSpinner(_ customView: UIView?, percentage : Double? = 0, isShowProgress : Bool? = false) {
            
    //        SVProgressHUD.setDefaultMaskType(.gradient)
        if(isShowProgress!) {
            let percent = String(format: "%.0f%% \("Uploaded".localized)", percentage! * 100)
            SVProgressHUD.showProgress(Float(percentage!), status: percent)
        }
        else {
            SVProgressHUD.show()
        }
        
        
        SVProgressHUD.setDefaultStyle(.light)
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.setDefaultAnimationType(.flat)
        SVProgressHUD.setForegroundColor(ColorManager.color(forKey: "themeBlue")!)
            
        }
    
    public func showSpinner1(_ customView: UIView?) {
        
        var window = customView
        
        if (window == nil) {
            window = returnTopWindow()
        }
        if ((window?.viewWithTag(spinnerViewConfig.tag)) != nil) {
            return
        }
        
        //background view
        let backgroundView = UIView(frame: CGRect.zero)
        backgroundView.tag = spinnerViewConfig.tag
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        backgroundView.backgroundColor = UIColor.clear.withAlphaComponent(0.5)
        window?.addSubview(backgroundView)
        window?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[backgroundView]|", options: [], metrics: nil, views: ["backgroundView" : backgroundView]))
        window?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[backgroundView]|", options: [], metrics: nil, views: ["backgroundView" : backgroundView]))
        
        //spinner
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.color = spinnerViewConfig.color
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.startAnimating()
        backgroundView.addSubview(activityIndicator)
        backgroundView.addConstraint(NSLayoutConstraint(item: backgroundView, attribute: .centerX, relatedBy: .equal, toItem: activityIndicator, attribute: .centerX, multiplier: 1.0, constant: 0.0))
        backgroundView.addConstraint(NSLayoutConstraint(item: backgroundView, attribute: .centerY, relatedBy: .equal, toItem: activityIndicator, attribute: .centerY, multiplier: 1.0, constant: 0.0))
    }
    
    //hide spinner
    public func hideSpinner() {
        
        SVProgressHUD.dismiss()
//        let window: UIWindow? = returnTopWindow()
//        window?.viewWithTag(spinnerViewConfig.tag)?.removeFromSuperview()
    }
}

// MARK: - Helper methods
extension AFNetwork {
    
    //set progress and text of progress view
    func setProgressProgress(_ fractionCompleted:Float) {
        
        self.progressView?.progress = Float(fractionCompleted)
        self.progressLabel?.text = String(format: "Uploading Image %.0f%%", fractionCompleted * 100)
    }
    
    //return top window
    func returnTopWindow() -> UIWindow {
        
        let windows: [UIWindow] = UIApplication.shared.windows
        
        for topWindow: UIWindow in windows {
            if topWindow.windowLevel == UIWindow.Level.normal {
                return topWindow
            }
        }
        return UIApplication.shared.keyWindow!
    }
    
    //return merge headers
    func mergeWithCommonHeaders(_ headers: [String : String]?) -> HTTPHeaders/*Dictionary<String, String>*/ {
        
        AFNetwork.shared.commonHeaders["Authorization"] = ""
        if headers != nil {
            for header in headers! {
                AFNetwork.shared.commonHeaders[header.key] = header.value
            }
        }
        // return AFNetwork.shared.commonHeaders
        return HTTPHeaders(AFNetwork.shared.commonHeaders)
        
    }
}

class Internet {
    class var isConnected:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

