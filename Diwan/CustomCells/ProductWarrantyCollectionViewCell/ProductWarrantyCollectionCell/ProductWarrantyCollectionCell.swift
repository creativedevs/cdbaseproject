//
//  PhotosCollectionCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 12/07/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class ProductWarrantyCollectionCell: UICollectionViewCell {

    @IBOutlet weak var warrantyImg: BaseUIImageView!
    @IBOutlet weak var descLbl: BaseUILabel!
    var data : ProductMediaDetailModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(type : WarrantyType) {
        switch type {
        case .authenticate:
            warrantyImg.image = UIImage(named: "productauthentic")
            descLbl.text = "Authentic".localized
        case .easyReturn:
            warrantyImg.image = UIImage(named: "producteasy")
            descLbl.text = "Easy Returns".localized
        case .protection:
            warrantyImg.image = UIImage(named: "productprotection")
            descLbl.text = "14 Days Consumer protection".localized
        }
    }
    func setInitialData() {
        
        
    }
    

}
