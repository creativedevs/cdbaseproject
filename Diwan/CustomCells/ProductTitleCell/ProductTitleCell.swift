//
//  PharmacyCategoryProductCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 25/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class ProductTitleCell: UITableViewCell {

    @IBOutlet weak var descLbl: BaseUILabel!
    @IBOutlet weak var titleLbl: BaseUILabel!
    var data : ProductModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(data : ProductModel) {
        self.data = data
        if let title = self.data?.title {
            titleLbl.text = title
        }
        if let desc = self.data?.descriptionField {
                 descLbl.text = "   "//desc
             }
    }
}
