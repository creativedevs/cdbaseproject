//
//  AuthorMainCollectionCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 27/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import BMPlayer
class BrandCollectionCell: UICollectionViewCell {

    @IBOutlet weak var brandImg: BaseUIImageView!
    var data : BrandModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        brandImg.backgroundColor = ColorManager.color(forKey: "grayplaceholder2")
        // Initialization code
    }
    func setData(data : BrandModel) {
        self.data = data
        setInitialData()
    }
    func setInitialData() {
        
        brandImg.image = UIImage(named: "placeholderlogosquare")
        //brandImg.backgroundColor = .clear
        
        if let img = self.data?.imageUrl {
            brandImg.setImage(url: img, placeholder: nil, isActivity: true, activityColor: ColorManager.color(forKey: "white"))
        }
    }
}
