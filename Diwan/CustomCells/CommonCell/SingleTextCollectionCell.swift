//
//  SingleTextCollectionCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 25/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class SingleTextCollectionCell: UICollectionViewCell {

    @IBOutlet weak var titleAttributedLbl: UILabel!
    @IBOutlet weak var titleLbl: BaseUILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLbl.backgroundColor = .clear
        // Initialization code
    }
    func setData(type : SingleTextType) {
        
        switch type {
        case .library:
            print("")
            titleLbl.isHidden = true
            setLibraryAttributedTitle()
        case .pharmacy:
            print("")
            titleAttributedLbl.isHidden = true
        default:
            print("")
        }
        
    }
    func setLibraryAttributedTitle() {
        
        let firstSentence = "Do you want to".localized
        let secondSentence = "Read Something?".localized
        var firstSentenceMutableString = NSMutableAttributedString()
        var secondSentenceMutableString = NSMutableAttributedString()
        let firstSentenceFont = UIFont(name: FontManager.constant(forKey: "fontSFLight")!,size: CGFloat(FontManager.style(forKey: "sizexXXLarge")))!
        let secondSentenceFont = UIFont(name: FontManager.constant(forKey: "fontSFRegular")!,size: CGFloat(FontManager.style(forKey: "largeNo")))!
        let textColor = ColorManager.color(forKey: "themeBlue")!
        firstSentenceMutableString = NSMutableAttributedString(string: firstSentence,
                                                           attributes: [NSAttributedString.Key.font:firstSentenceFont,NSAttributedString.Key.foregroundColor : textColor])
        secondSentenceMutableString = NSMutableAttributedString(string: secondSentence,
                                                          attributes: [NSAttributedString.Key.font:secondSentenceFont,NSAttributedString.Key.foregroundColor : textColor])
        let combination = NSMutableAttributedString()
        combination.append(firstSentenceMutableString)
        combination.append(NSAttributedString(string: "\n"))
        combination.append(secondSentenceMutableString)
        titleAttributedLbl.attributedText = combination
    }
}
