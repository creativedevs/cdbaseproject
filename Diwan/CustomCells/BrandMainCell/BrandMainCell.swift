//
//  BrandMainCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 26/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class BrandMainCell: UITableViewCell {
    
    @IBOutlet weak var brandImg: BaseUIImageView!
    @IBOutlet weak var titleLbl: BaseUILabel!
    var data : BrandModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(data:BrandModel) {
        self.data = data
        brandImg.image = UIImage(named: "placeholderlogo")
        brandImg.backgroundColor = .clear
        if let img = self.data?.imageUrl {
            brandImg.setImage(url: img, placeholder: UIImage(named: "placeholderlogo"), isActivity: true, activityColor: ColorManager.color(forKey: "white"))
        }
        if let title = self.data?.title {
            titleLbl.text = title
        }
        
    }
}
