//
//  PhotosCollectionCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 12/07/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import Cosmos
class ReviewsCollectionCell: UICollectionViewCell {

    @IBOutlet weak var contentView1: UIView!
    @IBOutlet weak var nameLbl: BaseUILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var ratingValLbl: BaseUILabel!
    @IBOutlet weak var dateLbl: BaseUILabel!
    @IBOutlet weak var titleLbl: BaseUILabel!
    @IBOutlet weak var descLbl: BaseUILabel!
    
    var data : ProductUserReviewModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        ratingView.settings.starSize = Double(DesignUtility.getValueFromRatio(14))
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width - 24).isActive = true

//        if #available(iOS 12, *) { setupSelfSizingForiOS12(contentView: contentView) }
//        if #available(iOS 12, *) { setupSelfSizingForiOS12(contentView: contentView1) }
    }
  
    func setData(data : ProductUserReviewModel?) {
        self.data = data
        nameLbl.text = ""
        ratingView.rating = 0.0
        ratingValLbl.text = "0.0"
        dateLbl.text = ""
        titleLbl.text = ""
        descLbl.text = ""
        if let name = self.data?.userName {
            nameLbl.text = name
        }
        if let rate = self.data?.starRating {
            ratingView.rating = Double(rate)
            ratingValLbl.text = String(format: "%.1f", ratingView.rating)
        }
        if let title = self.data?.title {
            titleLbl.text = title
        }
        if let desc = self.data?.descriptionField {
            descLbl.text = desc
        }
        if let date = self.data?.reviewDate {
            let date = date.toDateString(DateFormatType.reviewDisplayFormat,dateFormat: DateFormatType.serverFormatMilliSecond)
            dateLbl.text = "On".localized + " " + date
        }
    }
    func setInitialData() {
        
        //titleLbl.text = "qej iopjioerjhidfj ijdfsiojdfsioj iojdfiosjdfsioj iojdfsiojfdsioj ioj"
    }
    
//    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
//        setNeedsLayout()
//        layoutIfNeeded()
//
//        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
//
//        var frame = layoutAttributes.frame
//        frame.size.height = ceil(size.height)
//
//        layoutAttributes.frame = frame
//
//        return layoutAttributes
//    }

}
