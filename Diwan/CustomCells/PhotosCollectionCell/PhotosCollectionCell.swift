//
//  PhotosCollectionCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 12/07/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class PhotosCollectionCell: UICollectionViewCell {

    @IBOutlet weak var mainImg: BaseUIImageView!
    var data : ProductMediaDetailModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(data : ProductMediaDetailModel) {
        self.data = data
        setInitialData()
    }
    func setInitialData() {
        mainImg.image = UIImage()
        mainImg.backgroundColor = ColorManager.color(forKey: "grayborder")
        
        if let img = self.data?.thumbnailUrl {
            mainImg.setImage(url: img, placeholder: UIImage(named: "photoplaceholder"), isActivity: false, activityColor: ColorManager.color(forKey: "white"))
        }
    }
}
