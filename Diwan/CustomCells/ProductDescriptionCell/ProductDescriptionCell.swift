//
//  PharmacyCategoryProductCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 25/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class ProductDescriptionCell: UITableViewCell {

    @IBOutlet weak var descLbl: BaseUILabel!
    @IBOutlet weak var viewAllBtn: FlashButton!
    @IBOutlet weak var titleLbl: BaseUILabel!
    @IBOutlet weak var collectionView: BaseUICollectionView!
    var data : CategoryWiseProductModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(data : CategoryWiseProductModel) {
        self.data = data
        if let title = self.data?.title {
            titleLbl.text = title
        }
    }
    func setDescriptionData(text : String?) {
        viewAllBtn.isHidden = true
        titleLbl.text = "Description".localized
        if let desc = text {
            descLbl.text = desc
        }
    }
}
