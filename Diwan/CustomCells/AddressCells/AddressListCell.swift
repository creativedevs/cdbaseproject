//
//  AddressListCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class AddressListCell: BaseUITableViewCell {
   
    @IBOutlet weak var addressDesc: BaseUILabel!
    @IBOutlet weak var checkBtn: BaseUIButton!
    @IBOutlet weak var deleteBtn: BaseUIButton!
    @IBOutlet weak var editBtn: BaseUIButton!
    @IBOutlet weak var titleLbl: BaseUILabel!
    
    var data : AddressModel?
    var didTapOnEdit:((_ data : AddressModel)-> Void)?
    var didTapOnDelete:((_ data : AddressModel)-> Void)?
    var didTapOnMarkDefault:((_ data : AddressModel)-> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func setData(data : AddressModel) {
        self.data = data
        if let title = data.fullName {
            titleLbl.text = title
        }
        var addressLine = ""
        if let line1 = data.line1 {
            addressDesc.text = line1
        }
//        if let line2 = data.fullName {
//            titleLbl.text = title
//        }
        let isDefaultImg = self.data?.isPrimary == true ? UIImage(named: "addressselect") : UIImage(named: "addressunselect")
        checkBtn.setImage(isDefaultImg, for: .normal)
    }
    @IBAction func deleteAddress(_ sender: Any) {
        
        Alert.showWithTwoActions(title: "", msg: "Are you sure to want to delete the address?".localized, okBtnTitle: "YES", okBtnAction: {
            self.didTapOnDelete?(self.data!)
        }, cancelBtnTitle: "NO") {
            
        }
        
    }
    @IBAction func editAddress(_ sender: Any) {
        
        self.didTapOnEdit?(self.data!)
        
    }
    @IBAction func makeDefaultAddress(_ sender: Any) {
        
        guard data!.isPrimary == false else {
            return
        }
        self.didTapOnMarkDefault?(data!)
    }
    
    
}
