//
//  PharmacyCategoryProductCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 25/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class BrandCollectionViewCell: UITableViewCell {

    @IBOutlet weak var viewAllBtn: FlashButton!
    @IBOutlet weak var titleLbl: BaseUILabel!
    @IBOutlet weak var collectionView: BaseUICollectionView!
    var data : CategoryWiseProductModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        setUpCollection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUpCollection(){
        
        collectionView.registerCell(BrandCollectionCell.self)
    }
    func setData(data : CategoryWiseProductModel) {
        self.data = data
        if let title = self.data?.title {
            titleLbl.text = title
        }
    }
    func setHomeData() {
        //viewAllBtn.isHidden = true
        titleLbl.text = "Popular Brands".localized
    }
}
extension BrandCollectionViewCell {

    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {

        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
    }

    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}
