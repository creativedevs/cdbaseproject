//
//  PharmacyCategoryProductCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 25/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class PharmacyCategoryProductCell: UITableViewCell {

    @IBOutlet weak var viewAllBtn: FlashButton!
    @IBOutlet weak var titleLbl: BaseUILabel!
    @IBOutlet weak var collectionView: BaseUICollectionView!
    var data : CategoryWiseProductModel?
    var attributedData : AttributeWiseProductModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        setUpCollection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUpCollection(){
        
        collectionView.registerCell(StoreProductCollectionCell.self)
    }
    func setData(data : CategoryWiseProductModel) {
        self.data = data
        if let title = self.data?.title {
            titleLbl.text = title
        }
    }
    func setAttributedHomeData(data : AttributeWiseProductModel) {
        self.attributedData = data
        if let title = self.attributedData?.title {
            titleLbl.text = title
        }
    }
    func setProductDetailData() {
        titleLbl.text = "Similar Product".localized
    }
}
extension PharmacyCategoryProductCell {

    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int, subTag : Int = 0) {

        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.collectionSubTag = subTag
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
    }

    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}
