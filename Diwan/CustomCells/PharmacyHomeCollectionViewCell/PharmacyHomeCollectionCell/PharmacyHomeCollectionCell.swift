//
//  PharmacyCategoryCollectionCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 24/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class PharmacyHomeCollectionCell: BaseUICollectionViewCell {

    @IBOutlet weak var titleLblLeadingConstraint: NSLayoutConstraint!
    var data : CategoryModel?
    var libraryData : LibraryMainModel?
    @IBOutlet weak var lblTitle: BaseUILabel!
    @IBOutlet weak var imgMain: BaseUIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imgMain.backgroundColor = ColorManager.color(forKey: "themeBlue")
        // Initialization code
    }
    func setData(data:CategoryModel) {
        self.data = data
        
        if let img = self.data?.imageIcon {
            imgMain.setImage(url: img, placeholder: nil, isActivity: true, activityColor: ColorManager.color(forKey: "white"))
        }
        if let title = self.data?.title {
            lblTitle.text = title
        }
        
    }
    func setLibraryData(data:LibraryMainModel, type : LibraryListType) {
        
        self.libraryData = data
        
        if (type == .section) {
            titleLblLeadingConstraint.constant = 30
             ///let titleLblFont = UIFont(name: FontManager.constant(forKey: "fontSFMedium")!,size: CGFloat(FontManager.style(forKey: "sizexLarge")))!
            /// let titleLblColor = ColorManager.color(forKey: "grayplaceholder")
             lblTitle.fontColorTheme = "grayplaceholder"
             lblTitle.fontNameTheme = "fontSFMedium"
             lblTitle.fontSizeTheme = "sizexLarge"
             lblTitle.textAlignment = .left//CurrentUser.languageId == .english
        }
        imgMain.image = UIImage()
        imgMain.backgroundColor = ColorManager.color(forKey: "themeBlue")
        //imgMain.setNeedsDisplay()
        if let img = self.libraryData?.imageUrl {
            imgMain.setImage(url: img, placeholder: nil, isActivity: true, activityColor: ColorManager.color(forKey: "white"))
        }
        if let title = self.libraryData?.title {
            lblTitle.text = title
        }
        
    }
    
    func setHomeData() {
        
    }
    
}
