//
//  ItemListingTableViewCell.swift
//  ASI
//
//  Created by Waqas Ali on 28/08/2019.
//  Copyright © 2019 Shujaat Khan. All rights reserved.
//

import UIKit

class CelebrityMainCell: UITableViewCell {

    @IBOutlet weak var profileImgHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgImage: BaseUIImageView!
    @IBOutlet weak var shadowImageView: BaseUIImageView!
    @IBOutlet weak var lblTitle: BaseUILabel!
    @IBOutlet weak var imgMain: BaseUIImageView!
    var data : User?
    override func awakeFromNib() {
        super.awakeFromNib()
        bgImage.backgroundColor = .orange//ColorManager.color(forKey: "themeBlue")
       // shadowImageView.image = shadowImageView.image?.flipIfNeeded()
//        /lblCounter.isHidden = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setCelebrityDetailData(data:User) {
        self.data = data
        profileImgHeightConstraint.constant = DesignUtility.getValueFromRatio(203)
        setInitialData()
        
    }
    func setData(data:User) {
        self.data = data
        setInitialData()
    }
    func setInitialData() {
        imgMain.backgroundColor = .clear// ColorManager.color(forKey: "themeBlue")
        bgImage.backgroundColor = ColorManager.color(forKey: "themeBlue")
        if let img = self.data?.profilePictureUrl {
            imgMain.setImage(url: img, placeholder: nil, isActivity: true, activityColor: ColorManager.color(forKey: "white"))
        }
        if let img = self.data?.profileBgUrl {
            bgImage.setImage(url: img, placeholder: nil, isActivity: false, activityColor: ColorManager.color(forKey: "white"))
        }
        if let name = self.data?.firstName {
            let lastWord = self.data?.lastName ?? "" //title.lastWord ?? ""
            let otherString = self.data?.firstName ?? ""//title.removeLastWord ?? ""
            //let lastWord = "ABCDE"
           // let otherString = "FGHI"
            var lastWordMutableString = NSMutableAttributedString()
            var otherWordMutableString = NSMutableAttributedString()
            
            //sizeXXLarge
            //LargeXL
            let otherStringFont = UIFont(name: FontManager.constant(forKey: "fontMedium")!,size: CGFloat(FontManager.style(forKey: "sizeXXXLarge")))!
            let lastWordFont = UIFont(name: FontManager.constant(forKey: "fontMedium")!,size: CGFloat(FontManager.style(forKey: "sizexSmall")))!
            otherWordMutableString = NSMutableAttributedString(string: otherString,
                                                               attributes: [NSAttributedString.Key.font:otherStringFont,NSAttributedString.Key.foregroundColor : ColorManager.color(forKey: "themeBlue")!])
            lastWordMutableString = NSMutableAttributedString(string: lastWord,
                                                              attributes: [NSAttributedString.Key.font:lastWordFont,NSAttributedString.Key.foregroundColor : ColorManager.color(forKey: "themeBlue")!])
            let combination = NSMutableAttributedString()
            combination.append(otherWordMutableString)
            combination.append(NSAttributedString(string: "\n"))
            combination.append(lastWordMutableString)
            lblTitle.attributedText = combination

        }
        
    }
   
}
