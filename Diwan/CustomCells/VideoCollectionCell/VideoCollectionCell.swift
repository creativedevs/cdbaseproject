//
//  AuthorMainCollectionCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 27/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import BMPlayer
class VideoCollectionCell: UICollectionViewCell {

    @IBOutlet weak var videoPlayer: BMCustomPlayer!
    @IBOutlet weak var playBtn: BounceButton!
    @IBOutlet weak var playerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var videoNameLbl: UILabel!
    @IBOutlet weak var videoThumbnailImg: UIImageView!
    var data : EhsanModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        videoPlayer.layer.cornerRadius = 10
        videoPlayer.layer.masksToBounds = true
        videoThumbnailImg.backgroundColor = ColorManager.color(forKey: "grayborder")
        // Initialization code
    }
    func setData(data : EhsanModel) {
        self.data = data
        playBtn.isUserInteractionEnabled = false
        setInitialData()
    }
    func setDetailData(data : EhsanModel) {
        self.data = data
        playerHeightConstraint.constant = DesignUtility.getValueFromRatio(182)
        playBtn.isUserInteractionEnabled = false
        setInitialData()
    }
    func setInitialData() {
        videoPlayer.isHidden = true
        videoThumbnailImg.image = UIImage()
        videoThumbnailImg.backgroundColor = ColorManager.color(forKey: "grayborder")
        
        if let img = self.data?.thumbnailUrl {
            videoThumbnailImg.setImage(url: img, placeholder: nil, isActivity: true, activityColor: ColorManager.color(forKey: "white"))
        }
        if let title = self.data?.title {
            videoNameLbl.text = title
        }
        if let title = self.data?.descriptionField {
            videoNameLbl.text = "\(videoNameLbl.text!)\n\(title)"
        }
    }
}
