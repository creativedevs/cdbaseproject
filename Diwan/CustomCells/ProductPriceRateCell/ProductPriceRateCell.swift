//
//  PharmacyCategoryProductCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 25/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import Cosmos
class ProductPriceRateCell: UITableViewCell {

    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var ratingValLbl: BaseUILabel!
    @IBOutlet weak var reviewCountLbl: BaseUILabel!
    @IBOutlet weak var stockImg: BaseUIImageView!
    @IBOutlet weak var stockLbl: BaseUILabel!
    @IBOutlet weak var amountLbl: BaseUILabel!
    @IBOutlet weak var discountLbl: BaseUILabel!
    
    var data : ProductModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        ratingView.settings.starSize = Double(DesignUtility.getValueFromRatio(14))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(data : ProductModel) {
        self.data = data
        ratingView.rating = 0.0
        ratingValLbl.text = "0.0"
        reviewCountLbl.text = "0" + "reviews".localized
        reviewCountLbl.text = "0" + "reviews".localized
        amountLbl.text = " "
        discountLbl.text = " "
        if let reviewObj = self.data?.productReviewObject {
            if reviewObj.count > 0{
                ratingView.rating = Double(reviewObj[0].avgStarRating)
                ratingValLbl.text = String(format: "%.1f", ratingView.rating)
                reviewCountLbl.text = String(reviewObj[0].reviewCount) + "reviews".localized
            }
        }
        stockImg.image = data.quantity > 0 ? UIImage(named: "instockicon") : UIImage(named: "outstockicon")
        stockLbl.text = data.quantity > 0 ? "In Stock".localized : "Out of Stock".localized
        stockLbl.textColor = data.quantity > 0 ? ColorManager.color(forKey: "themeSkyBlue2") : ColorManager.color(forKey: "redLightColor")
        amountLbl.text = "\(data.currencyObject?.isoCode ?? "AED") \(data.price)"
        discountLbl.isHidden = data.markdownPrice > 0 ? false : true
        discountLbl.text = "\(data.currencyObject?.isoCode ?? "AED") \(data.markdownPrice)"
        let attributes: [NSAttributedString.Key : Any] = [.strokeWidth: -3.0,
                                                          .strokeColor: UIColor.yellow,
                                                          .foregroundColor: UIColor.red]
        discountLbl.attributedText = NSAttributedString(string: discountLbl.text!, attributes: attributes)
        let font = UIFont(name: FontManager.constant(forKey: "fontRegular")!,size: CGFloat(FontManager.style(forKey: "sizeNormal")))!
        let attributeString = NSMutableAttributedString(string:discountLbl.text!,
                                                         attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue,
                                                                      NSAttributedString.Key.font:font,NSAttributedString.Key.foregroundColor : ColorManager.color(forKey: "grayplaceholder")!])
         discountLbl.attributedText = attributeString
    }
}
