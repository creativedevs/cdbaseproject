//
//  HomeBannerCollectionCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 08/07/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import FSPagerView
class HomeBannerCollectionCell: FSPagerViewCell {

    @IBOutlet weak var bannerImg: BaseUIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    var data : ProductMediaDetailModel?
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
    }
    func setData(_ row : Int) {
        titleLbl.text = String(row + 1)
    }
    func setProductDetailBannerData(_ data : ProductMediaDetailModel) {
        self.data = data
        titleLbl.text = ""
        let image = UIImage(named: "placeholderbiglogo")
        if let img = data.mediaUrl {
            bannerImg.setImage(url: img, placeholder: image, isActivity: true, activityColor: ColorManager.color(forKey: "white"))
        }
    }
    func setHomeBannerData(_ data : ProductMediaDetailModel) {
        self.data = data
        titleLbl.text = ""
        let image = UIImage(named: "placeholderbiglogo")
        if let img = data.thumbnailUrl {
            bannerImg.setImage(url: img, placeholder: image, isActivity: true, activityColor: ColorManager.color(forKey: "white"))
        }
    }
}
