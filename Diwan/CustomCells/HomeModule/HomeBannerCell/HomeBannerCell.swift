//
//  HomeBannerCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 08/07/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import FSPagerView

enum BannerType {
    case none
    case homeTop
    case homeMiddle1
    case detailTop
}

class HomeBannerCell: UITableViewCell {

    @IBOutlet weak var shareBtn: BounceButton!
    @IBOutlet weak var favoriteBtn: BounceButton!
    @IBOutlet weak var bannerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var pagerHeightConstraint: NSLayoutConstraint!
    var numberOfItems = 0
    var data : [ProductMediaDetailModel]?
    var topHomeBanner : [String]?
    var selectedBannerType : BannerType = .none
    @IBOutlet weak var pagerView: FSPagerView!{
        didSet {
            self.pagerView.register(UINib(nibName: "HomeBannerCollectionCell", bundle: nil), forCellWithReuseIdentifier: "HomeBannerCollectionCell")
           // self.pagerView.register(HomeBannerCollectionCell.self, forCellWithReuseIdentifier: "HomeBannerCollectionCell")
            self.pagerView.itemSize = FSPagerView.automaticSize
            self.pagerView.automaticSlidingInterval = 5.0
            self.pagerView.backgroundColor = .white
            self.pagerView.isInfinite = false
           // self.pagerView.transformer = FSPagerViewTransformer(type:.depth)
           // self.pagerView.decelerationDistance = 1
        }
    }
    @IBOutlet weak var pageControl: FSPageControl! {
        didSet {
            self.pageControl.numberOfPages = 0
            self.pageControl.backgroundColor = .white
            self.pageControl.contentHorizontalAlignment = .center
            self.pageControl.itemSpacing = DesignUtility.getValueFromRatio(8)
            self.pageControl.interitemSpacing = DesignUtility.getValueFromRatio(7)
            self.pageControl.setFillColor(ColorManager.color(forKey: "grayborder")!, for: .normal)
            self.pageControl.setFillColor(ColorManager.color(forKey: "themeSkyBlue2")!, for: .selected)
            self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setDetailTopData(data : [ProductMediaDetailModel], type : BannerType) {
        self.data = data
        selectedBannerType = type
        bannerTopConstraint.constant = 0
        favoriteBtn.isHidden = false
        shareBtn.isHidden = false
        self.setInitialData()
    }
    func setHomeTopData(data : [ProductMediaDetailModel], type : BannerType) {
        self.data = data
        selectedBannerType = type
        bannerTopConstraint.constant = 0
        self.setInitialData()
    }
    func setHomeMiddleData(data : [ProductMediaDetailModel], type : BannerType) {
        self.data = data
        selectedBannerType = type
        bannerTopConstraint.constant = 10
        self.setInitialData()
    }
    func setHomeData(data : [String], type : BannerType) {
        self.topHomeBanner = data
        selectedBannerType = type
        if (selectedBannerType == .homeMiddle1) {
            pagerHeightConstraint.constant = DesignUtility.getValueFromRatio(249)
        }
        numberOfItems = data.count
        pageControl.numberOfPages = data.count
        if self.data!.count > 1 {
            self.pagerView.isInfinite = true
        }
        self.pagerView.reloadData()
    }
    
    func setInitialData() {
        if (selectedBannerType == .homeMiddle1) {
                   pagerHeightConstraint.constant = DesignUtility.getValueFromRatio(249)
               }
        numberOfItems = data!.count
        pageControl.numberOfPages = data!.count
        if self.data!.count > 1 {
            self.pagerView.isInfinite = true
        }
        self.pagerView.reloadData()
    }
    
}
extension HomeBannerCell : FSPagerViewDelegate, FSPagerViewDataSource {
   
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.numberOfItems
    }
    
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "HomeBannerCollectionCell", at: index) as! HomeBannerCollectionCell
        if (selectedBannerType == .homeTop) {
           // cell.setProductDetailBannerData(data![index])
            cell.setHomeBannerData(data![index])
        }
        else if (selectedBannerType == .homeMiddle1) {
               // cell.setProductDetailBannerData(data![index])
                cell.setHomeBannerData(data![index])
            }
        else if (selectedBannerType == .detailTop) {
            cell.setProductDetailBannerData(data![index])
        }
        
        //cell.imageView?.image = UIImage(named: self.imageNames[index])
//        cell.imageView?.backgroundColor = .yellow
//        if index % 2 == 0 {
//            cell.imageView?.backgroundColor = .green
//        }
//        cell.imageView?.contentMode = .scaleAspectFill
//        cell.imageView?.clipsToBounds = true
        //cell.textLabel?.text = index.description+index.description
        return cell
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
    }
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
           self.pageControl.currentPage = pagerView.currentIndex
    }
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
}
