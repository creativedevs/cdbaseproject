//
//  PharmacyCategoryProductCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 25/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class VideoCollectionViewCell: UITableViewCell {

    @IBOutlet weak var viewAllBtn: FlashButton!
    @IBOutlet weak var titleLbl: BaseUILabel!
    @IBOutlet weak var collectionView: BaseUICollectionView!
    var data : CategoryWiseProductModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        setUpCollection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUpCollection(){
        
        collectionView.registerCell(EhsanMainCollectionCell.self)
    }
    func setData(data : CategoryWiseProductModel) {
        self.data = data
        if let title = self.data?.title {
            titleLbl.text = title
        }
    }
    func setProductDetailData() {
        viewAllBtn.isHidden = true
        titleLbl.text = "Videos for this Product".localized
    }
    func setHomeData() {
        //viewAllBtn.isHidden = true
        titleLbl.text = "Diwan TV".localized
    }
    func setCelebrityDetailData() {
        //viewAllBtn.isHidden = true
        titleLbl.text = "My TV".localized
    }
}
extension VideoCollectionViewCell {

    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int, subTag : Int = 0) {

        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.collectionSubTag = subTag
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
    }

    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}
