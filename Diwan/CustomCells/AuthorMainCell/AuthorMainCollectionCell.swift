//
//  AuthorMainCollectionCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 27/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class AuthorMainCollectionCell: UICollectionViewCell {

    @IBOutlet weak var bgImg: BaseUIImageView!
    @IBOutlet weak var authorNameLbl: UILabel!
    @IBOutlet weak var authorImg: UIImageView!
    var data : AuthorModel?
    var celebrityData : User?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setAuthorData(data : AuthorModel) {
        self.data = data
        //authorImg.image = UIImage()
        //authorImg.backgroundColor = ColorManager.color(forKey: "themeBlue")
        
        authorImg.image =  data.genderId == UserGender.male.rawValue ? UIImage(named: "authorboy") : UIImage(named: "authorgirl")
        if let img = self.data?.imageUrl {
            authorImg.setImage(url: img, placeholder: nil, isActivity: true, activityColor: ColorManager.color(forKey: "white"))
        }
        if let title = self.data?.title {
            authorNameLbl.text = title
            //authorImg.setImageForName(title, backgroundColor: ColorManager.color(forKey: "themeBlue"), circular: false, textAttributes: nil, gradient: false)
        }
        
    }
    func setCelebrityData(data : User) {
        self.celebrityData = data
        authorImg.image = UIImage()
        bgImg.isHidden = false
        authorImg.backgroundColor = .clear
        bgImg.backgroundColor = ColorManager.color(forKey: "themeBlue")
        
        if let name = self.celebrityData?.fullName {
            authorNameLbl.text = name
           // authorImg.setImageForName(name, backgroundColor: ColorManager.color(forKey: "themeBlue"), circular: false, textAttributes: nil, gradient: false)
        }
        if let img = self.celebrityData?.profileBgUrl {
            bgImg.setImage(url: img, placeholder: nil, isActivity: false, activityColor: ColorManager.color(forKey: "white"))
        }
        if let img = self.celebrityData?.profilePictureUrl {
            authorImg.setImage(url: img, placeholder: nil, isActivity: false, activityColor: ColorManager.color(forKey: "white"))
        }
        
        
    }
}
