//
//  ItemListingTableViewCell.swift
//  ASI
//
//  Created by Waqas Ali on 28/08/2019.
//  Copyright © 2019 Shujaat Khan. All rights reserved.
//

import UIKit

class StoreMainCategoryCell: UITableViewCell {

    @IBOutlet weak var shadowImageView: BaseUIImageView!
    @IBOutlet weak var lblTitle: BaseUILabel!
    @IBOutlet weak var imgMain: BaseUIImageView!
    var data : CategoryModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        shadowImageView.image = shadowImageView.image?.flipIfNeeded()
//        /lblCounter.isHidden = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(data:CategoryModel) {
        self.data = data
        imgMain.backgroundColor = ColorManager.color(forKey: "themeBlue")
        if let img = self.data?.imageIcon {
            imgMain.setImage(url: img, placeholder: nil, isActivity: true, activityColor: ColorManager.color(forKey: "white"))
        }
        if let title = self.data?.title {
            let lastWord = title.lastWord ?? ""
            let otherString = title.removeLastWord ?? ""
            //let lastWord = "ABCDE"
           // let otherString = "FGHI"
            var lastWordMutableString = NSMutableAttributedString()
            var otherWordMutableString = NSMutableAttributedString()
            
            //sizeXXLarge
            //LargeXL
            let otherStringFont = UIFont(name: FontManager.constant(forKey: "fontSFLight")!,size: CGFloat(FontManager.style(forKey: "sizeXXLarge")))!
            let lastWordFont = UIFont(name: FontManager.constant(forKey: "fontSFRegular")!,size: CGFloat(FontManager.style(forKey: "LargeXL")))!
            otherWordMutableString = NSMutableAttributedString(string: otherString,
                                                               attributes: [NSAttributedString.Key.font:otherStringFont,NSAttributedString.Key.foregroundColor : UIColor.white])
            lastWordMutableString = NSMutableAttributedString(string: lastWord,
                                                              attributes: [NSAttributedString.Key.font:lastWordFont,NSAttributedString.Key.foregroundColor : UIColor.white])
            let combination = NSMutableAttributedString()
            combination.append(otherWordMutableString)
            combination.append(NSAttributedString(string: "\n"))
            combination.append(lastWordMutableString)
            lblTitle.attributedText = combination

        }
        //lblTitle.text = data.detail
//        lblTitle.text = data.name//
    //    imgMain.image = UIImage(named: "")

//        if data.imageUrl != "" {
//            if let url = URL(string: data.imageUrl!) {
//                imgMain.af_setImage(withURL: url)
//            }
//        }else{
//            imgMain.image = nil //UIImage(named: "placeholder-user")
//        }
        
    }
}
