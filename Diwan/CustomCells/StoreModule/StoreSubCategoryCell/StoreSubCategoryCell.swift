//
//  ItemListingTableViewCell.swift
//  ASI
//
//  Created by Waqas Ali on 28/08/2019.
//  Copyright © 2019 Shujaat Khan. All rights reserved.
//

import UIKit

class StoreSubCategoryCell: UITableViewCell {

    @IBOutlet weak var arrowImg: BaseUIImageView!
    @IBOutlet weak var bottomLine: BaseUIView!
    @IBOutlet weak var lblTitle: BaseUILabel!
    @IBOutlet weak var imgMain: BaseUIImageView!
    var data : CategoryModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        arrowImg.image = arrowImg.image?.flipIfNeeded()
//        /lblCounter.isHidden = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(data:CategoryModel) {
        bottomLine.backgroundColor = ColorManager.color(forKey: "themeBlue")
        self.data = data
//        imgMain.backgroundColor = ColorManager.color(forKey: "themeBlue")
//        if let img = self.data?.imageIcon {
//            imgMain.setImage(url: img, placeholder: nil, isActivity: true, activityColor: ColorManager.color(forKey: "white"))
//        }
        if let title = self.data?.title {
            lblTitle.text = title
        }
        //lblTitle.text = data.detail
//        lblTitle.text = data.name//
    //    imgMain.image = UIImage(named: "")

//        if data.imageUrl != "" {
//            if let url = URL(string: data.imageUrl!) {
//                imgMain.af_setImage(withURL: url)
//            }
//        }else{
//            imgMain.image = nil //UIImage(named: "placeholder-user")
//        }
        
    }
}
