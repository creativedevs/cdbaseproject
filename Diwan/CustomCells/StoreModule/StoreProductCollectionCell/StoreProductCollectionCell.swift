//
//  StoreProductCollectionCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 18/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import Cosmos

enum ProductCellPosition {
    case left
    case right
    case normal
}

class StoreProductCollectionCell: UICollectionViewCell {

    @IBOutlet weak var viewTrainingConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewLeadingConstraint: NSLayoutConstraint!
    
    var cellPosition = ProductCellPosition.normal
    
    @IBOutlet weak var soldOutLbl: PaddingLabel!
    @IBOutlet weak var triangleImgView: BaseUIImageView!
    @IBOutlet weak var produceImgView: BaseUIImageView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var productDescLbl: BaseUILabel!
    @IBOutlet weak var productNameLbl: BaseUILabel!
    @IBOutlet weak var discountPercentageLbl: PaddingLabel!
    @IBOutlet weak var newItemLbl: PaddingLabel!
    @IBOutlet weak var discountAmountLbl: BaseUILabel!
    @IBOutlet weak var amountLbl: BaseUILabel!
    @IBOutlet weak var addToCartBtn: BounceButton!
    @IBOutlet weak var favoriteBgView: BaseUIView!
    @IBOutlet weak var addToCartBgView: BaseUIView!
    @IBOutlet weak var addOrRemoveFavoriteBtn: BounceButton!
    var data : ProductModel?
    @IBAction func testMethod(_ sender: Any) {
        print("TEST METHHOD")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        soldOutLbl.backgroundColor = ColorManager.color(forKey: "grayplaceholder")
        favoriteBgView.backgroundColor = ColorManager.color(forKey: "themeSkyBlue")
        discountPercentageLbl.backgroundColor = ColorManager.color(forKey: "themeBlue")
        newItemLbl.backgroundColor = ColorManager.color(forKey: "themeBlue")
        addToCartBgView.backgroundColor = ColorManager.color(forKey: "themeBlue")
        addToCartBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        ratingView.settings.starSize = Double(DesignUtility.getValueFromRatio(11))
        if (CurrentUser.languageId == .arabic) {
            addToCartBgView.cornerSides = [.bottomRight]
            favoriteBgView.cornerSides = [.bottomLeft]
        }
        else {
            addToCartBgView.cornerSides = [.bottomLeft]
            favoriteBgView.cornerSides = [.bottomRight]
        }
    }
//    @objc dynamic var currencyObject: CurrencyModel!
//      @objc dynamic var isFeatured: Bool
//      @objc dynamic var markdownPrice: Int
//      @objc dynamic var price: Int
//      @objc dynamic var productId: Int = 0
    func setData(data : ProductModel, cellPosition : ProductCellPosition? = ProductCellPosition.normal) {
        
        self.data = data
        
        switch cellPosition {
        case .left:
            viewLeadingConstraint.constant = 16
            viewTrainingConstraint.constant = 8
            break
        case .right:
            viewLeadingConstraint.constant = 8
            viewTrainingConstraint.constant = 16
            break
        case .normal :
            viewLeadingConstraint.constant = 0
            viewTrainingConstraint.constant = 5
            break
        default:
            print("")
        }
//        newItemLbl.isHidden = data.isNew
//        triangleImgView.isHidden = data.isNew
        soldOutLbl.isHidden = true
        newItemLbl.isHidden = true
        triangleImgView.isHidden = true
        if self.data != nil {
            var isExclusive = false
            for attributeObj in self.data!.productAttributeObject {
                
                if (attributeObj.attributeId  == AttributesIdType.new.rawValue ||
                    attributeObj.attributeId  == AttributesIdType.exclusive.rawValue) {
                    newItemLbl.isHidden = false
                    triangleImgView.isHidden = false
                    if (isExclusive){
                        newItemLbl.text = attributeObj.title
                    }
                    
                }
                if (attributeObj.attributeId  == AttributesIdType.exclusive.rawValue) {
                    isExclusive = true
                    newItemLbl.isHidden = false
                    triangleImgView.isHidden = false
                    newItemLbl.text = attributeObj.title
                }
//                if (attributeObj.attributeId  == AttributesIdType.discount.rawValue) {
//                    isExclusive = true
//                    newItemLbl.isHidden = false
//                    triangleImgView.isHidden = false
//                    newItemLbl.text = attributeObj.title
//                }
                
            }
            

//            if self.data!.productAttributeObject.contains(where: { $0.attributeId == AttributesIdType.new.rawValue
//            }) {
//                newItemLbl.isHidden = false
//                triangleImgView.isHidden = false
//            }
        }
        
        productNameLbl.text = data.title
        productDescLbl.text = data.descriptionField
        if data.productReviewObject.count > 0{
            ratingView.rating = Double(data.productReviewObject[0].avgStarRating)
        }
        
        discountPercentageLbl.isHidden = data.discountPercentage > 0 ? false : true
        discountAmountLbl.isHidden = data.markdownPrice > 0 ? false : true
        discountPercentageLbl.text = " \(data.discountPercentage)% "
        amountLbl.text = "\(data.currencyObject?.isoCode ?? "AED") \(data.price)"
        discountAmountLbl.text = "\(data.currencyObject?.isoCode ?? "AED") \(data.markdownPrice)"
        produceImgView.backgroundColor = ColorManager.color(forKey: "themeBlue")
        addToCartBtn.setTitle(data.quantity > 0 ? "Add to Cart".localized : "Notify Me".localized , for: .normal)
        addToCartBtn.setImage(data.quantity > 0 ? UIImage.init(named: "productcart") : UIImage.init(named: "notifyme") , for: .normal)
        soldOutLbl.isHidden = data.quantity > 0 ? true : false
        addToCartBtn.isUserInteractionEnabled = true
        if (data.quantity <= 0 && data.isNotify == true) {
            addToCartBtn.setTitle("Notified".localized , for: .normal)
            addToCartBtn.isUserInteractionEnabled = false
        }
        
        addToCartBgView.backgroundColor = data.quantity > 0 ? ColorManager.color(forKey: "themeBlue") : ColorManager.color(forKey: "grayplaceholder")
        addOrRemoveFavoriteBtn.setImage(data.isFavorite == true ? UIImage.init(named: "productheartselcted") : UIImage.init(named: "productheartunselcted") , for: .normal)
        let attributes: [NSAttributedString.Key : Any] = [.strokeWidth: -3.0,
                                                          .strokeColor: UIColor.yellow,
                                                          .foregroundColor: UIColor.red]

        discountAmountLbl.attributedText = NSAttributedString(string: discountAmountLbl.text!, attributes: attributes)
        
         let font = UIFont(name: FontManager.constant(forKey: "fontRegular")!,size: CGFloat(FontManager.style(forKey: "sizexXSmall")))!
        let attributeString = NSMutableAttributedString(string:discountAmountLbl.text!,
                                                        attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue,
                                                                     NSAttributedString.Key.font:font,NSAttributedString.Key.foregroundColor : ColorManager.color(forKey: "grayplaceholder")!])
       // attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        discountAmountLbl.attributedText = attributeString
        produceImgView.image = UIImage()
        produceImgView.backgroundColor = ColorManager.color(forKey: "themeBlue")
//        guard data.productMediaObject.count == 0 else {
//            if let img = data.productMediaObject[0].mediaUrl {
//                    produceImgView.setImage(url: img, placeholder: nil, isActivity: true, activityColor: ColorManager.color(forKey: "white"))
//                }
//            return
//        }
        
        guard data.productMediaObject.bannerImages.count == 0 else {
            if let img = data.productMediaObject.bannerImages[0].mediaUrl {
                    produceImgView.setImage(url: img, placeholder: nil, isActivity: true, activityColor: ColorManager.color(forKey: "white"))
                }
            return
        }
            
        }
}
