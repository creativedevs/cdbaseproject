//
//  ItemCartCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 23/07/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class ItemCartCell: UITableViewCell {

    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var subtractBtn: UIButton!
    @IBOutlet weak var discountAmountLbl: BaseUILabel!
    @IBOutlet weak var amountLbl: BaseUILabel!
    @IBOutlet weak var subtitle2Lbl: BaseUILabel!
    @IBOutlet weak var subtitle1Lbl: BaseUILabel!
    @IBOutlet weak var itemCountLbl: BaseUILabel!
    @IBOutlet weak var titleLbl: BaseUILabel!
    @IBOutlet weak var productImg: BaseUIImageView!
    @IBOutlet weak var heartBtn: BounceButton!
    
    var data : CartModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setCartData(data : CartModel) {
        self.data = data
        titleLbl.text = ""
        itemCountLbl.text = ""
        if let title = self.data?.product.title {
            titleLbl.text = title
        }
        if let quantity = self.data?.quantity {
            itemCountLbl.text = String(quantity)
        }
        heartBtn.setImage(self.data!.product.isFavorite == true ? UIImage.init(named: "lightblueheartfill") : UIImage.init(named: "lightblueheart") , for: .normal)
        
        productImg.image = UIImage()
        productImg.backgroundColor = ColorManager.color(forKey: "themeBlue")
        guard self.data?.product.productMediaObject.bannerImages.count == 0 else {
            if let img = self.data?.product.productMediaObject.bannerImages[0].mediaUrl {
                    productImg.setImage(url: img, placeholder: nil, isActivity: true, activityColor: ColorManager.color(forKey: "white"))
                }
            return
        }
    }
    @IBAction func removeFromCart(_ sender: UIButton) {
        if (self.data!.quantity > 1){
            addToCart(isAdd: false)
        }
        
    }
    @IBAction func addToCart(_ sender: UIButton) {
        addToCart(isAdd: true)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension ItemCartCell {
    
    func addToCart(isAdd : Bool) {
        try! Constants.realm.write(){
            
            self.data!.quantity = isAdd == true ? self.data!.quantity + 1 : self.data!.quantity - 1
            itemCountLbl.text = String(self.data!.quantity)
            Constants.realm.add(self.data!, update: .all)
        }
//        else {
//            cartItem = CartModel()
//            cartItem!.id = data.productId
//            cartItem!.product = data
//            try! Constants.realm.write(){
//                Constants.realm.add(cartItem!, update: .all)
//            }
//        }
        
    }
}
