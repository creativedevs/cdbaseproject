//
//  PhotosCollectionCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 12/07/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class FeaturesCollectionCell: UICollectionViewCell {

    @IBOutlet weak var descLbl: BaseUILabel!
    @IBOutlet weak var titleLbl: BaseUILabel!
    var data : ProductMediaDetailModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(data : [String : Any]) {
        //self.data = data
        titleLbl.text = (data["key"] as! String).localized
        descLbl.text = data["value"] as? String
    }
    func setInitialData() {
        
        //titleLbl.text = "qej iopjioerjhidfj ijdfsiojdfsioj iojdfiosjdfsioj iojdfsiojfdsioj ioj"
    }
    
//    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
//        setNeedsLayout()
//        layoutIfNeeded()
//            
//        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
//            
//        var frame = layoutAttributes.frame
//        frame.size.height = ceil(size.height)
//            
//        layoutAttributes.frame = frame
//            
//        return layoutAttributes
//    }

}
