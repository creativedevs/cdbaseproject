//
//  PharmacyCategoryProductCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 25/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class FeaturesCollectionViewCell: UITableViewCell {

    @IBOutlet weak var viewAllBtn: FlashButton!
    @IBOutlet weak var titleLbl: BaseUILabel!
    @IBOutlet weak var collectionView: BaseUICollectionView!
    var data : CategoryWiseProductModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        setUpCollection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUpCollection(){
        
        collectionView.registerCell(FeaturesCollectionCell.self)
        
    }
    func setData(data : CategoryWiseProductModel) {
        self.data = data
        if let title = self.data?.title {
            titleLbl.text = title
        }
    }
    func setFeatureDetailData() {
        viewAllBtn.isHidden = true
        titleLbl.text = "Features & details".localized
    }
}
extension FeaturesCollectionViewCell {

    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {

        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
    }

    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}
extension FeaturesCollectionViewCell: UICollectionViewDelegateFlowLayout {
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        
        self.collectionView.layoutIfNeeded()
        let contentSize = self.collectionView.collectionViewLayout.collectionViewContentSize
//        if self.collectionView.numberOfItems(inSection: 0) < 4 {
//            return CGSize(width: contentSize.width, height: 114) // Static height if colview is not fitted properly.
//        }
        
        return CGSize(width: contentSize.width, height: contentSize.height + DesignUtility.getValueFromRatio(57))
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        
//        let width = collectionView.frame.size.width
//        return CGSize(width: width, height: 30)
//    }
}
