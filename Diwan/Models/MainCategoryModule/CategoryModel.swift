//
//  CategoryModel.swift
//  Diwan
//
//  Created by Muzamil Hassan on 17/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class CategoryModel: Object, Codable {

   @objc dynamic var categoryId: Int = 0
   @objc dynamic var descriptionField: String!
   @objc dynamic var hasChild: Bool = false
   @objc dynamic var imageIcon: String!
   @objc dynamic var parentCategory: Int = 0
   @objc dynamic var title: String!
   
    
    private enum CodingKeys: String, CodingKey {
        
        case categoryId             = "category_Id"
        case descriptionField       = "description"
        case hasChild               = "has_child"
        case imageIcon              = "image_url"
        case parentCategory         = "parent_category"
        case title                  = "title"
        
    }
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.categoryId = try container.decodeIfPresent(Int.self, forKey: .categoryId) ?? Int()
        self.descriptionField = try container.decodeIfPresent(String.self, forKey: .descriptionField) ?? String()
        self.hasChild = try container.decodeIfPresent(Bool.self, forKey: .hasChild) ?? Bool()
        self.imageIcon = try container.decodeIfPresent(String.self, forKey: .imageIcon) ?? String()
        self.parentCategory = try container.decodeIfPresent(Int.self, forKey: .parentCategory) ?? Int()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
    }
    
    
    override static func primaryKey() -> String? { return "categoryId" }
    

}
