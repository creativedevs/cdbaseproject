//
//  FilterSortModel.swift
//  Diwan
//
//  Created by Muzamil Hassan on 20/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

protocol SortFilterProtocol {
    
}

class SortModel: Object, Codable, SortFilterProtocol {

    @objc dynamic var id: Int = 0
    @objc dynamic var title: String!
    @objc dynamic var isSelected : Bool = false
    @objc dynamic var hasChild : Bool = false
    
    private enum CodingKeys: String, CodingKey {

        case id             = "id"
        case title       = "title"
        case isSelected   = "isSelected"
        case hasChild   = "hasChild"
    }
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? Int()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
        self.isSelected = try container.decodeIfPresent(Bool.self, forKey: .isSelected) ?? Bool()
        self.hasChild = try container.decodeIfPresent(Bool.self, forKey: .hasChild) ?? Bool()
    }


    override static func primaryKey() -> String? { return "id" }
}
class FilterModel: Object, Codable, SortFilterProtocol {

    @objc dynamic var id: Int = 0
    var filters = List<SubFilterModel>()
    @objc dynamic var screenId: Int = 0
    @objc dynamic var filterEntityId: Int = 0
    @objc dynamic var filterEntityName: String!
    @objc dynamic var filterSelectionTypeId: Int = 0
    @objc dynamic var filterSelectionTypeName: String!
    @objc dynamic var maxValue: String!
    @objc dynamic var minValue: String!
    @objc dynamic var screenName: String!
    @objc dynamic var isSelected : Bool = false
    
    private enum CodingKeys: String, CodingKey {

        case id                       = "id"
        case filters                  = "entity_data"
        case screenId                 = "screen_id"
        case filterEntityId           = "filter_entity_id"
        case filterEntityName         = "filter_entity_name"
        case filterSelectionTypeId    = "filter_selection_type_id"
        case filterSelectionTypeName  = "filter_selection_type_name"
        case maxValue                 = "max_value"
        case minValue                 = "min_value"
        case screenName               = "screen_name"
        case isSelected               = "isSelected"
    }
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? Int()
        self.screenId = try container.decodeIfPresent(Int.self, forKey: .screenId) ?? Int()
        self.filterEntityId = try container.decodeIfPresent(Int.self, forKey: .filterEntityId) ?? Int()
        self.filterSelectionTypeId = try container.decodeIfPresent(Int.self, forKey: .filterSelectionTypeId) ?? Int()
        self.maxValue = try container.decodeIfPresent(String.self, forKey: .maxValue) ?? String()
        self.minValue = try container.decodeIfPresent(String.self, forKey: .minValue) ?? String()
        self.filterEntityName = try container.decodeIfPresent(String.self, forKey: .filterEntityName) ?? String()
        self.filterSelectionTypeName = try container.decodeIfPresent(String.self, forKey: .filterSelectionTypeName) ?? String()
        self.filterSelectionTypeName = try container.decodeIfPresent(String.self, forKey: .filterSelectionTypeName) ?? String()
        self.isSelected = try container.decodeIfPresent(Bool.self, forKey: .isSelected) ?? Bool()
        let filtersArray = try (container.decodeIfPresent([SubFilterModel].self, forKey: .filters) ?? [])
        self.filters.append(objectsIn: filtersArray)
    }


    override static func primaryKey() -> String? { return "id" }
}
class SubFilterModel: Object, Codable, SortFilterProtocol {

    @objc dynamic var id: Int = 0
    @objc dynamic var title: String!
    @objc dynamic var isSelected : Bool = false
    @objc dynamic var hasChild : Bool = false
    @objc dynamic var starRating: Int = 0
    private enum CodingKeys: String, CodingKey {

        case id             = "id"
        case title       = "title"
        case isSelected   = "isSelected"
        case hasChild   = "hasChild"
    }
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? Int()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
        self.isSelected = try container.decodeIfPresent(Bool.self, forKey: .isSelected) ?? Bool()
        self.hasChild = try container.decodeIfPresent(Bool.self, forKey: .hasChild) ?? Bool()
    }


    override static func primaryKey() -> String? { return "id" }
}
protocol DetachableObject: AnyObject {
  func detached() -> Self
}

extension Object: DetachableObject {
  func detached() -> Self {
    let detached = type(of: self).init()
    for property in objectSchema.properties {
      guard let value = value(forKey: property.name) else {
        continue
      }
      if let detachable = value as? DetachableObject {
        detached.setValue(detachable.detached(), forKey: property.name)
      } else { // Then it is a primitive
        detached.setValue(value, forKey: property.name)
      }
    }
    return detached
  }
}

extension List: DetachableObject {
  func detached() -> List<Element> {
    let result = List<Element>()
    forEach {
      if let detachableObject = $0 as? DetachableObject,
        let element = detachableObject.detached() as? Element {
        result.append(element)
      } else { // Then it is a primitive
        result.append($0)
      }
    }
    return result
  }
}
