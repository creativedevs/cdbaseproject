//
//  BrandModel.swift
//  Diwan
//
//  Created by Muzamil Hassan on 27/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
class BrandModel: Object, Codable {

   @objc dynamic var Id: Int = 0
   @objc dynamic var imageUrl: String!
   @objc dynamic var logoUrl: String!
   @objc dynamic var sequence: Int = 0
   @objc dynamic var title: String!
   @objc dynamic var isPopular : Bool = false
    
    private enum CodingKeys: String, CodingKey {
        
        case Id             = "id"
        case sequence       = "sequence"
        case imageUrl       = "image_url"
        case logoUrl        = "logo_url"
        case title          = "title"
        case isPopular      = "is_popular"
        
    }
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.Id = try container.decodeIfPresent(Int.self, forKey: .Id) ?? Int()
        self.imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl) ?? String()
        self.logoUrl = try container.decodeIfPresent(String.self, forKey: .logoUrl) ?? String()
        self.sequence = try container.decodeIfPresent(Int.self, forKey: .sequence) ?? Int()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
        self.isPopular = try container.decodeIfPresent(Bool.self, forKey: .isPopular) ?? Bool()
    }
    
    
    override static func primaryKey() -> String? { return "Id" }
    

}
