//
//  CategoryModel.swift
//  Diwan
//
//  Created by Muzamil Hassan on 17/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift



class CartModel: Object, Codable {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var priceCart: Int = 0
    @objc dynamic var quantity: Int = 1
    @objc dynamic var product : ProductModel!
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        
    }
    override static func primaryKey() -> String? { return "id" }
    
    //Incrementa ID
    func IncrementaID() -> Int{
        let realm = try! Realm()
        if let retNext = realm.objects(CartModel.self).sorted(byKeyPath: "id").first?.id {
            return retNext + 1
        }else{
            return 1
        }
    }
}



/**/
