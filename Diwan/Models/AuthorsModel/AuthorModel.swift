//
//  BrandModel.swift
//  Diwan
//
//  Created by Muzamil Hassan on 27/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
class AuthorModel: Object, Codable {

   @objc dynamic var Id: Int = 0
   @objc dynamic var imageUrl: String!
   @objc dynamic var sequence: Int = 0
   @objc dynamic var title: String!
   @objc dynamic var genderId: Int = 0
    
    private enum CodingKeys: String, CodingKey {
        
        case Id             = "id"
        case sequence       = "sequence"
        case imageUrl       = "image_url"
        case title          = "title"
        case genderId       = "gender_id"
        
    }
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.Id = try container.decodeIfPresent(Int.self, forKey: .Id) ?? Int()
        self.imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl) ?? String()
        self.sequence = try container.decodeIfPresent(Int.self, forKey: .sequence) ?? Int()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
        self.genderId = try container.decodeIfPresent(Int.self, forKey: .genderId) ?? Int()
    }
    
    
    override static func primaryKey() -> String? { return "Id" }
    

}
