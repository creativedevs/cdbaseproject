//
//  CategoryModel.swift
//  Diwan
//
//  Created by Muzamil Hassan on 17/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class CategoryWiseProductModel: Object, Codable {

    @objc dynamic var categoryId: Int = 0
    let productlist = List<ProductModel>()
    let products = List<ProductModel>()
    @objc dynamic var title: String!
    @objc dynamic var totalCount: Int = 0
    private enum CodingKeys: String, CodingKey {
        
        case categoryId             = "id"
        case title                      = "title"
        case productlist         = "list_of_product"
        case products         = "products"
        
    }
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.categoryId = try container.decodeIfPresent(Int.self, forKey: .categoryId) ?? Int()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
        let productObjectArray = try (container.decodeIfPresent([ProductModel].self, forKey: .productlist) ?? [])
        let celebrityProducts = try (container.decodeIfPresent([ProductModel].self, forKey: .products) ?? [])
        self.totalCount = productObjectArray.count
        self.productlist.append(objectsIn: productObjectArray)
        self.products.append(objectsIn: celebrityProducts)
    }
    
    
    override static func primaryKey() -> String? { return "categoryId" }
    

}
