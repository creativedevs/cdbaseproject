//
//  State.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class State: Object, Codable, CountryStateCityProtocol {
    
    @objc dynamic var stateId: Int = 0
    @objc dynamic var countryId: Int = 0
    @objc dynamic var sequence: Int = 0
    @objc dynamic var title: String!
    

    private enum CodingKeys: String, CodingKey {
        
        case stateId          = "state_id"
        case countryId         = "country_id"
        case sequence          = "sequence"
        case title             = "title"
    }
    //func decode(from decoder: Decoder) throws {
    required convenience init(from decoder: Decoder) throws {
        
            self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.stateId = try container.decodeIfPresent(Int.self, forKey: .stateId) ?? Int()
        self.countryId = try container.decodeIfPresent(Int.self, forKey: .countryId) ?? Int()
        self.sequence = try container.decodeIfPresent(Int.self, forKey: .sequence) ?? Int()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
        
    }
    
    
    override static func primaryKey() -> String? { return "stateId" }

}
