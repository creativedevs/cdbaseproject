//
//  Currency.swift
//  Diwan
//
//  Created by Muzamil Hassan on 26/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class Currency: Object, Codable {

   @objc dynamic var Id: Int = 0
   @objc dynamic var imageUrl: String!
   @objc dynamic var title: String!
   
    
    private enum CodingKeys: String, CodingKey {
        
        case Id             = "id"
        case imageUrl              = "icon"
        case title                  = "title"
        
    }
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.Id = try container.decodeIfPresent(Int.self, forKey: .Id) ?? Int()
        self.imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl) ?? String()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
    }
    
    
    override static func primaryKey() -> String? { return "Id" }
    

}
