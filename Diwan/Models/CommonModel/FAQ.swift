//
//  FAQ.swift
//  Diwan
//
//  Created by Muzamil Hassan on 13/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class FAQ: Object, Codable {

    @objc dynamic var answer: String!
    @objc dynamic var question: String!
    @objc dynamic var sequence: Int
    @objc dynamic var serialNumber: Int

    private enum CodingKeys: String, CodingKey {
        
        case answer         = "answer"
        case question       = "question"
        case sequence       = "sequence"
        case serialNumber   = "serial_number"
    }
    func decode(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.answer = try container.decodeIfPresent(String.self, forKey: .answer)  ?? String()
        self.question = try container.decodeIfPresent(String.self, forKey: .question) ?? String()
        self.sequence = try container.decodeIfPresent(Int.self, forKey: .sequence) ?? Int()
        self.serialNumber = try container.decodeIfPresent(Int.self, forKey: .serialNumber) ?? Int()
    }
    
    
    override static func primaryKey() -> String? { return "serialNumber" }

}
