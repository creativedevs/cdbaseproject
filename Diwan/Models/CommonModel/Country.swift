//
//  Country.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

protocol CountryStateCityProtocol {
    
}

class Country: Object, Codable, CountryStateCityProtocol {
    
    @objc dynamic var countryId: Int = 0
    @objc dynamic var dialingCode: String!
    @objc dynamic var title: String!
    

    private enum CodingKeys: String, CodingKey {
        
        case countryId         = "country_id"
        case dialingCode       = "dialing_code"
        case title             = "title"
    }
    required convenience init(from decoder: Decoder) throws {
   // func decode(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.countryId = try container.decodeIfPresent(Int.self, forKey: .countryId) ?? Int()
        self.dialingCode = try container.decodeIfPresent(String.self, forKey: .dialingCode)  ?? String()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
        
    }
    
    
    override static func primaryKey() -> String? { return "countryId" }

}
