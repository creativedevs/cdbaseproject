//
//  CMSData.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class CMSData: Object, Codable {

    @objc dynamic var id: Int
    @objc dynamic var sequence: Int
    @objc dynamic var title: String
    @objc dynamic var descriptionField: String!
    

    private enum CodingKeys: String, CodingKey {
        
        case id         = "id"
        case sequence       = "sequence"
        case title       = "title"
        case descriptionField   = "description"
    }
    func decode(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.descriptionField = try container.decodeIfPresent(String.self, forKey: .descriptionField)  ?? String()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
        self.sequence = try container.decodeIfPresent(Int.self, forKey: .sequence) ?? Int()
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? Int()
    }
    
    
    override static func primaryKey() -> String? { return "id" }

}
