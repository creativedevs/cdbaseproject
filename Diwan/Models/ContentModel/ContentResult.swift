//
//	ContentResult.swift
//
//	Create by Waqas Ali on 1/6/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ContentResult : Codable {

	let contentID : Int?
	let contentKey : String?
	let contentValue : String?
	let createdBy : String?
	let creationDate : String?
	let updatedBy : String?
	let updatedDate : String?


	enum CodingKeys: String, CodingKey {
		case contentID = "ContentID"
		case contentKey = "ContentKey"
		case contentValue = "ContentValue"
		case createdBy = "CreatedBy"
		case creationDate = "CreationDate"
		case updatedBy = "UpdatedBy"
		case updatedDate = "UpdatedDate"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		contentID = try values.decodeIfPresent(Int.self, forKey: .contentID) ?? Int()
		contentKey = try values.decodeIfPresent(String.self, forKey: .contentKey) ?? String()
		contentValue = try values.decodeIfPresent(String.self, forKey: .contentValue) ?? String()
		createdBy = try values.decodeIfPresent(String.self, forKey: .createdBy) ?? String()
		creationDate = try values.decodeIfPresent(String.self, forKey: .creationDate) ?? String()
		updatedBy = try values.decodeIfPresent(String.self, forKey: .updatedBy) ?? String()
		updatedDate = try values.decodeIfPresent(String.self, forKey: .updatedDate) ?? String()
	}


}