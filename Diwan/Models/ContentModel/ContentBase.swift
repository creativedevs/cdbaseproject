//
//	ContentBase.swift
//
//	Create by Waqas Ali on 1/6/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ContentBase : Codable {

	let message : String?
	let response : String?
	let result : ContentResult?


	enum CodingKeys: String, CodingKey {
		case message = "Message"
		case response = "Response"
		case result = "Result"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		message = try values.decodeIfPresent(String.self, forKey: .message) ?? String()
		response = try values.decodeIfPresent(String.self, forKey: .response) ?? String()
		result = try values.decodeIfPresent(ContentResult.self, forKey: .result)  //?? ContentResult()
	}


}
