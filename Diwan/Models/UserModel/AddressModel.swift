//
//  AddressModel.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class AddressModel: Object, Codable {

   @objc dynamic var cityId: Int = 0
   @objc dynamic var countryId: Int = 0
   @objc dynamic var fullName: String!
   @objc dynamic var id: Int = 0
   @objc dynamic var isPrimary: Bool = false
   @objc dynamic var landlineCountryId: String!
   @objc dynamic var landlineNumber: String!
   @objc dynamic var line1: String!
   @objc dynamic var line2: String!
   @objc dynamic var mobileCountryId: String!
   @objc dynamic var mobileNumber: String!
   @objc dynamic var moreInfo: String!
   @objc dynamic var stateId: Int = 0
   @objc dynamic var userId: Int = 0
   @objc dynamic var zipCode: String!
    
    @objc dynamic var countryName: String!
    @objc dynamic var stateName: String!
    @objc dynamic var cityName: String!
    
    private enum CodingKeys: String, CodingKey {
        
        case cityId             = "city_id"
        case countryId          = "country_id"
        case fullName           = "full_name"
        case id                 = "id"
        case isPrimary          = "is_primary"
        case landlineCountryId  = "landline_country_id"
        case landlineNumber     = "landline_number"
        case line1              = "line_1"
        case line2              = "line_2"
        case mobileCountryId    = "mobile_country_id"
        case mobileNumber       = "mobile_number"
        case moreInfo           = "more_info"
        case stateId            = "state_id"
        case userId             = "user_id"
        case zipCode            = "zip_code"
        case countryName            = "country_name"
        case stateName             = "state_name"
        case cityName            = "city_name"
        
    }
    //func decode(from decoder: Decoder) throws {
    required convenience init(from decoder: Decoder) throws {
        
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.cityId = try container.decodeIfPresent(Int.self, forKey: .cityId) ?? Int()
        self.countryId = try container.decodeIfPresent(Int.self, forKey: .countryId) ?? Int()
        self.fullName = try container.decodeIfPresent(String.self, forKey: .fullName) ?? String()
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? Int()
        self.isPrimary = try container.decodeIfPresent(Bool.self, forKey: .isPrimary) ?? Bool()
        self.landlineCountryId = try container.decodeIfPresent(String.self, forKey: .landlineCountryId) ?? String()
        self.landlineNumber = try container.decodeIfPresent(String.self, forKey: .landlineNumber) ?? String()
        self.line1 = try container.decodeIfPresent(String.self, forKey: .line1) ?? String()
        self.line2 = try container.decodeIfPresent(String.self, forKey: .line2) ?? String()
        self.mobileCountryId = try container.decodeIfPresent(String.self, forKey: .mobileCountryId) ?? String()
        self.mobileNumber = try container.decodeIfPresent(String.self, forKey: .mobileNumber) ?? String()
        self.moreInfo = try container.decodeIfPresent(String.self, forKey: .moreInfo) ?? String()
        self.stateId = try container.decodeIfPresent(Int.self, forKey: .stateId) ?? Int()
        self.userId = try container.decodeIfPresent(Int.self, forKey: .userId) ?? Int()
        self.zipCode = try container.decodeIfPresent(String.self, forKey: .zipCode) ?? String()
        self.countryName = try container.decodeIfPresent(String.self, forKey: .countryName) ?? String()
        self.stateName = try container.decodeIfPresent(String.self, forKey: .stateName) ?? String()
        self.cityName = try container.decodeIfPresent(String.self, forKey: .cityName) ?? String()
    }
    
    
    override static func primaryKey() -> String? { return "id" }
    

}
