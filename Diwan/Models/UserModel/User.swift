//
//  UserResponseModel.swift
//  Diwan
//
//  Created by Muzamil Hassan on 09/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class User: Object, Codable {

    @objc dynamic var dateOfBirth: String!
    @objc dynamic var emailAddress: String!
    @objc dynamic var firstName: String!
    @objc dynamic var userName: String!
    @objc dynamic var firstNameAr: String!
    @objc dynamic var fullName: String!
    @objc dynamic var fullNameAr: String!
    @objc dynamic var genderId: Int = 0
    @objc dynamic var id: Int = 0
    @objc dynamic var lastName: String!
    @objc dynamic var lastNameAr: String!
    @objc dynamic var phoneNumber: String!
    @objc dynamic var profilePictureUrl: String!
    @objc dynamic var profileBgUrl: String!
    @objc dynamic var phoneCountryCode: String!
    @objc dynamic var authToken: String!
    @objc dynamic var isEmailVerified: Bool = false
    @objc dynamic var isSocialLogin: Bool = false
    @objc dynamic var userTypeId: Int = 0

    private enum CodingKeys: String, CodingKey {
        
        case dateOfBirth         = "date_of_birth"
        case emailAddress        = "email_address"
        case firstName           = "first_name"
        case firstNameAr         = "first_name_ar"
        case fullName            = "full_name"
        case fullNameAr          = "full_name_ar"
        case genderId            = "gender_id"
        case id                  = "user_id"
        case lastName            = "last_name"
        case lastNameAr          = "last_name_ar"
        case phoneNumber         = "phone_number"
        case profilePictureUrl   = "profile_picture_url"
        case profileBgUrl        = "profile_bg_url"
        case phoneCountryCode    = "phone_country_code"//phone_country_id"
        case authToken           = "auth_token"
        case isEmailVerified     = "is_email_verified"
        case userTypeId          = "user_type_id"
        case userName            = "user_name"
        //case isSocialLogin            = "isSocialLogin"
    }
    //func decode(from decoder: Decoder) throws {
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.dateOfBirth = try container.decodeIfPresent(String.self, forKey: .dateOfBirth)  ?? String()
        self.emailAddress = try container.decodeIfPresent(String.self, forKey: .emailAddress) ?? String()
        self.firstName = try container.decodeIfPresent(String.self, forKey: .firstName) ?? String()
        self.firstNameAr = try container.decodeIfPresent(String.self, forKey: .firstNameAr) ?? String()
        self.fullName = try container.decodeIfPresent(String.self, forKey: .fullName) ?? String()
        self.fullNameAr = try container.decodeIfPresent(String.self, forKey: .fullNameAr) ?? String()
        self.userName = try container.decodeIfPresent(String.self, forKey: .userName) ?? String()
        self.genderId = try container.decodeIfPresent(Int.self, forKey: .genderId) ?? Int()
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? Int()
        self.lastName = try container.decodeIfPresent(String.self, forKey: .lastName) ?? String()
        self.lastNameAr = try container.decodeIfPresent(String.self, forKey: .lastNameAr) ?? String()
        self.phoneNumber = try container.decodeIfPresent(String.self, forKey: .phoneNumber) ?? String()
        self.profilePictureUrl = try container.decodeIfPresent(String.self, forKey: .profilePictureUrl) ?? String()
        self.profileBgUrl = try container.decodeIfPresent(String.self, forKey: .profileBgUrl) ?? String()
        self.phoneCountryCode = try container.decodeIfPresent(String.self, forKey: .phoneCountryCode) ?? String()
        self.authToken = try container.decodeIfPresent(String.self, forKey: .authToken) ?? String()
        self.isEmailVerified = try container.decodeIfPresent(Bool.self, forKey: .isEmailVerified) ?? Bool()
        self.userTypeId = try container.decodeIfPresent(Int.self, forKey: .userTypeId) ?? Int()
        //self.isSocialLogin = try container.decodeIfPresent(Bool.self, forKey: .isSocialLogin) ?? Bool()
        //status = try values.decodeIfPresent(Bool.self, forKey: .status) ?? Bool()
       // self.errorInternal1 = try container.decode(String.self, forKey: .errorInternal1) ?? String()
    }
    
    
    override static func primaryKey() -> String? { return "id" }
    
//    class func fromDictionary(dictionary: [String:Any]) -> User    {
//        let this = User()
//        if let dateOfBirthValue = dictionary["date_of_birth"] as? String{
//            this.dateOfBirth = dateOfBirthValue
//        }
//        if let emailAddressValue = dictionary["email_address"] as? String{
//            this.emailAddress = emailAddressValue
//        }
//        if let firstNameValue = dictionary["first_name"] as? String{
//            this.firstName = firstNameValue
//        }
//        if let firstNameArValue = dictionary["first_name_ar"] as? String{
//            this.firstNameAr = firstNameArValue
//        }
//        if let fullNameValue = dictionary["full_name"] as? String{
//            this.fullName = fullNameValue
//        }
//        if let fullNameArValue = dictionary["full_name_ar"] as? String{
//            this.fullNameAr = fullNameArValue
//        }
//        if let genderIdValue = dictionary["gender_id"] as? Int{
//            this.genderId = genderIdValue
//        }
//        if let idValue = dictionary["id"] as? Int{
//            this.id = idValue
//        }
//        if let lastNameValue = dictionary["last_name"] as? String{
//            this.lastName = lastNameValue
//        }
//        if let lastNameArValue = dictionary["last_name_ar"] as? String{
//            this.lastNameAr = lastNameArValue
//        }
//        if let phoneNumberValue = dictionary["phone_number"] as? String{
//            this.phoneNumber = phoneNumberValue
//        }
//        if let profilePictureUrlValue = dictionary["profile_picture_url"] as? String{
//            this.profilePictureUrl = profilePictureUrlValue
//        }
//        return this
//    }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
}
