//
//  NavigationData.swift
//  Diwan
//
//  Created by Muzamil Hassan on 14/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class NavigationData: Object, Codable {

    @objc dynamic var iconImage: String!
    @objc dynamic var id: Int = 0
    dynamic var masterPage: Int? = 0
    //@objc dynamic var masterPage: String!
    @objc dynamic var menuClass: String!
    @objc dynamic var title: String!
    @objc dynamic var url: String!

    private enum CodingKeys: String, CodingKey {
        
        case iconImage         = "icon_image"
        case id                = "id"
        case masterPage        = "master_page"
        case menuClass         = "menu_class"
        case title             = "title"
        case url               = "url"
    }
    func decode(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? Int()
        self.masterPage = try container.decodeIfPresent(Int.self, forKey: .masterPage) ?? Int()
        self.iconImage = try container.decodeIfPresent(String.self, forKey: .iconImage)  ?? String()
        //self.masterPage = try container.decodeIfPresent(String.self, forKey: .masterPage) ?? String()
        self.menuClass = try container.decodeIfPresent(String.self, forKey: .menuClass) ?? String()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
        self.url = try container.decodeIfPresent(String.self, forKey: .url) ?? String()
    }
    
    
    override static func primaryKey() -> String? { return "id" }
    
}
class NavigationDataSaveDate: Object, Codable {

    @objc dynamic var created = Date()
    @objc dynamic var data :NavigationData!
    @objc dynamic var id: Int = 1
    override static func primaryKey() -> String? { return "id" }
    
}
