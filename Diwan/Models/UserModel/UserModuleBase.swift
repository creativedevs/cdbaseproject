//
//	UserModuleBase.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class UserModuleBase : Decodable {

	let message : String?
	let response : String?
	let result : UserModuleResult?


	enum CodingKeys: String, CodingKey {
		case message = "Message"
		case response = "Response"
		case result = "Result"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		message = try values.decodeIfPresent(String.self, forKey: .message) ?? String()
		response = try values.decodeIfPresent(String.self, forKey: .response) ?? String()
		result = try values.decodeIfPresent(UserModuleResult.self, forKey: .result)  //?? UserModuleResult()
	}
}
