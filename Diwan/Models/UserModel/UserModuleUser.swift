//
//	UserModuleUser.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import Realm
import RealmSwift

class UserModuleUser : Object, Decodable {

	@objc dynamic var address : String?
	@objc dynamic var city : String?
	@objc dynamic var countryCode : String?
	@objc dynamic var currencyCode : String?
	@objc dynamic var email : String?
	@objc dynamic var id : Int = 0
	@objc dynamic var image : String?
	@objc dynamic var isSocialLogin : Bool = false
	@objc dynamic var languageCode : String?
	@objc dynamic var name : String?
	@objc dynamic var phone : String?
	@objc dynamic var postalCode : String?


	enum CodingKeys: String, CodingKey {
		case address = "Address"
		case city = "City"
		case countryCode = "CountryCode"
		case currencyCode = "CurrencyCode"
		case email = "Email"
		case id = "Id"
		case image = "Image"
		case isSocialLogin = "IsSocialLogin"
		case languageCode = "LanguageCode"
		case name = "Name"
		case phone = "Phone"
		case postalCode = "PostalCode"
	}
    
    override static func primaryKey() -> String? { return "id" }
    
    
	required convenience init(from decoder: Decoder) throws {
        self.init()
		let values = try decoder.container(keyedBy: CodingKeys.self)
		address = try values.decodeIfPresent(String.self, forKey: .address) ?? String()
		city = try values.decodeIfPresent(String.self, forKey: .city) ?? String()
		countryCode = try values.decodeIfPresent(String.self, forKey: .countryCode) ?? String()
		currencyCode = try values.decodeIfPresent(String.self, forKey: .currencyCode) ?? String()
		email = try values.decodeIfPresent(String.self, forKey: .email) ?? String()
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
		image = try values.decodeIfPresent(String.self, forKey: .image) ?? String()
		isSocialLogin = try values.decodeIfPresent(Bool.self, forKey: .isSocialLogin) ?? Bool()
		languageCode = try values.decodeIfPresent(String.self, forKey: .languageCode) ?? String()
		name = try values.decodeIfPresent(String.self, forKey: .name) ?? String()
		phone = try values.decodeIfPresent(String.self, forKey: .phone) ?? String()
		postalCode = try values.decodeIfPresent(String.self, forKey: .postalCode) ?? String()
	}
    

    required init() {
        super.init()
    }
    
//    required init(value: Any, schema: RLMSchema) {
//        super.init(value: value, schema: schema)
//    }
//
//    required init(realm: RLMRealm, schema: RLMObjectSchema) {
//        super.init(realm: realm, schema: schema)
 //   }
}
