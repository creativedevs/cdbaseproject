//
//	UserModuleResult.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class UserModuleResult : Decodable {

	let authToken : String?
	let user : UserModuleUser?


	enum CodingKeys: String, CodingKey {
		case authToken = "AuthToken"
		case user = "User"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		authToken = try values.decodeIfPresent(String.self, forKey: .authToken) ?? String()
		user = try values.decodeIfPresent(UserModuleUser.self, forKey: .user)  //?? UserModuleUser()
	}


}
