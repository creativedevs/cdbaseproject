//
//  MainResponse.swift
//  Diwan
//
//  Created by Muzamil Hassan on 09/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class MainResponse: Object, Codable {

    dynamic var statusCode: Int?
    @objc dynamic var errorInternal: String?
    @objc dynamic var messageToShow: String?
    

    private enum CodingKeys: String, CodingKey {
        case errorInternal = "error_internal"
        case messageToShow = "message_to_show"
        case statusCode = "status_code"
//        case responseData = "response_data"
    }
    func decode(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.errorInternal = try container.decode(String.self, forKey: .errorInternal)
        self.messageToShow = try container.decode(String.self, forKey: .messageToShow)
        self.statusCode = try container.decode(Int.self, forKey: .statusCode) 
//        self.responseData = try container.decode([MainList].self, forKey: .responseData)
    }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
}

