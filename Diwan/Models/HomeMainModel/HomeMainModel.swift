//
//  HomeMainModel.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/07/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
//class HomeMainModel: Object, Codable {
//
//   let productReviewObject = List<ProductReviewModel>()
//    
//    private enum CodingKeys: String, CodingKey {
//        
//        case videoId          = "video_id"
//        case thumbnailUrl     = "thumbnail_url"
//        case userId           = "user_id"
//        case videoUrl         = "video_url"
//        case viewCount        = "view_count"
//        case sequence         = "sequence"
//        case title            = "title"
//        case descriptionField = "description"
//    }
//    required convenience init(from decoder: Decoder) throws
//    {
//        self.init()
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        self.videoId = try container.decodeIfPresent(Int.self, forKey: .videoId) ?? Int()
//        self.userId = try container.decodeIfPresent(Int.self, forKey: .userId) ?? Int()
//        self.viewCount = try container.decodeIfPresent(Int.self, forKey: .viewCount) ?? Int()
//        self.thumbnailUrl = try container.decodeIfPresent(String.self, forKey: .thumbnailUrl) ?? String()
//        self.videoUrl = try container.decodeIfPresent(String.self, forKey: .videoUrl) ?? String()
//        self.sequence = try container.decodeIfPresent(Int.self, forKey: .sequence) ?? Int()
//        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
//        self.descriptionField = try container.decodeIfPresent(String.self, forKey: .descriptionField) ?? String()
//    }
//    
//    
//    override static func primaryKey() -> String? { return "videoId" }
//    
//
//}
