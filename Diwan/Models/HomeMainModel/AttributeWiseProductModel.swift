//
//  AttributeWiseProductModel.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/07/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class AttributeWiseProductModel: Object, Codable {

    //@objc dynamic var value: String!
    @objc dynamic var title: String!
    @objc dynamic var sequence: Int = 0
    let productlist = List<ProductModel>()
    @objc dynamic var attributePosition: AttributePositionModel!
    @objc dynamic var attributeId: Int = 0
    @objc dynamic var tagId: Int = 0
    private enum CodingKeys: String, CodingKey {
        
       // case value               = "value"
        case title               = "title"
        case sequence            = "sequence"
        case productlist         = "list_of_product"
        case attributePosition   = "attribute_position"
        case attributeId         = "attribute_id"
        case tagId               = "tag_id"
    }
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        //self.value = try container.decodeIfPresent(String.self, forKey: .value) ?? String()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
        self.sequence = try container.decodeIfPresent(Int.self, forKey: .sequence) ?? Int()
        let productObjectArray = try (container.decodeIfPresent([ProductModel].self, forKey: .productlist) ?? [])
        self.productlist.append(objectsIn: productObjectArray)
        self.attributePosition = try container.decodeIfPresent(AttributePositionModel.self, forKey: .attributePosition) ?? AttributePositionModel()
        self.attributeId = try container.decodeIfPresent(Int.self, forKey: .attributeId) ?? Int()
        self.tagId = try container.decodeIfPresent(Int.self, forKey: .tagId) ?? Int()
    }
    
    
    //override static func primaryKey() -> String? { return "attributeId" }
    

}
class AttributePositionModel: Object, Codable {

    @objc dynamic var title: String!
    @objc dynamic var attributePositionId: Int = 0
    
    private enum CodingKeys: String, CodingKey {
        
        case title               = "title"
        case attributePositionId         = "id"
        
    }
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.attributePositionId = try container.decodeIfPresent(Int.self, forKey: .attributePositionId) ?? Int()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
    }
    
    
    override static func primaryKey() -> String? { return "attributePositionId" }
    

}
