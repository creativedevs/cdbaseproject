//
//  CategoryModel.swift
//  Diwan
//
//  Created by Muzamil Hassan on 17/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import Realm
import RealmSwift



class ProductModel: Object, Codable {
    
    @objc dynamic var currencyObject: CurrencyModel!
    @objc dynamic var descriptionField: String!
    @objc dynamic var isFeatured: Bool = false
    @objc dynamic var isNew: Bool = false
    @objc dynamic var isFavorite: Bool = false
    @objc dynamic var isNotify: Bool = false
    @objc dynamic var markdownPrice: Int = 0
    @objc dynamic var price: Int = 0
    @objc dynamic var discountPercentage: Int = 0
    @objc dynamic var productId: Int = 0
    //let productMediaObject = List<ProductMediaModel>()
    @objc dynamic var productMediaObject: ProductMediaModel!
    let productReviewObject = List<ProductReviewModel>()
    let productUserReviewObject = List<ProductUserReviewModel>()
    let productAttributeObject = List<ProductAttributesModel>()
    @objc dynamic var quantity: Int = 0
    @objc dynamic var title: String!
    
    
    @objc dynamic var manufacturingDate: String!
    @objc dynamic var expiryDate: String!
    @objc dynamic var bookLanguage: String!
    @objc dynamic var itemDimensions: String!
    @objc dynamic var shippingWeight: String!
    @objc dynamic var isbn13: String!
    @objc dynamic var sku: String!
    @objc dynamic var publishedDate: String!
    @objc dynamic var isExclusiveOffer: Bool = false
    @objc dynamic var isOurPick: Bool = false
    @objc dynamic var isPriceMarkdown: Bool = false
    @objc dynamic var isAuthentic: Bool = false
    @objc dynamic var isEasyReturn: Bool = false
    @objc dynamic var is14DaysConsumerProtection: Bool = false
    @objc dynamic var lowStockQuantity: Int = 0
    
    @objc dynamic var productTypeObject: ProductTypeModel!
    @objc dynamic var publisherObject: PublisherModel!
    
    private enum CodingKeys: String, CodingKey {
        
        case publisherObject            = "publisher"
        case productTypeObject          = "product_type"
        case currencyObject             = "currency"
        case descriptionField           = "description"
        case isFeatured                 = "is_featured"
        case isNew                      = "is_new"
        case markdownPrice              = "markdown_price"
        case price                      = "price"
        case productId                  = "product_id"
        case productMediaObject         = "product_media"
        case productReviewObject        = "product_review_avg"
        case productAttributeObject        = "product_attributes"
        case productUserReviewObject        = "product_reviews"
        case quantity                   = "quantity"
        case title                      = "title"
        case discountPercentage         = "discount_percentage"
        case isFavorite                 = "is_favorite"
        case isNotify                 = "is_notify"
       
        case manufacturingDate                 = "manufacturing_date"
        case expiryDate                 = "expiry_date"
        case isExclusiveOffer                 = "is_exclusive_offer"
        case isOurPick                 = "is_our_pick"
        case isPriceMarkdown                 = "is_price_markdown"
        case bookLanguage                 = "book_language"
        case itemDimensions                 = "item_dimensions"
        case shippingWeight                 = "shipping_weight"
        case isbn13                 = "isbn_13"
        case sku                 = "sku"
        case isAuthentic                 = "is_authentic"
        case isEasyReturn                 = "is_easy_return"
        case is14DaysConsumerProtection                 = "is_14_days_consumer_protection"
        case lowStockQuantity                 = "low_stock_quantity"
        case publishedDate                 = "published_date"
        
    }
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.isbn13 = try container.decodeIfPresent(String.self, forKey: .isbn13) ?? String()
        self.bookLanguage = try container.decodeIfPresent(String.self, forKey: .bookLanguage) ?? String()
        self.publishedDate = try container.decodeIfPresent(String.self, forKey: .publishedDate) ?? String()
        self.itemDimensions = try container.decodeIfPresent(String.self, forKey: .itemDimensions) ?? String()
        self.shippingWeight = try container.decodeIfPresent(String.self, forKey: .shippingWeight) ?? String()
        self.sku = try container.decodeIfPresent(String.self, forKey: .sku) ?? String()
        self.publisherObject = try container.decodeIfPresent(PublisherModel.self, forKey: .publisherObject) ?? PublisherModel()
          self.currencyObject = try container.decodeIfPresent(CurrencyModel.self, forKey: .currencyObject) ?? CurrencyModel()
            self.productTypeObject = try container.decodeIfPresent(ProductTypeModel.self, forKey: .productTypeObject) ?? ProductTypeModel()
          self.descriptionField = try container.decodeIfPresent(String.self, forKey: .descriptionField) ?? String()
          self.isFeatured = try container.decodeIfPresent(Bool.self, forKey: .isFeatured) ?? Bool()
          self.isFavorite = try container.decodeIfPresent(Bool.self, forKey: .isFavorite) ?? Bool()
          self.isNotify = try container.decodeIfPresent(Bool.self, forKey: .isNotify) ?? Bool()
          self.isNew = try container.decodeIfPresent(Bool.self, forKey: .isNew) ?? Bool()
          self.markdownPrice = try container.decodeIfPresent(Int.self, forKey: .markdownPrice) ?? Int()
          self.price = try container.decodeIfPresent(Int.self, forKey: .price) ?? Int()
          self.productId = try container.decodeIfPresent(Int.self, forKey: .productId) ?? Int()
        
          //let productMediaObjectArray = try (container.decodeIfPresent([ProductMediaModel].self, forKey: .productMediaObject) ?? [])
          //self.productMediaObject.append(objectsIn: productMediaObjectArray)
        self.productMediaObject = try container.decodeIfPresent(ProductMediaModel.self, forKey: .productMediaObject) ?? ProductMediaModel()
        let productReviewObjectArray = try (container.decodeIfPresent([ProductReviewModel].self, forKey: .productReviewObject) ?? [])
          self.productReviewObject.append(objectsIn: productReviewObjectArray)
        
        let productUserReviewObjectArray = try (container.decodeIfPresent([ProductUserReviewModel].self, forKey: .productUserReviewObject) ?? [])
        self.productUserReviewObject.append(objectsIn: productUserReviewObjectArray)
        
        
          self.quantity = try container.decodeIfPresent(Int.self, forKey: .quantity) ?? Int()
          self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
         self.discountPercentage = try container.decodeIfPresent(Int.self, forKey: .discountPercentage) ?? Int()
        self.isAuthentic = try container.decodeIfPresent(Bool.self, forKey: .isAuthentic) ?? Bool()
        self.isEasyReturn = try container.decodeIfPresent(Bool.self, forKey: .isEasyReturn) ?? Bool()
        self.is14DaysConsumerProtection = try container.decodeIfPresent(Bool.self, forKey: .is14DaysConsumerProtection) ?? Bool()
        
        let productAttributeObjectArray = try (container.decodeIfPresent([ProductAttributesModel].self, forKey: .productAttributeObject) ?? [])
        self.productAttributeObject.append(objectsIn: productAttributeObjectArray)
        
        
        
        
    }
    override static func primaryKey() -> String? { return "productId" }
    

}
class CurrencyModel: Object, Codable {

    @objc dynamic var id: Int = 0
    @objc dynamic var imageUrl: String!
    @objc dynamic var isoCode: String!
    @objc dynamic var title: String!
    
    private enum CodingKeys: String, CodingKey {
        
        case id             = "id"
        case imageUrl       = "image_url"
        case isoCode        = "iso_code"
        case title          = "title"
        
    }
     required convenience init(from decoder: Decoder) throws {
    //func decode(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? Int()
        self.imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl) ?? String()
        self.isoCode = try container.decodeIfPresent(String.self, forKey: .isoCode) ?? String()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
    }
    
    
    override static func primaryKey() -> String? { return "id" }
}
class ProductTypeModel: Object, Codable {

    @objc dynamic var id: Int = 0
    @objc dynamic var title: String!
    
    private enum CodingKeys: String, CodingKey {
        
        case id             = "id"
        case title          = "title"
        
    }
     required convenience init(from decoder: Decoder) throws {
    //func decode(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? Int()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
    }
    
    
    override static func primaryKey() -> String? { return "id" }
}
class PublisherModel: Object, Codable {

    @objc dynamic var id: Int = 0
    @objc dynamic var title: String!
    
    private enum CodingKeys: String, CodingKey {
        
        case id             = "id"
        case title          = "title"
        
    }
     required convenience init(from decoder: Decoder) throws {
    //func decode(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? Int()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
    }
    
    
    override static func primaryKey() -> String? { return "id" }
}
class ProductMediaModel: Object, Codable {

    let bannerImages = List<ProductMediaDetailModel>()
    let images = List<ProductMediaDetailModel>()
    let videos = List<EhsanModel>()
//    @objc dynamic var id: Int = 0
//    @objc dynamic var mediaUrl: String!
//    @objc dynamic var thumbnailUrl: String!

    private enum CodingKeys: String, CodingKey {

        case bannerImages       = "banner_images"
        case images             = "images"
        case videos             = "videos"
//        case id             = "id"
//        case mediaUrl       = "media_url"
//        case thumbnailUrl   = "thumbnail_url"

    }
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let bannerImagesArray = try (container.decodeIfPresent([ProductMediaDetailModel].self, forKey: .bannerImages) ?? [])
        self.bannerImages.append(objectsIn: bannerImagesArray)
        let imagesArray = try (container.decodeIfPresent([ProductMediaDetailModel].self, forKey: .images) ?? [])
        self.images.append(objectsIn: imagesArray)
        let videosArray = try (container.decodeIfPresent([EhsanModel].self, forKey: .videos) ?? [])
        self.videos.append(objectsIn: videosArray)
    }
    
    
    //override static func primaryKey() -> String? { return "id" }
}
class ProductMediaDetailModel: Object, Codable {

    @objc dynamic var id: Int = 0
    @objc dynamic var mediaUrl: String!
    @objc dynamic var thumbnailUrl: String!
    @objc dynamic var entityId: Int = 0
    @objc dynamic var bannerTypeId: Int = 0
    @objc dynamic var mediaTypeId: Int = 0
    @objc dynamic var userId: Int = 0
    
    private enum CodingKeys: String, CodingKey {
        case id             = "id"
        case mediaUrl       = "media_url"
        case thumbnailUrl   = "thumbnail_url"
        case entityId       = "entity_id"
        case bannerTypeId   = "banner_type_id"
        case mediaTypeId    = "media_type_id"
        case userId        = "user_id"
    }
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? Int()
        self.entityId = try container.decodeIfPresent(Int.self, forKey: .entityId) ?? Int()
        self.bannerTypeId = try container.decodeIfPresent(Int.self, forKey: .bannerTypeId) ?? Int()
        self.mediaUrl = try container.decodeIfPresent(String.self, forKey: .mediaUrl) ?? String()
        self.thumbnailUrl = try container.decodeIfPresent(String.self, forKey: .thumbnailUrl) ?? String()
        self.mediaTypeId = try container.decodeIfPresent(Int.self, forKey: .mediaTypeId) ?? Int()
        self.userId = try container.decodeIfPresent(Int.self, forKey: .userId) ?? Int()
    }
    
    
    override static func primaryKey() -> String? { return "id" }
}
class ProductReviewModel: Object, Codable {

    @objc dynamic var id: Int = 0
    @objc dynamic var reviewCount: Int = 0
    @objc dynamic var avgStarRating: Int = 0

    private enum CodingKeys: String, CodingKey {

        case id             = "id"
        case reviewCount       = "review_count"
        case avgStarRating   = "avg_star_rating"

    }
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? Int()
        self.reviewCount = try container.decodeIfPresent(Int.self, forKey: .reviewCount) ?? Int()
        self.avgStarRating = try container.decodeIfPresent(Int.self, forKey: .avgStarRating) ?? Int()
    }


    override static func primaryKey() -> String? { return "id" }
}
class ProductUserReviewModel: Object, Codable {

    @objc dynamic var id: Int = 0
    @objc dynamic var userId: Int = 0
    @objc dynamic var userName: String!
    @objc dynamic var title: String!
    @objc dynamic var descriptionField: String!
    @objc dynamic var starRating: Int = 0
    @objc dynamic var reviewDate: String!
    
    private enum CodingKeys: String, CodingKey {

        case id               = "id"
        case userId           = "user_id"
        case userName         = "user_name"
        case title            = "title"
        case descriptionField = "description"
        case starRating       = "star_rating"
        case reviewDate       = "review_date"
        
    }
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? Int()
        self.userId = try container.decodeIfPresent(Int.self, forKey: .userId) ?? Int()
        self.starRating = try container.decodeIfPresent(Int.self, forKey: .starRating) ?? Int()
        self.userName = try container.decodeIfPresent(String.self, forKey: .userName) ?? String()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
        self.descriptionField = try container.decodeIfPresent(String.self, forKey: .descriptionField) ?? String()
        self.reviewDate = try container.decodeIfPresent(String.self, forKey: .reviewDate) ?? String()
    }


    override static func primaryKey() -> String? { return "id" }
}
class ProductAttributesModel: Object, Codable {

    @objc dynamic var attributeId: Int = 0
    @objc dynamic var title: String!
    @objc dynamic var sequence: Int = 0
    @objc dynamic var attributePosition: AttributePositionModel!
    
    private enum CodingKeys: String, CodingKey {

        case attributeId               = "attribute_id"
        case title            = "title"
        case sequence           = "sequence"
        case attributePosition         = "attribute_position"
        
        
    }
    required convenience init(from decoder: Decoder) throws
    {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.attributeId = try container.decodeIfPresent(Int.self, forKey: .attributeId) ?? Int()
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? String()
        self.attributePosition = try container.decodeIfPresent(AttributePositionModel.self, forKey: .attributePosition) ?? AttributePositionModel()
        self.sequence = try container.decodeIfPresent(Int.self, forKey: .sequence) ?? Int()
        
    }


    override static func primaryKey() -> String? { return "attributeId" }
}



/**/
