//
//  Router.swift
//  ___PROJECTNAME___
//
//  Created ___FULLUSERNAME___ on ___DATE___.
//  Copyright © ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//


import UIKit


class Router  {
   typealias viewController  =  StoryBoardHandler & UIViewController
    
    func splashToLoginAsRoot(vc:viewController){
        //vc.navigationController?.setViewControllers([LoginViewController.loadVC()], animated: true)
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Storyboards.userModule.rawValue, bundle: nil)
        
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginNavigation") as! BaseNavigationController
        UIApplication.shared.keyWindow?.rootViewController = viewController
        UIView.transition(with: (UIApplication.shared.keyWindow)!, duration: 0.5, options: .transitionCrossDissolve, animations: nil, completion: nil)
        
    }
    
    func splashToLoginAsRoot(){
        //vc.navigationController?.setViewControllers([LoginViewController.loadVC()], animated: true)
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Storyboards.userModule.rawValue, bundle: nil)
        
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginNavigation") as! BaseNavigationController
        UIApplication.shared.keyWindow?.rootViewController = viewController
        //UIView.transition(with: (UIApplication.shared.keyWindow)!, duration: 0.5, options: .transitionCrossDissolve, animations: nil, completion: nil)
    }
    func goToLoginAsRoot() {

        CurrentUser.data = nil
        CurrentUser.userType = .registered
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Storyboards.userModule.rawValue, bundle: nil)
        let navigationVC = mainStoryboard.instantiateViewController(withIdentifier: "LoginNavigation") as! BaseNavigationController
        navigationVC.setViewControllers([LoginViewController.loadVC()], animated: true)
        UIApplication.shared.keyWindow?.rootViewController = navigationVC
        UIView.transition(with: (UIApplication.shared.keyWindow)!, duration: 0.5, options: .transitionCrossDissolve, animations: nil, completion: nil)
        //transitionFlipFromRight
    }
    
    func goToHomeAsRoot(from vc : viewController) {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Storyboards.sideMenu.rawValue, bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuController") as! SideMenuController
        viewController.rootViewController = TabBarProvider.tabbarWithNavigationStyle(index: 0)
        UIApplication.shared.keyWindow?.rootViewController = viewController
        UIView.transition(with: (UIApplication.shared.keyWindow)!, duration: 0.5, options: .transitionCrossDissolve, animations: nil, completion: nil)
        //transitionFlipFromRight
    }
    func goToHomeAsRoot() {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Storyboards.sideMenu.rawValue, bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuController") as! SideMenuController
        viewController.rootViewController = TabBarProvider.tabbarWithNavigationStyle(index: 0)
        UIApplication.shared.keyWindow?.rootViewController = viewController
        UIView.transition(with: (UIApplication.shared.keyWindow)!, duration: 0.5, options: .transitionCrossDissolve, animations: nil, completion: nil)
        
    }
    
    
/*
 
    
    func goToLoginAsRoot()
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Storyboards.userModule.rawValue, bundle: nil)
        let navigationVC = mainStoryboard.instantiateViewController(withIdentifier: "LoginNavigation") as! BaseNavigationController
        navigationVC.setViewControllers([SignInController.loadVC()], animated: false)
        UIApplication.shared.keyWindow?.rootViewController = navigationVC
        UIView.transition(with: (UIApplication.shared.keyWindow)!, duration: 0.5, options: .transitionFlipFromRight, animations: nil, completion: nil)
        
    }
    
 
    func goToLoginScreen(from vc : viewController) {
        
        vc.navigationController?.setViewControllers([SignInController.loadVC()], animated: true)
    }

    func goToVerifyCodeVC(from vc : viewController, fromSighUp : Bool) {
        vc.show(VerifyCodeController.loadVC()) { (vc : VerifyCodeController) in
            vc.fromSignUp = fromSighUp
        }
    }


    */
   
}
