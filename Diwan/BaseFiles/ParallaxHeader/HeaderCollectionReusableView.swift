//
//  HeaderCollectionReusableView.swift
//  Test
//
//  Created by Waqas Ali on 6/21/18.
//  Copyright © 2018 Waqas ali. All rights reserved.
//

import UIKit
import ImageSlideshow

class HeaderCollectionReusableView: UICollectionReusableView {
//PharmacySubCategoryController
    var homeController:BaseViewController?
    
    @IBOutlet weak var viewParallax: ParallaxHeaderView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapImgSlider))
        viewParallax.addGestureRecognizer(gestureRecognizer)
//        viewParallax.homeTopView.setImageInputs([ImageSource(image: UIImage(named: "banner")!)])
        //([AlamofireSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080")!, AlamofireSource(urlString: "https://images.unsplash.com/photo-1447746249824-4be4e1b76d66?w=1080")!])
    }
    
    @objc func didTapImgSlider() {
        viewParallax.homeTopView.presentFullScreenController(from: homeController!)
    }
    
}
