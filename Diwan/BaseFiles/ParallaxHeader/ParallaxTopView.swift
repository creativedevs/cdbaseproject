//
//  ParallaxTopView.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/1/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
import ImageSlideshow
final class ParallaxTopView: UIView {
    
    fileprivate var heightLayoutConstraint = NSLayoutConstraint()
    fileprivate var bottomLayoutConstraint = NSLayoutConstraint()
    fileprivate var containerView = UIView()
    fileprivate var containerLayoutConstraint = NSLayoutConstraint()
    var homeTopView = ImageSlideshow()  // UIImageView! //
    var isAnimating = false
    // let alamofireSource = [AlamofireSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080")!, AlamofireSource(urlString: "https://images.unsplash.com/photo-1447746249824-4be4e1b76d66?w=1080")!, AlamofireSource(urlString: "https://images.unsplash.com/photo-1463595373836-6e0b0a8ee322?w=1080")!]
    
    //let localSource = [ImageSource(imageString: "homeimage1")!, ImageSource(imageString: "e1")!, ImageSource(imageString: "e3")!]
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit() 
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
      commonInit()
    }
    
    func commonInit() {
        
     
        self.backgroundColor = .clear
       // self.translatesAutoresizingMaskIntoConstraints = false
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.backgroundColor = UIColor.clear
        
        self.addSubview(containerView)
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[containerView]|",
                                                           options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                           metrics: nil,
                                                           views: ["containerView" : containerView]))
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[containerView]|",
                                                           options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                           metrics: nil,
                                                           views: ["containerView" : containerView]))
        
        containerLayoutConstraint = NSLayoutConstraint(item: containerView,
                                                       attribute: .height,
                                                       relatedBy: .equal,
                                                       toItem: self,
                                                       attribute: .height,
                                                       multiplier: 1.0,
                                                       constant: 0.0)
        self.addConstraint(containerLayoutConstraint)
        
        
        
        
        homeTopView.contentMode = .scaleAspectFill
        homeTopView.backgroundColor = UIColor.clear
        homeTopView.clipsToBounds = false
        homeTopView.translatesAutoresizingMaskIntoConstraints = false
        homeTopView.slideshowInterval = 5.0
        homeTopView.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
        homeTopView.contentScaleMode = .scaleAspectFill
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = ColorManager.color(forKey: "red")
        pageControl.pageIndicatorTintColor = UIColor.white
        homeTopView.pageIndicator = pageControl
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        homeTopView.activityIndicator = DefaultActivityIndicator()
        homeTopView.currentPageChanged = { page in
            print("current page:", page)
        }
        
        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        //homeTopView.setImageInputs(localSource)
        containerView.addSubview(homeTopView)
        
        
        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[homeTopView]|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: ["homeTopView" : homeTopView]))
        
        bottomLayoutConstraint = NSLayoutConstraint(item: homeTopView,
                                                    attribute: .bottom,
                                                    relatedBy: .equal,
                                                    toItem: containerView,
                                                    attribute: .bottom,
                                                    multiplier: 1.0,
                                                    constant: 0.0)
        
        containerView.addConstraint(bottomLayoutConstraint)
        
        heightLayoutConstraint = NSLayoutConstraint(item: homeTopView,
                                                    attribute: .height,
                                                    relatedBy: .equal,
                                                    toItem: containerView,
                                                    attribute: .height,
                                                    multiplier: 1.0,
                                                    constant: 0.0)
        
        containerView.addConstraint(heightLayoutConstraint)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        containerLayoutConstraint.constant = scrollView.contentInset.top;
        let offsetY = -(scrollView.contentOffset.y + scrollView.contentInset.top);
        containerView.clipsToBounds = offsetY <= 0
        bottomLayoutConstraint.constant = offsetY >= 0 ? 0 : -offsetY / 2
        heightLayoutConstraint.constant = max(offsetY + scrollView.contentInset.top, scrollView.contentInset.top)
  
    }
}

