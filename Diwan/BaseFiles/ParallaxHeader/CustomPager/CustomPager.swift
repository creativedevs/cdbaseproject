//
//  OptionsView.swift
//  ASI
//
//  Created by Waqas Ali on 28/08/2019.
//  Copyright © 2019 Shujaat Khan. All rights reserved.
//

import UIKit

class CustomPager: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    var currentPage = 0 {
        didSet{
            updatePage()
        }
    }
    var totalPages = 0 {
        didSet{
            addViews()
        }
    }
    var dataArray : [String] = [] {
        didSet{
          addViews()
        }
    }
    
  var didTapOnOption : (((Int) -> Void))?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commomInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
        commomInit()
    }
    
    
    
    func commomInit(){
        
        Bundle.main.loadNibNamed("CustomPager", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth , .flexibleHeight]
    
    }
    
    func addViews(){
        let width = self.frame.size.width / CGFloat(totalPages)
        for page in 0 ..< totalPages {
            if(page < stackView.subviews.count) {
                let answerView = stackView.subviews[page] as! PagerIndicatorView
                answerView.updateShape(index: page, isSelected: false)
            }
            else {
                
                let answerView = PagerIndicatorView.init(frame:CGRect(x: 0, y: 0, width: width, height: self.frame.size.height))
                answerView.backgroundColor = UIColor.clear
                if (page == 0) {
                    answerView.createShape(index: page, isSelected: true)
                    
                }
                else {
                    answerView.createShape(index: page, isSelected: false)
                }
                stackView.addArrangedSubview(answerView)
            }
            
        }
    }
    
    func updatePage() {
        for page in 0 ..< totalPages {
            let answerView = stackView.subviews[page] as! PagerIndicatorView
            if (page == currentPage) {
                answerView.updateShape(index: page, isSelected: true)
                //answerView.draw(answerView.frame, isSelected: true)
                
            }
            else {
                answerView.updateShape(index: page, isSelected: false)
                //answerView.draw(answerView.frame, isSelected: false)
            }
            
        }
    }
    
    @objc func buttonAction(sender: UIButton!) {

//      for subViews in stackView.subviews{
//            if let view = subViews as? AnswerView{
//                 view.imgTick.isHidden = true
//            }
//        }
//
//        if let selectedView = stackView.subviews[sender.tag] as? AnswerView{
//        selectedView.imgTick.isHidden = false
//        }
//          didTapOnOption?(sender.tag)
    }
    
    
}


class PagerIndicatorView : UIView {
    var isSelected = false
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = .clear
    }
    func createShape(index  : Int, isSelected : Bool) {
        let size = self.bounds.size
        let spacing = DesignUtility.getValueFromRatio(5)
        let p1 = CGPoint(x:spacing, y:0)
        let p2 = CGPoint(x:size.width, y:p1.y)
        let p3 = CGPoint(x:size.width - spacing, y:size.height)
        let p4 = CGPoint(x:0, y:size.height)
        let path = UIBezierPath()
        path.move(to: p1)
        path.addLine(to: p2)
        path.addLine(to: p3)
        path.addLine(to: p4)
        //        path.addLine(to: p5)
        path.close()
        let layer = CAShapeLayer()
        layer.path = path.cgPath
        //CFCED3  gray
        //C52030 red
        
        if(isSelected == true) {
            layer.strokeColor = ColorManager.color(forKey: "themeBlue")!.cgColor
            layer.fillColor = ColorManager.color(forKey: "themeBlue")!.cgColor
        }
        else {
            layer.strokeColor = UIColor.init(hexString: "FFFFFF").cgColor
            layer.fillColor = UIColor.init(hexString: "FFFFFF").cgColor
        }
        layer.name = "\(index)"
        
        self.layer.addSublayer(layer)
    }
    func updateShape(index  : Int, isSelected : Bool) {
        //var layer = self.layer.sublayers
        let selectedLayer = (self.layer.sublayers!.filter { layer in return layer.name == "\(index)" })[0] as! CAShapeLayer
        if(isSelected == true) {
            selectedLayer.strokeColor = ColorManager.color(forKey: "themeBlue")!.cgColor
            selectedLayer.fillColor = ColorManager.color(forKey: "themeBlue")!.cgColor
        }
        else {
            selectedLayer.strokeColor = UIColor.init(hexString: "FFFFFF").cgColor
            selectedLayer.fillColor = UIColor.init(hexString: "FFFFFF").cgColor
        }
        
        
        
    }
}
