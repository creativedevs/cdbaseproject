//
//  FormErrors.swift


import UIKit


enum FormError: Error {
    case empty(name : String)
    case onlyAlphabets(name : String )
    case minimumLength(name : String, len : Int)
    case maximumLength(name : String, len : Int)
    case validEmail
    case onlyNumbers
    case shouldAlphaNumeric(name : String)
    case isContainsWhitespace(name : String)
    case unknown
}


extension FormError: CustomStringConvertible {
    
    var description: String {
        switch self {
        case .empty(let name): return "Kindly provide".localized + " \(name)"
        case .onlyAlphabets(let name): return "\(name) " + "can only contain alphabets".localized
        case .minimumLength(let name, let val): return "\(name) " + "minimum length should be".localized + " \(val)"
        case .maximumLength(let name, let val): return "\(name) " + "maximum length should be" + " \(val)"
        case .validEmail: return "Kindly provide valid email address ".localized
        case .onlyNumbers: return "Kindly provide valid mobile number ".localized
        case .shouldAlphaNumeric(let name) : return "\(name) " + "should contains at least one number and at least one alphabet character".localized
        case .isContainsWhitespace(let name): return "\(name) " + "should not contain white spaces ".localized
        case .unknown : return "Unknown validation".localized
        }
    }
}



