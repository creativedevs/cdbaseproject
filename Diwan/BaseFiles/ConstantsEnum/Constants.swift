//
//  Constants.swift
//  Test
//
//  Created Waqas Ali on 6/12/18.
//  Copyright © 2018 Waqas ali. All rights reserved.
//

import UIKit
import RealmSwift
//BASE URL : "http://diwanapi.ingicweb.com/api/"
//STAGGING SWAGGER URL : "http://diwandevapi.ingicweb.com/swagger/index.html"
enum Constants {
    static let minimumLengthName = 3
    static let maximumLengthName = 30
    static let maximumLengthNumber = 20
    static let minimumLengthNumber = 8
    static let maximumLengthText = 50
    static let minimumLengthPwd = 6
    static let maximumLengthPwd = 20
    static let maximumLengthTextView = 500
    static let networkSessionToken  = ""
    static let selectedColor = UIColor(red: 198/255, green: 0/255, blue: 37/255, alpha: 1)
    
    static let realm = try! Realm()
    
    static let kWINDOW_FRAME                = UIScreen.main.bounds
    static let kSCREEN_SIZE                 = UIScreen.main.bounds.size
    static let kWINDOW_WIDTH                = UIScreen.main.bounds.size.width
    static let kWINDOW_HIEGHT               = UIScreen.main.bounds.size.height
    
    static let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
    static let UIWINDOW                    = UIApplication.shared.delegate!.window!
}



enum FP {
    static var emailUser = ""
    static var loginGuest  = false
}

enum CurrentUser{
    static var platformId : Int = 1 // ios,android,backend
    static var data : User? = nil
    static var token : String = ""
    static var userType : UserType = .registered
    static var userLoginType : UserLoginType = .user
    static var languageId : UserLanguage = .english
    static var selectedAddressID : Int = 0
    static var deviceToken : String = "123456"
   //static var deviceType = 2
   static var headers: [String : String] {
       if let userData = CurrentUser.data {
        return ["user_type_id" :String(CurrentUser.userLoginType.rawValue),"language_Id":String(CurrentUser.languageId.rawValue),"Authorization" : "bearer \(userData.authToken!)"]
       }
       else {
        return ["user_type_id" :String(CurrentUser.userLoginType.rawValue),"language_Id":String(CurrentUser.languageId.rawValue)]
       }
    }
}

enum Login {
    static let isLoggedIn : String = "isLoggedIn"
    static let userData : String = "userData"
    static let token : String = "token"
}


enum CartData {
    static var cartDict : [Int : (quantity : Int, amount: Float , item : String , section :Int)] = [:]
    static var totalAmount : Float  = 0
    static var totalQuantity : Int = 0
    static var params : [String : AnyObject] = [:]
    static var orderDetail : [[String : AnyObject]] = [[:]]
    static var instanceSurCharge : Float = 0.0
    static var amtFinal : Float = 0.0
    static var SD : SheduleData =  SheduleData()
}

enum GoogleMap{
    static let key : String = "AIzaSyBbm5LaMZF6g7RcATzFsVyz9BuaPrrefxM" // "AIzaSyD7sU52onvkEkZd0NwlFXGbOQ-C791LVh4"
    
}

enum twitter {
    static let key : String = "Y4C1ynYlXMdgf7xjinEj41ecU"
    static let secretKey : String  = "cq4iBrgNTWbDNKd9ETwuJraRydqB8auGny6z8QlMVHRfFCklwI"
}

enum ServiceCodes  {
    static let successCode : Int = 200
    static let authExpireCode : Int = 401
    static let userBlockCode : Int = 402
    static let errorCode : Int = 500
}

enum UserDefaultKey {
    static let isConfigurationSaved = "isConfigurationSaved"
    static let selectedAddress = "selectedAddress"
    static let deviceToken = "deviceToken"
    static let cardData = "cardData"
    static let launchedBefore = "launchedBefore"
    static let userLoginType = "userLoginType"
    static let isSocialLogin = "isSocialLogin"
}

enum UserType{
    case registered
    case guest
    
}

enum UserLoginType : Int{
    case user = 1
    case celebrity = 2
    case guest = 3
    
}
enum UserLanguage : Int{
        case english = 1
        case arabic = 2
}
enum UserGender : Int{
        case male = 1
        case female = 2
}
enum UserSocialLoginType : Int{
        case facebook = 1
        case twitter = 2
        case google = 3
        case apple = 4
}
enum OrderData{
    static var orderNotification : (type: String, id :Int) = ("", 0)
}

struct SheduleData {
    var isInstance  = false
    var selectedTimes : (String? , String?) = (nil, nil)
    var selectedDates : (Date? , Date?) = (nil, nil)
    var selectedAddress : (String? , String?) = (nil, nil)
}



enum CodeType {
    case signup , forgetPwd
}


extension Notification.Name {
    static let setHomeOnLeftMenu = Notification.Name(rawValue: "setHomeOnLeftMenu")
    static let updateVideoScreen = Notification.Name(rawValue: "updateVideoScreen")
}

enum CMSType : Int {
    case terms , privacy, about, returnExchange, none
}
enum DateFormatType {
    static let defaultDisplayFormat : String = "dd, MMM yyyy"
    static let reviewDisplayFormat : String = "MMMM dd, yyyy"
    static let defaultDate : String = "yyyy-MM-dd"
    static let serverFormatMilliSecond : String = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    static let serverFormatSecond : String = "yyyy-MM-dd'T'HH:mm:ss"
}
enum SingleTextType {
    case pharmacy
    case library
}
enum LibraryListType {
    case genre
    case section
}

enum ListViewType {
    case address
    case store
    case brand
    case wishlist
    case pharmacy
    case author
    case library
    case normal
    case celebrity
    case cart
}
enum ContactType : Int {
    case contactUs = 1
    case support
}
enum VideoType {
    case ehsanProject
    case myTV
    case diwanTV
}
enum WarrantyType {
    case authenticate
    case easyReturn
    case protection
}
enum MainProductType : Int {
    case book = 1
    case item = 2
}
enum BannerPositionType : Int {
    case top = 1
    case middle1 = 2
}

enum AttributesIdType : Int {
    case new = 1
    case discount
    case bestSellers
    case featured
    case specialOffers
    case exclusive
    case ourPicks
    case authentic
    case easyReturns
    case protection
}
