//
//  Storyboards.swift
//  Test
//
//  Created Waqas Ali on 6/12/18.
//  Copyright © 2018 Waqas ali. All rights reserved.
//

import Foundation
import UIKit

public enum Storyboards : String {
    
    // As enum is extends from String then its case name is also its value
    case main = "Main"
    case userModule = "UserModule"
    case home = "HomeModule"
    case setting = "Setting"
    case sideMenu = "SideMenu"
    case dashboard = "Dashboard"
    case store = "Store"
    case pharmacy = "Pharmacy"
    case library = "Library"
    case diwanMain = "DiwanMainModule"
    case celebrities = "Celebrities"
    case author = "Author"
    case brands = "Brands"
    case TV = "TVModule"
    case ehsanProject = "Ehsan"
    case chat = "Chat"
    case myDiwan = "MyDiwan"
    case orderManagement = "OrderManagement"
}

enum Navigation {
    static var currentNavigation  : UINavigationController?  = nil
}


