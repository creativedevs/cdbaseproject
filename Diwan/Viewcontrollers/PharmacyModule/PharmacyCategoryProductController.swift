//
//  PharmacyCategoryProductController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 25/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import ImageSlideshow
import Kingfisher
class PharmacyCategoryProductController:  BaseViewController , StoryBoardHandler {

   static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.pharmacy.rawValue , nil)

    @IBOutlet weak var tableView: BaseUITableView!
    var selectedMainCategory : CategoryModel?
    var categoryList = [CategoryWiseProductModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTable()
        //categoryListTbl.isHidden = true
        self.navigationItem.title = selectedMainCategory?.title
        createBackBtn()
        //createRefreshView(tblView: categoryListTbl)/
        self.getProductsByCategoryApi()
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarheart")!, .BarButtonItemPositionLeft, self, #selector(showWishListScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarsearch")!, .BarButtonItemPositionRight, self, #selector(showSearchScreen))
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupHeaderData()
    }
    func setUpTable(){
        let parallaxViewFrame = CGRect(x: 0, y: 0, width: self.tableView.bounds.width, height: DesignUtility.getValueFromRatio(265))
        tableView.tableHeaderView  = ParallaxHeaderView(frame: parallaxViewFrame)
        tableView.registerCell(PharmacyCategoryProductCell.self)
        tableView.registerCell(SingleTextCell.self)
    }
    func setupHeaderData() {
        var source = [InputSource]()
        if let url = selectedMainCategory?.imageIcon.getValidUrlString() {
            source.append(KingfisherSource(urlString: url)!)
        }
        let headerView = self.tableView.tableHeaderView as! ParallaxHeaderView
        headerView.setSliderImages(images: source)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PharmacyCategoryProductController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {return 1}
        return self.categoryList.count
//        if self.categoryList.count == 0 {
//            UtilityEmptyListView.emptyTableViewMessage(message: "test", image: nil, tableView: tableView, listType: .store)
//            if let emptyView = tableView.backgroundView as? EmptyTableViewBackgroundView {
//                emptyView.actionBtn.didTapOnBounceEnd = {
//                    //self.show(AddAddressController.loadVC())
//                }
//            }
//            return 0
//        }
//        UtilityEmptyListView.hideEmptyTableViewMessage(tableView: tableView)
//        return self.categoryList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: SingleTextCell.identifier()) as! SingleTextCell
            
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyCategoryProductCell.identifier()) as! PharmacyCategoryProductCell
        let data = self.categoryList[indexPath.row]
        print(data)
        cell.setData(data: data)
        cell.viewAllBtn.didTapOnBounceEnd = {
            
            let category = CategoryModel()
            category.categoryId = data.categoryId
            category.title = data.title
            let controller = StoreProductListController.loadVC()
            controller.selectedMainCategory = category
            controller.filledData = ( category.title, .pharmacy, .bookPharmacy)
            self.show(controller)
           // self.addToCartOrNotifyMe(data: data,cell: cell,indexPath: indexPath)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section != 0 {
            guard let tableViewCell = cell as? PharmacyCategoryProductCell else { return }

            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }
        
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section != 0 {
            guard let tableViewCell = cell as? PharmacyCategoryProductCell else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }
        
        
    }
}

//MARK:- Collection Datasource and Delegate
extension PharmacyCategoryProductController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let headerView = self.tableView.tableHeaderView as! ParallaxHeaderView
        headerView.scrollViewDidScroll(scrollView: scrollView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categoryList[collectionView.tag].productlist.count
        //return model[collectionView.tag].count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoreProductCollectionCell.identifier(), for: indexPath) as! StoreProductCollectionCell
        let data = self.categoryList[collectionView.tag].productlist[indexPath.item]
        //var data = ProductModel()
        cell.setData(data: data,cellPosition: ProductCellPosition.normal)
        cell.addToCartBtn.didTapOnBounceEnd = {
           // self.addToCartOrNotifyMe(data: data,cell: cell,indexPath: indexPath)
        }
        cell.addOrRemoveFavoriteBtn.didTapOnBounceEnd = {
            self.addOrRemoveFavorite(data: data, indexPath: indexPath, collectionView: collectionView)
            
            //self.addOrRemoveFavorite(data: data,cell: cell,indexPath: indexPath)
        }

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           //
           return CGSize(width: DesignUtility.getValueFromRatio(162), height: DesignUtility.getValueFromRatio(298))
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
           return UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 0)
       }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        let data = self.categoryList[collectionView.tag].productlist[indexPath.item]
        let controller = ProductDetailController.loadVC()
        controller.selectedProduct = data
        self.show(controller)
    }
}
//MARK:-API's
extension PharmacyCategoryProductController {
    
    func getProductsByCategoryApi() {
        
        let param:[String:Any] = ["page_number": currentPage,
                                  "page_size": 100000,
                                  "parent_category_id" : selectedMainCategory!.categoryId,
                                  "number_of_products" : 10]
        print(param)
        StoreManager().getCategoryWiseroductListApi(endPoint: ApiEndPoints.getSubjectWiseProductsByCategory, param: param, header: [:], completion: { (response,msg,errorMsg) in
            //self.storeCollectionView.isHidden = false
            print(response)
           // self.refreshView.endRefreshing()
            if let data = response {
                self.categoryList.append(contentsOf: data)
                self.categoryList = self.categoryList.filter({$0.totalCount > 0})
                //self.categoryList.append(contentsOf: data)
                self.tableView.reloadData()
            }

        }){ (error) in
            self.refreshView.endRefreshing()
                print("Error: ", error as Any)
            }
        
    }
    
}
