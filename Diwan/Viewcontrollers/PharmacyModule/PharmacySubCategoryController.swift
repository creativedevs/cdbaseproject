//
//  StoreCategoryListController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import BottomPopup
import FittedSheets
import ImageSlideshow
import Kingfisher
class PharmacySubCategoryController: BaseViewController , StoryBoardHandler {

    var headerView:HeaderCollectionReusableView!
    
    @IBOutlet weak var filterBtn: BounceButton!
    @IBOutlet weak var sortBtn: BounceButton!
    @IBOutlet weak var storeCollectionView: BaseUICollectionView!
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.pharmacy.rawValue , nil)
    var categoryList = [CategoryModel]()
    var selectedMainCategory : CategoryModel?
    var selectedSort : SortModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = selectedMainCategory?.title.localized
        createBackBtn()
        self.storeCollectionView.isHidden = true
        //createRefreshView(tblView: categoryListTbl)
        setUpCollection()
        self.getPharmacyListApi()
        self.addBarButtonItemWithImage(UIImage(named: "topbarheart")!, .BarButtonItemPositionLeft, self, #selector(showWishListScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarsearch")!, .BarButtonItemPositionRight, self, #selector(showSearchScreen))
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    func setupHeaderData() {
        
        if categoryList.count == 0 {
            return
        }
        storeCollectionView.reloadData()
        storeCollectionView.layoutIfNeeded()
        var source = [InputSource]()
         if let url = selectedMainCategory?.imageIcon.getValidUrlString() {
             source.append(KingfisherSource(urlString: url)!)
//            source.append(KingfisherSource(urlString: url)!)
//            source.append(KingfisherSource(urlString: url)!)
//            source.append(KingfisherSource(urlString: url)!)
//            source.append(KingfisherSource(urlString: url)!)
            
            self.headerView.viewParallax.setSliderImages(images: source)
         }
        
    }
    func setUpCollection(){
        storeCollectionView.register(UINib.init(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        storeCollectionView.registerCell(PharmacyCategoryCollectionCell.self)
        storeCollectionView.registerCell(SingleTextCollectionCell.self)
    }
    
    override func refreshData() {
        print("refresh")
        self.currentPage = 1
        self.categoryList.removeAll()
        //self.getCategoryListApi()
       // self.refreshView.endRefreshing()
    }
    
    
    
}
//MARK:- Collection Datasource and Delegate
extension PharmacySubCategoryController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if headerView.viewParallax != nil {
            headerView.viewParallax.scrollViewDidScroll(scrollView: scrollView)
           
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind.isEqual(UICollectionView.elementKindSectionHeader) {
            headerView  = (collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView)
            headerView.homeController = self
            headerView.clipsToBounds = false
            return headerView
        }
        
        return UICollectionReusableView()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        //|| categoryList.count == 0
        if (section == 1 ||  self.categoryList.count == 0) {
            return CGSize.zero
        }
      return CGSize(width: collectionView.frame.width, height: DesignUtility.getValueFromRatio(265)) //add your height here 265
        
        // Get the view for the first header
//        let indexPath = IndexPath(row: 0, section: section)
//        let headerView1 = self.collectionView(collectionView, viewForSupplementaryElementOfKind: UICollectionView.elementKindSectionHeader, at: indexPath)
//
//        // Use this view to calculate the optimal size based on the collection view's width
//        return headerView1.systemLayoutSizeFitting(CGSize(width: collectionView.frame.width, height: UIView.layoutFittingExpandedSize.height),
//                                                  withHorizontalFittingPriority: .required, // Width is fixed
//                                                  verticalFittingPriority: .fittingSizeLevel) // Height can be as large as needed
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 10
        if section == 0 {
            if self.categoryList.count == 0 {
                return 0
            }
            return 1
            
        }
        //return 0
        if self.categoryList.count == 0 {
            UtilityEmptyListView.emptycollectionViewMessage(message: "test", image: "emptylist", collectionView: collectionView, listType: .pharmacy)
            if let emptyView = collectionView.backgroundView as? EmptyTableViewBackgroundView {
                emptyView.actionBtn.didTapOnBounceEnd = {
                    //self.show(AddAddressController.loadVC())
                }
            }
            return 0
        }
        UtilityEmptyListView.hideEmptyCollectionViewMessage(collectionView: collectionView)
        return categoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SingleTextCollectionCell.identifier(), for: indexPath) as! SingleTextCollectionCell
            cell.setData(type: .pharmacy)
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PharmacyCategoryCollectionCell.identifier(), for: indexPath) as! PharmacyCategoryCollectionCell
        cell.lblTitle.fontSizeTheme = "sizeLarge"
        cell.setData(data: self.categoryList[indexPath.row])
        //var position : ProductCellPosition = indexPath.row % 2 == 0 ? .left : .right
        //cell.setData(data: data,cellPosition: position)
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = categoryList[indexPath.row]
        let controller = PharmacyCategoryProductController.loadVC()
        controller.selectedMainCategory = data
        self.show(controller)
        /*
        if (data.hasChild) {
            let controller = PharmacySubCategoryController.loadVC()
            controller.selectedMainCategory = data
            self.show(controller)
            
        }
        else {
            let controller = PharmacyCategoryProductController.loadVC()
            controller.selectedMainCategory = data
            self.show(controller)
        }
        */
    }
    func heightForLable(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        // pass string, font, LableWidth
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
         label.numberOfLines = 0
         label.lineBreakMode = NSLineBreakMode.byWordWrapping
        let subtitleFont = UIFont(name: FontManager.constant(forKey: "fontSFMedium")!,size: CGFloat(FontManager.style(forKey: "sizeNormal")))!
        label.font = subtitleFont
         label.text = "The definition is brief on the feelings section of the Pharmacy on two lines."
         label.sizeToFit()

         return label.frame.height
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //heightForLable(text: "", font: UIFont(), width: collectionView.frame.size.width - 30)
        if indexPath.section == 0 {
            return CGSize(width: collectionView.frame.size.width, height: DesignUtility.getValueFromRatio(80))
            
        }
        
        let width = (collectionView.frame.size.width - 44)/2
        return CGSize(width: width, height: DesignUtility.getValueFromRatio(80))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0 {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        return UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return 15
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: collectionView.frame.width, height: DesignUtility.getValueFromRatio(156)) //add your height here
//    }
}
extension PharmacySubCategoryController {
    

    func getPharmacyListApi() {
        
        let param:[String:Any] = ["page_number": currentPage,
                                  "page_size": 100000]
        StoreManager().getCategoryApi(endPoint: ApiEndPoints.getCategoriesByParentId + "/\(selectedMainCategory!.categoryId)", param: nil, header: [:], completion: { (response,msg,errorMsg) in
            
            print(response)
           // self.refreshView.endRefreshing()
            if let data = response {
                self.categoryList.append(contentsOf: data)
                //self.categoryList.append(contentsOf: data)
                if self.categoryList.count > 0 {
                    self.setupHeaderData()
                }
                let delay = self.isFirstTimeLoad == true ? 0.2 : 0.0
                Utility.delay(seconds: delay) {
                    self.storeCollectionView.isHidden = false
                    self.animateCollectionView(collectionView: self.storeCollectionView)
                }
                
                //self.storeCollectionView.reloadData()
            }

        }){ (error) in
            self.refreshView.endRefreshing()
                print("Error: ", error as Any)
            }
        
    }
    
}
