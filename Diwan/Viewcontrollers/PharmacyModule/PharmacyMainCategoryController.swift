//
//  StoreCategoryListController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import BottomPopup
import FittedSheets

class PharmacyMainCategoryController: BaseViewController , StoryBoardHandler {

    @IBOutlet weak var filterBtn: BounceButton!
    @IBOutlet weak var sortBtn: BounceButton!
    @IBOutlet weak var storeCollectionView: BaseUICollectionView!
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.pharmacy.rawValue , nil)
    var productList = [ProductModel]()
    var sortList = [SortModel]()
    var filterList = [FilterModel]()
    var selectedMainCategory : CategoryModel?
    var selectedSort : SortModel?
    var categoryList = [CategoryModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.storeCollectionView.isHidden = true
        self.navigationItem.title = "Book Pharmacy".localized
        createBackBtn()
        //self.storeCollectionView.isHidden = true
        setupAppDefaultNavigationBar()
        createNavSideMenuBtn()
        //createRefreshView(tblView: categoryListTbl)
        setUpCollection()
        self.getPharmacyListApi()
        self.addBarButtonItemWithImage(UIImage(named: "topbarheart")!, .BarButtonItemPositionLeft, self, #selector(showWishListScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarsearch")!, .BarButtonItemPositionRight, self, #selector(showSearchScreen))
    }
    
    func setUpCollection(){
        
        storeCollectionView.registerCell(PharmacyCategoryCollectionCell.self)
    }
    
    override func refreshData() {
        print("refresh")
        self.currentPage = 1
        self.productList.removeAll()
        self.getPharmacyListApi()
       // self.refreshView.endRefreshing()
    }
    func refreshPressed() {
        self.currentPage = 1
        self.productList.removeAll()
        self.getPharmacyListApi()
    }
    
    
}
//MARK:- Collection Datasource and Delegate
extension PharmacyMainCategoryController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return 10
        if self.categoryList.count == 0 {
            UtilityEmptyListView.emptycollectionViewMessage(message: "test", image: "emptylist", collectionView: collectionView, listType: .pharmacy)
            if let emptyView = collectionView.backgroundView as? EmptyTableViewBackgroundView {
                emptyView.actionBtn.didTapOnBounceEnd = {
                    self.refreshPressed()
                }
            }
            return 0
        }
        UtilityEmptyListView.hideEmptyCollectionViewMessage(collectionView: collectionView)
        return categoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PharmacyCategoryCollectionCell.identifier(), for: indexPath) as! PharmacyCategoryCollectionCell
        cell.setData(data: self.categoryList[indexPath.row])
       // let data = self.productList[indexPath.row]
        //var position : ProductCellPosition = indexPath.row % 2 == 0 ? .left : .right
        //cell.setData(data: data,cellPosition: position)
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let data = categoryList[indexPath.row]
        let controller = PharmacySubCategoryController.loadVC()
        controller.selectedMainCategory = categoryList[indexPath.row]
        self.show(controller)
        /*
        if (data.hasChild) {
            let controller = PharmacySubCategoryController.loadVC()
            controller.selectedMainCategory = categoryList[indexPath.row]
            self.show(controller)
        }
        else {
            let controller = StoreProductListController.loadVC()
            controller.selectedMainCategory = data
            controller.filledData = ( data.title, .pharmacy, .bookPharmacy)
            self.show(controller)
        }*/
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width - 32, height: DesignUtility.getValueFromRatio(141))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: collectionView.frame.width, height: DesignUtility.getValueFromRatio(156)) //add your height here
//    }
}
extension PharmacyMainCategoryController {
    

    func getPharmacyListApi() {
        
        let param:[String:Any] = ["page_number": currentPage,
                                  "page_size": 100000]
        StoreManager().getCategoryApi(endPoint: ApiEndPoints.getCategoriesByParentId + "/1", param: nil, header: [:], completion: { (response,msg,errorMsg) in
            self.storeCollectionView.isHidden = false
            print(response)
           // self.refreshView.endRefreshing()
            if let data = response {
                self.categoryList.append(contentsOf: data)
                self.animateCollectionViewBottomToTopCustom(colView: self.storeCollectionView)
               // self.storeCollectionView.reloadData()
            }

        }){ (error) in
            self.refreshView.endRefreshing()
                print("Error: ", error as Any)
            }
        
    }
    
}
