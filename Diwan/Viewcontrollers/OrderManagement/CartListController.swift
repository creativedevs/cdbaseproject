//
//  CartListController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 23/07/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class CartListController: BaseViewController, StoryBoardHandler {

    //let items = Cart<CartModel>()
    @IBOutlet weak var totalAmountLbl: BaseUILabel!
    var cartList = [CartModel]()
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.orderManagement.rawValue, nil)
    @IBOutlet weak var tableView: BaseUITableView!
    //var cartList = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStatusBarStyle(.lightContent)
        self.setNavBGColor()
        self.createNavSideMenuBtn()
        createBackBtn()
        setupAppDefaultNavigationBar()
        setUpTable()
        self.tableView.delaysContentTouches = false
        self.navigationItem.title = "My Cart".localized
        cartList.append(contentsOf: Constants.realm.objects(CartModel.self))
        
       //setDummyCart()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    @IBAction func checkoutPressed(_ sender: Any) {
        
    }
    
    func setDummyCart() {
        let pro1 = ProductModel()
        pro1.productId = 111
            let car1 = CartModel()
        car1.id = pro1.productId//car1.IncrementaID()
        car1.priceCart =  11
        pro1.title = "First Product"
        car1.product = pro1
        
        //items.add(car1, quantity: 177)
//               pro1.title = "First Product"
//               let pro2 = ProductModel()
//               pro2.title = "Second Product"
//               let pro3 = ProductModel()
//               pro3.title = "Third Product"
//               let pro4 = ProductModel()
//               pro4.title = "Fourth Product"
//               let pro5 = ProductModel()
//               pro5.title = "Fifth Product"
//               items.add(pro1, quantity: 177)
//               items.add(pro2, quantity: 199)
//               items.add(pro3, quantity: 999)
//               items.add(pro4, quantity: 1)
//               items.add(pro5, quantity: 4)
               print(cartList.count)
              // print(items.countQuantities)
              // print(items.amount)
        
//        try! Constants.realm.write(){
//             Constants.realm.add(car1, update: .all)
//        }
    }
    
    
    func setUpTable(){
        
        tableView.registerCell(ItemCartCell.self)
    }

    func saveToRealm() {
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CartListController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
           
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return 10
        if self.cartList.count == 0 {
            UtilityEmptyListView.emptyTableViewMessage(message: "test", image: "emptycart", tableView: tableView, listType: .cart)
            if let emptyView = tableView.backgroundView as? EmptyTableViewBackgroundView {
                emptyView.actionBtn.didTapOnBounceEnd = {
                    //self.show(AddAddressController.loadVC())
                }
                emptyView.actionBtn2.didTapOnBounceEnd = {
                    //self.show(AddAddressController.loadVC())
                }
            }
           return 0
        }
        return cartList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 76
//    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ItemCartCell.identifier()) as! ItemCartCell
        cell.selectionStyle = .none
        let data = cartList[indexPath.row]
        cell.setCartData(data: data)
        cell.heartBtn.didTapOnBounceEnd = {
            self.addOrRemoveFavorite(data: data.product, indexPath: indexPath,tableView: tableView)
        }
        return cell

       
    }
    
}
