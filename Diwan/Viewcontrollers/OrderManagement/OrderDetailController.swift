//
//  OrderDetailController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 24/07/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

enum orderDetailScreenType {
    case checkout
    case orderDetail
}

class OrderDetailController: BaseViewController, StoryBoardHandler  {

    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.orderManagement.rawValue, nil)
    var orderType:orderDetailScreenType = .checkout
    @IBOutlet weak var tableView: BaseUITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStatusBarStyle(.lightContent)
        self.setNavBGColor()
        self.createNavSideMenuBtn()
        createBackBtn()
        setupAppDefaultNavigationBar()
        setUpTable()
        self.tableView.delaysContentTouches = false
        self.navigationItem.title =  "My Cart".localized
    }
    func setUpTable(){
        
        //tableView.registerCell(ItemCartCell.self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
