//
//  HomeViewController.swift
//  Volchain
//
//  Created by Waqas Ali on 15/04/2019.
//  Copyright © 2019 Waqas Ali. All rights reserved.
//

import UIKit


class MyDiwanController: BaseViewController, StoryBoardHandler {
    
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.myDiwan.rawValue, nil)
    @IBOutlet weak var tableView: BaseUITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setStatusBarStyle(.lightContent)
        self.setNavBGColor()
        self.createNavSideMenuBtn()
        setupAppDefaultNavigationBar()
        self.navigationItem.title = "My Diwan".localized
        //setUpTable()
        //tableView.reloadData()
    }
    func setUpTable(){
        
        tableView.registerCell(HomeBannerCell.self)
        tableView.registerCell(HomeCelebrityCell.self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MyDiwanController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
           
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 0
        if section == 0 || section == 1 {return 1}
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            return DesignUtility.getValueFromRatio(212)
        case 1:
            return UITableView.automaticDimension
        default:
            return UITableView.automaticDimension
        }
        
        
        //return UITableView.automaticDimension
    }
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 76
//    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeBannerCell.identifier()) as! HomeBannerCell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeCelebrityCell.identifier()) as! HomeCelebrityCell
            return cell
        default:
            return UITableViewCell()
        }
        
       // cell.setData(data: self.categoryList[indexPath.row])

       
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            guard let tableViewCell = cell as? HomeCelebrityCell else { return }

            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }
        
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            guard let tableViewCell = cell as? HomeCelebrityCell else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }
    }
}
//MARK:- Collection Datasource and Delegate
extension MyDiwanController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 19
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AuthorMainCollectionCell.identifier(), for: indexPath) as! AuthorMainCollectionCell
        //let data = self.authorList[indexPath.row]
//        let position : ProductCellPosition = indexPath.row % 2 == 0 ? .left : .right
        //cell.setCelebrityData(data: data)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width - 75
        return CGSize(width: width/2, height: DesignUtility.getValueFromRatio(184))
        //return CGSize(width: DesignUtility.getValueFromRatio(188), height: DesignUtility.getValueFromRatio(198))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 12, left: 12, bottom: 20, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: collectionView.frame.width, height: DesignUtility.getValueFromRatio(156)) //add your height here
//    }
}
