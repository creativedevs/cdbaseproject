//
//  LibraryMainListController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 25/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import ViewAnimator
class LibraryMainListController: BaseViewController , StoryBoardHandler {
    
    let getDispatchGroup = DispatchGroup()
    @IBOutlet weak var collectionView: BaseUICollectionView!
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.library.rawValue , nil)
    var genreList = [LibraryMainModel]()
    var sectionList = [LibraryMainModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.storeCollectionView.isHidden = true
        self.navigationItem.title = "Library".localized
        setupAppDefaultNavigationBar()
        createNavSideMenuBtn()
        //createRefreshView(tblView: categoryListTbl)
        setUpCollection()
        getGenreSectionListApi(apiUrl: ApiEndPoints.getGenres)
        getGenreSectionListApi(apiUrl: ApiEndPoints.getSections)
        getDispatchGroup.notify(queue: .main) {
            print("END CALLS BOTH")
            self.collectionView.reloadData()
            //self.animate()
           // self.stopAnimating()
        }
        self.addBarButtonItemWithImage(UIImage(named: "topbarheart")!, .BarButtonItemPositionLeft, self, #selector(showWishListScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarsearch")!, .BarButtonItemPositionRight, self, #selector(showSearchScreen))
    }
    func setUpCollection(){
        collectionView.registerCell(PharmacyCategoryCollectionCell.self)
        collectionView.registerCell(SingleTextCollectionCell.self)
        collectionView.registerCell(PharmacyCategoryCollectionCell.self)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
//MARK:- Collection Datasource and Delegate
extension LibraryMainListController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0 {return 1}
        else if (section == 1) {
            return self.genreList.count
        }
        else if (section == 2) {
            return self.sectionList.count
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         
        if indexPath.section == 0 {
            return CGSize(width: collectionView.frame.size.width, height: DesignUtility.getValueFromRatio(120))
        }
        else if indexPath.section == 1 {
            let width = (collectionView.frame.size.width - 44)/2
            return CGSize(width: width, height: DesignUtility.getValueFromRatio(80))
        }
        else if indexPath.section == 2 {
            return CGSize(width: collectionView.frame.size.width - 32, height: DesignUtility.getValueFromRatio(141))
        }
        return CGSize.zero
        
     }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SingleTextCollectionCell.identifier(), for: indexPath) as! SingleTextCollectionCell
            cell.setData(type: .library)
            return cell
        }
        
        else if indexPath.section == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PharmacyCategoryCollectionCell.identifier(), for: indexPath) as! PharmacyCategoryCollectionCell
            cell.lblTitle.fontSizeTheme = "sizeLarge"
            let data = self.genreList[indexPath.row]
            cell.setLibraryData(data: data,type: .genre)
            return cell
            //
        }
        else if indexPath.section == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PharmacyCategoryCollectionCell.identifier(), for: indexPath) as! PharmacyCategoryCollectionCell
            let data = self.sectionList[indexPath.row]
            cell.setLibraryData(data: data,type: .section)
            return cell
            //cell.setData(data: self.categoryList[indexPath.row])
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0 {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        else  if section == 1 {
                   return UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
               }
        else  if section == 2 {
            return UIEdgeInsets(top: 70, left: 0, bottom: 0, right: 0)
        }
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        else  if section == 1 {
            return 15
        }
        else  if section == 2 {
            return 15
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        
        if indexPath.section == 0 {
            return
        }
        let category = CategoryModel()
        let controller = StoreProductListController.loadVC()
        if indexPath.section == 1 {
            let data = self.genreList[indexPath.row]
            category.categoryId = data.Id
            category.title = data.title
            controller.filledData = ( category.title, .library, .libraryGenre)
        }
        else if indexPath.section == 2 {
            let data = self.sectionList[indexPath.row]
            category.categoryId = data.Id
            category.title = data.title
            controller.filledData = ( category.title, .library, .librarySection)
        }
        
        controller.selectedMainCategory = category
        
        
        self.show(controller)
    }
}
extension LibraryMainListController {
    

    func getGenreSectionListApi(apiUrl : String) {
        self.getDispatchGroup.enter()
        LibraryManager().getGenreSectionApi(endPoint: apiUrl, param: nil, header: [:], completion: { (response,msg,errorMsg) in
            //self.storeCollectionView.isHidden = false
            print(apiUrl)
            print("STRT CALLS ")
           // print(response)
           // self.refreshView.endRefreshing()
            if let data = response {
                if (apiUrl == ApiEndPoints.getGenres) {
                    self.genreList.append(contentsOf: data)
                }
                else if (apiUrl == ApiEndPoints.getSections) {
                    self.sectionList.append(contentsOf: data)
                }
             //   self.categoryList.append(contentsOf: data)
              //  self.storeCollectionView.reloadData()
            }
            self.getDispatchGroup.leave()
        }){ (error) in
            //self.refreshView.endRefreshing()
                print("Error: ", error as Any)
            self.getDispatchGroup.leave()
            }
        
    }
    @IBAction func animate() {
        // Combined animations example
        collectionView.reloadData()
        collectionView.layoutIfNeeded()
        let fromAnimation = AnimationType.from(direction: .right, offset: 30.0)
        let zoomAnimation = AnimationType.zoom(scale: 0.2)
        let rotateAnimation = AnimationType.rotate(angle: CGFloat.pi/6)
        var array = collectionView.visibleCells
        var arrayIndexpath = collectionView.indexPathsForVisibleItems
        var sec = arrayIndexpath.filter { (data) -> Bool in
            data.section == 0
        }
//        sec.
        array.removeFirst()
        collectionView.visibleCells[0].animate(animations: [zoomAnimation],delay: 0, duration:0.5 )
//        UIView.animate(views: array,
//                       animations: [zoomAnimation, rotateAnimation],
//                       duration: 0.5)
        
        //UIView.animate(views: collectionView.visibleCells,
          //             animations: [fromAnimation, zoomAnimation], delay: 0.5)
    }
}
