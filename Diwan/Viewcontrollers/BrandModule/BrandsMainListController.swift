//
//  LibraryMainListController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 25/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class BrandsMainListController: BaseViewController , StoryBoardHandler {

    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.brands.rawValue , nil)
    @IBOutlet weak var filterTopView: HorizontalDynamicCollectionView!
    var brandList = [BrandModel]()
    @IBOutlet weak var tableView: BaseUITableView!
    var sortCharacter = "All"
    var isPopular = false
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.isHidden = true
        self.navigationItem.title = "Brands".localized
        setupAppDefaultNavigationBar()
        createNavSideMenuBtn()
        createBackBtn()
        //createRefreshView(tblView: categoryListTbl)
        setUpTable()
        setupFilterView()
        self.getBrandListApi()
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarheart")!, .BarButtonItemPositionLeft, self, #selector(showWishListScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarsearch")!, .BarButtonItemPositionRight, self, #selector(showSearchScreen))
    }
    
    func setUpTable(){
        
        tableView.registerCell(BrandMainCell.self)
    }
    func setupFilterView() {
        
        filterTopView.didSelectItem = {[weak self] item in
            print(item)
            if (self?.sortCharacter != item) {
                self?.sortCharacter = item
                self?.refreshData()
            }
        }
        
    }
    override func refreshData() {
        print("refresh")
        self.currentPage = 1
        self.brandList.removeAll()
        self.getBrandListApi()
       // self.refreshView.endRefreshing()
    }

}
extension BrandsMainListController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return 10
        if self.brandList.count == 0 {
            UtilityEmptyListView.emptyTableViewMessage(message: "test", image: "emptylist", tableView: tableView, listType: .brand)
            if let emptyView = tableView.backgroundView as? EmptyTableViewBackgroundView {
                emptyView.actionBtn.didTapOnBounceEnd = {
                    //self.show(AddAddressController.loadVC())
                }
            }
            return 0
        }
        UtilityEmptyListView.hideEmptyTableViewMessage(tableView: tableView)
        return self.brandList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 176
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BrandMainCell.identifier()) as! BrandMainCell
        cell.selectionStyle = .none
        cell.setData(data: self.brandList[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let category = CategoryModel()
        let controller = StoreProductListController.loadVC()
        let data = self.brandList[indexPath.row]
        category.categoryId = data.Id
        category.title = data.title
        category.imageIcon = data.imageUrl
        controller.selectedMainCategory = category
        controller.selectedBrand = data
        controller.filledData = ( category.title, .brand, .brand )
        self.show(controller)
        
    }
    
}
extension BrandsMainListController {
    
    func getBrandListApi() {
          var param:[String:Any] = ["page_number": currentPage,
                                    "page_size": 100000,
                                    //"search_keyword" : "",
                                    "sort_start_with":sortCharacter]
            
        if(isPopular) {
            param["is_popular"] = isPopular
        }
        print(param)
            BrandManager().getBrandListApi(endPoint: ApiEndPoints.getBrands, param: param, header: [:], completion: { (response,msg,errorMsg) in
                //self.tableView.isHidden = false
                print(response)
                //self.refreshView.endRefreshing()
                if let data = response {
                    self.brandList.append(contentsOf: data)
                    let delay = self.isFirstTimeLoad == true ? 0.2 : 0.0
                    Utility.delay(seconds: delay) {
                        self.tableView.isHidden = false
                        self.animationTableViewFromBottom(tblView: self.tableView)
                    }
                    
                    //self.tableView.reloadData()
                }

            }){ (error) in
                self.tableView.isHidden = false
               // self.refreshView.endRefreshing()
                    print("Error: ", error as Any)
                }
            
        }
    
    
}
