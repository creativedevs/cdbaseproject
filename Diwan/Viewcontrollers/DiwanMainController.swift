//
//  DiwanMainController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 26/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class DiwanMainController:BaseViewController , StoryBoardHandler{
    
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.userModule.rawValue , nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
