//
//  ProductDetailController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 30/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import ImageSlideshow
enum DetailSectionType : Int {
    case productBanner = 0
    case productTitle = 1
    case productPriceRate = 2
    case productDescription = 3
    case productWarranty = 4
    case productFeatures = 5
    case productPhotos = 6
    case productVideos = 7
    case productReviews = 8
    case similarProduct = 9
}
class ProductDetailController: BaseViewController , StoryBoardHandler {

    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.store.rawValue , nil)
    @IBOutlet weak var tableView: BaseUITableView!
    var selectedProduct : ProductModel?
    var similarProducts = [ProductModel]()
    var warrantyArray = [WarrantyType]()
    var featureArray = [[String : Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTable()
        self.navigationItem.title = selectedProduct?.title
        createBackBtn()
//        self.getSimilarProductsApi()
        self.getProductDetailApi()
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarheart")!, .BarButtonItemPositionLeft, self, #selector(showWishListScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //setupHeaderData()
    }
    
    
    func setUpTable(){
        let parallaxViewFrame = CGRect(x: 0, y: 0, width: self.tableView.bounds.width, height: DesignUtility.getValueFromRatio(287))
        //tableView.tableHeaderView  = ParallaxHeaderView(frame: parallaxViewFrame)
        tableView.registerCell(HomeBannerCell.self)
        tableView.registerCell(PharmacyCategoryProductCell.self)
        tableView.registerCell(VideoCollectionViewCell.self)
        tableView.registerCell(PhotosCollectionViewCell.self)
        tableView.registerCell(FeaturesCollectionViewCell.self)
        tableView.registerCell(ProductDescriptionCell.self)
        tableView.registerCell(ProductWarrantyCollectionViewCell.self)
        tableView.registerCell(ProductTitleCell.self)
        tableView.registerCell(ProductPriceRateCell.self)
        tableView.registerCell(ReviewsCollectionViewCell.self)
    }
    func setupHeaderData() {
        var source = [InputSource]()
        let image = UIImage(named: "placeholderbiglogo")
        source.append(ImageSource(image: image!))
        if let banners = selectedProduct?.productMediaObject.bannerImages {
            if banners.count > 0 {
                //source.removeAll()
                for data in banners {
                    if let url = data.mediaUrl.getValidUrlString() {
                        source.append(KingfisherSource(urlString: url, placeholder: image, options: nil)!)
                    }
                }
            }
        }
        if source.count > 1 {
            source.removeFirst()
        }
        let headerView = self.tableView.tableHeaderView as! ParallaxHeaderView
        headerView.setSliderImages(images: source)
    }
    func getWarrantyArray() {
        
        if selectedProduct!.productAttributeObject.contains(where: { $0.attributeId == AttributesIdType.authentic.rawValue }) {
             warrantyArray.append(.authenticate)
        }
        if selectedProduct!.productAttributeObject.contains(where: { $0.attributeId == AttributesIdType.easyReturns.rawValue }) {
             warrantyArray.append(.easyReturn)
        }
        if selectedProduct!.productAttributeObject.contains(where: { $0.attributeId == AttributesIdType.protection.rawValue }) {
             warrantyArray.append(.protection)
        }
//        if selectedProduct!.productAttributeObject.contains(where: { $0.attributeId == 75 }) {
//             warrantyArray.append(.easyReturn)
//        }
        
//        if let authenticate = selectedProduct?.isAuthentic {
//            if authenticate {
//                warrantyArray.append(.authenticate)
//            }
//        }
//        if let easyReturn = selectedProduct?.isEasyReturn {
//            if easyReturn {
//                warrantyArray.append(.easyReturn)
//            }
//        }
//        if let protection = selectedProduct?.is14DaysConsumerProtection {
//            if protection {
//                warrantyArray.append(.protection)
//            }
//        }
    }
    func getFeatureArray() {
        if (selectedProduct?.productTypeObject.id == MainProductType.book.rawValue) {
            if let publisher = selectedProduct?.publisherObject.title {
                let param = ["key" : "Publisher", "value" : publisher]
                featureArray.append(param)
                
            }
            if let publisher = selectedProduct?.publishedDate {
                let param = ["key" : "Publication date", "value" : publisher]
                featureArray.append(param)
            }
            if let publisher = selectedProduct?.bookLanguage {
                let param = ["key" : "Language", "value" : publisher]
                featureArray.append(param)
            }
            if let publisher = selectedProduct?.itemDimensions {
                let param = ["key" : "Book Dimensions", "value" : publisher]
                featureArray.append(param)
            }
            if let publisher = selectedProduct?.shippingWeight {
                let param = ["key" : "Shopping Weight", "value" : publisher]
                featureArray.append(param)
            }
            if let publisher = selectedProduct?.isbn13 {
                let param = ["key" : "ISBN-13", "value" : publisher]
                featureArray.append(param)
            }
            if let publisher = selectedProduct?.sku {
                let param = ["key" : "SQU", "value" : publisher]
                featureArray.append(param)
            }
        }
        else if (selectedProduct?.productTypeObject.id == MainProductType.item.rawValue) {
            if let publisher = selectedProduct?.sku {
                let param = ["key" : "Brand", "value" : publisher]
                featureArray.append(param)
            }
            if let publisher = selectedProduct?.itemDimensions {
                let param = ["key" : "Dimensions", "value" : publisher]
                featureArray.append(param)
            }
            if let publisher = selectedProduct?.shippingWeight {
                let param = ["key" : "Shopping Weight", "value" : publisher]
                featureArray.append(param)
            }
            if let publisher = selectedProduct?.sku {
                let param = ["key" : "SQU", "value" : publisher]
                featureArray.append(param)
            }
        }
    }

}
extension ProductDetailController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 10
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == DetailSectionType.productBanner.rawValue {
            return 1
        }
        if section == DetailSectionType.productTitle.rawValue {
            return 1
        }
        if section == DetailSectionType.productPriceRate.rawValue {
            return 1
        }
        
        if section == DetailSectionType.productDescription.rawValue {
            return 1
        }
        else if section == DetailSectionType.productWarranty.rawValue {
                return warrantyArray.count > 0 ? 1 : 0
            }
        else if section == DetailSectionType.productFeatures.rawValue {
            return featureArray.count > 0 ? 1 : 0
        }
        else if section == DetailSectionType.productPhotos.rawValue {
            return selectedProduct?.productMediaObject.bannerImages.count ?? 0 > 0 ? 1 : 0
        }
        else if section == DetailSectionType.productVideos.rawValue {
            return selectedProduct?.productMediaObject.videos.count ?? 0 > 0 ? 1 : 0
             
        }
        else if section == DetailSectionType.productReviews.rawValue {
            return selectedProduct?.productUserReviewObject.count ?? 0 > 0 ? 1 : 0
        }
        else if section == DetailSectionType.similarProduct.rawValue {
            return similarProducts.count > 0 ? 1 : 0
        }
        //if section == 0 {return 1}
        //return self.categoryList.count
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case DetailSectionType.productBanner.rawValue:
            return DesignUtility.getValueFromRatio(212)
        default:
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case DetailSectionType.productBanner.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeBannerCell.identifier()) as! HomeBannerCell
            let data = Array((selectedProduct?.productMediaObject.bannerImages)!)
            cell.setDetailTopData(data: data, type: .detailTop)
            return cell
        case DetailSectionType.productTitle.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: ProductTitleCell.identifier()) as! ProductTitleCell
            cell.setData(data: selectedProduct ?? ProductModel())
            return cell
        case DetailSectionType.productPriceRate.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: ProductPriceRateCell.identifier()) as! ProductPriceRateCell
            cell.setData(data: selectedProduct ?? ProductModel())
            return cell
        case DetailSectionType.productDescription.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: ProductDescriptionCell.identifier()) as! ProductDescriptionCell
            cell.setDescriptionData(text: selectedProduct?.descriptionField)
            return cell
        case DetailSectionType.productWarranty.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: ProductWarrantyCollectionViewCell.identifier()) as! ProductWarrantyCollectionViewCell
            //cell.setDescriptionData(text: selectedProduct?.descriptionField)
            return cell
        case DetailSectionType.productFeatures.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: FeaturesCollectionViewCell.identifier()) as! FeaturesCollectionViewCell
            cell.setFeatureDetailData()
            return cell
        case DetailSectionType.productPhotos.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: PhotosCollectionViewCell.identifier()) as! PhotosCollectionViewCell
            cell.setProductDetailData()
            return cell
        case DetailSectionType.productVideos.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: VideoCollectionViewCell.identifier()) as! VideoCollectionViewCell
            cell.setProductDetailData()
            return cell
        case DetailSectionType.productReviews.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: ReviewsCollectionViewCell.identifier()) as! ReviewsCollectionViewCell
            cell.setData()
            return cell
        case DetailSectionType.similarProduct.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyCategoryProductCell.identifier()) as! PharmacyCategoryProductCell
            cell.setProductDetailData()
            return cell
        default:
            print("")
        }
        return UITableViewCell()
        
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case DetailSectionType.productFeatures.rawValue:
            guard let tableViewCell = cell as? FeaturesCollectionViewCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.section] ?? 0
        case DetailSectionType.productWarranty.rawValue:
            guard let tableViewCell = cell as? ProductWarrantyCollectionViewCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.section] ?? 0
        case DetailSectionType.productPhotos.rawValue:
            guard let tableViewCell = cell as? PhotosCollectionViewCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.section] ?? 0
        case DetailSectionType.productVideos.rawValue:
            guard let tableViewCell = cell as? VideoCollectionViewCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.section] ?? 0
        case DetailSectionType.productReviews.rawValue:
            guard let tableViewCell = cell as? ReviewsCollectionViewCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.section] ?? 0
        case DetailSectionType.similarProduct.rawValue:
            guard let tableViewCell = cell as? PharmacyCategoryProductCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.section] ?? 0
        default:
            print("")
        }
        
        
    }

    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
            
        case DetailSectionType.productFeatures.rawValue:
            guard let tableViewCell = cell as? FeaturesCollectionViewCell else { return }
            storedOffsets[indexPath.section] = tableViewCell.collectionViewOffset
        case DetailSectionType.productWarranty.rawValue:
            guard let tableViewCell = cell as? ProductWarrantyCollectionViewCell else { return }
            storedOffsets[indexPath.section] = tableViewCell.collectionViewOffset
        case DetailSectionType.productPhotos.rawValue:
            guard let tableViewCell = cell as? PhotosCollectionViewCell else { return }
            storedOffsets[indexPath.section] = tableViewCell.collectionViewOffset
        case DetailSectionType.productVideos.rawValue:
            guard let tableViewCell = cell as? VideoCollectionViewCell else { return }
            storedOffsets[indexPath.section] = tableViewCell.collectionViewOffset
        case DetailSectionType.productReviews.rawValue:
            guard let tableViewCell = cell as? ReviewsCollectionViewCell else { return }
            storedOffsets[indexPath.section] = tableViewCell.collectionViewOffset
            
        case DetailSectionType.similarProduct.rawValue:
            guard let tableViewCell = cell as? PharmacyCategoryProductCell else { return }
            storedOffsets[indexPath.section] = tableViewCell.collectionViewOffset
        default:
            print("")
        }
        
        
    }
}
//MARK:- Collection Datasource and Delegate
extension ProductDetailController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if let headerView = self.tableView.tableHeaderView as? ParallaxHeaderView {
            headerView.scrollViewDidScroll(scrollView: scrollView)
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case DetailSectionType.productWarranty.rawValue:
            return warrantyArray.count
        case DetailSectionType.productFeatures.rawValue:
            return featureArray.count
        case DetailSectionType.productPhotos.rawValue:
            return selectedProduct?.productMediaObject.bannerImages.count ?? 0
        case DetailSectionType.productVideos.rawValue:
            return selectedProduct?.productMediaObject.videos.count ?? 0
        case DetailSectionType.productReviews.rawValue:
            return selectedProduct?.productUserReviewObject.count ?? 0
        case DetailSectionType.similarProduct.rawValue:
            return similarProducts.count
        default:
            print("")
        }
        return 0
        //return model[collectionView.tag].count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView.tag {
        case DetailSectionType.productWarranty.rawValue:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductWarrantyCollectionCell.identifier(), for: indexPath) as! ProductWarrantyCollectionCell
                    let data = warrantyArray[indexPath.item]
            cell.setData(type: data)
            return cell
        case DetailSectionType.productFeatures.rawValue:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeaturesCollectionCell.identifier(), for: indexPath) as! FeaturesCollectionCell
            //            let data = selectedProduct?.productMediaObject.bannerImages[indexPath.item]
            cell.setData(data: featureArray[indexPath.item])
            return cell
        case DetailSectionType.productPhotos.rawValue:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotosCollectionCell.identifier(), for: indexPath) as! PhotosCollectionCell
            let data = selectedProduct?.productMediaObject.bannerImages[indexPath.item]
            cell.setData(data: data ?? ProductMediaDetailModel())
            return cell
        case DetailSectionType.productVideos.rawValue:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EhsanMainCollectionCell.identifier(), for: indexPath) as! EhsanMainCollectionCell
            let data = selectedProduct?.productMediaObject.videos[indexPath.item]
            cell.setData(data: data ?? EhsanModel())
            return cell
        case DetailSectionType.productReviews.rawValue:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReviewsCollectionCell.identifier(), for: indexPath) as! ReviewsCollectionCell
                        let data = selectedProduct?.productUserReviewObject[indexPath.item]
            cell.setData(data: data)
           // cell.systemLayoutSizeFitting(UICollectionViewFlowLayout.automaticSize)
            return cell
        case DetailSectionType.similarProduct.rawValue:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoreProductCollectionCell.identifier(), for: indexPath) as! StoreProductCollectionCell
            let data = similarProducts[indexPath.item]
            cell.setData(data: data,cellPosition: ProductCellPosition.normal)
            return cell
        default:
            print("")
        }
        return UICollectionViewCell()
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           //
        switch collectionView.tag {
        case DetailSectionType.productWarranty.rawValue:
            var width : CGFloat = 0.0
            if (warrantyArray[indexPath.row] == .authenticate) {
                width = collectionView.frame.size.width/3 - 20
            }
            else if (warrantyArray[indexPath.row] == .easyReturn) {
                width = collectionView.frame.size.width/3 - 10
            }
            else if (warrantyArray[indexPath.row] == .protection) {
                width = collectionView.frame.size.width/3 + 30
            }
            return CGSize(width: width, height: DesignUtility.getValueFromRatio(30))
        case DetailSectionType.productFeatures.rawValue:
            return CGSize(width: collectionView.frame.size.width - 24, height: DesignUtility.getValueFromRatio(38))
        case DetailSectionType.productPhotos.rawValue:
            return CGSize(width: collectionView.frame.size.width - 24, height: DesignUtility.getValueFromRatio(184))
        case DetailSectionType.productVideos.rawValue:
            let width = collectionView.frame.size.width - 42
            return CGSize(width: width/2, height: DesignUtility.getValueFromRatio(135))
        case DetailSectionType.productReviews.rawValue:
            let sectionInset = (collectionViewLayout as! UICollectionViewFlowLayout).sectionInset
            let referenceHeight: CGFloat = 100 // Approximate height of your cell
            let referenceWidth = collectionView.frame.size.width - 24
            print(referenceWidth)
            return CGSize(width: referenceWidth, height: referenceHeight)
        case DetailSectionType.similarProduct.rawValue:
            return CGSize(width: DesignUtility.getValueFromRatio(162), height: DesignUtility.getValueFromRatio(298))
        default:
            print("")
        }
        return CGSize.zero
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            switch collectionView.tag {
            case DetailSectionType.productVideos.rawValue:
               return UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
            case DetailSectionType.similarProduct.rawValue:
                return UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 0)
            default:
                print("")
            }
        
          return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
       }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        switch collectionView.tag {
        case DetailSectionType.productFeatures.rawValue:
            return 5
        case DetailSectionType.productPhotos.rawValue:
            return 12
        default:
            print("")
        }
        return 0
    }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
    }
}
//MARK:-API's
extension ProductDetailController {
    
    func getProductDetailApi() {
        
        StoreManager().getProductDetailApi(endPoint: ApiEndPoints.getProductDetail + "/\(selectedProduct!.productId)", param: nil, header: [:], completion: { (response,msg,errorMsg) in
            //self.storeCollectionView.isHidden = false
           // self.refreshView.endRefreshing()
            if let data = response {
                //self.similarProducts.append(contentsOf: data)
                self.selectedProduct = data
                print(self.selectedProduct!)
                self.getSimilarProductsApi()
                self.getWarrantyArray()
                self.getFeatureArray()
                //self.setupHeaderData()
                self.tableView.reloadData()
                
            }

        }){ (error) in
            //self.refreshView.endRefreshing()
                print("Error: ", error as Any)
            }
        
    }
    
    func getSimilarProductsApi() {
        
        StoreManager().getSimilarProductsApi(endPoint: ApiEndPoints.getSimilarProducts + "/\(selectedProduct!.productId)", param: nil, header: [:], completion: { (response,msg,errorMsg) in
            //self.storeCollectionView.isHidden = false
           // self.refreshView.endRefreshing()
            if let data = response {
                self.similarProducts.append(contentsOf: data)
                self.tableView.reloadData()
            }

        }){ (error) in
            //self.refreshView.endRefreshing()
                print("Error: ", error as Any)
            }
        
    }
    
}
