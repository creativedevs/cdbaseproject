//
//  StoreCategoryListController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import BottomPopup
import FittedSheets
import ImageSlideshow
class StoreProductListController: BaseViewController , StoryBoardHandler {
    
    var headerView:HeaderCollectionReusableView!
    
    @IBOutlet weak var topView: BaseUIView!
    @IBOutlet weak var topviewHeight: NSLayoutConstraint!
    @IBOutlet weak var sortyTitleLbl: BaseUILabel!
    @IBOutlet weak var filterCounterLbl: BaseUILabel!
    @IBOutlet weak var filterBtn: BounceButton!
    @IBOutlet weak var sortBtn: BounceButton!
    @IBOutlet weak var storeCollectionView: BaseUICollectionView!
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.store.rawValue , nil)
    var productList = [ProductModel]()
    var sortList = [SortModel]()
    var filterList = [FilterModel]()
    var selectedMainCategory : CategoryModel?
    var selectedAuthor : AuthorModel?
    var selectedBrand : BrandModel?
    var selectedSort : SortModel?
    var isShowAnimationHeart = true
//    var selectedFilter = [FilterModel]()
    //= (title: "", listType: .wishlist)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //storeCollectionView.isHidden = true
        //        sortyTitleLbl.isHidden = true
        filterCounterLbl.isHidden = true
        self.navigationItem.title = filledData?.title//selectedMainCategory?.title
        createBackBtn()
        setupAppDefaultNavigationBar()
        createNavSideMenuBtn()
        self.storeCollectionView.isHidden = true
        //createRefreshView(tblView: categoryListTbl)
        setUpCollection()
        setupInitialData()
        
        
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarsearch")!, .BarButtonItemPositionRight, self, #selector(showSearchScreen))
        
        
    }
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    func callHeader() {
        switch filledData?.listType {
        case .author,.brand:
            setupHeaderData()
            
        default:
            print("")
        }
    }
    func setupInitialData() {
        
        switch filledData?.listType {
        case .wishlist:
            topviewHeight.constant = 0
            topView.isHidden = true
            self.getWishListApi()
        case .author,.brand:
            if selectedSort == nil {
                getSortJSONList()
            }
            setupButtons()
            //getFilterJSONList()
            getFilterListApi()
            self.getStoreProductListApi()
        default:
            self.addBarButtonItemWithImage(UIImage(named: "topbarheart")!, .BarButtonItemPositionLeft, self, #selector(showWishListScreen))
            if selectedSort == nil {
                getSortJSONList()
            }
            setupButtons()
            //getFilterJSONList()
            getFilterListApi()
            self.getStoreProductListApi()
            print("")
        }
        
        
    }
    func setUpCollection(){
        
        switch filledData?.listType {
        case .author,.brand:
            storeCollectionView.register(UINib.init(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        default:
            print("")
        }
        
        storeCollectionView.registerCell(StoreProductCollectionCell.self)
    }
        func setupHeaderData() {
            
            storeCollectionView.reloadData()
            storeCollectionView.layoutIfNeeded()
            var source = [InputSource]()
            switch filledData?.listType {
            case .author:
                let image = selectedAuthor?.genderId == UserGender.male.rawValue ? UIImage(named: "authorboybig") : UIImage(named: "authorgirlbig")
                source.append(ImageSource(image: image!))
                if let url = selectedMainCategory?.imageIcon.getValidUrlString() {
                   source.removeAll()
                    source.append(KingfisherSource(urlString: url, placeholder: image, options: nil)!)
                }
            case .brand:
                let image = UIImage(named: "placeholderbiglogo")
                source.append(ImageSource(image: image!))
                if let url = selectedMainCategory?.imageIcon.getValidUrlString() {
                    source.removeAll()
                    source.append(KingfisherSource(urlString: url, placeholder: image, options: nil)!)
                    
                }
            default:
                print("")
            }
             self.headerView.viewParallax.setSliderImages(images: source)
            
        }
    override func refreshData() {
        print("refresh")
        self.currentPage = 1
        self.productList.removeAll()
        switch filledData?.listType {
        case .wishlist:
            self.getWishListApi()
        default:
            self.getStoreProductListApi()
            print("")
        }
        //self.getCategoryListApi()
        // self.refreshView.endRefreshing()
    }
    func setupButtons(){
        
        if (CurrentUser.languageId == .arabic) {
            sortBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -20)
            filterBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -20)
        }
        else {
            sortBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
            filterBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
        }
        sortBtn.didTapOnBounceEnd = {
            self.showSortView()
        }
        filterBtn.didTapOnBounceEnd = {
            self.showFilterView()
        }
    }
    func getSortJSONList() {
        
        if let path = Bundle.main.path(forResource: "Sort", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let decoder = JSONDecoder()
                let dict = try JSONSerialization.jsonObject(with: data, options:[]) as? Dictionary<String, Any>
                let arr = dict?["sort"] as? [Any]
                let jsonData = try? JSONSerialization.data(withJSONObject:arr!)
                let result = try decoder.decode([SortModel].self, from: jsonData!)
                sortList.append(contentsOf: result)
                sortList[0].isSelected = true
                sortyTitleLbl.text = sortList[0].title
                self.selectedSort = sortList[0]
                print(sortList)
            } catch let error{
                print("Error: ", error)
            }
        }
    }
    func getFilterJSONList() {
        
        if let path = Bundle.main.path(forResource: "Filter", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let decoder = JSONDecoder()
                let dict = try JSONSerialization.jsonObject(with: data, options:[]) as? Dictionary<String, Any>
                let arr = dict?["filter"] as? [Any]
                let jsonData = try? JSONSerialization.data(withJSONObject:arr!)
                let result = try decoder.decode([FilterModel].self, from: jsonData!)
                filterList.append(contentsOf: result)
                print(filterList)
            } catch let error{
                print("Error: ", error)
            }
            
        }
    }
    func showSortView() {
        
        let controller = FilterPopUpController()
        //.fixed(300),
        let sheetController = SheetViewController(controller: controller, sizes: [  .halfScreen, .fullScreen])
        // Adjust how the bottom safe area is handled on iPhone X screens
        sheetController.blurBottomSafeArea = false
        sheetController.adjustForBottomSafeArea = true
        sheetController.topCornersRadius = 10
        // Disable the dismiss on background tap functionality
        sheetController.dismissOnBackgroundTap = true
        // Extend the background behind the pull bar instead of having it transparent
        sheetController.extendBackgroundBehindHandle = true
        // Change the overlay color
        //sheetController.overlayColor = UIColor.red
        // Change the handle color
        sheetController.handleColor = UIColor.orange
        sheetController.handleSize = CGSize(width: 26, height: 2)
        sheetController.handleColor = ColorManager.color(forKey: "grayplaceholder")!
        sheetController.pullBarView.isHidden = true
        self.present(sheetController, animated: false, completion: nil)
        
        controller.setData(dataList: sortList, type: .sort)
        controller.didTapOnSortDone = {sortArray in
            print(self.sortList)
            if let selected = self.sortList.filter({$0.isSelected}).first {
                self.sortyTitleLbl.text = selected.title
                self.selectedSort = selected
                self.refreshData()
            }
        }
    }
    func showFilterView() {
        
        
        let controller = FilterPopUpController()
        //.fixed(300),
        let sheetController = SheetViewController(controller: controller, sizes: [  .halfScreen, .fullScreen])
        // Adjust how the bottom safe area is handled on iPhone X screens
        sheetController.blurBottomSafeArea = false
        sheetController.adjustForBottomSafeArea = true
        sheetController.topCornersRadius = 10
        // Disable the dismiss on background tap functionality
        sheetController.dismissOnBackgroundTap = true
        // Extend the background behind the pull bar instead of having it transparent
        sheetController.extendBackgroundBehindHandle = true
        // Change the overlay color
        //sheetController.overlayColor = UIColor.red
        // Change the handle color
        sheetController.handleColor = UIColor.orange
        sheetController.handleSize = CGSize(width: 26, height: 2)
        sheetController.handleColor = ColorManager.color(forKey: "grayplaceholder")!
        sheetController.pullBarView.isHidden = true
        self.present(sheetController, animated: false, completion: nil)
        var array = [FilterModel]()
        for data in filterList {
            array.append(data.detached())
        }
        
       // let duplicateArray = filterList.map{$0.copy()} as! [FilterModel]
        controller.setData(dataList: array, type: .filter(type: .store))
        controller.didTapOnFilterClear = {sortArray in
            print(self.filterList)
            self.filterList.removeAll()
            self.filterList.append(contentsOf: sortArray)
            self.filterCounterLbl.isHidden = true
            self.refreshData()
        }
        controller.didTapOnFilterDone = {sortArray in
            
            self.filterList.removeAll()
            self.filterList.append(contentsOf: sortArray)
            print(self.filterList)
            var count =  0
            self.filterCounterLbl.isHidden = true
            self.filterList.forEach { (model) in
                if model.isSelected {
                    model.filters.forEach { (subModel) in
                        if(subModel.isSelected == true) {
                            count = count + 1
                        }
                    }
                }
                
            }
            if count > 0 {
                self.filterCounterLbl.isHidden = false
                self.filterCounterLbl.text = "\(count)"
            }
            self.refreshData()
        }
    }
    
    
    
}
//MARK:- Collection Datasource and Delegate
extension StoreProductListController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        guard headerView != nil else {
            return
        }
        if headerView.viewParallax != nil {
            headerView.viewParallax.scrollViewDidScroll(scrollView: scrollView)
           
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind.isEqual(UICollectionView.elementKindSectionHeader) {
            headerView  = (collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView)
            headerView.homeController = self
            headerView.clipsToBounds = false
            return headerView
        }
        
        return UICollectionReusableView()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
            
        switch filledData?.listType {
        case .author,.brand:
            if productList.count == 0 {
                return CGSize.zero
            }
            return CGSize(width: collectionView.frame.width, height: DesignUtility.getValueFromRatio(203)) //add your height here 265
        default:
            return CGSize.zero
        }
          
            
    }
    func showEmptyView() {
        
        var imageName = ""
        var message = ""
        var type : ListViewType = .normal
        switch self.filledData?.listType {
        case .wishlist:
            imageName = "emptywishlist"
            type = .wishlist
        case .author:
            imageName = "emptyproduct"
            type = .author
        default:
            imageName = "emptyproduct"
            print("")
        }
        if (isShowAnimationHeart == false) {
            return
        }
        if self.productList.count == 0 {
            UtilityEmptyListView.emptycollectionViewMessage(message: "", image: imageName, collectionView: storeCollectionView, listType: type)
            if let emptyView = storeCollectionView.backgroundView as? EmptyTableViewBackgroundView {
                emptyView.actionBtn.didTapOnBounceEnd = {
                    self.sideMenuController?.rootViewController = TabBarProvider.tabbarWithNavigationStyle(index: 0)
                }
            }
        }
        else {
            UtilityEmptyListView.hideEmptyCollectionViewMessage(collectionView: storeCollectionView)
        }
        
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        showEmptyView()
        return productList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoreProductCollectionCell.identifier(), for: indexPath) as! StoreProductCollectionCell
        let data = self.productList[indexPath.row]
        let position : ProductCellPosition = indexPath.row % 2 == 0 ? .left : .right
        cell.setData(data: data,cellPosition: position)
        cell.addToCartBtn.didTapOnBounceEnd = {
            self.addToCartOrNotifyMe(data: data,cell: cell,indexPath: indexPath,collectionView: collectionView)
        }
        cell.addOrRemoveFavoriteBtn.didTapOnBounceEnd = {
            if (self.filledData?.listType == ListViewType.wishlist) {
                self.addOrRemoveFavoriteWishList(data: data,cell: cell,indexPath: indexPath)
            }
            else {
                self.addOrRemoveFavorite(data: data, indexPath: indexPath, collectionView: collectionView)
            }
            
            //self.addOrRemoveFavorite(data: data,cell: cell,indexPath: indexPath)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let controller = ProductDetailController.loadVC()
        controller.selectedProduct = productList[indexPath.row]
        self.show(controller)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/2, height: DesignUtility.getValueFromRatio(298))
        //return CGSize(width: DesignUtility.getValueFromRatio(188), height: DesignUtility.getValueFromRatio(198))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
extension StoreProductListController {
    
    
    
    func addOrRemoveFavoriteWishList(data : ProductModel, cell : StoreProductCollectionCell, indexPath : IndexPath) {
        
        let param:[String:Any] = ["product_id": data.productId,"is_favorite":!data.isFavorite]
        
        print(param)
        StoreManager().addOrRemoveFavoriteApi(endPoint: ApiEndPoints.addToFavorite, param: param, header: [:], completion: { (response,msg,errorMsg) in
            
            
            print(response)
            switch self.filledData?.listType {
            case .wishlist:
                // data.isFavorite = !data.isFavorite
                self.storeCollectionView.performBatchUpdates({
                    self.isShowAnimationHeart = false
                    self.productList.remove(at: indexPath.row)
                    self.storeCollectionView.deleteItems(at: [indexPath])
                    
                }) { (finished) in
                    self.isShowAnimationHeart = true
                    self.storeCollectionView.reloadData()
                }
            default:
                data.isFavorite = !data.isFavorite
                self.storeCollectionView.reloadItems(at: [indexPath])
                print("")
            }
            if let data = response {
                
            }
            
        }){ (error) in
            // self.refreshView.endRefreshing()
            print("Error: ", error as Any)
        }
        
    }
    func  getStoreProductListApi() {
        
        var param:[String:Any] = ["page_number": currentPage,
        "page_size": 100000,
        "sort" : selectedSort!.id]
        
        var filterParam:[String:Any] = [:]//["values" : values, "key" : "6"]
        var filterArray = [Any]()
        print(filterArray.count)
        self.filterList.forEach { (model) in
            if model.isSelected {
                filterParam["key"] = String(model.filterEntityId)
                var array = [Int]()
                if(model.filterSelectionTypeId == FilterSelectionType.starRating.rawValue) {
                    array.append(model.filters[0].starRating)
                }
                else {
                    model.filters.forEach { (subModel) in
                        if(subModel.isSelected == true) {
                            array.append(subModel.id)
                        }
                    }
                }
                
                filterParam["values"] = array
                filterArray.append(filterParam)
            }
            
        }
        switch filledData?.screenFilterType {
        case .libraryGenre:
            print("")
            filterParam["key"] = String(FilterParamKey.genre.rawValue)
            filterParam["values"] = [selectedMainCategory!.categoryId]
            filterArray.append(filterParam)
        case .librarySection:
            filterParam["key"] = String(FilterParamKey.sections.rawValue)
            filterParam["values"] = [selectedMainCategory!.categoryId]
            filterArray.append(filterParam)
        case .author:
            filterParam["key"] = String(FilterParamKey.author.rawValue)
            filterParam["values"] = [selectedMainCategory!.categoryId]
            filterArray.append(filterParam)
        case .brand:
            filterParam["key"] = String(FilterParamKey.brands.rawValue)
            filterParam["values"] = [selectedMainCategory!.categoryId]
            filterArray.append(filterParam)
        case .bookPharmacy:
            filterParam["key"] = String(FilterParamKey.subjects.rawValue)
            filterParam["values"] = [selectedMainCategory!.categoryId]
            filterArray.append(filterParam)
        default:
            param["parent_category_id"] = "\(selectedMainCategory!.categoryId)"
            print("")
        }
        print(filterArray)
        print(filterArray.count)
        //let values = ["1", "2"]//"filter"
       // let param1:[String:Any] = ["values" : values, "key" : "6"]
        
        if filterArray.count > 0 {
            param["filter"] = filterArray
        }
        print(param)
        StoreManager().getStoreProductListApi(endPoint: ApiEndPoints.getProducts, param: param, header: [:], completion: { (response,msg,errorMsg) in
            //   self.categoryListTbl.isHidden = false
            print(response)
            self.storeCollectionView.isHidden = false
            //self.refreshView.endRefreshing()
            if let data = response {
                self.productList.append(contentsOf: data)
                if self.productList.count > 0 {
                    self.callHeader()
                }
                self.animateCollectionView(collectionView: self.storeCollectionView)
                //self.storeCollectionView.reloadData()
            }
            
        }){ (error) in
            self.storeCollectionView.isHidden = true
            //self.refreshView.endRefreshing()
            print("Error: ", error as Any)
        }
        
    }
    func getWishListApi() {
        
        
        StoreManager().getWishListApi(endPoint: ApiEndPoints.getUserWishList, param: nil, header: [:], completion: { (response,msg,errorMsg) in
            //   self.categoryListTbl.isHidden = false
            print(response)
            self.storeCollectionView.isHidden = false
            //self.refreshView.endRefreshing()
            if let data = response {
                self.productList.append(contentsOf: data)
                self.animateCollectionView(collectionView: self.storeCollectionView)
                //self.storeCollectionView.reloadData()
            }
            
        }){ (error) in
            self.storeCollectionView.isHidden = true
            //self.refreshView.endRefreshing()
            print("Error: ", error as Any)
        }
        
    }
    func getFilterListApi() {
        
        var filterId = 0
        if let id = filledData?.screenFilterType.rawValue {
            filterId = id
        }
//        switch filledData?.listType {
//        case .store:
//            filterId = 4
////        case .store:
////            filterId = 4
//        default:
//            print("")
//        }
        print(filterId)
        let param:[String:Any] = ["id": filterId]
        print(param)
        StoreManager().getFilterListApi(endPoint: ApiEndPoints.getScreenFilterbyScreenId, param: param, header: [:], completion: { (response,msg,errorMsg) in
            //   self.categoryListTbl.isHidden = false
            print(response)
         //   self.storeCollectionView.isHidden = false
            //self.refreshView.endRefreshing()
            if let data = response {
                self.filterList.append(contentsOf: data)
               // self.animateCollectionView(collectionView: self.storeCollectionView)
                //self.storeCollectionView.reloadData()
            }
            
        }){ (error) in
           // self.storeCollectionView.isHidden = true
           // self.refreshView.endRefreshing()
            print("Error: ", error as Any)
        }
        
    }
}
extension StoreProductListController: BottomPopupDelegate {
    
    func bottomPopupViewLoaded() {
        print("bottomPopupViewLoaded")
    }
    
    func bottomPopupWillAppear() {
        print("bottomPopupWillAppear")
    }
    
    func bottomPopupDidAppear() {
        print("bottomPopupDidAppear")
    }
    
    func bottomPopupWillDismiss() {
        print("bottomPopupWillDismiss")
    }
    
    func bottomPopupDidDismiss() {
        print("bottomPopupDidDismiss")
    }
    
    func bottomPopupDismissInteractionPercentChanged(from oldValue: CGFloat, to newValue: CGFloat) {
        print("bottomPopupDismissInteractionPercentChanged fromValue: \(oldValue) to: \(newValue)")
    }
}
