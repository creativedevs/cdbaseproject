//
//  StoreCategoryListController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import ViewAnimator
class StoreSubCategoryListController: BaseViewController , StoryBoardHandler {

    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.store.rawValue , nil)
    var categoryList = [CategoryModel]()
    var selectedMainCategory : CategoryModel?
    @IBOutlet weak var categoryListTbl: BaseUITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryListTbl.isHidden = true
        self.navigationItem.title = selectedMainCategory?.title
        createBackBtn()
        createRefreshView(tblView: categoryListTbl)
        setUpTable()
        self.getCategoryListApi()
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarheart")!, .BarButtonItemPositionLeft, self, #selector(showWishListScreen))
        
         self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarsearch")!, .BarButtonItemPositionRight, self, #selector(showSearchScreen))
        
       
    }
    
    func setUpTable(){
        
        categoryListTbl.registerCell(StoreSubCategoryCell.self)
    }
    override func refreshData() {
        print("refresh")
        self.currentPage = 1
        self.categoryList.removeAll()
        self.getCategoryListApi()
       // self.refreshView.endRefreshing()
    }

}
extension StoreSubCategoryListController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return 10
        if self.categoryList.count == 0 {
            UtilityEmptyListView.emptyTableViewMessage(message: "test", image: "emptylist", tableView: tableView, listType: .store)
            if let emptyView = tableView.backgroundView as? EmptyTableViewBackgroundView {
                emptyView.actionBtn.didTapOnBounceEnd = {
                    //self.show(AddAddressController.loadVC())
                }
            }
            return 0
        }
        UtilityEmptyListView.hideEmptyTableViewMessage(tableView: tableView)
        return self.categoryList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: StoreSubCategoryCell.identifier()) as! StoreSubCategoryCell
        cell.setData(data: self.categoryList[indexPath.row])
//        cell.didTapOnMarkDefault = { data in
//            self.defaultAddressApi(data: data)
//        }
//        cell.didTapOnDelete = { data in
//            self.deleteAddressApi(data: data)
//        }
//        cell.didTapOnEdit = { data in
//            let address = AddAddressController.loadVC()
//            address.isEdit = true
//            address.currentAddress = data
//            self.show(address)
//        }
        return cell
    }
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 56
//    }
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressFooterTableViewCell") as! AddressFooterTableViewCell
//        cell.delegate = self
//
//        return cell
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = categoryList[indexPath.row]
        if (data.hasChild) {
            let controller = StoreSubCategoryListController.loadVC()
            controller.selectedMainCategory = data
            self.show(controller)
            
        }
        else {
            let controller = StoreProductListController.loadVC()
            controller.selectedMainCategory = data
            controller.filledData = ( data.title, .store, .store)
            self.show(controller)
        }
        
        
    }
    
}
extension StoreSubCategoryListController {
    
    func getCategoryListApi() {
            
            let param:[String:Any] = ["page_number": currentPage,
                                      "page_size": 100000]
        StoreManager().getCategoryApi(endPoint: ApiEndPoints.getCategoriesByParentId + "/\(selectedMainCategory!.categoryId)", param: nil, header: [:], completion: { (response,msg,errorMsg) in
                self.categoryListTbl.isHidden = false
                print(response)
                self.refreshView.endRefreshing()
                if let data = response {
                    self.categoryList.append(contentsOf: data)
                    self.animateTableViewFromZoom(tblView: self.categoryListTbl)
                    //self.categoryListTbl.reloadData()
                }

            }){ (error) in
                self.refreshView.endRefreshing()
                    print("Error: ", error as Any)
                }
            
        }
    
    
}
