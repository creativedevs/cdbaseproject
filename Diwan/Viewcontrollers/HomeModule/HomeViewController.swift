//
//  HomeViewController.swift
//  Volchain
//
//  Created by Waqas Ali on 15/04/2019.
//  Copyright © 2019 Waqas Ali. All rights reserved.
//

import UIKit

enum HomeViewCellType : Int {
    case topBanner = 0
    case celebrity = 1
    case bestSeller = 2
    case middleBanner1 = 3
    case pharmacy = 4
    case attributesList = 5
    case diwanTV = 6
    case popularBrands = 7
}
class HomeViewController: BaseViewController, StoryBoardHandler {
    
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.home.rawValue, nil)
    @IBOutlet weak var tableView: BaseUITableView!
    var topBanners = [ProductMediaDetailModel]()
    var middleBanners = [ProductMediaDetailModel]()
    var celebrityList = [User]()
    var attributeProductList = [AttributeWiseProductModel]()
    var bestSellerProductList = [AttributeWiseProductModel]()
    var ehsanVideoList = [EhsanModel]()
    var brandList = [BrandModel]()
    var pharmacyList = [CategoryModel]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setStatusBarStyle(.lightContent)
        self.setNavBGColor()
        self.createNavSideMenuBtn()
        
        setupAppDefaultNavigationBar()
        setUpTable()
        setupColorNavBarWithLogo()
        tableView.reloadData()
        self.getBannerImagesApi()
        self.addBarButtonItemWithImage(UIImage(named: "topbarheart")!, .BarButtonItemPositionLeft, self, #selector(showWishListScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarsearch")!, .BarButtonItemPositionRight, self, #selector(showSearchScreen))
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }

    
    func setUpTable(){
        
        tableView.registerCell(HomeBannerCell.self)
        tableView.registerCell(HomeCelebrityCell.self)
        tableView.registerCell(PharmacyCategoryProductCell.self)
        tableView.registerCell(VideoCollectionViewCell.self)
        tableView.registerCell(BrandCollectionViewCell.self)
        tableView.registerCell(PharmacyHomeCollectionViewCell.self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension HomeViewController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        var count = 8
//        if brandList.count <= 0 {
//            count = count - 1
//        }
//        if ehsanVideoList.count <= 0 {
//            count = count - 1
//        }
        return count
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == HomeViewCellType.topBanner.rawValue ||
            section == HomeViewCellType.celebrity.rawValue ||
            section == HomeViewCellType.middleBanner1.rawValue {
            
            return 1
            
        }
        else if section == HomeViewCellType.bestSeller.rawValue {
            return self.bestSellerProductList.count
        }
        else if section == HomeViewCellType.attributesList.rawValue {
            return self.attributeProductList.count
        }
        else if section == HomeViewCellType.popularBrands.rawValue {
            return self.brandList.count > 0 ? 1 : 0
        }
        else if section == HomeViewCellType.diwanTV.rawValue {
            return self.ehsanVideoList.count > 0 ? 1 : 0
        }
        else if section == HomeViewCellType.pharmacy.rawValue {
            return self.pharmacyList.count > 0 ? 1 : 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case HomeViewCellType.topBanner.rawValue:
            return DesignUtility.getValueFromRatio(202)
        case HomeViewCellType.middleBanner1.rawValue:
            return DesignUtility.getValueFromRatio(285)
        default:
            return UITableView.automaticDimension
        }
        
        
        //return UITableView.automaticDimension
    }
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 76
//    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case HomeViewCellType.topBanner.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeBannerCell.identifier()) as! HomeBannerCell
            cell.setHomeTopData(data: topBanners, type: .homeTop)
            return cell
        case HomeViewCellType.celebrity.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeCelebrityCell.identifier()) as! HomeCelebrityCell
            cell.viewAllBtn.didTapOnBounceEnd = {
                let controller = CelebritiesMainListController.loadVC()
                self.show(controller)
            }
            return cell
        case HomeViewCellType.bestSeller.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyCategoryProductCell.identifier()) as! PharmacyCategoryProductCell
            let data = self.bestSellerProductList[indexPath.row]
            cell.setAttributedHomeData(data: data)
            return cell
        case HomeViewCellType.middleBanner1.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeBannerCell.identifier()) as! HomeBannerCell
            cell.setHomeMiddleData(data: middleBanners, type: .homeMiddle1)
            return cell
        case HomeViewCellType.pharmacy.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyHomeCollectionViewCell.identifier()) as! PharmacyHomeCollectionViewCell
            cell.setHomeData()
            cell.viewAllBtn.didTapOnBounceEnd = {
                let controller = PharmacyMainCategoryController.loadVC()
                self.show(controller)
            }
            return cell
        case HomeViewCellType.attributesList.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyCategoryProductCell.identifier()) as! PharmacyCategoryProductCell
            let data = self.attributeProductList[indexPath.row]
            cell.setAttributedHomeData(data: data)
            return cell
        case HomeViewCellType.diwanTV.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: VideoCollectionViewCell.identifier()) as! VideoCollectionViewCell
            cell.setHomeData()
            cell.viewAllBtn.didTapOnBounceEnd = {
                let controller = EhsanMainListController.loadVC()
                controller.videoType = .diwanTV
                self.show(controller)
            }
            return cell
        case HomeViewCellType.popularBrands.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: BrandCollectionViewCell.identifier()) as! BrandCollectionViewCell
            cell.setHomeData()
            cell.viewAllBtn.didTapOnBounceEnd = {
                let controller = BrandsMainListController.loadVC()
                controller.isPopular = true
                self.show(controller)
            }
            return cell
        
        default:
            return UITableViewCell()
        }
        
       // cell.setData(data: self.categoryList[indexPath.row])

       
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == HomeViewCellType.celebrity.rawValue {
            guard let tableViewCell = cell as? HomeCelebrityCell else { return }

            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section,subTag: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.section] ?? 0
        }
        else if indexPath.section == HomeViewCellType.bestSeller.rawValue {
            guard let tableViewCell = cell as? PharmacyCategoryProductCell else { return }

            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section,subTag: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }
        else if indexPath.section == HomeViewCellType.pharmacy.rawValue {
                guard let tableViewCell = cell as? PharmacyHomeCollectionViewCell else { return }
                tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
                tableViewCell.collectionViewOffset = storedOffsets[indexPath.section] ?? 0
            }
        else if indexPath.section == HomeViewCellType.attributesList.rawValue {
                guard let tableViewCell = cell as? PharmacyCategoryProductCell else { return }

                tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section,subTag: indexPath.row)
                tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
            }
        else if indexPath.section == HomeViewCellType.diwanTV.rawValue {
            guard let tableViewCell = cell as? VideoCollectionViewCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.section] ?? 0
        }
        else if indexPath.section == HomeViewCellType.popularBrands.rawValue {
            guard let tableViewCell = cell as? BrandCollectionViewCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.section] ?? 0
        }
        
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == HomeViewCellType.celebrity.rawValue {
            guard let tableViewCell = cell as? HomeCelebrityCell else { return }
            storedOffsets[indexPath.section] = tableViewCell.collectionViewOffset
        }
        else if indexPath.section == HomeViewCellType.bestSeller.rawValue {
            guard let tableViewCell = cell as? PharmacyCategoryProductCell else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }
        else if indexPath.section == HomeViewCellType.pharmacy.rawValue {
                guard let tableViewCell = cell as? PharmacyHomeCollectionViewCell else { return }
                storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
            }
        else if indexPath.section == HomeViewCellType.attributesList.rawValue {
                guard let tableViewCell = cell as? PharmacyCategoryProductCell else { return }
                storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
            }
        else if indexPath.section == HomeViewCellType.diwanTV.rawValue {
            guard let tableViewCell = cell as? VideoCollectionViewCell else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }
        else if indexPath.section == HomeViewCellType.popularBrands.rawValue {
            guard let tableViewCell = cell as? BrandCollectionViewCell else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }
        
    }
}
//MARK:- Collection Datasource and Delegate
extension HomeViewController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case HomeViewCellType.celebrity.rawValue:
            return celebrityList.count
        case HomeViewCellType.bestSeller.rawValue:
            let collection = collectionView as! BaseUICollectionView
            return bestSellerProductList[collection.collectionSubTag].productlist.count
        case HomeViewCellType.pharmacy.rawValue:
            return pharmacyList.count > 4 ? 4 : pharmacyList.count
        case HomeViewCellType.attributesList.rawValue:
            let collection = collectionView as! BaseUICollectionView
            return attributeProductList[collection.collectionSubTag].productlist.count
        case HomeViewCellType.diwanTV.rawValue:
            return ehsanVideoList.count
        case HomeViewCellType.popularBrands.rawValue:
            return brandList.count
        
            
        default:
            print("")
        }
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView.tag {
        case HomeViewCellType.celebrity.rawValue:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AuthorMainCollectionCell.identifier(), for: indexPath) as! AuthorMainCollectionCell
                    let data = self.celebrityList[indexPath.row]
            //        let position : ProductCellPosition = indexPath.row % 2 == 0 ? .left : .right
                    print(indexPath.row)
                    cell.setCelebrityData(data: data)
                    return cell
        case HomeViewCellType.bestSeller.rawValue :
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoreProductCollectionCell.identifier(), for: indexPath) as! StoreProductCollectionCell
            let collection = collectionView as! BaseUICollectionView
            let data = self.bestSellerProductList[collection.collectionSubTag].productlist[indexPath.item]
            cell.setData(data: data,cellPosition: ProductCellPosition.normal)
            
            cell.addToCartBtn.didTapOnBounceEnd = {
               // self.addToCartOrNotifyMe(data: data,cell: cell,indexPath: indexPath)
            }
            cell.addOrRemoveFavoriteBtn.didTapOnBounceEnd = {
                self.addOrRemoveFavorite(data: data, indexPath: indexPath, collectionView: collectionView)
                //self.addOrRemoveFavorite(data: data,cell: cell,indexPath: indexPath)
            }
            return cell
        case HomeViewCellType.pharmacy.rawValue:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PharmacyCategoryCollectionCell.identifier(), for: indexPath) as! PharmacyCategoryCollectionCell
            cell.setData(data: self.pharmacyList[indexPath.row])
            return cell
        case HomeViewCellType.attributesList.rawValue :
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoreProductCollectionCell.identifier(), for: indexPath) as! StoreProductCollectionCell
            let collection = collectionView as! BaseUICollectionView
            let data = self.attributeProductList[collection.collectionSubTag].productlist[indexPath.item]
            cell.setData(data: data,cellPosition: ProductCellPosition.normal)
            
            cell.addToCartBtn.didTapOnBounceEnd = {
                self.addToCartOrNotifyMe(data: data,cell: cell,indexPath: indexPath,collectionView: collectionView)
            }
            cell.addOrRemoveFavoriteBtn.didTapOnBounceEnd = {
                self.addOrRemoveFavorite(data: data, indexPath: indexPath, collectionView: collectionView)
                //self.addOrRemoveFavorite(data: data,cell: cell,indexPath: indexPath)
            }
            return cell
        case HomeViewCellType.diwanTV.rawValue:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EhsanMainCollectionCell.identifier(), for: indexPath) as! EhsanMainCollectionCell
            let data = ehsanVideoList[indexPath.item]
            cell.setData(data: data )
            return cell
        case HomeViewCellType.popularBrands.rawValue:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BrandCollectionCell.identifier(), for: indexPath) as! BrandCollectionCell
            let data = brandList[indexPath.item]
            cell.setData(data: data )
            return cell
        
        default:
            print("")
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch collectionView.tag {
        case HomeViewCellType.celebrity.rawValue:
            let width = collectionView.frame.size.width - 75
            return CGSize(width: width/2, height: DesignUtility.getValueFromRatio(184))
        case HomeViewCellType.bestSeller.rawValue:
            return CGSize(width: DesignUtility.getValueFromRatio(162), height: DesignUtility.getValueFromRatio(298))
        case HomeViewCellType.pharmacy.rawValue:
            let width = (collectionView.frame.size.width - 34)/2
            return CGSize(width: width, height: DesignUtility.getValueFromRatio(107))
        case HomeViewCellType.attributesList.rawValue:
            return CGSize(width: DesignUtility.getValueFromRatio(162), height: DesignUtility.getValueFromRatio(298))
            
        case HomeViewCellType.diwanTV.rawValue:
            let width = collectionView.frame.size.width - 42
            return CGSize(width: width/2, height: DesignUtility.getValueFromRatio(135))
        case HomeViewCellType.popularBrands.rawValue:
            return CGSize(width: DesignUtility.getValueFromRatio(100), height: DesignUtility.getValueFromRatio(100))
        
        default:
            print("")
        }
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch collectionView.tag {
        case HomeViewCellType.celebrity.rawValue:
            return UIEdgeInsets(top: 12, left: 12, bottom: 20, right: 0)
        case HomeViewCellType.bestSeller.rawValue:
            return UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 0)
        case HomeViewCellType.pharmacy.rawValue:
            return UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        case HomeViewCellType.attributesList.rawValue:
            return UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 0)
        case HomeViewCellType.diwanTV.rawValue:
            return UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 0)
        case HomeViewCellType.popularBrands.rawValue:
            return UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 0)
        
        default:
            print("")
        }
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        switch collectionView.tag {
        case HomeViewCellType.celebrity.rawValue:
             return 15
        case HomeViewCellType.pharmacy.rawValue:
            return 10
        case HomeViewCellType.diwanTV.rawValue:
            return 10
        case HomeViewCellType.popularBrands.rawValue:
            return 10
        
        default:
            print("")
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        switch collectionView.tag {
        case HomeViewCellType.celebrity.rawValue:
            return 15
        default:
            print("")
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch collectionView.tag {
        case HomeViewCellType.celebrity.rawValue:
            let controller = CelebrityDetailController.loadVC()
            controller.selectedCelebrity = celebrityList[indexPath.row]
            self.show(controller)
//            self.alertNextPhase()
//            print("")
        case HomeViewCellType.bestSeller.rawValue:
            let collection = collectionView as! BaseUICollectionView
            let data = self.bestSellerProductList[collection.collectionSubTag].productlist[indexPath.item]
            let controller = ProductDetailController.loadVC()
            controller.selectedProduct = data
            self.show(controller)
        case HomeViewCellType.pharmacy.rawValue:
            let data = pharmacyList[indexPath.item]
            let controller = PharmacySubCategoryController.loadVC()
            controller.selectedMainCategory = data
            self.show(controller)
        case HomeViewCellType.attributesList.rawValue:
            let collection = collectionView as! BaseUICollectionView
            let data = self.attributeProductList[collection.collectionSubTag].productlist[indexPath.item]
            let controller = ProductDetailController.loadVC()
            controller.selectedProduct = data
            self.show(controller)
        case HomeViewCellType.diwanTV.rawValue:
            let controller = EhsanDetailController.loadVC()
            controller.ehsanVideo = self.ehsanVideoList[indexPath.row]
            self.show(controller)
        case HomeViewCellType.popularBrands.rawValue:
            let category = CategoryModel()
            let controller = StoreProductListController.loadVC()
            let data = self.brandList[indexPath.row]
            category.categoryId = data.Id
            category.title = data.title
            category.imageIcon = data.imageUrl
            controller.selectedMainCategory = category
            controller.selectedBrand = data
            controller.filledData = ( category.title, .brand, .brand )
            self.show(controller)
        default:
            print("")
        }
        
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: collectionView.frame.width, height: DesignUtility.getValueFromRatio(156)) //add your height here
//    }
}

//MARK:- API's
extension HomeViewController {
    
    func getBannerImagesApi() {
            let param:[String:Any] = ["page_number": currentPage,
                                      "page_size": 100000]
            HomeManager().getHomeBannerApi(endPoint: ApiEndPoints.getHomeMainData, param: nil, header: [:], completion: { (response,msg,errorMsg) in
                //self.categoryListTbl.isHidden = false
                //print(response)
                //self.refreshView.endRefreshing()
                if let topBanner = response.0 {
                    self.topBanners.append(contentsOf: topBanner)
                    //self.animateTableView(tblView: self.categoryListTbl)
                    //self.categoryListTbl.reloadData()
                }
                if let middleBanner = response.1 {
                    self.middleBanners.append(contentsOf: middleBanner)
                    //self.animateTableView(tblView: self.categoryListTbl)
                    //self.categoryListTbl.reloadData()
                }
                if let users = response.2 {
                    self.celebrityList.append(contentsOf: users)
                    //self.animateTableView(tblView: self.categoryListTbl)
                    //self.categoryListTbl.reloadData()
                }
                self.getAttributeWiseProductApi(url: ApiEndPoints.getHomeAttributeData)
                self.tableView.reloadData()

            }){ (error) in
                self.refreshView.endRefreshing()
                    print("Error: ", error as Any)
                }
            
        }
    func getAttributeWiseProductApi(url : String) {
        let param:[String:Any] = ["page_number": currentPage,
                                  "page_size": 100000]
        HomeManager().getAttributeTagWiseProductListApi(endPoint: url, param: nil, header: [:], completion: { (response,msg,errorMsg) in
            //self.categoryListTbl.isHidden = false
            //print(response)
            //self.refreshView.endRefreshing()
            if let data = response {
                self.attributeProductList.append(contentsOf: data)
                self.attributeProductList = self.attributeProductList.filter({$0.productlist.count > 0})
                print(self.attributeProductList)
                if (url == ApiEndPoints.getHomeAttributeData) {
                    self.bestSellerProductList = self.attributeProductList.filter({$0.attributeId == 3})
                    print(self.bestSellerProductList)
                    print(self.attributeProductList.count)
                    self.attributeProductList.removeAll {$0.attributeId == 3}
                    print(self.bestSellerProductList)
                    print(self.attributeProductList.count)
                    self.getAttributeWiseProductApi(url: ApiEndPoints.getHomeTagsData)
                }
                else if (url == ApiEndPoints.getHomeTagsData) {
                    self.getPharmacyListApi()
                    
                }
                //self.animateTableView(tblView: self.categoryListTbl)
                //self.categoryListTbl.reloadData()
                
                
            }
            self.tableView.reloadData()

        }){ (error) in
            //self.refreshView.endRefreshing()
                print("Error: ", error as Any)
            }
        
    }
    func getPharmacyListApi() {
        
        let param:[String:Any] = ["page_number": currentPage,
                                  "page_size": 100000]
        StoreManager().getCategoryApi(endPoint: ApiEndPoints.getCategoriesByParentId + "/1", param: nil, header: [:], completion: { (response,msg,errorMsg) in
            print(response)
           // self.refreshView.endRefreshing()
            if let data = response {
                self.pharmacyList.append(contentsOf: data)
            }
            self.tableView.reloadData()
            self.getEhsanVideoListApi()
        }){ (error) in
            self.refreshView.endRefreshing()
                print("Error: ", error as Any)
            }
        
    }
    func getEhsanVideoListApi() {
      let param:[String:Any] = ["page_number": currentPage,
                                "page_size": 100000]
        EhsanManager().getEhsanVideoListApi(endPoint: ApiEndPoints.getVideos, param: param, header: [:], completion: { (response,msg,errorMsg) in
            //self.collectionView.isHidden = false
            print(response)
            //self.refreshView.endRefreshing()
            if let data = response {
                self.ehsanVideoList.append(contentsOf: data)
                self.tableView.reloadData()
                self.getBrandListApi()
            }

        }){ (error) in
            //self.collectionView.isHidden = false
           // self.refreshView.endRefreshing()
                print("Error: ", error as Any)
            }
        
    }
    func getBrandListApi() {
      let param:[String:Any] = ["page_number": currentPage,
                                "page_size": 100000,
                                //"search_keyword" : "",
                                "sort_start_with":"all",
                                "is_popular" : true]
    
        BrandManager().getBrandListApi(endPoint: ApiEndPoints.getBrands, param: param, header: [:], completion: { (response,msg,errorMsg) in
            //self.tableView.isHidden = false
            print(response)
            //self.refreshView.endRefreshing()
            if let data = response {
                self.brandList.append(contentsOf: data)
                self.tableView.reloadData()
            }

        }){ (error) in
            self.tableView.isHidden = false
           // self.refreshView.endRefreshing()
                print("Error: ", error as Any)
            }
        
    }
    
    
}
