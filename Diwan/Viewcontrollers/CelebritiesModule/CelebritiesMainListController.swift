//
//  CelebritiesMainListController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 08/07/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class CelebritiesMainListController: BaseViewController , StoryBoardHandler {

    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.celebrities.rawValue , nil)
    @IBOutlet weak var tableView: BaseUITableView!
    var celebritiesList = [User]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.isHidden = true
        self.navigationItem.title = "Celebrities".localized
        setupAppDefaultNavigationBar()
        createNavSideMenuBtn()
        createBackBtn()
        //createRefreshView(tblView: categoryListTbl)
        setUpTable()
        self.getCelebrityListApi()
        self.addBarButtonItemWithImage(UIImage(named: "topbarheart")!, .BarButtonItemPositionLeft, self, #selector(showWishListScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarsearch")!, .BarButtonItemPositionRight, self, #selector(showSearchScreen))
    }
    
    func setUpTable(){
        
        tableView.registerCell(CelebrityMainCell.self)
    }
    override func refreshData() {
        print("refresh")
        self.currentPage = 1
        self.celebritiesList.removeAll()
        //self.getAuthorListApi()
       // self.refreshView.endRefreshing()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK:- TableView Datasource and Delegate
extension CelebritiesMainListController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return 10
        if self.celebritiesList.count == 0 {
            UtilityEmptyListView.emptyTableViewMessage(message: "test", image: nil, tableView: tableView, listType: .celebrity)
            if let emptyView = tableView.backgroundView as? EmptyTableViewBackgroundView {
                emptyView.actionBtn.didTapOnBounceEnd = {
                    //self.show(AddAddressController.loadVC())
                }
            }
            return 0
        }
        UtilityEmptyListView.hideEmptyTableViewMessage(tableView: tableView)
        return self.celebritiesList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CelebrityMainCell.identifier()) as! CelebrityMainCell
        cell.setData(data: self.celebritiesList[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //self.alertNextPhase()
        let controller = CelebrityDetailController.loadVC()
        controller.selectedCelebrity = celebritiesList[indexPath.row]
        self.show(controller)
        
    /*
        let data = categoryList[indexPath.row]
        if (data.hasChild) {
            let controller = StoreSubCategoryListController.loadVC()
            controller.selectedMainCategory = categoryList[indexPath.row]
            self.show(controller)
        }
        else {
            let controller = StoreProductListController.loadVC()
            controller.selectedMainCategory = data
            controller.filledData = ( data.title, .store)
            self.show(controller)
        }
        */
        
    }
    
}
extension CelebritiesMainListController {
    
    func getCelebrityListApi() {
        let param:[String:Any] = ["page_number": currentPage,
                                  "page_size": 100000]
        
            CelebrityManager().getCelebrityListApi(endPoint: ApiEndPoints.getCelebrities, param: param, header: [:], completion: { (response,msg,errorMsg) in
              //  self.collectionView.isHidden = false
                print(response)
                //self.refreshView.endRefreshing()
                if let data = response {
                    self.celebritiesList.append(contentsOf: data)
                    let delay = self.isFirstTimeLoad == true ? 0.2 : 0.0
                    Utility.delay(seconds: delay) {
                        self.tableView.isHidden = false
                         self.animateTableView(tblView: self.tableView)
                    }
                   // self.collectionView.reloadData()
                }

            }){ (error) in
                self.tableView.isHidden = false
               // self.refreshView.endRefreshing()
                    print("Error: ", error as Any)
                }
            
        }
    
    
}
