//
//  PharmacyCategoryProductController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 25/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import ImageSlideshow
import Kingfisher
enum CelebrityDetailCellType : Int {
    case userPicture = 0
    case products = 1
    case userTV = 2
}
class CelebrityDetailController:  BaseViewController , StoryBoardHandler {

   static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.celebrities.rawValue , nil)

    @IBOutlet weak var tableView: BaseUITableView!
    var selectedCelebrity : User?
    var categoryList = [CategoryWiseProductModel]()
    var myVideoList = [EhsanModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTable()
        //categoryListTbl.isHidden = true
        self.navigationItem.title = selectedCelebrity?.fullName
        createBackBtn()
        //createRefreshView(tblView: categoryListTbl)/
        self.getCelebrityDetailApi()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //setupHeaderData()
    }
    func setUpTable(){
       // let parallaxViewFrame = CGRect(x: 0, y: 0, width: self.tableView.bounds.width, height: DesignUtility.getValueFromRatio(265))
       // tableView.tableHeaderView  = ParallaxHeaderView(frame: parallaxViewFrame)
        tableView.registerCell(CelebrityMainCell.self)
        tableView.registerCell(VideoCollectionViewCell.self)
        tableView.registerCell(PharmacyCategoryProductCell.self)
    }
    func setupHeaderData() {
//        var source = [InputSource]()
//        if let url = selectedMainCategory?.imageIcon.getValidUrlString() {
//            source.append(KingfisherSource(urlString: url)!)
//        }
//        let headerView = self.tableView.tableHeaderView as! ParallaxHeaderView
//        headerView.setSliderImages(images: source)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CelebrityDetailController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == CelebrityDetailCellType.userPicture.rawValue {
            return 1
        }
        else if section == CelebrityDetailCellType.products.rawValue {
               return self.categoryList.count
            }
        else if section == CelebrityDetailCellType.userTV.rawValue {
            return 1
        }
        return 0
//        if self.categoryList.count == 0 {
//            UtilityEmptyListView.emptyTableViewMessage(message: "test", image: nil, tableView: tableView, listType: .store)
//            if let emptyView = tableView.backgroundView as? EmptyTableViewBackgroundView {
//                emptyView.actionBtn.didTapOnBounceEnd = {
//                    //self.show(AddAddressController.loadVC())
//                }
//            }
//            return 0
//        }
//        UtilityEmptyListView.hideEmptyTableViewMessage(tableView: tableView)
//        return self.categoryList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case CelebrityDetailCellType.userPicture.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: CelebrityMainCell.identifier()) as! CelebrityMainCell
            cell.setCelebrityDetailData(data: self.selectedCelebrity!)
            return cell
        case CelebrityDetailCellType.products.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyCategoryProductCell.identifier()) as! PharmacyCategoryProductCell
            let data = self.categoryList[indexPath.row]
            print(data)
            cell.setData(data: data)
            cell.viewAllBtn.didTapOnBounceEnd = {
                
                let category = CategoryModel()
                category.categoryId = data.categoryId
                category.title = data.title
                let controller = StoreProductListController.loadVC()
                controller.selectedMainCategory = category
                controller.filledData = ( category.title, .celebrity, .bookPharmacy)
                self.show(controller)
               // self.addToCartOrNotifyMe(data: data,cell: cell,indexPath: indexPath)
            }
            return cell
        case CelebrityDetailCellType.userTV.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: VideoCollectionViewCell.identifier()) as! VideoCollectionViewCell
            cell.setCelebrityDetailData()
            return cell
        default:
            print("")
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == CelebrityDetailCellType.userTV.rawValue {
            guard let tableViewCell = cell as? VideoCollectionViewCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section,subTag: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.section] ?? 0
        }
        else if indexPath.section == CelebrityDetailCellType.products.rawValue {
            guard let tableViewCell = cell as? PharmacyCategoryProductCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section,subTag: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.section] ?? 0
        }
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == CelebrityDetailCellType.userTV.rawValue {
            guard let tableViewCell = cell as? VideoCollectionViewCell else { return }
            storedOffsets[indexPath.section] = tableViewCell.collectionViewOffset
        }
        else if indexPath.section == CelebrityDetailCellType.products.rawValue {
            guard let tableViewCell = cell as? PharmacyCategoryProductCell else { return }
            storedOffsets[indexPath.section] = tableViewCell.collectionViewOffset
        }
        
    }
}

//MARK:- Collection Datasource and Delegate
extension CelebrityDetailController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
  
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case CelebrityDetailCellType.userTV.rawValue:
            return myVideoList.count
        case CelebrityDetailCellType.products.rawValue:
            let collection = collectionView as! BaseUICollectionView
            return self.categoryList[collection.collectionSubTag].products.count
        default:
            print("")
        }
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView.tag {
            case CelebrityDetailCellType.userTV.rawValue:
                 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EhsanMainCollectionCell.identifier(), for: indexPath) as! EhsanMainCollectionCell
                           let data = myVideoList[indexPath.item]
                           cell.setData(data: data )
                           return cell
            case CelebrityDetailCellType.products.rawValue:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoreProductCollectionCell.identifier(), for: indexPath) as! StoreProductCollectionCell
                let collection = collectionView as! BaseUICollectionView
                let data = self.categoryList[collection.collectionSubTag].products[indexPath.item]
                //var data = ProductModel()
                cell.setData(data: data,cellPosition: ProductCellPosition.normal)
                cell.addToCartBtn.didTapOnBounceEnd = {
                    // self.addToCartOrNotifyMe(data: data,cell: cell,indexPath: indexPath)
                }
                cell.addOrRemoveFavoriteBtn.didTapOnBounceEnd = {
                    self.addOrRemoveFavorite(data: data, indexPath: indexPath, collectionView: collectionView)
                
                    //self.addOrRemoveFavorite(data: data,cell: cell,indexPath: indexPath)
                }
                return cell
            default:
                print("")
        }
        
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           //
           switch collectionView.tag {
               case CelebrityDetailCellType.userTV.rawValue:
                    let width = collectionView.frame.size.width - 42
                    return CGSize(width: width/2, height: DesignUtility.getValueFromRatio(135))
            case CelebrityDetailCellType.products.rawValue:
                return CGSize(width: DesignUtility.getValueFromRatio(162), height: DesignUtility.getValueFromRatio(298))
               default:
                   print("")
           }
            return CGSize.zero
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
          
        switch collectionView.tag {
               case CelebrityDetailCellType.userTV.rawValue:
                    return UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 0)
                case CelebrityDetailCellType.products.rawValue:
                    return UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 0)
               default:
                   print("")
           }
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
       }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
            switch collectionView.tag {
                case CelebrityDetailCellType.userTV.rawValue:
                    return 10
                default:
                    print("")
        }
           return 0
       }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        
        switch collectionView.tag {
            case CelebrityDetailCellType.userTV.rawValue:
                let controller = EhsanDetailController.loadVC()
                controller.ehsanVideo = self.myVideoList[indexPath.row]
                self.show(controller)
            case CelebrityDetailCellType.products.rawValue:
                let collection = collectionView as! BaseUICollectionView
                let data = self.categoryList[collection.collectionSubTag].products[indexPath.item]
                 let controller = ProductDetailController.loadVC()
                 controller.selectedProduct = data
                 self.show(controller)
            default:
                print("")
        }
    }
}
//MARK:-API's
extension CelebrityDetailController {
    
    func getCelebrityDetailApi() {
        
        CelebrityManager().getCelebrityDetailApi(endPoint: ApiEndPoints.getCelebritiesDetail + "\(selectedCelebrity!.id)", param: nil, header: [:], completion: { (response,msg,errorMsg) in
            //self.storeCollectionView.isHidden = false
            print(response)
           // self.refreshView.endRefreshing()
            if let data = response.0 {
                self.selectedCelebrity = data
                //self.categoryList.append(contentsOf: data)
                //self.categoryList = self.categoryList.filter({$0.totalCount > 0})
                //self.categoryList.append(contentsOf: data)
                
            }
            if let data = response.1 {
                self.myVideoList.append(contentsOf: data)
                //self.categoryList = self.categoryList.filter({$0.totalCount > 0})
                //self.categoryList.append(contentsOf: data)
            }
            if let data = response.2 {
                self.categoryList.append(contentsOf: data)
                //self.categoryList = self.categoryList.filter({$0.totalCount > 0})
                //self.categoryList.append(contentsOf: data)
            }
            self.tableView.reloadData()
        }){ (error) in
           // self.refreshView.endRefreshing()
                print("Error: ", error as Any)
            }
        
    }
    
}
