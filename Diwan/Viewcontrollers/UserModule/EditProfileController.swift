//
//  EditProfileController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 08/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import FlagPhoneNumber
import ActionSheetPicker_3_0
class EditProfileController: BaseViewController , StoryBoardHandler {

    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.userModule.rawValue , nil)
    
    @IBOutlet weak var viewContainerForm: BaseUIView!
    @IBOutlet weak var btnSkip: BaseUIButton!
    
    @IBOutlet weak var firstNameStackView: UIStackView!
    @IBOutlet weak var lastNameStackView: UIStackView!
    @IBOutlet weak var emailStackView: UIStackView!
    @IBOutlet weak var mobileStackView: UIStackView!
    @IBOutlet weak var dobStackView: UIStackView!
    @IBOutlet weak var genderStackView: UIStackView!
    
    
    @IBOutlet weak var firstNameTextField: BaseFloatingTextField!
    @IBOutlet weak var lastNameTextField: BaseFloatingTextField!
    @IBOutlet weak var emailTextField: BaseFloatingTextField!
    @IBOutlet weak var mobileTextField: FPNTextField!
    @IBOutlet weak var dobTextField: BaseFloatingTextField!
    @IBOutlet weak var genderTextField: BaseFloatingTextField!
    
    @IBOutlet weak var firstNameLabel: BaseUILabel!
    @IBOutlet weak var lastNameLabel: BaseUILabel!
    @IBOutlet weak var emailLabel: BaseUILabel!
    @IBOutlet weak var mobileLabel: BaseUILabel!
    @IBOutlet weak var dobLabel: BaseUILabel!
    @IBOutlet weak var genderLabel: BaseUILabel!
    
    @IBOutlet weak var saveBtn: FlashButton!
    @IBOutlet weak var changePasswordBtn: FlashButton!
    
    var isValidate = true
    var countryCode = "+971"
    var selectedDob : Date?
    var selectedGender : UserGender?
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    var validPhoneImage = UIImageView(image: UIImage(named: "errorphone"))
    var isPhoneNumberValid = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createBackBtn()
        setupButton()
        self.navigationItem.title = "Edit Profile".localized
        setUpViews()
        setUserData()
       // countryCode = "+92"
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            // self.isHideNavigation(true)
        resetErrorViews()
    }
    func setupButton() {
           saveBtn.didTapOnBounceStart = {[weak self] in
           }
           
           saveBtn.didTapOnBounceEnd = {[weak self] in
               self?.saveProfile(self!.saveBtn!)
           }
            changePasswordBtn.didTapOnBounceStart = {[weak self] in
            }
        
            changePasswordBtn.didTapOnBounceEnd = {[weak self] in
            self?.changePasswordPressed(self!.changePasswordBtn!)
            }
       }
    func resetErrorViews() {
        
        firstNameLabel.text = ""
        lastNameLabel.text = ""
        emailLabel.text = "You can't change the email".localized
        mobileLabel.text = ""
        dobLabel.text = ""
        genderLabel.text = ""
    }
    func setUpViews(){
    
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        emailTextField.delegate = self
        mobileTextField.delegate = self
        dobTextField.delegate = self
        genderTextField.delegate = self
    
        firstNameTextField.setInitialUI()
        lastNameTextField.setInitialUI()
        emailTextField.setInitialUI()
        //mobileTextField.setInitialUI()
        dobTextField.setInitialUI()
        genderTextField.setInitialUI()
        
        let mobileFont = UIFont.init(name: FontManager.constant(forKey: "fontSFRegular")!, size: CGFloat(FontManager.style(forKey: "sizeMedium")))
        mobileTextField.font = mobileFont
        mobileTextField.textColor = ColorManager.color(forKey: "themeBlue")
        
        mobileTextField.displayMode = .list
        listController.setup(repository: mobileTextField.countryRepository)
        listController.didSelect = { [weak self] country in
            self?.mobileTextField.setFlag(countryCode: country.code)
        }
        mobileTextField.setFlag(key: .AE)
        mobileTextField.keyboardType = .asciiCapableNumberPad
        mobileTextField.placeholder = "Mobile Number Optional".localized
        
        
    }
    func setUserData() {
        firstNameTextField.text = CurrentUser.data?.firstName ?? ""
        lastNameTextField.text = CurrentUser.data?.lastName ?? ""
        emailTextField.text = CurrentUser.data?.emailAddress ?? ""
        if let genderId = CurrentUser.data?.genderId {
            switch genderId {
            case UserGender.male.rawValue:
                self.selectedGender = .male
                genderTextField.text = "Male".localized
            case UserGender.female.rawValue:
                self.selectedGender = .female
                genderTextField.text = "Female".localized
            default:
                genderTextField.text = ""
            }
        }
        if let dob = CurrentUser.data?.dateOfBirth, dob.count > 0 {
            print(dob)
            let date = dob.toDateString(DateFormatType.defaultDisplayFormat,dateFormat: DateFormatType.serverFormatSecond)
            selectedDob = dob.toDate(DateFormatType.serverFormatSecond)
            print(selectedDob)
            dobTextField.text = date
        }
        if let phone = CurrentUser.data?.phoneNumber, phone.count > 0 {
            print(phone)
            let number = CurrentUser.data!.phoneCountryCode! + phone
            print(phone)
            countryCode = CurrentUser.data!.phoneCountryCode!
            isPhoneNumberValid = true
            mobileTextField.set(phoneNumber: phone.first != "+" ? "+\(number)" : number)
        }
    }
    @IBAction func selectDateOfBirth(_ sender: Any) {
        
        //let secondsInDay: TimeInterval = 24 * 60 * 60;
        //let previousDate = Date(timeInterval: -secondsInWeek, since: Date())
        let datePicker = ActionSheetDatePicker(title: "Date of Brith".localized,
                                               datePickerMode: UIDatePicker.Mode.date,
                                               selectedDate: selectedDob ?? Date().decrementYears(numberOfYears: 18),
                                               target: self,
                                               action: #selector(datePicked(_:)),
                                               origin: self.view)
       // let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
        //datePicker?.minimumDate = Date(timeInterval: -secondsInWeek, since: Date())
        datePicker?.maximumDate = Date()//Date(timeInterval: secondsInWeek, since: Date())

        datePicker?.show()
    }
    @objc func datePicked(_ date: Date) {
        print("Date picked \(date)")
        //2020-06-09T09:39:24.976Z
        print(date.toString(DateFormatType.defaultDate))
        selectedDob = date
        dobTextField.text = date.toString(DateFormatType.defaultDisplayFormat)
    }
    @IBAction func selectGender(_ sender: Any) {
        
        ActionSheetStringPicker.show(withTitle: "Gender".localized,
                                     rows: ["Male".localized, "Female".localized],
                                     initialSelection: (selectedGender?.rawValue ?? 1) - 1,
        doneBlock: { picker, value, index in
           print("picker = \(String(describing: picker))")
           print("value = \(value)")
            self.selectedGender = UserGender(rawValue: value + 1)
            self.genderTextField.text = self.selectedGender == UserGender.male ? "Male".localized : "Female".localized
           print("index = \(String(describing: index))")
           return
        },
        cancel: { picker in
           return
        },
        origin: sender)
    }
    @IBAction func saveProfile(_ sender: FlashButton) {
        
            isValidate = true
            let fv = FormValidator()
            let firstName = fv.validateName(title: firstNameTextField.placeholder!, text: firstNameTextField.text!)
            let lastName = fv.validateName(title: lastNameTextField.placeholder!, text: lastNameTextField.text!)
            let email = fv.validateEmail(title: emailTextField.placeholder!, text: emailTextField.text!)
            let mobileNum = fv.validatePhoneNo(title: mobileTextField.placeholder!, text: mobileTextField.text!)
            let dob = fv.validateName(title: dobTextField.placeholder!, text: dobTextField.text!)
            let gender = fv.validateName(title: genderTextField.placeholder!, text: genderTextField.text!)
            
            if (firstName.0 == false) {
                firstNameLabel.text = firstName.1
                firstNameStackView.shake()
                isValidate = false
            }
            if (lastName.0 == false) {
                lastNameLabel.text = lastName.1
                lastNameStackView.shake()
                isValidate = false
            }
            
            if (email.0 == false) {
                emailLabel.text = email.1
                emailStackView.shake()
                isValidate = false
            }
            if (mobileTextField.text!.count > 0) {
                if (!isPhoneNumberValid) {
                    mobileLabel.text = "Enter valid mobile number".localized
                    mobileStackView.shake()
                    isValidate = false
                }
            }
            if isValidate {
                print("Validation sucess")
                self.view.endEditing(true)
                self.callEditProfileService()
               // self.show(VerifyCodeController.loadVC())
    //            router.goToHomeAsRoot(from: self)
                }
        }
    //MARK:- APIS
      func callEditProfileService() {
          self.view.endEditing(true)
          //let code = btnCountryCode.titleLabel?.text!.trimmingCharacters(in: .whitespaces)
        var param:[String:Any] = ["first_name":firstNameTextField.text!,
                                  "last_name":lastNameTextField.text!]
        
        if let gender = selectedGender {
            param["gender_id"] = gender.rawValue
        }
        if let date = selectedDob {
            param["date_of_birth"] = date.toString("yyyy-MM-dd") + "T00:00:00.000"
        }
        "ds"
        if (isPhoneNumberValid) {
            param["phone_country_id"] = countryCode.removeCharacter(str: "+", with: "")
            param["phone_number"] = mobileTextField.text!.onlyDigits()
        }
        print(param)
        UserModuleManager().userLoginApi(endPoint: ApiEndPoints.editProfile, param: param, header: [:], completion: { (response,msg,errorMsg) in
            
            let token = CurrentUser.data!.authToken!
            CurrentUser.data = response
            CurrentUser.data?.authToken = token
            if (CurrentUser.data?.isSocialLogin == true){
                //CurrentUser.data?.fullName = CurrentUser.data!.userName!
            }
            else {
                CurrentUser.data?.fullName = "\(String(describing: CurrentUser.data!.firstName!)) \(String(describing: CurrentUser.data!.lastName!))"
            }
            //self.setUserData()
            try! Constants.realm.write(){
                if CurrentUser.data != nil {
                    Constants.realm.add(CurrentUser.data!, update: .all)
                }
            }
            
            var message = ""
            if let mesg = msg {
                message = mesg
            }
            Alert.showSuccessMessage(msg: message, duration: 1)
            self.popViewController()
            
      }){ (error) in
              print("Error: ", error as Any)
          }
    }
    // MARK: - changePasswordPressed
    @IBAction func changePasswordPressed(_ sender: FlashButton) {
        let vc = ChangePasswordController.loadVC()
        vc.isChangePass = true
        self.show(vc)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension EditProfileController : UITextFieldDelegate {
    // MARK: - TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let currentCharacterCount = textField.text?.count ?? 0
        
        if (range.length + range.location > currentCharacterCount)
        {
            return false
        }
        
        let newLength = currentCharacterCount + string.count - range.length
        var maxLength = 50
        
        
        if (textField == firstNameTextField)
        {
            self.firstNameLabel.text = ""
            maxLength = 20
        }
        
        if (textField == lastNameTextField)
        {
            self.lastNameLabel.text = ""
            maxLength = 20
        }
        if (textField == emailTextField)
        {
            //self.emailLabel.text = ""
            maxLength = 30
        }
        
        if (textField == mobileTextField)
        {
            self.mobileLabel.text = ""
            maxLength = 20
        }
        
        if (textField == dobTextField)
        {
            self.dobLabel.text = ""
            maxLength = 20
        }
        if (textField == genderTextField)
        {
            self.genderLabel.text = ""
             maxLength = 10
        }
        
        return newLength <= maxLength
        
    }
}
extension EditProfileController: FPNTextFieldDelegate {

   /// The place to present/push the listController if you choosen displayMode = .list
   func fpnDisplayCountryList(textField: FPNTextField) {
       let navigationViewController = UINavigationController(rootViewController: listController)

       listController.title = "Countries"
       listController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))

       self.present(navigationViewController, animated: true, completion: nil)
   }
    @objc func dismissCountries() {
        listController.dismiss(animated: true, completion: nil)
    }
   /// Lets you know when a country is selected
   func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
      print(name, dialCode, code) // Output "France", "+33", "FR"
    countryCode = dialCode
    if (isPhoneNumberValid == false) {
        mobileTextField.text = ""
    }
    
    
   }

   /// Lets you know when the phone number is valid or not. Once a phone number is valid, you can get it in severals formats (E164, International, National, RFC3966)
   func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        textField.rightViewMode = .always
        let cross = UIImage(named: "errorphone")
        let success = UIImage(named: "successphone")
        mobileLabel.text = ""
        isPhoneNumberValid = isValid
        if textField.text!.count <= 0 {
            mobileTextField.rightView = nil
            mobileTextField.placeholder = "Mobile Number Optional".localized
        }
        else {
            textField.setRightIcon((isValid ? success : cross)!)
        }
      // textField.rightView = UIImageView(image: isValid ? success : cross)
       // validPhoneImage.image = isValid ? success : cross
    
       print(
           isValid,
           textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
           textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
           textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
           textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
           textField.getRawPhoneNumber() ?? "Raw: nil"
       )
   }
}
