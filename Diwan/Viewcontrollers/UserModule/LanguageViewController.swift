//
//  LanguageViewController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 04/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import MOLH
import ViewAnimator
class LanguageViewController: BaseViewController, StoryBoardHandler {

    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.userModule.rawValue, nil)
    
    
    @IBOutlet weak var englishBtn: BounceButton!
    @IBOutlet weak var arabicBtn: BounceButton!
    
    @IBOutlet weak var languageBtnStack: UIStackView!
    @IBOutlet weak var languageTextStack: UIStackView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        arabicBtn.borderWidth = 0
        setupButton()
        let zoomAnimation = AnimationType.zoom(scale: 0.2)
        UIView.animate(views: [languageBtnStack,languageTextStack],
        animations: [zoomAnimation],
        duration: 0.5)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isHideNavigation(true)
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
    }
    func setupButton() {
        englishBtn.didTapOnBounceStart = {[weak self] in
            self?.englishBtn.borderWidth = 1
            self?.arabicBtn.borderWidth = 0
        }
        arabicBtn.didTapOnBounceStart = {[weak self] in
            self?.arabicBtn.borderWidth = 1
            self?.englishBtn.borderWidth = 0
        }
        englishBtn.didTapOnBounceEnd = {[weak self] in
            UserDefaults.standard.set(true, forKey: UserDefaultKey.launchedBefore)
            MOLH.setLanguageTo("en")
            //MOLH.reset()
            self?.showLoginScreen()
        }
        arabicBtn.didTapOnBounceEnd = {[weak self] in
            self?.alertNextPhase()
//            UserDefaults.standard.set(true, forKey: UserDefaultKey.launchedBefore)
//            MOLH.setLanguageTo("ar")
//            //MOLH.reset()
//            self?.showLoginScreen()
        }
    }
    
    func showLoginScreen() {
        //router.goToHomeAsRoot(from: <#T##Router.viewController#>)
        let controller = LoginViewController.loadVC()
        //controller.modalPresentationStyle = .fullScreen
        //self.present(controller, animated: true, completion: nil)
        self.show(controller)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
