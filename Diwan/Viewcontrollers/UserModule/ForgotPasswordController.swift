//
//  ForgotPasswordController.swift
//  SalonDesBeaute
//
//  Created by Shujaatkhan on 21/11/2018.
//  Copyright © 2018 High- Sierra 13.6. All rights reserved.
//

import UIKit
import MOLH

class ForgotPasswordController: BaseViewController ,UITextFieldDelegate, StoryBoardHandler{
    
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.userModule.rawValue , nil)
    
    // MARK: - IBOutlets
    @IBOutlet weak var emailStackView: UIStackView!
    @IBOutlet weak var emailTextField: BaseFloatingTextField!
    @IBOutlet weak var emailLabel: BaseUILabel!
    
    @IBOutlet weak var submitBtn: FlashButton!
    

    // MARK: - ViewCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Forgot Password".localized
        createBackBtn()
        setupButton()
        setUpViews()
        //self.navigationItem.setHidesBackButton(true, animated: false)
        //createNavBlackBackBtn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        emailLabel.text = ""
        resetErrorViews()
    }
    func setUpViews(){
        emailTextField.delegate = self
        
        emailTextField.setInitialUI()
    }
    func setupButton() {
        submitBtn.didTapOnBounceStart = {[weak self] in
        }
        
        submitBtn.didTapOnBounceEnd = {[weak self] in
            self?.submitClicked(self!.submitBtn)
        }
    }
    func resetErrorViews() {
        
        emailLabel.text = ""
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction
    
    @IBAction func submitClicked(_ sender: Any) {

        self.view.endEditing(true)
        
        let fv = FormValidator()
        
        let email = fv.validateEmail(title: emailTextField.placeholder!, text: emailTextField.text!)
        
        if (email.0 == false) {
            emailLabel.text = email.1
            emailStackView.shake()
            return
        }
        
        self.view.endEditing(true)
        
        callForgotPassService()
        
//        self.showAlert(title: nil, subTitle: NSLocalizedString("An email has been sent to your account with verification code", comment: "")) {
//            self.show(VerifyCodeController.loadVC())
//        }
        
    }
    
    // MARK: - APIS
    func callForgotPassService() {
        self.view.endEditing(true)
        
        let param:[String:Any] = ["email_address": emailTextField.text!]
        UserModuleManager().userLoginApi(endPoint: ApiEndPoints.forgotEmail, param: param, header: [:], completion: { (response,msg,errorMsg) in
            
            print(response)
            var message = ""
            if let mesg = msg {
                message = mesg
            }
            self.showAlert(title: nil, subTitle: message, doneBtnTitle: "OK".localized, completionAction: {
                let vc = VerifyCodeController.loadVC()
                vc.email = self.emailTextField.text!
                self.show(vc)
            })

        }){ (error) in
                print("Error: ", error as Any)
            }

    }
    // MARK: - TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool{
        
        let currentCharacterCount = textField.text?.count ?? 0
        
        if (range.length + range.location > currentCharacterCount)
        {
            return false
        }
        
        let newLength = currentCharacterCount + string.count - range.length
        var maxLength = 50
        
        if (textField == emailTextField)
        {
            self.emailLabel.text = ""
        }
        
        return newLength <= maxLength
        
    }
    
}
