//
//  AddressListingController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class AddressListingController: BaseViewController , StoryBoardHandler {

    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.userModule.rawValue , nil)
    var addressList = [AddressModel]()
    @IBOutlet weak var addressListTbl: BaseUITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addressListTbl.isHidden = true
        self.navigationItem.title = "My Addresses".localized
        createBackBtn()
        createRefreshView(tblView: addressListTbl)
        setUpTable()
        self.addBarButtonItemWithImage(UIImage(named: "topplusicon")!, .BarButtonItemPositionRight, self, #selector(addNewAddress))
    }
    @objc func addNewAddress() {
        self.show(AddAddressController.loadVC())
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.addressList.removeAll()
        self.getAddressListApi()
    }
    func setUpTable(){
        
        addressListTbl.registerCell(AddressListCell.self)
        //addressListTbl.reloadData()
    }
    override func refreshData() {
        print("refresh")
        self.currentPage = 1
        self.addressList.removeAll()
        self.getAddressListApi()
       // self.refreshView.endRefreshing()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddressListingController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return 10
        if self.addressList.count == 0 {
            UtilityEmptyListView.emptyTableViewMessage(message: "", image: "addressemptyicon", tableView: tableView, listType: .address)
            if let emptyView = tableView.backgroundView as? EmptyTableViewBackgroundView {
                emptyView.actionBtn.didTapOnBounceEnd = {
                    self.show(AddAddressController.loadVC())
                }
            }
            return 0
        }
        UtilityEmptyListView.hideEmptyTableViewMessage(tableView: tableView)
        return self.addressList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: AddressListCell.identifier()) as! AddressListCell
        cell.setData(data: self.addressList[indexPath.row])
        cell.didTapOnMarkDefault = { data in
            self.defaultAddressApi(data: data)
        }
        cell.didTapOnDelete = { data in
            self.deleteAddressApi(data: data)
        }
        cell.didTapOnEdit = { data in
            let address = AddAddressController.loadVC()
            address.isEditAddress = true
            address.currentAddress = data
            self.show(address)
        }
//        cell.delegate = self
//        cell.btnDelete.tag = useraddress[indexPath.row].Id
//        cell.lblTitle.text = useraddress[indexPath.row].Subject
//        cell.lblAddress.text = appendAddress(index: indexPath.row)
//        if (indexPath.row == 0) {
//            cell.bgView.roundCorners(corners: [.layerMinXMinYCorner,.layerMaxXMinYCorner], radius: 10.0)
//        }
//        //        if useraddress[indexPath.row].IsSelected {
//        //            self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
//        //
//        //        } else {
//        //            self.tableView.deselectRow(at: indexPath, animated: false)
//        //        }
//        if isPreviousScreenEdit {
//            cell.btnDelete.isHidden = false
//        } else {
//            cell.btnDelete.isHidden = true
//        }
        return cell
    }
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 56
//    }
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressFooterTableViewCell") as! AddressFooterTableViewCell
//        cell.delegate = self
//
//        return cell
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
}
//MARK:- API's
extension AddressListingController {
    
    func getAddressListApi() {
        
        UserModuleManager().getAddressListApi(endPoint: ApiEndPoints.getUserShippingAddress, param: nil, header: [:], completion: { (response,msg,errorMsg) in
            print(response)
            self.addressListTbl.isHidden = false
            if let data = response {
                self.addressList.append(contentsOf: data)
                self.addressListTbl.reloadData()
            }
        }){ (error) in
                print("Error: ", error as Any)
            
                self.addressListTbl.isHidden = false
                self.addressListTbl.reloadData()
            }
        
    }
    func deleteAddressApi(data : AddressModel) {
        
        UserModuleManager().deleteAddressApi(endPoint: ApiEndPoints.deleteUserShippingAddress + "/\(data.id)", param: nil, header: [:], completion: { (response,msg,errorMsg) in
            //print(response)
            var message = ""
            if let mesg = msg {
                message = mesg
            }
            Alert.showSuccessMessage(msg: message, duration: 2)
            self.addressList.removeAll { (address) -> Bool in
                return data.id == address.id
            }
            self.addressListTbl.reloadData()
            
        }){ (error) in
                print("Error: ", error as Any)
                //self.addressListTbl.isHidden = false
                //self.addressListTbl.reloadData()
            }

    }
    func defaultAddressApi(data : AddressModel) {
        
        UserModuleManager().defaultAddressApi(endPoint: ApiEndPoints.setDefaultShippingAddress + "/\(data.id)", param: nil, header: [:], completion: { (response,msg,errorMsg) in
            //print(response)
            var message = ""
            if let mesg = msg {
                message = mesg
            }
            Alert.showSuccessMessage(msg: message, duration: 2)
            let previousDefaultData = self.addressList.filter { (addr) -> Bool in
               return addr.isPrimary == true
            }[0]
            previousDefaultData.isPrimary = false
            data.isPrimary = true
            self.addressListTbl.reloadData()
            
        }){ (error) in
                print("Error: ", error as Any)
                //self.addressListTbl.isHidden = false
                //self.addressListTbl.reloadData()
            }

    }
    
    
}
