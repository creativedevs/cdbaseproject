//
//  ChangePasswordController.swift
//  YallaEvents
//
//  Created by Shujaatkhan on 08/01/2019.
//  Copyright © 2019 High- Sierra 13.6. All rights reserved.
//

import UIKit

class ChangePasswordController: BaseViewController ,StoryBoardHandler, UITextFieldDelegate{

    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.userModule.rawValue , nil)
    
    @IBOutlet weak var changeTopStack: NSLayoutConstraint!
    
    @IBOutlet weak var oldPasswordStackView: UIStackView!
    @IBOutlet weak var confirmPasswordStackView: UIStackView!
    @IBOutlet weak var newPasswordStackView: UIStackView!
    
    @IBOutlet weak var oldTextField: BaseFloatingTextField!
    @IBOutlet weak var confirmTextField: BaseFloatingTextField!
    @IBOutlet weak var newPasswordTextField: BaseFloatingTextField!
    
    @IBOutlet weak var oldLabel: BaseUILabel!
    @IBOutlet weak var confirmLabel: BaseUILabel!
    @IBOutlet weak var newPasswordLabel: BaseUILabel!
    
    @IBOutlet weak var mainFieldsStackView: UIStackView!
    @IBOutlet weak var oldPasswordMainStackView: UIStackView!
    
    
    @IBOutlet weak var descLabel: BaseUILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    var isValidate:Bool = true
    var email = ""
    var auth = ""
    var isChangePass:Bool = false
    @IBOutlet weak var submitBtn: FlashButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Forgot Password".localized
        createBackBtn()
        setupButton()
        setUpViews()
        if isChangePass{
            self.navigationItem.title = "Change Password".localized
            mainFieldsStackView.removeConstraint(changeTopStack)
            
            view.addConstraint(NSLayoutConstraint(item: mainFieldsStackView!, attribute: .top, relatedBy: .equal, toItem: view.safeAreaLayoutGuide, attribute: .top, multiplier: 1, constant: DesignUtility.getValueFromRatio(22)))
            iconImageView.isHidden = true
            descLabel.isHidden = true
            //createBackBtn()
            submitBtn.setTitle("SAVE".localized, for: .normal)
        }

        
        oldPasswordMainStackView.isHidden = isChangePass ? false:true
    }
    func setUpViews(){
        
        oldTextField.delegate = self
        confirmTextField.delegate = self
        newPasswordTextField.delegate = self
        oldTextField.setInitialUI()
        confirmTextField.setInitialUI()
        newPasswordTextField.setInitialUI()
    }
    func setupButton() {
        submitBtn.didTapOnBounceStart = {[weak self] in
        }
        
        submitBtn.didTapOnBounceEnd = {[weak self] in
            self?.submitClicked(self!.submitBtn!)
        }
    }
    func resetErrorViews() {
        
        oldLabel.text = ""
        confirmLabel.text = ""
        newPasswordLabel.text = ""
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        self.isHideNavigation(false)
        resetErrorViews()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        if isChangePass{
//            self.isHideNavigation(false)
//        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showCurrentPassword(_ sender: UIButton) {
        if oldTextField.text != "" {
            sender.isSelected = !sender.isSelected
            oldTextField.isSecureTextEntry = sender.isSelected ? false:true
        }
    }
    @IBAction func showNewPassword(_ sender: UIButton) {
        if newPasswordTextField.text != "" {
            sender.isSelected = !sender.isSelected
            newPasswordTextField.isSecureTextEntry = sender.isSelected ? false:true
        }
    }
    
    @IBAction func showConfirmPassword(_ sender: UIButton) {
        if confirmTextField.text != "" {
            sender.isSelected = !sender.isSelected
            confirmTextField.isSecureTextEntry = sender.isSelected ? false:true
        }
    }
    

    
    @IBAction func submitClicked(_ sender: Any) {
        self.view.endEditing(true)
        isValidate = true
        
        let fv = FormValidator()
        
        let password = fv.validatePassword(title: newPasswordTextField.placeholder!, text: newPasswordTextField.text!)
        let confirmPassword = fv.validateConfirmPassword(title: confirmTextField.placeholder!, pwd: newPasswordTextField.text!, cpwd: confirmTextField.text!)
        
        if isChangePass {
            let currentPassword = fv.validatePassword(title: oldTextField.placeholder!, text: oldTextField.text!)
            if (currentPassword.0 == false) {
                oldLabel.text = currentPassword.1
                oldPasswordStackView.shake()
                isValidate = false
            }
        }
        
        if (password.0 == false) {
            newPasswordLabel.text = password.1
            newPasswordStackView.shake()
            isValidate = false
        }
        if (confirmPassword.0 == false) {
            confirmLabel.text = confirmPassword.1
            confirmPasswordStackView.shake()
            isValidate = false
        }
        
        if isValidate {
    
            if isChangePass {
               // callChangePasswordService()
                callResetPasswordService()
            }else{
                callResetPasswordService()
            }
            
        }
    }
    
    // MARK: - APIS
    func callChangePasswordService() {
        self.view.endEditing(true)

//        let parameter: [String : Any] = ["currentPassword": txtFldCurrentPass.text!, "newPassword" : txtFldNewPass.text!]
//        //let header:[String:String] = ["Authorization":(CurrentUser.data?.token)!]
//
//        UserModuleManager().postApi(endPoint: CHANGE_PASS, param: parameter, header: CurrentUser.headers) { (response) in
//            //NSLocalizedString("Your password has been changed successfully", comment: "")
//            self.showAlert(title: NSLocalizedString("Thank You!", comment: ""), subTitle: response.message!) {
//                self.popViewController()
//            }
//
//        }

    }


    func callResetPasswordService() {
        
        self.view.endEditing(true)
        var param:[String:Any] = ["new_password": newPasswordTextField.text!]
        var endPoint = ""
        if isChangePass {
            param["old_password"] = oldTextField.text!
            param["platform_id"] = CurrentUser.platformId
            endPoint = ApiEndPoints.updatePassword
        }
        else {
            param["email_address"] = email
            endPoint = ApiEndPoints.resetPassword
        }
        print(endPoint)
        print(param)
        UserModuleManager().userLoginApi(endPoint: endPoint, param: param, header: [:], completion: { (response,msg,errorMsg) in
            
            print(response)
            var message = ""
            if let mesg = msg {
                message = mesg
            }
            
            Alert.showSuccessMessage(msg: message, duration: 2)
            self.pushOrPopViewController(navigationController: self.navigationController!, animation: true, viewControllerClass: LoginViewController.self, viewControllerStoryboad: LoginViewController.myStoryBoard)

            }){ (error) in
                print("Error: ", error as Any)
            }

    }
    
    // MARK: - TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool{
        
        
        let currentCharacterCount = textField.text?.count ?? 0
        
        if (range.length + range.location > currentCharacterCount)
        {
            return false
        }
        
        let newLength = currentCharacterCount + string.count - range.length
        let maxLength = 20
        
        if (textField == oldTextField)
        {
            self.oldLabel.text = ""
        }
        if (textField == newPasswordTextField)
        {
            self.newPasswordLabel.text = ""
            
        }
        if (textField == confirmTextField)
        {
            self.confirmLabel.text = ""
            
        }
        
        return newLength <= maxLength
        
    }
    
    
    
}
