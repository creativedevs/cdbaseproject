//
//  StoreCategoryListController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import BottomPopup
import FittedSheets

class WishListController: BaseViewController , StoryBoardHandler {

    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.userModule.rawValue , nil)
    @IBOutlet weak var storeCollectionView: BaseUICollectionView!
    
    var productList = [ProductModel]()
    var selectedMainCategory : CategoryModel?
    var selectedSort : SortModel?
    //var filledData : (String, ListViewType)?//= (title: "", listType: .wishlist)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = selectedMainCategory?.title
        createBackBtn()
        setupAppDefaultNavigationBar()
        createNavSideMenuBtn()
        self.storeCollectionView.isHidden = true
        //createRefreshView(tblView: categoryListTbl)
        setUpCollection()
        setupButtons()
        self.getwishListApi()
    }
    
    func setUpCollection(){
        
        storeCollectionView.registerCell(StoreProductCollectionCell.self)
    }
    
    override func refreshData() {
        print("refresh")
        self.currentPage = 1
        self.productList.removeAll()
        //self.getCategoryListApi()
       // self.refreshView.endRefreshing()
    }
    func setupButtons(){
    }
    
    
    
}
//MARK:- Collection Datasource and Delegate
extension WishListController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return 10
        if self.productList.count == 0 {
            UtilityEmptyListView.emptycollectionViewMessage(message: "test", image: "emptyproduct", collectionView: collectionView, listType: .wishlist)
            if let emptyView = collectionView.backgroundView as? EmptyTableViewBackgroundView {
                emptyView.actionBtn.didTapOnBounceEnd = {
                    //self.show(AddAddressController.loadVC())
                }
            }
            return 0
        }
        UtilityEmptyListView.hideEmptyCollectionViewMessage(collectionView: collectionView)
        return productList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoreProductCollectionCell.identifier(), for: indexPath) as! StoreProductCollectionCell
        let data = self.productList[indexPath.row]
        let position : ProductCellPosition = indexPath.row % 2 == 0 ? .left : .right
        cell.setData(data: data,cellPosition: position)
        cell.addToCartBtn.didTapOnBounceEnd = {
            self.addToCartOrNotifyMe(data: data,cell: cell,indexPath: indexPath)
        }
        cell.addOrRemoveFavoriteBtn.didTapOnBounceEnd = {
            self.addOrRemoveFavoriteWishList(data: data,cell: cell,indexPath: indexPath)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/2, height: DesignUtility.getValueFromRatio(298))
        //return CGSize(width: DesignUtility.getValueFromRatio(188), height: DesignUtility.getValueFromRatio(198))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: collectionView.frame.width, height: DesignUtility.getValueFromRatio(156)) //add your height here
//    }
}
extension WishListController {
    
    func addToCartOrNotifyMe(data : ProductModel, cell : StoreProductCollectionCell, indexPath : IndexPath) {
        
        var apiUrl = ""
        if (data.quantity > 0) {
            return
        }
        else {
            if (data.isNotify == true) {return}
            apiUrl = ApiEndPoints.notifyMe
        }
        let param:[String:Any] = ["product_id": data.productId,"is_notify":!data.isNotify]
        StoreManager().addToCartOrNotifyMeApi(endPoint: apiUrl, param: param, header: [:], completion: { (response,msg,errorMsg) in
                
            data.isNotify = !data.isNotify
            self.storeCollectionView.reloadItems(at: [indexPath])
                print(response)
                if let data = response {
                        
                }

            }){ (error) in
               // self.refreshView.endRefreshing()
                    print("Error: ", error as Any)
                }
    
    }
    
    func addOrRemoveFavoriteWishList(data : ProductModel, cell : StoreProductCollectionCell, indexPath : IndexPath) {
        
        let param:[String:Any] = ["product_id": data.productId,"is_favorite":!data.isFavorite]
        
        print(param)
    StoreManager().addOrRemoveFavoriteApi(endPoint: ApiEndPoints.addToFavorite, param: param, header: [:], completion: { (response,msg,errorMsg) in
            
        data.isFavorite = !data.isFavorite
        self.storeCollectionView.reloadItems(at: [indexPath])
            print(response)
            if let data = response {
                    
            }

        }){ (error) in
           // self.refreshView.endRefreshing()
                print("Error: ", error as Any)
            }
        
    }
    func getwishListApi() {
        
        let param:[String:Any] = ["page_number": currentPage,
                                  "page_size": 100000,
                                  "parent_category_id": "\(selectedMainCategory!.categoryId)"]
    StoreManager().getStoreProductListApi(endPoint: ApiEndPoints.getProducts, param: param, header: [:], completion: { (response,msg,errorMsg) in
         //   self.categoryListTbl.isHidden = false
            print(response)
            self.storeCollectionView.isHidden = false
            //self.refreshView.endRefreshing()
            if let data = response {
                self.productList.append(contentsOf: data)
                self.storeCollectionView.reloadData()
            }

        }){ (error) in
            self.storeCollectionView.isHidden = true
            self.refreshView.endRefreshing()
                print("Error: ", error as Any)
            }
        
    }
    
}
