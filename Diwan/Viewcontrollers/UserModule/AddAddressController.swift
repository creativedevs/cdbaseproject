//
//  AddressListController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 14/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import FlagPhoneNumber
import ActionSheetPicker_3_0
import BottomPopup
class AddAddressController : BaseViewController , StoryBoardHandler {

    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.userModule.rawValue , nil)
    
    @IBOutlet weak var viewContainerForm: BaseUIView!
    @IBOutlet weak var btnSkip: BaseUIButton!
    
    @IBOutlet weak var fullNameStackView: UIStackView!
    @IBOutlet weak var addressLine1StackView: UIStackView!
    @IBOutlet weak var addressLine2StackView: UIStackView!
    @IBOutlet weak var mobileStackView: UIStackView!
    @IBOutlet weak var mobile2StackView: UIStackView!
    @IBOutlet weak var countryStackView: UIStackView!
    @IBOutlet weak var stateStackView: UIStackView!
    @IBOutlet weak var cityStackView: UIStackView!
     @IBOutlet weak var zipCodeStackView: UIStackView!
    
    @IBOutlet weak var fullNameTextField: BaseFloatingTextField!
    @IBOutlet weak var addressLine1TextField: BaseFloatingTextField!
    @IBOutlet weak var addressLine2TextField: BaseFloatingTextField!
    @IBOutlet weak var mobileTextField: FPNTextField!
    @IBOutlet weak var mobile2TextField: FPNTextField!
    @IBOutlet weak var countryTextField: BaseFloatingTextField!
    @IBOutlet weak var stateTextField: BaseFloatingTextField!
    @IBOutlet weak var cityTextField: BaseFloatingTextField!
    @IBOutlet weak var zipCodeTextField: BaseFloatingTextField!
    
    @IBOutlet weak var fullNameLabel: BaseUILabel!
    @IBOutlet weak var addressLine1Label: BaseUILabel!
    @IBOutlet weak var addressLine2Label: BaseUILabel!
    @IBOutlet weak var mobileLabel: BaseUILabel!
    @IBOutlet weak var mobile2Label: BaseUILabel!
    @IBOutlet weak var countryLabel: BaseUILabel!
    @IBOutlet weak var stateLabel: BaseUILabel!
    @IBOutlet weak var cityLabel: BaseUILabel!
    @IBOutlet weak var zipCodeLabel: BaseUILabel!
    
    @IBOutlet weak var moreInfoTxtView: BaseUITextView!
    @IBOutlet weak var saveBtn: FlashButton!
    @IBOutlet weak var changePasswordBtn: FlashButton!
    
    var isValidate = true
    var countryCode = "971"
    var countryCode2 = "971"
    var selectedPhoneTextField : FPNTextField??
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    var listController2: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    var validPhoneImage = UIImageView(image: UIImage(named: "errorphone"))
    var isPhoneNumberValid = false
    var isPhoneNumberValid2 = false
    var currentAddress : AddressModel?
    @IBOutlet weak var characterCountLbl: BaseUILabel!
    
    @IBOutlet weak var moreInfoLbl: BaseUILabel!
    var isEditAddress = false
    var countryList = [Country]()
    var stateList = [State]()
    var cityList = [City]()
    var selectedCountry : Country?
    var selectedCity : City?
    var selectedState : State?
    var locationList = [CountryStateCityProtocol]()
    var locationNavigationController : UINavigationController?
    override func viewDidLoad() {
        super.viewDidLoad()
        createBackBtn()
        setupButton()
        moreInfoLbl.sizeToFit()
        //"Add New Address" : "Edit Address"
        let navTitle : String = isEditAddress == false ? "Add New Address".localized : "Edit Address".localized
        self.navigationItem.title = navTitle
        setUpViews()
        if isEditAddress {
            setAddressrData()
        }
        getCountriesService(type: .country, api: ApiEndPoints.getCountries, modelType: Country.self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            // self.isHideNavigation(true)
        resetErrorViews()
    }
    func setupButton() {
           saveBtn.didTapOnBounceStart = {[weak self] in
           }
           
           saveBtn.didTapOnBounceEnd = {[weak self] in
               self?.saveProfile(self!.saveBtn!)
           }
            changePasswordBtn.didTapOnBounceStart = {[weak self] in
            }
        
            changePasswordBtn.didTapOnBounceEnd = {[weak self] in
            self?.changePasswordPressed(self!.changePasswordBtn!)
            }
       }
    func resetErrorViews() {
        
        fullNameLabel.text = ""
        countryLabel.text = ""
        stateLabel.text = ""
        cityLabel.text = ""
        addressLine1Label.text = ""
        addressLine2Label.text = ""
        zipCodeLabel.text = ""
        mobileLabel.text = ""
        mobile2Label.text = ""
        
    }
    func setUpViews(){
    
        fullNameTextField.delegate = self
        countryTextField.delegate = self
        stateTextField.delegate = self
        cityTextField.delegate = self
        addressLine1TextField.delegate = self
        addressLine2TextField.delegate = self
        zipCodeTextField.delegate = self
        mobileTextField.delegate = self
        mobile2TextField.delegate = self
        
    
        fullNameTextField.setInitialUI()
        countryTextField.setInitialUI()
        stateTextField.setInitialUI()
        cityTextField.setInitialUI()
        addressLine1TextField.setInitialUI()
        addressLine2TextField.setInitialUI()
        zipCodeTextField.setInitialUI()
        //mobileTextField.setInitialUI()
        
        
        
        let mobileFont = UIFont.init(name: FontManager.constant(forKey: "fontSFRegular")!, size: CGFloat(FontManager.style(forKey: "sizeMedium")))
        mobileTextField.font = mobileFont
        mobileTextField.textColor = ColorManager.color(forKey: "themeBlue")
        
        mobile2TextField.font = mobileFont
        mobile2TextField.textColor = ColorManager.color(forKey: "themeBlue")
        
        mobileTextField.displayMode = .list
        mobile2TextField.displayMode = .list
        
        listController.setup(repository: mobileTextField.countryRepository)
        listController.didSelect = { [weak self] country in
            self?.selectedPhoneTextField = self?.mobileTextField
            self?.mobileTextField.setFlag(countryCode: country.code)
            
        }
        listController2.setup(repository: mobile2TextField.countryRepository)
        listController2.didSelect = { [weak self] country in
            self?.selectedPhoneTextField = self?.mobile2TextField
            self?.mobile2TextField.setFlag(countryCode: country.code)
            
        }
        mobileTextField.setFlag(key: .AE)
        mobileTextField.keyboardType = .asciiCapableNumberPad
        mobileTextField.placeholder = "Mobile Number".localized
        
        mobile2TextField.setFlag(key: .AE)
        mobile2TextField.keyboardType = .asciiCapableNumberPad
        mobile2TextField.placeholder = "Mobile Number Optional".localized
        
    }
    func setAddressrData() {
        
        if let address = currentAddress {
            fullNameTextField.text = address.fullName ?? ""
            addressLine1TextField.text = address.line1 ?? ""
            addressLine2TextField.text = address.line2 ?? ""
            zipCodeTextField.text = address.zipCode ?? ""
            moreInfoTxtView.text = address.moreInfo ?? ""
            selectedCountry = Country()
            selectedCountry?.countryId = currentAddress?.countryId ?? 0
            selectedCountry?.title = currentAddress?.countryName ?? ""
            selectedState = State()
            selectedState?.stateId = currentAddress?.stateId ?? 0
            selectedState?.title = currentAddress?.stateName ?? ""
            selectedCity = City()
            selectedCity?.citiesId = currentAddress?.cityId ?? 0
            selectedCity?.title = currentAddress?.cityName ?? ""
            isPhoneNumberValid = true
            countryCode = currentAddress?.mobileCountryId ?? "971"
            let number1 = currentAddress?.mobileNumber ?? ""
            mobileTextField.set(phoneNumber: "+\(countryCode)\(number1)")
//            mobileTextField.text =
//            mobile2TextField.text = mobileTextField.getFormattedPhoneNumber(format:.International)
            countryTextField.text = selectedCountry?.title
            stateTextField.text = selectedState?.title
            cityTextField.text = selectedCity?.title
            if let mobileSecond = currentAddress?.landlineCountryId {
                countryCode2 = mobileSecond
                let number2 = currentAddress?.landlineNumber ?? ""
                mobile2TextField.set(phoneNumber: "+\(countryCode2)\(number2)")
                isPhoneNumberValid2 = true
            }
            //isPhoneNumberValid2 = true
            self.getCountriesService(type: .state, param: nil, api: ApiEndPoints.getStateByCountry + "/\(self.selectedCountry!.countryId)", modelType: State.self)
            self.getCountriesService(type: .city, param: nil, api: ApiEndPoints.getCityByState + "/\(self.selectedState!.stateId)", modelType: City.self)
        }
        
//        selectedCountry.coun
        //addressLine1TextField.text = CurrentUser.data?.addressLine1 ?? ""
//        if let phone = CurrentUser.data?.phoneNumber, phone.count > 0 {
//            print(phone)
//            let number = CurrentUser.data!.phoneCountryCode! + phone
//            print(phone)
//            countryCode = CurrentUser.data!.phoneCountryCode!
//            isPhoneNumberValid = true
//            mobileTextField.set(phoneNumber: phone.first != "+" ? "+\(number)" : number)
//        }
    }
    
    
    @IBAction func selectCountry(_ sender: Any) {
        print("Country")
        self.showCountryPicker(type: .country, dataList: self.countryList)
    }
    @IBAction func selectState(_ sender: Any) {
        print("State")
        if selectedCountry == nil   {
            Alert.showErrorMessage(msg: "Select country".localized)
            return
        }
        self.showCountryPicker(type: .state, dataList: self.stateList)
    }
    @IBAction func selectCity(_ sender: Any) {
        print("City")
        if selectedState == nil   {
            Alert.showErrorMessage(msg: "Select state".localized)
            return
        }
        self.showCountryPicker(type: .city, dataList: self.cityList)
    }
    
    func showCountryPicker(type : PopUpListType, dataList : [CountryStateCityProtocol]) {
        
        let popupVC = BottomPopUpController(nibName: "BottomPopUpController", bundle: nil)
       // guard let popupVC = storyboard?.instantiateViewController(withIdentifier: "secondVC") as? ExamplePopupViewController else { return }
        popupVC.height = self.view.frame.height - 400
        popupVC.topCornerRadius = 10.0
        popupVC.presentDuration = 0.5
        popupVC.dismissDuration = 0.5
        popupVC.shouldDismissInteractivelty = true
       // popupVC.popupDelegate = self
        popupVC.didTapOnCell = {[weak self] data in
            print(data)
            switch type {
            case .country:
                self!.selectedCountry = data as? Country
                
                self!.countryLabel.text = ""
                self!.selectedState = nil
                self!.selectedCity = nil
                self!.cityTextField.text = ""
                self!.stateTextField.text = ""
                self!.countryTextField.text = self?.selectedCountry?.title
                self!.getCountriesService(type: .state, param: nil, api: ApiEndPoints.getStateByCountry + "/\(self!.selectedCountry!.countryId)", modelType: State.self)
            case .state:
                self!.selectedState = data as? State
                self!.stateLabel.text = ""
                self!.selectedCity = nil
                self!.cityTextField.text = ""
                self!.stateTextField.text = self?.selectedState?.title
                self!.getCountriesService(type: .city, param: nil, api: ApiEndPoints.getCityByState + "/\(self!.selectedState!.stateId)", modelType: City.self)
            case .city:
                self!.selectedCity = data as? City
                self!.cityLabel.text = ""
                self!.cityTextField.text = self?.selectedCity?.title
            default:
                print("")
            }
        }
        popupVC.setInitialData(dataList: dataList, type: type)
        
        locationNavigationController = UINavigationController(rootViewController: popupVC)
           
//              popupVC.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissLocationNavigation))
//        popupVC.view.backgroundColor = .white
        self.present(locationNavigationController!, animated: true, completion: {
            popupVC.reloadData()
        })
        
          //    self.present(navigationViewController, animated: true, completion: nil)
        //present(popupVC, animated: true, completion: nil)
        //popupVC.setData(dataList: dataList, type: type)
    }
//    @objc func dismissLocationNavigation() {
//        locationNavigationController?.dismiss(animated: true, completion: {
//
//        })
//    }
    @IBAction func saveProfile(_ sender: FlashButton) {
        
       // print(mobileTextField.selectedCountry?.phoneCode)
            isValidate = true
            let fv = FormValidator()
            let fullName = fv.validateName(title: fullNameTextField.placeholder!, text: fullNameTextField.text!)
            let addressLine1 = fv.validateStreet(title: addressLine1TextField.placeholder!, text: addressLine1TextField.text!)
        let addressLine2 = fv.validateStreet(title: addressLine2TextField.placeholder!, text: addressLine2TextField.text!)
            let mobileNum = fv.validatePhoneNo(title: mobileTextField.placeholder!, text: mobileTextField.text!)
            //let country = fv.validateName(title: countryTextField.placeholder!, text: countryTextField.text!)
            //let state = fv.validateName(title: stateTextField.placeholder!, text: stateTextField.text!)
            let zipCode = fv.validateZipCode(title: zipCodeTextField.placeholder!, text: zipCodeTextField.text!)
        
            if (fullName.0 == false) {
                fullNameLabel.text = fullName.1
                fullNameStackView.shake()
                isValidate = false
            }
        
            if (addressLine1.0 == false) {
                addressLine1Label.text = addressLine1.1
                addressLine1StackView.shake()
                isValidate = false
            }
            if (zipCode.0 == false) {
                zipCodeLabel.text = zipCode.1
                zipCodeStackView.shake()
                isValidate = false
            }
        if (!isPhoneNumberValid) {
            mobileLabel.text = "Enter valid mobile number".localized
            mobileStackView.shake()
            isValidate = false
        }
           if (mobile2TextField.text!.count > 0) {
                if (!isPhoneNumberValid2) {
                    mobile2Label.text = "Enter valid mobile number".localized
                    mobile2StackView.shake()
                    isValidate = false
                }
            }
        if selectedCountry == nil   {
            
            countryLabel.text = "Select country".localized
            countryLabel.shake()
            isValidate = false
        }
        if  selectedState == nil    {
            
            stateLabel.text = "Select state".localized
            stateLabel.shake()
            isValidate = false
        }
        if  selectedCity == nil   {
            
            cityLabel.text = "Select city".localized
            cityLabel.shake()
            isValidate = false
        }
            if isValidate {
                print("Validation sucess")
                self.view.endEditing(true)
                self.callAddAddressService()
               // self.show(VerifyCodeController.loadVC())
    //            router.goToHomeAsRoot(from: self)
                }
        }
    //MARK:- APIS
      func callAddAddressService() {
          self.view.endEditing(true)
          //let code = btnCountryCode.titleLabel?.text!.trimmingCharacters(in: .whitespaces)
        var param:[String:Any] = ["full_name":fullNameTextField.text!,
                                  "country_id":selectedCountry!.countryId,
                                  "state_id":selectedState!.stateId,
                                  "city_id":selectedCity!.citiesId,
                                  "line_1":addressLine1TextField.text!,
                                  "zip_code":zipCodeTextField.text!,
                                  "mobile_country_id" : countryCode.removeCharacter(str: "+", with: ""),
                                  "mobile_number" : mobileTextField.text!.onlyDigits(),
                                  "is_primary" : true]
        
        if (isPhoneNumberValid2) {
            param["landline_country_id"] = countryCode2.removeCharacter(str: "+", with: "")
            param["landline_number"] = mobile2TextField.text!.onlyDigits()
        }
        if(addressLine2TextField.text!.count > 0) {
           param["line_2"] = addressLine2TextField.text!
        }
        if(addressLine2TextField.text!.count > 0) {
           param["line_2"] = addressLine2TextField.text!
        }
        if(moreInfoTxtView.text!.count > 0) {
           param["more_info"] = moreInfoTxtView.text!
        }
        var apiUrl = ""
        if (isEditAddress){
            apiUrl = ApiEndPoints.editUserShippingAddress
            param["id"] = currentAddress?.id
        }
        else {
            apiUrl = ApiEndPoints.addUserShippingAddress
        }
        print(param)
        UserModuleManager().addAddressApi(endPoint: apiUrl, param: param, header: [:], completion: { (response,msg,errorMsg) in
            
            var message = ""
            if let mesg = msg {
                message = mesg
            }
            Alert.showSuccessMessage(msg: message, duration: 1)
            self.popViewController()
            
      }){ (error) in
              print("Error: ", error as Any)
          }
    }
    // MARK: - changePasswordPressed
    @IBAction func changePasswordPressed(_ sender: FlashButton) {
        let vc = ChangePasswordController.loadVC()
        vc.isChangePass = true
        self.show(vc)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddAddressController : UITextFieldDelegate {
    // MARK: - TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let currentCharacterCount = textField.text?.count ?? 0
        
        if (range.length + range.location > currentCharacterCount)
        {
            return false
        }
        
        let newLength = currentCharacterCount + string.count - range.length
        var maxLength = 50
        
        
        if (textField == fullNameTextField)
        {
            self.fullNameLabel.text = ""
            maxLength = 20
        }
        if (textField == zipCodeTextField)
        {
            self.zipCodeLabel.text = ""
            maxLength = 10
        }
        if (textField == addressLine1TextField)
        {
            self.addressLine1Label.text = ""
            maxLength = 20
        }
        if (textField == addressLine2TextField)
        {
            self.addressLine1Label.text = ""
            maxLength = 20
        }
        
        if (textField == mobileTextField)
        {
            self.mobileLabel.text = ""
            maxLength = 20
        }
        
        if (textField == countryTextField)
        {
            self.countryLabel.text = ""
            maxLength = 20
        }
        if (textField == stateTextField)
        {
            self.stateLabel.text = ""
             maxLength = 10
        }
        
        return newLength <= maxLength
        
    }
}
extension AddAddressController: FPNTextFieldDelegate {

   /// The place to present/push the listController if you choosen displayMode = .list
    func fpnDisplayCountryList() {
        print("fpnDisplayCountryList")
    }
   func fpnDisplayCountryList(textField: FPNTextField) {
    print(textField.tag)
    let controller = textField.tag == 100 ? listController : listController2
       let navigationViewController = UINavigationController(rootViewController: controller)

    controller.title = "Countries".localized
       controller.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))

       self.present(navigationViewController, animated: true, completion: nil)
   }
    @objc func dismissCountries() {
        listController.dismiss(animated: true, completion: nil)
        listController2.dismiss(animated: true, completion: nil)
    }
   /// Lets you know when a country is selected
   func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
      print(name, dialCode, code) // Output "France", "+33", "FR"
   // countryCode = code
    if (selectedPhoneTextField == mobileTextField) {
        mobileTextField.text = ""
        countryCode = dialCode
    }
    else if(selectedPhoneTextField == mobile2TextField) {
        mobile2TextField.text = ""
        countryCode2 = dialCode
    }
    
    
   }

   /// Lets you know when the phone number is valid or not. Once a phone number is valid, you can get it in severals formats (E164, International, National, RFC3966)
   func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        textField.rightViewMode = .always
        let cross = UIImage(named: "errorphone")
        let success = UIImage(named: "successphone")
        if (textField == mobileTextField) {
            mobileLabel.text = ""
            isPhoneNumberValid = isValid
            if textField.text!.count <= 0 {
                mobileTextField.rightView = nil
                mobileTextField.placeholder = mobileTextField.placeholder!.localized
            }
            else {
                textField.setRightIcon((isValid ? success : cross)!)
            }
        }
        else if (textField == mobile2TextField) {
            mobile2Label.text = ""
            isPhoneNumberValid2 = isValid
            if textField.text!.count <= 0 {
                mobile2TextField.rightView = nil
                mobile2TextField.placeholder = mobile2TextField.placeholder!.localized
            }
            else {
                textField.setRightIcon((isValid ? success : cross)!)
            }
        }
//        mobileLabel.text = ""
//        isPhoneNumberValid = isValid
//        if textField.text!.count <= 0 {
//            mobileTextField.rightView = nil
//            mobileTextField.placeholder = mobileTextField.placeholder!.localized
//        }
//        else {
//            textField.setRightIcon((isValid ? success : cross)!)
//        }
//
       print(
           isValid,
           textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
           textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
           textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
           textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
           textField.getRawPhoneNumber() ?? "Raw: nil"
       )
   }
}

extension AddAddressController {
    func getCountriesService<T : Decodable>(type : PopUpListType, param : [String : Any]? = nil, api : String, modelType : T.Type) {
        CommonModuleManager().getCountriesApi(endPoint: api, param: param, header: [:], modelType: modelType, completion: { (response,msg,errorMsg) in
                
                print(response)
                if let data = response {
                    switch type {
                    case .country:
                        self.countryList = data as! [Country]
                    case .state:
                        self.stateList = data as! [State]
                    case .city:
                        self.cityList = data as! [City]
                    default:
                        print("")
                    }
                    //self.locationList.append(contentsOf: data)
                    print(response)
                }
            }){ (error) in
                    print("Error: ", error as Any)
                }
            
        }
}

extension AddAddressController: BottomPopupDelegate {
    
    func bottomPopupViewLoaded() {
        print("bottomPopupViewLoaded")
    }
    
    func bottomPopupWillAppear() {
        print("bottomPopupWillAppear")
    }
    
    func bottomPopupDidAppear() {
        print("bottomPopupDidAppear")
    }
    
    func bottomPopupWillDismiss() {
        print("bottomPopupWillDismiss")
    }
    
    func bottomPopupDidDismiss() {
        print("bottomPopupDidDismiss")
    }
    
    func bottomPopupDismissInteractionPercentChanged(from oldValue: CGFloat, to newValue: CGFloat) {
        print("bottomPopupDismissInteractionPercentChanged fromValue: \(oldValue) to: \(newValue)")
    }
}
extension AddAddressController : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        //errorMessageLbl.text = ""
        
        if numberOfChars <= 500 {
            self.characterCountLbl.text = "\(numberOfChars)/500"
        }
        
        return numberOfChars < 501
    }
}
