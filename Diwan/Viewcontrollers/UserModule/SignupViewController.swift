//
//  SignupViewController.swift
//  Volchain
//
//  Created by Waqas Ali on 13/04/2019.
//  Copyright © 2019 Waqas Ali. All rights reserved.
//

import UIKit
//
//import CountryPickerViewSwift
import FlagPhoneNumber
import ActionSheetPicker_3_0
class SignupViewController: BaseViewController, StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.userModule.rawValue, nil)
    
    @IBOutlet weak var viewContainerForm: BaseUIView!
    @IBOutlet weak var btnSkip: BaseUIButton!
    
    @IBOutlet weak var firstNameStackView: UIStackView!
    @IBOutlet weak var lastNameStackView: UIStackView!
    @IBOutlet weak var emailStackView: UIStackView!
    @IBOutlet weak var mobileStackView: UIStackView!
    @IBOutlet weak var dobStackView: UIStackView!
    @IBOutlet weak var genderStackView: UIStackView!
    @IBOutlet weak var passwordStackView: UIStackView!
    @IBOutlet weak var confirmPasswordStackView: UIStackView!
    
    
    @IBOutlet weak var firstNameTextField: BaseFloatingTextField!
    @IBOutlet weak var lastNameTextField: BaseFloatingTextField!
    @IBOutlet weak var emailTextField: BaseFloatingTextField!
    @IBOutlet weak var mobileTextField: FPNTextField!
    @IBOutlet weak var dobTextField: BaseFloatingTextField!
    @IBOutlet weak var genderTextField: BaseFloatingTextField!
    @IBOutlet weak var passwordTextField: BaseFloatingTextField!
    @IBOutlet weak var confirmPasswordTextField: BaseFloatingTextField!
    
    @IBOutlet weak var firstNameLabel: BaseUILabel!
    @IBOutlet weak var lastNameLabel: BaseUILabel!
    @IBOutlet weak var emailLabel: BaseUILabel!
    @IBOutlet weak var mobileLabel: BaseUILabel!
    @IBOutlet weak var dobLabel: BaseUILabel!
    @IBOutlet weak var genderLabel: BaseUILabel!
    @IBOutlet weak var passwordLabel: BaseUILabel!
    @IBOutlet weak var confirmPasswordLabel: BaseUILabel!
    
    @IBOutlet weak var btnAgreeTerms: UIButton!
    
    @IBOutlet weak var signUpBtn: FlashButton!
    @IBOutlet weak var termsConditionBtn: FlashButton!
    
    let imgChecked = UIImage.init(named: "login_boxtick1")
    let imgUnChecked = UIImage.init(named: "login_boxwithouttick1")
    var isTermChecked = false
     var isValidate = true
    var countryCode = "+971"
    var selectedDob : Date?
    var selectedGender : UserGender?
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    var validPhoneImage = UIImageView(image: UIImage(named: "errorphone"))
    var isPhoneNumberValid = false
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Sign up".localized
        createBackBtn()
        // Do any additional setup after loading the view.
        //self.isHideNavigation(false)
        setUpViews()
        setupButton()
         self.setStatusBarStyle(.default)
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            // self.isHideNavigation(true)
        resetErrorViews()
    }
    
    func resetErrorViews() {
        
        firstNameLabel.text = ""
        lastNameLabel.text = ""
        emailLabel.text = ""
        mobileLabel.text = ""
        dobLabel.text = ""
        genderLabel.text = ""
        passwordLabel.text = ""
        confirmPasswordLabel.text = ""
    }
    
    
    func setUpViews(){
        
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        emailTextField.delegate = self
        mobileTextField.delegate = self
        dobTextField.delegate = self
        genderTextField.delegate = self
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        
        firstNameTextField.setInitialUI()
        lastNameTextField.setInitialUI()
        emailTextField.setInitialUI()
        //mobileTextField.setInitialUI()
        dobTextField.setInitialUI()
        genderTextField.setInitialUI()
        passwordTextField.setInitialUI()
        confirmPasswordTextField.setInitialUI()
        
        let mobileFont = UIFont.init(name: FontManager.constant(forKey: "fontSFRegular")!, size: CGFloat(FontManager.style(forKey: "sizeMedium")))
        mobileTextField.font = mobileFont
        mobileTextField.textColor = ColorManager.color(forKey: "themeBlue")
        
        mobileTextField.displayMode = .list
        listController.setup(repository: mobileTextField.countryRepository)
        listController.didSelect = { [weak self] country in
            self?.mobileTextField.setFlag(countryCode: country.code)
        }
        mobileTextField.setFlag(key: .AE)
        mobileTextField.keyboardType = .asciiCapableNumberPad
        mobileTextField.placeholder = "Mobile Number Optional".localized
        //mobileTextField.hasPhoneNumberExample = false
        //mobileTextField.setFlag(key: .AE)
       //mobileTextField.leftView?.backgroundColor = .red
       // let rect = mobileTextField.leftView?.bounds
       // print(rect)
    
        //mobileTextField.leftViewRect(forBounds: CGRect(x: rect!.minX, y: rect!.minY, width: rect!.width + 20, height: rect!.height))
       //  mobileTextField.flagButtonSize = CGSize(width: 50, height: 25)
    }
    
    @IBAction func selectDateOfBirth(_ sender: Any) {
        
        //let secondsInDay: TimeInterval = 24 * 60 * 60;
        //let previousDate = Date(timeInterval: -secondsInWeek, since: Date())
        let datePicker = ActionSheetDatePicker(title: "Date of Brith".localized,
                                               datePickerMode: UIDatePicker.Mode.date,
                                               selectedDate: selectedDob ?? Date().decrementYears(numberOfYears: 18),
                                               target: self,
                                               action: #selector(datePicked(_:)),
                                               origin: self.view)
       // let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
        //datePicker?.minimumDate = Date(timeInterval: -secondsInWeek, since: Date())
        datePicker?.maximumDate = Date()//Date(timeInterval: secondsInWeek, since: Date())

        datePicker?.show()
    }
    @objc func datePicked(_ date: Date) {
        print("Date picked \(date)")
        //2020-06-09T09:39:24.976Z
        print(date.toString(DateFormatType.defaultDate))
        selectedDob = date
        dobTextField.text = date.toString(DateFormatType.defaultDisplayFormat)
    }
    @IBAction func selectGender(_ sender: Any) {
        
        ActionSheetStringPicker.show(withTitle: "Gender".localized,
                                     rows: ["Male".localized, "Female".localized],
                                     initialSelection: (selectedGender?.rawValue ?? 1) - 1,
        doneBlock: { picker, value, index in
           print("picker = \(String(describing: picker))")
           print("value = \(value)")
            self.selectedGender = UserGender(rawValue: value + 1)
            self.genderTextField.text = self.selectedGender == UserGender.male ? "Male".localized : "Female".localized
           print("index = \(String(describing: index))")
           return
        },
        cancel: { picker in
           return
        },
        origin: sender)
    }
    
    
    func setupButton() {
        signUpBtn.didTapOnBounceStart = {[weak self] in
        }
        termsConditionBtn.didTapOnBounceStart = {[weak self] in
        }
        
        signUpBtn.didTapOnBounceEnd = {[weak self] in
            self?.signUpBtnTapped(self!.termsConditionBtn)
        }
        termsConditionBtn.didTapOnBounceEnd = {[weak self] in
            self?.btnTermsTapped(self!.termsConditionBtn)
        }
    }
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showCountryPicker(_ sender: Any) {
    
//        let countryView = CountrySelectView.shared
//        countryView.show()
//        //        countryView.dismiss() //dismiss the picker view
//        countryView.barTintColor = .gray //default is green
//        countryView.searchBarPlaceholder = "phone code" //default is "search"
//        countryView.displayLanguage = .english //default is english
//        countryView.countryNameFont = UIFont.systemFont(ofSize: 15) //default is  UIFont.systemFont(ofSize: 17)
//       countryView.countryPhoneCodeFont = UIFont.systemFont(ofSize: 15)//default is  UIFont.systemFont(ofSize: 14)
//        countryView.countryNameColor = .darkGray //default is gray
//       //  countryView.countryPhoneCodeColor = ColorManager.sharedInstance.color(forKey: "theme") ?? .black
//        countryView.selectedCountryCallBack = { countryDic in
//            print(countryDic)
//            /* countrydic format like
//             [
//             "en": "Angola",
//             +                    "es": "Angola",
//             "zh": "安哥拉",
//             "locale": "AO",
//             "code": 244
//             "countryImage": UIImage
//             ]
//             
//             */
//            var codeStr = "+971"
//            if let cc = countryDic["code"] as? Int{
//                codeStr = "+\(cc)"
//            }
//            
//            self.btnCountryCode.setTitle(codeStr, for: .normal)
//        }
    }
    
  
    
    @IBAction func agreeTermsTapped(_ sender: Any) {
        
        
        if isTermChecked{
           btnAgreeTerms.setImage(imgUnChecked!, for: .normal)
        }else{
          btnAgreeTerms.setImage(imgChecked!, for: .normal)
        }
        isTermChecked = !isTermChecked
    }
    
    
    @IBAction func btnTermsTapped(_ sender: Any) {
        
        let controller = TermsAndConditionController.loadVC()
        controller.type = .terms
        self.show(controller)
    }
    
   
    @IBAction func signUpBtnTapped(_ sender: Any) {
        //self.show(VerifyCodeController.loadVC())
        isValidate = true
        
        let fv = FormValidator()
        
        let firstName = fv.validateName(title: firstNameTextField.placeholder!, text: firstNameTextField.text!)
        let lastName = fv.validateName(title: lastNameTextField.placeholder!, text: lastNameTextField.text!)
        let email = fv.validateEmail(title: emailTextField.placeholder!, text: emailTextField.text!)
        let mobileNum = fv.validatePhoneNo(title: mobileTextField.placeholder!, text: mobileTextField.text!)
        let dob = fv.validateName(title: dobTextField.placeholder!, text: dobTextField.text!)
        let gender = fv.validateName(title: genderTextField.placeholder!, text: genderTextField.text!)
        let password = fv.validatePassword(title: passwordTextField.placeholder!, text: passwordTextField.text!)
        let confirmPassword = fv.validateConfirmPassword(title: confirmPasswordTextField.placeholder!, pwd: passwordTextField.text!, cpwd: confirmPasswordTextField.text!)
        
        if (firstName.0 == false) {
            firstNameLabel.text = firstName.1
            firstNameStackView.shake()
            isValidate = false
        }
        if (lastName.0 == false) {
            lastNameLabel.text = lastName.1
            lastNameStackView.shake()
            isValidate = false
        }
        
        if (email.0 == false) {
            emailLabel.text = email.1
            emailStackView.shake()
            isValidate = false
        }
        if (mobileTextField.text!.count > 0) {
            if (!isPhoneNumberValid) {
                mobileLabel.text = "Enter valid mobile number".localized
                mobileStackView.shake()
                isValidate = false
            }
        }
        //        if (btnCountryCode.titleLabel?.text)! == "Code" {
        //            lblErrorMobileNum.text = "Select country code"
        //            stackViewMobileNumber.shake()
        //            isValidate = false
        //        }
//        if (mobileNum.0 == false) {
//            mobileLabel.text = mobileNum.1
//            mobileStackView.shake()
//            isValidate = false
//        }
        
        
//        if (dob.0 == false) {
//            dobLabel.text = dob.1
//            dobStackView.shake()
//            isValidate = false
//        }
//
//        if (gender.0 == false) {
//            genderLabel.text = gender.1
//            genderStackView.shake()
//            isValidate = false
//        }
        
      
        
        if (password.0 == false) {
            passwordLabel.text = password.1
            passwordStackView.shake()
            isValidate  = false
        }
        
        if (confirmPassword.0 == false) {
            confirmPasswordLabel.text = confirmPassword.1
            confirmPasswordStackView.shake()
            isValidate = false
        }
        
      
        if isTermChecked == false  && isValidate == true{
            isValidate = false
           
            Alert.showErrorMessage(msg: "Agree Terms And Conditions".localized)
        }
 
        
        if isValidate {
            print("Validation sucess")
            self.view.endEditing(true)
            self.callSignUpService()
           // self.show(VerifyCodeController.loadVC())
//            router.goToHomeAsRoot(from: self)
            }
    }
      //MARK:- APIS
      func callSignUpService() {
          self.view.endEditing(true)
          //let code = btnCountryCode.titleLabel?.text!.trimmingCharacters(in: .whitespaces)
        var param:[String:Any] = ["first_name":firstNameTextField.text!,
                                  "last_name":lastNameTextField.text!,
                                  "email_address": emailTextField.text!,
                                  "password": passwordTextField.text!,
                                  "device_token": CurrentUser.deviceToken,
                                  "platform_id":CurrentUser.platformId]
        
        if let gender = selectedGender {
            param["gender_id"] = gender.rawValue
        }
        if let date = selectedDob {
            param["date_of_birth"] = date.toString("yyyy-MM-dd")
        }
        if (isPhoneNumberValid) {
            param["phone_country_id"] = countryCode.removeCharacter(str: "+", with: "")
            param["phone_number"] = mobileTextField.text!.onlyDigits()
        }
        print(param)
        UserModuleManager().userLoginApi(endPoint: ApiEndPoints.signUp, param: param, header: [:], completion: { (response,msg,errorMsg) in
            
            var message = ""
            if let mesg = msg {
                message = mesg
            }
            self.showAlert(title: nil, subTitle: message, doneBtnTitle: "OK".localized) {
                               // CurrentUser.data = response
                                let vc = VerifyCodeController.loadVC()
                                vc.email = self.emailTextField.text!
                                vc.fromSignUp = true
                                self.show(vc)
            }

      }){ (error) in
              print("Error: ", error as Any)
          }
    }
}


extension SignupViewController : UITextFieldDelegate {
    // MARK: - TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let currentCharacterCount = textField.text?.count ?? 0
        
        if (range.length + range.location > currentCharacterCount)
        {
            return false
        }
        
        let newLength = currentCharacterCount + string.count - range.length
        var maxLength = 50
        
        
        if (textField == firstNameTextField)
        {
            self.firstNameLabel.text = ""
            maxLength = 20
        }
        
        if (textField == lastNameTextField)
        {
            self.lastNameLabel.text = ""
            maxLength = 20
        }
        if (textField == emailTextField)
        {
            self.emailLabel.text = ""
            maxLength = 30
        }
        
        if (textField == mobileTextField)
        {
            self.mobileLabel.text = ""
            maxLength = 20
        }
        
        if (textField == dobTextField)
        {
            self.dobLabel.text = ""
            maxLength = 20
        }
        if (textField == genderTextField)
        {
            self.genderLabel.text = ""
             maxLength = 10
        }
        
        
        if (textField == passwordTextField)
        {
            self.passwordLabel.text = ""
            maxLength = 20
        }
        
       if (textField == confirmPasswordTextField)
        {
            self.confirmPasswordLabel.text = ""
            maxLength = 20
        }
        
       
        
        return newLength <= maxLength
        
    }
}
extension SignupViewController: FPNTextFieldDelegate {

   /// The place to present/push the listController if you choosen displayMode = .list
   func fpnDisplayCountryList(textField: FPNTextField) {
       let navigationViewController = UINavigationController(rootViewController: listController)

       listController.title = "Countries"
       listController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))

       self.present(navigationViewController, animated: true, completion: nil)
   }
    @objc func dismissCountries() {
        listController.dismiss(animated: true, completion: nil)
    }
   /// Lets you know when a country is selected
   func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
      print(name, dialCode, code) // Output "France", "+33", "FR"
    countryCode = dialCode
    mobileTextField.text = ""
    
   }

   /// Lets you know when the phone number is valid or not. Once a phone number is valid, you can get it in severals formats (E164, International, National, RFC3966)
   func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        textField.rightViewMode = .always
        let cross = UIImage(named: "errorphone")
        let success = UIImage(named: "successphone")
        mobileLabel.text = ""
        isPhoneNumberValid = isValid
        if textField.text!.count <= 0 {
            mobileTextField.rightView = nil
            mobileTextField.placeholder = "Mobile Number Optional".localized
        }
        else {
            textField.setRightIcon((isValid ? success : cross)!)
        }
      // textField.rightView = UIImageView(image: isValid ? success : cross)
       // validPhoneImage.image = isValid ? success : cross
    
       print(
           isValid,
           textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
           textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
           textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
           textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
           textField.getRawPhoneNumber() ?? "Raw: nil"
       )
   }
}
extension FPNTextField {

 /// set icon of 20x20 with left padding of 8px
 func setRightIcon(_ icon: UIImage) {

    let padding = 8
    let size = 20

    let outerView = UIView(frame: CGRect(x: 0, y: 0, width: size+padding, height: size) )
    let iconView  = UIImageView(frame: CGRect(x: padding, y: 0, width: size, height: size))
    iconView.image = icon
    outerView.addSubview(iconView)

    rightView = outerView
    rightViewMode = .always
  }
}
