//
//  LoginViewController.swift
//  Volchain
//
//  Created by Waqas Ali on 13/04/2019.
//  Copyright © 2019 Waqas Ali. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import Firebase
import AuthenticationServices
import TwitterKit
import ViewAnimator

class LoginViewController: BaseViewController, StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.userModule.rawValue, nil)
    
    @IBOutlet weak var signInBtn: FlashButton!
    @IBOutlet weak var resetBtn: FlashButton!
    @IBOutlet weak var guestBtn: FlashButton!
    @IBOutlet weak var signUpBtn: FlashButton!
    
    @IBOutlet weak var facebookBtn: BounceButton!
    @IBOutlet weak var twitterBtn: BounceButton!
    @IBOutlet weak var googleBtn: BounceButton!
    @IBOutlet weak var appleBtn: BounceButton!
    
    @IBOutlet weak var btnSkip: BaseUIButton!
    
    @IBOutlet weak var stackViewEmail: UIStackView!
    @IBOutlet weak var stackViewPassword: UIStackView!
    @IBOutlet weak var txtFieldEmail: BaseFloatingTextField!
    @IBOutlet weak var lblErrorEmail: BaseUILabel!
    @IBOutlet weak var txtFieldPassword: BaseFloatingTextField!
    @IBOutlet weak var lblErrorPassword: BaseUILabel!
    @IBOutlet weak var btnResetNow: BaseUIButton!
    @IBOutlet weak var btnCreateAccount: BaseUIButton!
    @IBOutlet weak var viewContainerTextFields: BaseUIView!
    
    
    var isValidate = true
    var isShowPassword = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            appleBtn.isHidden = false
        } else {
            appleBtn.isHidden = true
        }
        GIDSignIn.sharedInstance().delegate = self
        self.navigationItem.title = "Sign In".localized
        setupAppDefaultNavigationBar()
        createBackBtn()
        // Do any additional setup after loading the view.
        setUpViews()
        setupButton()
     self.setStatusBarStyle(.default)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
        self.isHideNavigation(false)
        
        lblErrorEmail.text = ""
        lblErrorPassword.text = ""
       
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if (isFirstTimeLoad == true) {
            animateButtons()
            isFirstTimeLoad = false
        }
        
    }
    
    func setUpViews()  {
        let resetfont = UIFont(name: FontManager.constant(forKey: "fontRegular")!,size: CGFloat(FontManager.style(forKey: "sizeSmall")))!
        
        let attributesResetNow = [ NSAttributedString.Key.font: resetfont,
                                   NSAttributedString.Key.foregroundColor : ColorManager.color(forKey: "blueText")!,
                                   NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue
            ] as [NSAttributedString.Key : Any]
        
        let attrResetString = NSMutableAttributedString(string: "Reset Now", attributes: attributesResetNow)
        let attrTermsString = NSMutableAttributedString(string: "Create Account", attributes: attributesResetNow)
        
        //btnResetNow.setAttributedTitle(attrResetString, for: .normal)
        //btnCreateAccount.setAttributedTitle(attrTermsString, for: .normal)
        
        txtFieldEmail.delegate = self
        txtFieldPassword.delegate = self
        txtFieldEmail.setInitialUI()
        txtFieldPassword.setInitialUI()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        txtFieldPassword.rightView?.addGestureRecognizer(tap)
        
        viewContainerTextFields.layer.shadowColor = UIColor.lightGray.cgColor
        viewContainerTextFields.layer.shadowOpacity = 0.3
        viewContainerTextFields.layer.shadowOffset = CGSize(width: 2 , height:2)
        viewContainerTextFields.layer.shadowRadius = 4
        //viewContainerTextFields.layer.shadowPath = UIBezierPath(rect: viewContainerTextFields.bounds).cgPath
    
        
      //  let buttonHeight = viewContainerTextFields.frame.height
      //  let buttonWidth = viewContainerTextFields.frame.width
        
     //   let shadowSize: CGFloat = 10
      //  let contactRect = CGRect(x: -shadowSize, y: buttonHeight - (shadowSize * 0.2), width: buttonWidth + shadowSize * 2, height: shadowSize)
       // viewContainerTextFields.layer.shadowPath = UIBezierPath(ovalIn: contactRect).cgPath
      //  viewContainerTextFields.layer.shadowRadius = 5
      //  viewContainerTextFields.layer.shadowOpacity = 0.6
    }
    func setupButton() {
        
        facebookBtn.alpha = 0.0
        twitterBtn.alpha = 0.0
        googleBtn.alpha = 0.0
        appleBtn.alpha = 0.0
        facebookBtn.didTapOnBounceStart = {[weak self] in
        }
        twitterBtn.didTapOnBounceStart = {[weak self] in
        }
        googleBtn.didTapOnBounceStart = {[weak self] in
        }
        appleBtn.didTapOnBounceStart = {[weak self] in
        }
        
        resetBtn.didTapOnBounceStart = {[weak self] in
        }
        signUpBtn.didTapOnBounceStart = {[weak self] in
        }
        guestBtn.didTapOnBounceStart = {[weak self] in
        }
        
        facebookBtn.didTapOnBounceEnd = {[weak self] in
            self?.loginWithFacebook()
        }
        twitterBtn.didTapOnBounceEnd = {[weak self] in
            self?.loginWithTwitter()
        }
        googleBtn.didTapOnBounceEnd = {[weak self] in
            GIDSignIn.sharedInstance()?.presentingViewController = self
            GIDSignIn.sharedInstance().signIn()
        }
        appleBtn.didTapOnBounceEnd = {[weak self] in
            
            if #available(iOS 13.0, *) {
                self?.loginWithApple()
            } else {
                // Fallback on earlier versions
            }
        }
        signInBtn.didTapOnBounceEnd = {[weak self] in
            self?.signInBtnTapped(self!.signInBtn)
        }
        resetBtn.didTapOnBounceEnd = {[weak self] in
            self?.show(ForgotPasswordController.loadVC())
        }
        signUpBtn.didTapOnBounceEnd = {[weak self] in
            //self?.navigationController?.pushViewController(SignupViewController.loadVC(), animated: false)
            let controller = SignupViewController.loadVC()
            controller.heroModalAnimationType = .zoomSlide(direction: .up)
//             controller.heroModalAnimationType = .zoomSlide(direction: HeroDefaultAnimationType.Direction.left)
           self?.show(controller)
            //self?.show(SignupViewController.loadVC())
        }
        guestBtn.didTapOnBounceEnd = {[weak self] in
            CurrentUser.userLoginType = .guest
            UserDefaults.standard.set(CurrentUser.userLoginType.rawValue, forKey: UserDefaultKey.userLoginType)
            router.goToHomeAsRoot()
           // self.show(SignupViewController.loadVC())
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        guard let password = txtFieldPassword.text, password != "" else {
            return
        }
        print("jadssjdsjdsjdsojdosjdsj ")
        isShowPassword = !isShowPassword
        txtFieldPassword.rightImage = isShowPassword == true ? UIImage(named: "eyeicon")?.maskWithColor(color: ColorManager.color(forKey: "themeBlue")!) : UIImage(named: "eyeicon")
        txtFieldPassword.isSecureTextEntry = isShowPassword == true ? false : true
        txtFieldPassword.rightView?.gestureRecognizers?.removeAll()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        txtFieldPassword.rightView?.addGestureRecognizer(tap)
        //UIImage(named: "eyeicon")?.maskWithColor(color: ColorManager.color(forKey: "themeBlue")!)
        
    }
    @IBAction func btnSkipTapped(_ sender: Any) {
        self.view.endEditing(true)
     
        return
    }
    
    
    @IBAction func createAccountTapped(_ sender: Any) {
        self.show(FAQsController.loadVC())
        //self.show(MyAccountController.loadVC())
          //self.show(SignupViewController.loadVC())
    }
    
    @IBAction func signInBtnTapped(_ sender: Any) {
       // txtFieldEmail.text = "a@a.com"
        //txtFieldPassword.text = "123456"
        self.view.endEditing(true)
//        router.goToHomeAsRoot()
       isValidate = true
        let fv = FormValidator()
        
        let email = fv.validateEmail(title: txtFieldEmail.placeholder!, text: txtFieldEmail.text!)
        let password = fv.validatePassword(title: txtFieldPassword.placeholder!, text: txtFieldPassword.text!)
        
        if (email.0 == false) {
            lblErrorEmail.text = email.1
            stackViewEmail.shake()
            isValidate  = false
        }
        
        if (password.0 == false) {
            lblErrorPassword.text = password.1
            stackViewPassword.shake()
            isValidate  = false
        }
        
        if isValidate {
            callSignInService()
            //self.show(SignupViewController.loadVC())
            
        }
    }

    

    
}

extension LoginViewController:UITextFieldDelegate {
    // MARK: - TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let currentCharacterCount = textField.text?.count ?? 0
        
        if (range.length + range.location > currentCharacterCount)
        {
            return false
        }
        
        let newLength = currentCharacterCount + string.count - range.length
        var maxLength = 50
        
        if (textField == txtFieldEmail)
        {
            self.lblErrorEmail.text = ""
            maxLength = 30
        }
        if (textField == txtFieldPassword)
        {
            self.lblErrorPassword.text = ""
            maxLength = 30
        }
        return newLength <= maxLength
        
    }
}

extension LoginViewController : GIDSignInDelegate {

    
    func loginWithTwitter() {

            TWTRTwitter.sharedInstance().logIn { (session, error) in
                
                print(session)
                
                if (session != nil)
                {
                    DispatchQueue.main.async {
                        AFNetwork.shared.showSpinner(nil)
                    }
                    print(session!);
                    print("signed in as \(session!.userName)");
                    
                    let client = TWTRAPIClient.withCurrentUser()
                    let request = client.urlRequest(withMethod: "GET",
                                                    urlString: "https://api.twitter.com/1.1/account/verify_credentials.json",
                                                    parameters: ["include_email": "true", "skip_status": "true"],
                                                    error: nil)
                    client.sendTwitterRequest(request) { response, data, connectionError in
                        
                        
                        
                        if (connectionError == nil)
                        {
                            do
                            {
                                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                                let email = json["email"] as? String
                               // self.socialType = "twitter"
                                
                                client.loadUser(withID: session!.userID, completion: { (user, error) in
                                    
                                    let FullName = user?.name ?? ""
                                    let ProviderKey = user?.userID ?? ""
                                    let ProfileImage = user?.profileImageLargeURL ?? ""
                                    let EmailID = email ?? ProviderKey + "@twitter.com"
                                    
                                    print("name: ", FullName)
                                    print("email: ", EmailID)
                                    print("avatar: ", ProfileImage)
                                    print("socialId: ", ProviderKey)
                                    print("JSON response: ", json)
                                    
                                    let param:[String:Any] = ["user_name": FullName,
                                                                         "email_address": EmailID,
                                                                         "provider_key": ProviderKey,
                                                                         "device_token":CurrentUser.deviceToken,
                                                                         "profile_image":ProfileImage,
                                                                         "social_provider_id": UserSocialLoginType.twitter.rawValue]
                                    self.callSocialSignInService(param: param, loginType: .twitter)
                                    
                                })
                            }
                            catch
                            {
                                DispatchQueue.main.async {
                                    AFNetwork.shared.hideSpinner()
                                }
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                AFNetwork.shared.hideSpinner()
                            }
                            print("Error: \(String(describing: connectionError))")
                        }
                    }
                }
                else
                {
                    //Constants.UIWINDOW?.makeToast(NSLocalizedString("Kindly login to your twitter account in setting", comment: ""), duration: 2, position: .center)
    //                self.btnTwitter.isUserInteractionEnabled = true
                    print("error: \(String(describing: error?.localizedDescription))");
                }
            }

            
        }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
            let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(okayAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        //Alert.showLoader()
        guard let authentication = user.authentication else {return }
        
        let FullName = user.profile.name ?? ""
        let ProviderKey = user.userID ?? ""
        let ProfileImage = user.profile.imageURL(withDimension: 400)?.absoluteString ?? ""
        let EmailID = user.profile.email ?? ""
        
        let param:[String:Any] = ["user_name": FullName,
                                  "email_address": EmailID,
                                  "provider_key": ProviderKey,
                                  "device_token":CurrentUser.deviceToken,
                                  "profile_image":ProfileImage,
                                  "social_provider_id": UserSocialLoginType.google.rawValue]
        self.callSocialSignInService(param: param, loginType: .google)
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print(user)
        print(error)
    }
    
    
    func loginWithFacebook() {
        
        
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
            
            guard let result = result else {
                print("No result found")
                return
            }
            
            guard AccessToken.current != nil else {
                return
            }
            if result.isCancelled {
                print("Cancelled \(error?.localizedDescription ?? "")")
            } else if let error = error {
                print("Process error \(error.localizedDescription)")
            } else {
                print("Logged in")
                self.getFBUserData()
            }
            //Alert.showLoader()
    //       let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
            //self.getFBUserData()
            // Perform login by calling Firebase APIs
//            Auth.auth().signIn(with: credential, completion: { (user, error) in
//                if let error = error {
//                    let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
//                    let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
//                    alertController.addAction(okayAction)
//                    self.present(alertController, animated: true, completion: nil)
//                    return
//                }else {
//
//                    self.callSocialSignInService(user: user, loginType: .facebook)
//                }
//
//                })
            
        }
    }
    public func getFBUserData(){
        
        DispatchQueue.main.async {
            AFNetwork.shared.showSpinner(nil)
        }
        if AccessToken.current != nil{
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    guard let res = result as? NSDictionary else {return}
                    let FullName = (res["name"] as? String) ?? ""
                    let ProviderKey = (res["id"] as? String) ?? ""
                    var ProfileImage = ""
                    if let picture = res["picture"] as? NSDictionary{
                        if let data = picture["data"] as? NSDictionary{
                            if let url = data["url"] as? String{
                                ProfileImage = "https://graph.facebook.com/\(ProviderKey)/picture?type=large&width=400&height=400"
                                //ProfileImage = url
                            }
                        }
                    }
                    let EmailID = (res["email"] as? String) ?? ""
                    let param:[String:Any] = ["user_name": FullName,
                                                         "email_address": EmailID,
                                                         "provider_key": ProviderKey,
                                                         "device_token":CurrentUser.deviceToken,
                                                         "profile_image":ProfileImage,
                                                         "social_provider_id": UserSocialLoginType.facebook.rawValue]
                    self.callSocialSignInService(param: param, loginType: .facebook)
                }
                else {
                    DispatchQueue.main.async {
                        AFNetwork.shared.hideSpinner()
                    }
                }
            })
        }
    }
    @available(iOS 13.0, *)
    func loginWithApple() {
        self.handleLogInWithAppleIDButtonPress()
    }
    
    @available(iOS 13.0, *)
     private func performExistingAccountSetupFlows() {
         // Prepare requests for both Apple ID and password providers.
         let requests = [ASAuthorizationAppleIDProvider().createRequest(), ASAuthorizationPasswordProvider().createRequest()]
         
         // Create an authorization controller with the given requests.
         let authorizationController = ASAuthorizationController(authorizationRequests: requests)
         authorizationController.delegate = self
         authorizationController.presentationContextProvider = self
         authorizationController.performRequests()
     }
     @available(iOS 13.0, *)
    private func handleLogInWithAppleIDButtonPress() {
         let appleIDProvider = ASAuthorizationAppleIDProvider()
         let request = appleIDProvider.createRequest()
         request.requestedScopes = [.fullName, .email]
         
         let authorizationController = ASAuthorizationController(authorizationRequests: [request])
         authorizationController.delegate = self
         authorizationController.presentationContextProvider = self
         authorizationController.performRequests()
     }
    

}
extension LoginViewController : ASAuthorizationControllerDelegate {
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
//        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
//        self.present(alert, animated: true, completion: nil)
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            
            // Create an account in your system.
            // For the purpose of this demo app, store the these details in the keychain.
            
            if let identityTokenData = appleIDCredential.identityToken,
                let identityTokenString = String(data: identityTokenData, encoding: .utf8) {
                print("Identity Token \(identityTokenString)")
            }
            
            //Perform apple login
            let EmailID = appleIDCredential.email ?? "Apple user\(Int.random(in: 0...100000))"
            
            let FirstName = "\(appleIDCredential.fullName?.givenName ?? "")"
            let LastName = "\(appleIDCredential.fullName?.familyName ?? "")"
            
            var FullName = "\(FirstName) \(LastName)"
            
            if FirstName.isEmpty{
                FullName = EmailID.components(separatedBy: "@").first ?? ""
            }
            if FullName.isEmpty{
                FullName = EmailID
            }
            let ProviderKey = "\(appleIDCredential.user)"
            let param:[String:Any] = ["user_name": FullName,
                                                 "email_address": EmailID,
                                                 "provider_key": ProviderKey,
                                                 "device_token":CurrentUser.deviceToken,
                                                 "profile_image":"",
                                                 "social_provider_id": UserSocialLoginType.apple.rawValue]
            print(param)
            self.callSocialSignInService(param: param, loginType: .apple)
            //print(params)
            //self.socialLogin(params: params)
            
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
            // For the purpose of this demo app, show the password credential as an alert.
            DispatchQueue.main.async {
                let message = "The app has received your selected credential from the keychain. \n\n Username: \(username)\n Password: \(password)"
                let alertController = UIAlertController(title: "Keychain Credential Received",
                                                        message: message,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
}
//MARK:- APIS
extension LoginViewController {
    

    func callSignInService() {
        let param:[String:Any] = ["email_address": txtFieldEmail.text!,
                                  "password": txtFieldPassword.text!,
                                  "device_token": CurrentUser.deviceToken,
                                  "platform_id":CurrentUser.platformId]
        
        
        UserModuleManager().userLoginApi(endPoint: ApiEndPoints.signIn, param: param, header: [:], completion: { (response,msg,errorMsg) in
            
            print(response)
            
            
            if (response!.isEmailVerified) {
                CurrentUser.data?.isSocialLogin = false
                CurrentUser.userLoginType = .user
                UserDefaults.standard.set(false, forKey: UserDefaultKey.isSocialLogin)
                CurrentUser.data = response
                try! Constants.realm.write(){
                    if CurrentUser.data != nil {
                        Constants.realm.add(CurrentUser.data!, update: .all)
                    }
                }
                router.goToHomeAsRoot()
            }else{
                var message = ""
                if let mesg = errorMsg {
                    message = mesg
                }
                self.showAlert(title: nil, subTitle: message, doneBtnTitle: "OK".localized, completionAction: {
                    let vc = VerifyCodeController.loadVC()
                    vc.email = self.txtFieldEmail.text!
                    vc.fromSignUp = true
                    self.show(vc)
                })

            }

        }){ (error) in
                print("Error: ", error as Any)
            }

    }
    
    func callSocialSignInService(param:[String:Any], loginType : UserSocialLoginType) {
            
            print(param)
        
        UserModuleManager().userLoginApi(endPoint: ApiEndPoints.socialSignIn, param: param, header: [:], completion:  { (response,msg,errorMsg) in
                        
                        print(response)
                        
                        
            if (response!.isEmailVerified) {
                            
                            CurrentUser.data = response
                CurrentUser.userLoginType = .user
                CurrentUser.data?.isSocialLogin = true
                UserDefaults.standard.set(true, forKey: UserDefaultKey.isSocialLogin)
                            try! Constants.realm.write(){
                                if CurrentUser.data != nil {
                                    Constants.realm.add(CurrentUser.data!, update: .all)
                                }
                            }
                        
                          router.goToHomeAsRoot()
                        }else{
                            var message = ""
                            if let mesg = msg {
                                message = mesg
                            }
                            self.showAlert(title: nil, subTitle: message, doneBtnTitle: "OK".localized, completionAction: {
                                let vc = VerifyCodeController.loadVC()
                                vc.email = self.txtFieldEmail.text!
                                vc.fromSignUp = true
                                self.show(vc)
                            })

                        }

                    }){ (error) in
                        print("Error: ", error as Any)
                    }

        }
}

extension LoginViewController : ASAuthorizationControllerPresentationContextProviding {
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
//ANIMATION
extension LoginViewController {
    
    func animateButtons() {
        
//        facebookBtn.alpha = 0.0
//        twitterBtn.alpha = 0.0
//        googleBtn.alpha = 0.0
//        appleBtn.alpha = 0.0
        let zoomAnimation = AnimationType.zoom(scale: 0.2)
        let rotateAnimation = AnimationType.rotate(angle: CGFloat.pi/6)
//        UIView.animate(views: [facebookBtn,twitterBtn,googleBtn,appleBtn],
//        animations: [zoomAnimation, rotateAnimation],
//        duration: 0.5)
        
        UIView.animate(views: [facebookBtn,twitterBtn,googleBtn,appleBtn], animations: [zoomAnimation, rotateAnimation], reversed: false, initialAlpha: 0.0, finalAlpha: 1.0, delay: 0, animationInterval: 0.3, duration: 0.5) {
            
        }
    }
}
