//
//  MyAccountController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 07/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import InitialsImageView
import RSKImageCropper
class MyAccountController: BaseViewController , StoryBoardHandler {

    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.userModule.rawValue , nil)
    
    
    @IBOutlet weak var camera1Btn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var camera2Btn: UIButton!
    
    @IBOutlet weak var nameLabel: BaseUILabel!
    @IBOutlet weak var emailLabel: BaseUILabel!
    @IBOutlet weak var phoneLabel: BaseUILabel!
    @IBOutlet weak var profileImageView: BaseUIImageView!
    
    @IBOutlet weak var arrow1: BaseUIImageView!
    @IBOutlet weak var arrow2: BaseUIImageView!
    @IBOutlet weak var arrow3: BaseUIImageView!
    @IBOutlet weak var arrow4: BaseUIImageView!
    @IBOutlet weak var arrow5: BaseUIImageView!
    var userData : User? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = " "
        emailLabel.text = " "
        phoneLabel.text = " "
        self.navigationItem.title = "Account".localized
        setupAppDefaultNavigationBar()
        createNavSideMenuBtn()
        setupLanguageData()
        self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard userData != nil else {
            self.getUserProfile()
            return
        }
        self.setUserData()
    }
    func setupLanguageData() {
        
        arrow1.image = arrow1.image?.flipIfNeeded()
        arrow2.image = arrow2.image?.flipIfNeeded()
        arrow3.image = arrow3.image?.flipIfNeeded()
        arrow4.image = arrow4.image?.flipIfNeeded()
        arrow5.image = arrow5.image?.flipIfNeeded()
        
    }
    @IBAction func orderHistory(_ sender: UIButton) {
        self.alertNextPhase()
    }
    
    @IBAction func myAddresses(_ sender: Any) {
        
    }
    @IBAction func returnExchange(_ sender: Any) {
        let controller = TermsAndConditionController.loadVC()
        controller.type = .returnExchange
        self.show(controller)
    }
    @IBAction func supportAction(_ sender: Any) {
        
        let controller = ContactUsController.loadVC()
        controller.contactType = ContactType.support
        self.show(controller)
        
    }
    @IBAction func editProfile(_ sender: Any) {
        
  //      self.show(AddAddressController.loadVC())
        self.show(EditProfileController.loadVC())
    }
    @IBAction func changeProfilePicture(_ sender: Any) {
        
        var isValidPic = false
        if let pic = CurrentUser.data?.profilePictureUrl {
            if (pic.verifyUrl()) {
                isValidPic = true
            }
        }
        self.showImagePicker(isShowRemoveButton: isValidPic, allowEditing: false)
        
    }
    
    @IBAction func signoutPressed(_ sender: BaseUIButton) {
        self.logoutAlert()
    }
    // MARK: - changePasswordPressed
    @IBAction func changePasswordPressed(_ sender: Any) {
        let vc = ChangePasswordController.loadVC()
        vc.isChangePass = true
        self.show(vc)
    }
    func setUserData() {
        
        let isSocialLogin = UserDefaults.standard.bool(forKey: UserDefaultKey.isSocialLogin)
        print(isSocialLogin)
        if (isSocialLogin) {
            editBtn.isHidden = true
            camera2Btn.isHidden = true
            camera2Btn.isHidden = true
        }
        CurrentUser.data = Constants.realm.objects(User.self).first
        userData = CurrentUser.data
        if let user = userData {
        
            nameLabel.text = user.fullName
            //nameLabel.text = user.firstName + " " + user.lastName
            profileImageView.setImageForName(nameLabel.text!, backgroundColor: nil, circular: true, textAttributes: nil, gradient: false)
            profileImageView.setImageNamePlaceholderCircular(url: user.profilePictureUrl,placeholder: profileImageView.image)
            emailLabel.text = user.emailAddress
            
            phoneLabel.isHidden = true
            if let number = user.phoneNumber {
                if (number.count > 0) {
                    phoneLabel.isHidden = false
                    phoneLabel.text = "+" + user.phoneCountryCode + user.phoneNumber
                }
            }

            
            
        }
    }
    
    //MARK:- APIS

    func getUserProfile() {
        let param:[String:Any] = [:]
        UserModuleManager().getApi(endPoint: ApiEndPoints.getProfile, param: nil, header: [:], completion: { (response,msg,errorMsg) in
            
            print(response)
            let token = CurrentUser.data!.authToken!
            let isSocialLogin = CurrentUser.data!.isSocialLogin
            
            CurrentUser.data = response
            CurrentUser.data?.isSocialLogin = isSocialLogin
            CurrentUser.data?.authToken = token
            if (CurrentUser.data?.isSocialLogin == true){
                //CurrentUser.data?.fullName = CurrentUser.data!.userName!
            }
            else {
                CurrentUser.data?.fullName = "\(String(describing: CurrentUser.data!.firstName!)) \(String(describing: CurrentUser.data!.lastName!))"
            }
            self.userData = CurrentUser.data
            
            try! Constants.realm.write(){
                if CurrentUser.data != nil {
                    Constants.realm.add(CurrentUser.data!, update: .all)
                }
            }
            self.setUserData()
        }){ (error) in
                print("Error: ", error as Any)
            }

    }

}
extension MyAccountController : ImagePickerPresentable,RSKImageCropViewControllerDelegate {
    
    //MARK:-RSKImageCropViewControllerDelegate
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        //_ = self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true) {
            
        }
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
//        self.avatarImageView.image = croppedImage
        //let imageVuew = UIImageView(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
        //self.view.addSubview(imageVuew)
        //imageVuew.image = croppedImage
        self.dismiss(animated: true) {
            print(croppedImage)
            self.addPictureApi(img : croppedImage)
        }
       // _ = self.navigationController?.popViewController(animated: true)
    }
    //MARK:-ImagePickerPresentable
    func deletePicture() {
        print("delete picture")
        Alert.showWithTwoActions(title: "", msg: "Are you sure you want remove profile picture".localized, okBtnTitle: "YES".localized, okBtnAction: {
            self.removePictureApi()
        }, cancelBtnTitle: "NO".localized) {
            
        }
        
    }
    
    func showImagePicker() {
        //self.showImagePicker(isShowRemoveButton: true)
    }
    
    func selectedImage(data: Data?) {
        print(data)
    }
    
    func getImage(img: UIImage?) {
        var imageCropVC : RSKImageCropViewController!
        guard img != nil else {
            return
        }
        imageCropVC = RSKImageCropViewController(image: img!, cropMode: RSKImageCropMode.circle)

        imageCropVC.delegate = self
        self.present(imageCropVC, animated: true) {
            
        }
       // self.navigationController?.pushViewController(imageCropVC, animated: true)

    }
    
    func removePictureApi() {
        UserModuleManager().getApi(endPoint: ApiEndPoints.removeProfilePicture, param: nil, header: [:], completion: { (response,msg,errorMsg) in
            
            print(response)
           // self.profileImageView.setImageForName(self.nameLabel.text!, backgroundColor: nil, circular: true, textAttributes: nil, gradient: false)
           // self.profileImageView.setImageNamePlaceholderCircular(url: "",placeholder: self.profileImageView.image)
            
            try! Constants.realm.write(){
                if CurrentUser.data != nil {
                    CurrentUser.data?.profilePictureUrl = String()
                    Constants.realm.add(CurrentUser.data!, update: .all)
                }
            }
            self.setUserData()
            
        }){ (error) in
                print("Error: ", error as Any)
            }

    }
    func addPictureApi(img : UIImage) {
        
        let imgArr:[UIImage] = [img.kf.resize(to: CGSize(width:1500,height:1500), for: .aspectFill)]
        //let imgArr:[UIImage] = [img]
        UserModuleManager().apiEditProfilePicture(endPoint: ApiEndPoints.updateProfilePicture, param: nil, header: [:], image: imgArr) { (user,msg,errorMsg) in
            
            //let token = CurrentUser.data!.authToken!
            //CurrentUser.data = user
            //CurrentUser.data?.authToken = token
            self.userData = CurrentUser.data
            
            try! Constants.realm.write(){
                if CurrentUser.data != nil {
                    CurrentUser.data?.profilePictureUrl = user?.profilePictureUrl
                    Constants.realm.add(CurrentUser.data!, update: .all)
                }
            }
            self.userData = CurrentUser.data
            self.setUserData()
        }

        

    }
    
}
