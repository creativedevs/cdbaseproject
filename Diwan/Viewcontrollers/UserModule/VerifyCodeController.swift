//
//  VerifyCodeController.swift
//  SalonDesBeaute
//
//  Created by Shujaatkhan on 22/11/2018.
//  Copyright © 2018 High- Sierra 13.6. All rights reserved.
//

import UIKit


class VerifyCodeController: BaseViewController , StoryBoardHandler{
    
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.userModule.rawValue , nil)

    @IBOutlet weak var resendStackView: BaseUIStackView!
    @IBOutlet weak var btnResendCode: BaseUIButton!
    @IBOutlet weak var txtFldPincode: PinCodeTextField!
    @IBOutlet weak var lblErrorPinCode: BaseUILabel!
    @IBOutlet weak var countDownLabel: UILabel!
    var fromSignUp = false
    
    var email = ""
    
    
    var timer = Timer()
    var secondsLeft = 120
    var minute: Int!
    var seconds: Int!
    var timeformat = "seconds"
    
    @IBOutlet weak var counterView: UIView!
    @IBOutlet weak var submitBtn: FlashButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Verification".localized
        createBackBtn()
        setupButton()
        txtFldPincode.text = ""
        txtFldPincode.delegate = self
        txtFldPincode.keyboardType = .asciiCapableNumberPad
        txtFldPincode.becomeFirstResponder()
        resendStackView.isHidden = true
//        if fromSignIn {
//            resendCode()
//        }
        
        startTimer()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        timer.invalidate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isHideNavigation(false)
        lblErrorPinCode.text = ""
    }
    
    func setupButton() {
        submitBtn.didTapOnBounceStart = {[weak self] in
        }
        
        submitBtn.didTapOnBounceEnd = {[weak self] in
            self?.verifyClicked(self!.submitBtn!)
        }
    }

    @IBAction func verifyClicked(_ sender: Any) {
        
        //self.view.endEditing(true)
        
        guard let count =  txtFldPincode.text?.count else {
            txtFldPincode.shake()
            lblErrorPinCode.text = NSLocalizedString("Enter 4 digit code".localized, comment: "")
            return
        }
        
        if count < 4 {
            txtFldPincode.shake()
            lblErrorPinCode.text = NSLocalizedString("Enter 4 digit code".localized, comment: "")
            return
        }
        
        callVerifyCodeService()
    
    }
    
    
    func startTimer() {
        
        secondsLeft = 120
        btnResendCode.isEnabled = false
        btnResendCode.alpha = 0.5
        countDownLabel.text = String(format: "02:00")
        //let min = Utility.convertNumberToEnglish(number: String(format: "00"))
        //let sec = Utility.convertNumberToEnglish(number: String(format: "10"))
       // countDownLabel.text = min + ":" + sec
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
    
    
    @objc func updateCounter() {
        
        // if(secondsLeft >= 0 ) {
        secondsLeft -= 1
        
        minute = (secondsLeft % 3600) / 60
        seconds = (secondsLeft % 3600) % 60
        
//        if minute < 1{
//            timeformat = "seconds"
//            minutes.text = timeformat
//        }
        //let min = Utility.convertNumberToEnglish(number: String(format: "%02d",minute))
        //let sec = Utility.convertNumberToEnglish(number: String(format: "%02d",seconds))
        //countDownLabel.text = min + ":" + sec
        countDownLabel.text = String(format: "%02d:%02d", minute, seconds)
        
        
        if secondsLeft == 0 {
            
            timer.invalidate()
            btnResendCode.isEnabled = true
            btnResendCode.alpha = 1.0
            resendStackView.isHidden = false
            counterView.isHidden = true
        }
        
        
    }
    
    @IBAction func resendCode(_ sender: Any) {
        resendCode()
        
    }
    

    func callVerifyCodeService() {
        self.view.endEditing(true)
        var otpType = 2
        if self.fromSignUp {
            otpType = 1
        }
        let param:[String:Any] = ["email_address": email,
                                  "otp_type_id": otpType,
                                  "otp_code": txtFldPincode.text!]
        
        print(param)
        UserModuleManager().verifyOTPApi(endPoint: ApiEndPoints.verifyOTP, param: param, header: [:], completion: { (user,msg,errorMsg) in
            
            print(msg)
            if self.fromSignUp {
                CurrentUser.userLoginType = .user
                CurrentUser.data?.isSocialLogin = false
                CurrentUser.data = user
                try! Constants.realm.write(){
                    if CurrentUser.data != nil {
                        Constants.realm.add(CurrentUser.data!, update: .all)
                    }
                }

                router.goToHomeAsRoot()
            }else{

                let vc = ChangePasswordController.loadVC()
                vc.email = self.email
                //vc.auth = (response.user?.token)!
                self.show(vc)
            }

        }){ (error) in
            print("Error: ", error as Any)
            self.txtFldPincode.text = ""
            self.txtFldPincode.becomeFirstResponder()
        }

    }
    func resendCode() {
        self.view.endEditing(true)
        
       // txtFldPincode.becomeFirstResponder()
        
        var otpType = 2
        if self.fromSignUp {
            otpType = 1
        }
        let param:[String:Any] = ["email_address": email,
                                  "otp_type_id": otpType]
        
        print(param)
        UserModuleManager().verifyOTPApi(endPoint: ApiEndPoints.resendOTP, param: param, header: [:], completion: { (user,msg,errorMsg) in
            
            print(msg)
            var message = ""
            if let mesg = msg {
                message = mesg
            }
            Alert.showSuccessMessage(msg: message, duration: 1)
            Utility.delay(seconds: 1.2) {
                self.txtFldPincode.becomeFirstResponder()
                self.startTimer()
                self.txtFldPincode.text = ""
                self.resendStackView.isHidden = true
                self.counterView.isHidden = false
            }
            
            
        }){ (error) in
            print("Error: ", error as Any)
            self.txtFldPincode.text = ""
            self.txtFldPincode.becomeFirstResponder()
        }
    }
    
//    func callForgotPassService() {
//        txtFldPincode.text = ""
//        self.view.endEditing(true)
//
//        let parameter: [String : Any] = ["email": email]
//
//        UserModuleManager().postApis(endPoint: FORGOT_PASS, param: parameter, header: [:]) { (response) in
//
//            Alert.showWithCompletion(title: "", msg: "An email has been sent to your account with verification code", btnActionTitle: "OK") {
//
//            }
//
//        }

 //   }
    
}

//MARK:- PinCodeTextFiled Delegate
extension VerifyCodeController: PinCodeTextFieldDelegate {

    func textFieldValueChanged(_ textField: PinCodeTextField) {
        lblErrorPinCode.text = ""
    }

}
