//
//  SplashViewController.swift
//  Volchain
//
//  Created by Waqas Ali on 13/04/2019.
//  Copyright © 2019 Waqas Ali. All rights reserved.
//

import UIKit


class SplashViewController: UIViewController, StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.userModule.rawValue, nil)
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      //  self.setStatusBarStyle(.lightContent)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //let controller = LanguageViewController.loadVC()
        //controller.modalPresentationStyle = .fullScreen
        
        Utility.delay(seconds: 4) {
            //self.show(controller)
            //self.navigationController?.pushViewController(controller, animated: true)
            //self.present(controller, animated: false, completion: nil)
           // router.splashToLoginAsRoot()
            //router.splashToLoginAsRoot(vc: self)
            self.checkUserLoggedIn()
        }
        
    }
    func checkUserLoggedIn(){
        
        CurrentUser.data = Constants.realm.objects(User.self).first
        if CurrentUser.data != nil {
            CurrentUser.userLoginType = .user
            router.goToHomeAsRoot()
        }else{
            CurrentUser.userLoginType = .guest
            router.splashToLoginAsRoot()
        }
        
    }
}
