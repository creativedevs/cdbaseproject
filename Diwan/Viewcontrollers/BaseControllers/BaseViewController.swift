//
//  BaseViewController.swift
//

import UIKit
import SwiftMessages
import MOLH
import FCAlertView
import LGRefreshView
import ViewAnimator
class BaseViewController: UIViewController {
    
    var storedOffsets = [Int: CGFloat]()
    var topSafeArea:CGFloat = 0.0
    var yPostion:CGFloat = 0.0
    
    var refreshView:LGRefreshView!
    var isRefreshing:Bool = false
    var perPageCount = 50
    var currentPage = 1
    var shouldShowLoadingCell = false
    var isFirstTimeLoad = true
    var filledData : (title : String, listType : ListViewType, screenFilterType : FilterScreenListType)?
    //var refreshData : ((() -> Void))?
    func alertNextPhase() {
        Alert.showInfoMessage(msg: "This feature will be implement in next phase", duration: 2.0)
    }
    @objc func showWishListScreen() {
        let storyboard = UIStoryboard(name: "Store", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "StoreProductListController") as! StoreProductListController
        controller.filledData = ( "My Wishlist".localized, .wishlist, .none)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @objc func showCartScreen() {
        let storyboard = UIStoryboard(name: "OrderManagement", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CartListController") as! CartListController
        self.navigationController?.pushViewController(controller, animated: true)
        
       // self.alertNextPhase()
    }
    @objc func showSearchScreen() {
        self.alertNextPhase()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let window = UIApplication.shared.keyWindow

        if #available(iOS 11.0, *) {
            topSafeArea = (window?.safeAreaInsets.top)!
            //    bottomSafeArea = view.safeAreaInsets.bottom
        } else {
            topSafeArea = topLayoutGuide.length
            //     bottomSafeArea = bottomLayoutGuide.length
        }
        
        topSafeArea = (topSafeArea < 1) ? 20 : topSafeArea
        
        if topSafeArea > 20 {
            yPostion = DesignUtility.getValueFromRatio(45)
        }else{
           yPostion = DesignUtility.getValueFromRatio(42)
        }
 

        self.hideKeyboardWhenTappedAround()
    }
   func setupColorNavBarWithLogo(){
       
    //self.setupTransparentNavBar()
       let logoContainer = UIView(frame: CGRect(x: 2, y: 0, width: 130, height: 50))
       let imageView = UIImageView(frame: CGRect(x: 2, y: 0, width: 130, height: 44))
       imageView.contentMode = .scaleAspectFit
       imageView.image = UIImage(named: "topbardiwanicon")
       logoContainer.addSubview(imageView)
       self.navigationItem.titleView = logoContainer
       
   }
    func createRefreshView(tblView : UITableView) {
        refreshView = LGRefreshView.init(scrollView: tblView, refreshHandler: { ( ref ) in
            
            if Internet.isConnected {
                self.isRefreshing = true
                self.shouldShowLoadingCell = false
                self.currentPage = 1
                self.refreshData()
            }else{
                self.refreshView.endRefreshing()
                Alert.showMsg(msg: NSLocalizedString("No internet connection!.", comment: ""))
            }
            
        })
        refreshView.tintColor = ColorManager.color(forKey: "themeBlue")
        refreshView.backgroundColor = UIColor.clear
    }
    func refreshData() {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hideNavigation(status: Bool){
        self.navigationController?.navigationBar.isHidden = status
    }
    func isHideNavigation(_ status: Bool){
        //self.navigationController?.navigationBar.isHidden = status
        self.navigationController?.setNavigationBarHidden(status, animated: true)
    }
    
    func setNavBGColor() {
        self.isHideNavigation(false)
        self.navigationController?.navigationBar.barTintColor = ColorManager.color(forKey: "theme")
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationItem.hidesBackButton = true
    }
    

    func createSideMenuButton(){
        let btn = UIButton()
        btn.frame = CGRect(x: DesignUtility.getValueFromRatio(16), y: yPostion, width: DesignUtility.getValueFromRatio(27), height: DesignUtility.getValueFromRatio(22))
        btn.setImage(UIImage(named: "booking_nav"), for: .normal)
        btn.addTarget(self, action: #selector(sideMenuController?.showLeftViewAnimated(_:)), for: .touchUpInside)
        self.view.addSubview(btn)
    }

//    func setUpNavBottomImage(){
//        let imageView = UIImageView(image: UIImage(named: "navbar_bottom"))
//
    //        imageView.frame = CGRect(x: 30, y: 300, width: DesignUtility.getValueFromRa@objc tio(30), height: DesignUtility.getValueFromRatio(30))
//        imageView.translatesAutoresizingMaskIntoConstraints = false
//        self.view.addSubview(imageView)
//
//        if #available(iOS 11, *) {
//            imageView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0.0).isActive = true
//        } else {
//            imageView.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor, constant: 0.0).isActive = true
//        }
//
//        imageView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0.0).isActive = true
//        imageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0.0).isActive = true
//    }
    
    func setTitle(title:String){
        let font = UIFont.init(name: FontManager.constant(forKey: "fontMulEBold")!, size: CGFloat(FontManager.style(forKey: "sizeLarge")))
        
        let lblTitle = UILabel()
        lblTitle.text = title
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.numberOfLines = 1
        lblTitle.textAlignment = .center
        lblTitle.textColor = UIColor.white
        lblTitle.font = font
        
        lblTitle.backgroundColor = UIColor.red
        
        self.view.addSubview(lblTitle)
        lblTitle.heightAnchor.constraint(equalToConstant: 30).isActive = true
        lblTitle.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 250).isActive = true
        lblTitle.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        
    }

    func betaMessage() -> Void {
        let info = MessageView.viewFromNib(layout: .cardView)
        info.configureTheme(.info)
        info.button?.isHidden = true
        info.configureContent(title: "Info", body: "Will be implemented in beta")
        var infoConfig = SwiftMessages.defaultConfig
        infoConfig.presentationStyle = .bottom
        //infoConfig.presentationStyle = .top
        infoConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        infoConfig.duration = .seconds(seconds: 0.7)
        SwiftMessages.show(config: infoConfig, view: info)
    }
    
    //Mark : Customizing navigation bar, adding bar buttons and custom title view
    func setupAppDefaultNavigationBar()  {
        
        //Setting navigation bar background color, its font and title color
        let barBgColor = UIColor.init(hexString: "#F8F8F8")
        let titleFont = UIFont.init(name: "SFUIDisplay-Medium", size: DesignUtility.getFontSize(fSize: 19))
        
        self.navigationController?.navigationBar.setCustomNavigationBarWith(navigationBarTintColor: barBgColor, navigationBarTitleFont: titleFont!, navigationBarForegroundColor: ColorManager.color(forKey: "themeBlue")!)
        
//        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationItem.hidesBackButton = true
        //If navigation controller have more than 1 view controller then add backbutton
        if self.navigationController != nil{
            
//            self.setupSideButton()
        }
    }
    
    func createBackBtn(){
        guard let nav = self.navigationController else {
            return
        }
        if (self.navigationController?.viewControllers.count)! > 1{
            //Adding bar button items with given image and its position inside navigation bar and its selector
            let img = UIImage.init(named: "backarrow")?.flipIfNeeded()
            self.addBarButtonItemWithImage(img ?? UIImage(), .BarButtonItemPositionLeft, self, #selector(self.popViewController))
        }
        
    }
    
    func createNavSideMenuBtn(){
        guard let nav = self.navigationController else {
            return
        }
        if (self.navigationController?.viewControllers.count)! == 1{
            //Adding bar button items with given image and its position inside navigation bar and its selector
            let img = UIImage.init(named: "hamburger")?.flipIfNeeded()
            if MOLHLanguage.currentAppleLanguage() == "ar" {
            self.addBarButtonItemWithImage(img ?? UIImage(), .BarButtonItemPositionLeft, self, #selector(SideMenuController.showRightViewAnimated(_:)))
               }else{
                   self.addBarButtonItemWithImage(img ?? UIImage(), .BarButtonItemPositionLeft, self, #selector(SideMenuController.showLeftViewAnimated(_:)))
               }
            
        }
        
    }

    
    //Pop view controller
    @objc func popViewController() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //Adding bar button items with given image and its position inside navigation bar and its selector
    func addBarButtonItemWithImage(_ image:UIImage,_ postion: CustomBarButtonItemPosition, _ target:UIViewController, _ selector:Selector) {
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(image, for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: DesignUtility.getValueFromRatio(44), height: DesignUtility.getValueFromRatio(44))
        btn1.addTarget(self, action: selector, for: .touchUpInside)
        
        let item1 = UIBarButtonItem(customView: btn1)
     
            
            switch postion {
            case .BarButtonItemPositionLeft:
                
                if self.navigationItem.leftBarButtonItem != nil{
                    
                    if (self.navigationItem.leftBarButtonItems?.count)! > 0{
                        
                        self.navigationItem.leftBarButtonItems?.append(item1)
                    }
                }
                else{
                    
                    self.navigationItem.leftBarButtonItem = item1
                }
                
            case .BarButtonItemPositionRight:
                
                if self.navigationItem.rightBarButtonItem != nil{
                    
                    if (self.navigationItem.rightBarButtonItems?.count)! > 0{
                        
                        self.navigationItem.rightBarButtonItems?.append(item1)
                    }
                }
                else{
                    
                    self.navigationItem.rightBarButtonItem = item1
                }
            }
    }
    
    // Adding custom title view for navigation bar
    func addCustomTitleView(_ tileView:UIView) {
        
        // tileView.widthAnchor.constraint(equalToConstant: 150).isActive = true
        //tileView.heightAnchor.constraint(equalToConstant: 33).isActive = true
        
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        tileView.frame = titleView.bounds
        self.navigationItem.titleView = tileView
    }
    
    
    @objc func slideCell(cell : UITableViewCell) {
        
     //   Animations.slideView(view: cell)
    }
    
    func setLeftPaddingTextField(_ textfield : BaseUITextField) {
        
        let leftView =  UILabel(frame: CGRect(x: 0, y: 0, width: DesignUtility.getValueFromRatio(15), height: textfield.frame.size.height))
        leftView.backgroundColor = UIColor.clear
        
        textfield.leftView = leftView
        textfield.leftViewMode = .always
        textfield.contentVerticalAlignment = .center
    }
}

//Mark : - Custom delegate for accessing diferrent properties of navigation bar
//protocol CustomNavBarProtocol { }
//
//enum CustomNavBarEnum: CustomNavBarProtocol {
//
//    enum CustomBarButtonItemPosition: Int, CustomNavBarProtocol {
//
//        case  BarButtonItemPositionRight = 1
//        case  BarButtonItemPositionLeft = 2
//    }
//}

enum CustomBarButtonItemPosition: Int {
    
    case  BarButtonItemPositionRight = 1
    case  BarButtonItemPositionLeft = 2
}

//Customzing default navigation bar appearence
extension UINavigationBar
{
    
    func setCustomNavigationBarWith(navigationBarTintColor color:UIColor, navigationBarTitleFont titleFont:UIFont, navigationBarForegroundColor foregroundColor:UIColor)
    {
        
        //165    77    52
        barTintColor = color
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: foregroundColor, NSAttributedString.Key.font: titleFont]
        titleTextAttributes = (titleDict as! [NSAttributedString.Key : Any])
    }
}

//Mark : - Delegate for pressing navigation controller toolbar buttons
protocol DelegateToolbarButtons {
    
    func chatButtonPressed()
    func forwardButtonPressed()
}


extension UIViewController {
    
    func setStatusBarStyle(_ style: UIStatusBarStyle) {
        
        if #available(iOS 13.0, *) {
//            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
//            statusBar.backgroundColor = style == .lightContent ? UIColor.clear : UIColor.clear
//        
//            statusBar.setValue(style == .lightContent ? UIColor.white : .black, forKey: "foregroundColor")
            
        } else {
             if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                 statusBar.backgroundColor = style == .lightContent ? UIColor.clear : UIColor.clear
                 statusBar.setValue(style == .lightContent ? UIColor.white : .black, forKey: "foregroundColor")
             }
        }
        
    }
}
extension BaseViewController {
    
    func showAlert(title:String? , subTitle:String? , doneBtnTitle:String? = NSLocalizedString("OK", comment: ""), completionAction: (() -> Void)? = nil) {
        let subtitleFont = UIFont(name: FontManager.constant(forKey: "fontRegular")!,size: CGFloat(FontManager.style(forKey: "sizeSmall")))!
        
        let alert = FCAlertView()
        //withCustomImage:UIImage(named: "logo"),
        alert.showAlert(inView: self,
                        withTitle:title,
                        withSubtitle:subTitle,
                        withCustomImage:nil,
                        withDoneButtonTitle:doneBtnTitle,
                        andButtons:nil)
        
        alert.customImageScale = 1.2
        alert.cornerRadius = 10
        alert.subtitleFont = subtitleFont
        
        //alert.alertBackgroundColor = UIColor.init(hexString: "1D1D1D")
        alert.colorScheme = ColorManager.color(forKey: "themeBlue")
        alert.hideSeparatorLineView = true
        alert.detachButtons = true
        alert.subTitleColor = ColorManager.color(forKey: "darkGreyText")
        //alert.subTitleColor = ColorManager.color(forKey: "themeText")
        alert.titleColor = ColorManager.color(forKey: "themeBlue")
        
        alert.doneButtonCustomFont = subtitleFont
        
        alert.doneActionBlock {
            completionAction?()
        }

    }
    
}
//MARK:- Logout
extension BaseViewController {
    func logoutAlert() {
        DispatchQueue.main.async {
            Alert.showWithTwoActions(title: "Logout".localized, msg: "Are your sure you want to log out?".localized, okBtnTitle: "YES".localized, okBtnAction: {
                    
                print("Yes")
                self.logoutApi()
                
            }, cancelBtnTitle: "NO".localized) {
                print("No")
            }
        }
        
    }
    func logInAlert() {
        DispatchQueue.main.async {
            Alert.showWithTwoActions(title: "Logout".localized, msg: "Are your sure you want to log in?".localized, okBtnTitle: "YES".localized, okBtnAction: {
                    
                print("Yes")
                router.goToLoginAsRoot()
                
            }, cancelBtnTitle: "NO".localized) {
                print("No")
            }
        }
        
    }
    func logoutApi() {
        
        let param:[String:Any] = ["device_token": CurrentUser.deviceToken,
                                  "platform_id":CurrentUser.platformId]
        UserModuleManager().userLoginApi(endPoint: ApiEndPoints.logout, param: param, header: [:], completion: { (response,msg,errorMsg) in
            
            UserDefaults.standard.set(false, forKey: UserDefaultKey.isSocialLogin)
            try! Constants.realm.write {
                Constants.realm.delete(CurrentUser.data!)
            }
            CurrentUser.data = nil
            router.goToLoginAsRoot()
        }){ (error) in
                print("Error: ", error as Any)
            }
        
    }
}
//MARK:-APIS
extension BaseViewController {
    
    func addToCart(data : ProductModel) {
        var cartItem = Constants.realm.object(ofType: CartModel.self, forPrimaryKey: data.productId)
        if cartItem != nil {
            try! Constants.realm.write(){
                cartItem!.quantity = cartItem!.quantity + 1
                Constants.realm.add(cartItem!, update: .all)
            }
            
        }
        else {
            cartItem = CartModel()
            cartItem!.id = data.productId
            cartItem!.product = data
            try! Constants.realm.write(){
                Constants.realm.add(cartItem!, update: .all)
            }
        }
        
    }
       
       func addToCartOrNotifyMe(data : ProductModel, cell : StoreProductCollectionCell, indexPath : IndexPath,collectionView : UICollectionView) {
           
        if (CurrentUser.userLoginType == .guest) {
            self.logInAlert()
            return
        }
        
           var apiUrl = ""
           if (data.quantity > 0) {
            self.addToCart(data: data)
               return
           }
           else {
               if (data.isNotify == true) {return}
               apiUrl = ApiEndPoints.notifyMe
           }
           let param:[String:Any] = ["product_id": data.productId,"is_notify":!data.isNotify]
           StoreManager().addToCartOrNotifyMeApi(endPoint: apiUrl, param: param, header: [:], completion: { (response,msg,errorMsg) in
               
               data.isNotify = !data.isNotify
               collectionView.reloadItems(at: [indexPath])
               print(response)
               if let data = response {
                   
               }
               
           }){ (error) in
               // self.refreshView.endRefreshing()
               print("Error: ", error as Any)
           }
           
       }
       
       
    func addOrRemoveFavorite(data : ProductModel, indexPath : IndexPath, collectionView : UICollectionView? = nil, tableView : UITableView? = nil) {
        
        if (CurrentUser.userLoginType == .guest) {
            self.logInAlert()
            return
        }
        
        let param:[String:Any] = ["product_id": data.productId,"is_favorite":!data.isFavorite]
        print(param)
        StoreManager().addOrRemoveFavoriteApi(endPoint: ApiEndPoints.addToFavorite, param: param, header: [:], completion: { (response,msg,errorMsg) in 
            
            try! Constants.realm.write(){
                data.isFavorite = !data.isFavorite
            }
            
            if let col = collectionView {
                col.reloadItems(at: [indexPath])
            }
            if let tbl = tableView {
                tbl.reloadRows(at: [indexPath], with: .none)
            }
            
        }){ (error) in
            // self.refreshView.endRefreshing()
            print("Error: ", error as Any)
        }
        
    }
    
    
    
}
//ANIMATIONS
extension BaseViewController {
    
    func animateTableView(tblView: UITableView){
        
        guard  isFirstTimeLoad == true else {
            
            tblView.reloadData()
            return
        }
        isFirstTimeLoad = false
        tblView.reloadData()
        let cells = tblView.visibleCells
        let tableViewHeight = tblView.bounds.size.height
        var delayCounter = 0.0
        
        for cell in cells{
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        
        for cell in cells{
            UIView.animate(withDuration: 1.5, delay: (delayCounter * 0.09), usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            
            delayCounter += 1
        }
    }
    func animateCollectionViewBottomToTopCustom(colView: UICollectionView){
        
        guard  isFirstTimeLoad == true else {
            
            colView.reloadData()
            return
        }
        isFirstTimeLoad = false
        colView.reloadData()
        colView.layoutIfNeeded()
        print(colView.visibleCells)
        let cells = colView.visibleCells
        let tableViewHeight = colView.bounds.size.height
        var delayCounter = 0.0
        
        for cell in cells{
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        
        for cell in cells{
            UIView.animate(withDuration: 1.5, delay: (delayCounter * 0.09), usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            
            delayCounter += 1
        }
    }
    func animateCollectionView(collectionView : UICollectionView) {
        guard  isFirstTimeLoad == true else {
                   
                   collectionView.reloadData()
                   return
               }
               isFirstTimeLoad = false
        collectionView.reloadData()
        collectionView.animationBottomToTop()
    }
    func animateTableViewFromZoom(tblView: UITableView) {
        guard  isFirstTimeLoad == true else {
                   
                   tblView.reloadData()
                   return
               }
               isFirstTimeLoad = false
        tblView.reloadData()
        tblView.animateTableViewFromZoom()
    }
    func animationTableViewFromBottom(tblView: UITableView) {
        guard  isFirstTimeLoad == true else {
                   
                   tblView.reloadData()
                   return
               }
               isFirstTimeLoad = false
        tblView.reloadData()
        tblView.animationTableViewFromBottom()
    }
    func animationTableViewFromLeft(tblView: UITableView) {
        guard  isFirstTimeLoad == true else {
                   
                   tblView.reloadData()
                   return
               }
               isFirstTimeLoad = false
        tblView.reloadData()
        tblView.animationTableViewFromLeft()
    }
    
    public func zoomAnimation(view : UIView) {
        let zoomAnimation = AnimationType.zoom(scale: 0.2)
        UIView.animate(views: [view],
        animations: [zoomAnimation],
        duration: 0.5)
    }
    
}
