

import UIKit
import LGSideMenuController
import MOLH

class SideMenuController: LGSideMenuController, StoryBoardHandler{
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.sideMenu.rawValue , nil)
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        self.leftViewWidth =  DesignUtility.getValueFromRatio(300)
        self.rightViewWidth =  DesignUtility.getValueFromRatio(300)
        self.leftViewStatusBarStyle = .default
        self.rightViewStatusBarStyle = .default
        self.isLeftViewSwipeGestureEnabled = false
        self.isRightViewSwipeGestureEnabled = false
        self.leftViewPresentationStyle = .slideAbove
        self.rightViewPresentationStyle = .slideAbove
        setMenu()
    }
    func setMenu(){
        let vc = LeftMenuController.loadVC()
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            self.rightViewController = vc
            self.leftViewController = nil
        }else{
            self.leftViewController = vc
            self.rightViewController = nil
        }
    }
}


