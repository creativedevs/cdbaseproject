//
//  BaseNavigationController.swift

//

import UIKit
import Hero

class BaseNavigationController: UINavigationController, StoryBoardHandler
{
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.main.rawValue, nil)
    
    var isInternal = false
    //var controller: controllers!
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return UIStatusBarStyle.default
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.interactivePopGestureRecognizer?.isEnabled = false
        let navigationBar = self.navigationBar
        self.isHeroEnabled = true
        let titleFont = UIFont.init(name: FontManager.constant(forKey: "fontRegular")!, size: CGFloat(FontManager.style(forKey: "sizeLarge")))
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.green, NSAttributedString.Key.font: titleFont!]
        navigationBar.titleTextAttributes = (titleDict as! [NSAttributedString.Key : Any])
        navigationBar.tintColor = UIColor.white
       //navigationBar.shadowImage = UIImage()
      // navigationBar.isTranslucent = true
     //   navigationBar.setBackgroundImage(UIImage(named: "navbar_img")?.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0 ,right: 0), resizingMode: .stretch), for: .default)

        self.view.backgroundColor = UIColor.white
        self.navigationBar.barTintColor = UIColor.white
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
    }
    
}
