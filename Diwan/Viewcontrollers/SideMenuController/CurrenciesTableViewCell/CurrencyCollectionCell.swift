//
//  CurrencyCollectionCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 26/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class CurrencyCollectionCell: UICollectionViewCell {

    @IBOutlet weak var shadowImageView: BaseUIImageView!
    @IBOutlet weak var currencyImageView: BaseUIImageView!
    var data : Currency?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(data:Currency) {
        self.data = data
        
        currencyImageView.image = UIImage()
        currencyImageView.backgroundColor = UIColor.white

        if let img = self.data?.imageUrl {
            currencyImageView.setImage(url: img, placeholder: nil, isActivity: true, activityColor: ColorManager.color(forKey: "white"))
        }
        
    }

}
