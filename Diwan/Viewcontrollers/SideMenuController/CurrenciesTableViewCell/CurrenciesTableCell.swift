//
//  CurrenciesTableCell.swift
//  Diwan
//
//  Created by Muzamil Hassan on 26/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class CurrenciesTableCell: UITableViewCell {

    @IBOutlet weak var collectionViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: BaseUICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCollection()
    }
    func setUpCollection(){
        collectionViewWidthConstraint.constant = (DesignUtility.getValueFromRatio(35) * 4) + 24
        collectionView.registerCell(CurrencyCollectionCell.self)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension CurrenciesTableCell {

    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {

        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
    }

    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}
