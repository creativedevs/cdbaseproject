//
//  LeftMenuController.swift

import UIKit
import LGSideMenuController
import SwiftMessages
import Alamofire
import MOLH
enum Menu: String{
    case diwan = "Diwan"
    case celebrities = "Celebrities"
    case bookPharmacy = "BookPharmacy"
    case library = "Library"
    case authors = "Authors"
    case store = "Store"
    case brands = "Brands"
    case TV = "TV"
    case ehsanProject = "EhsanProject"
    case contactUS = "ContactUS"
    case FAQs = "FAQs"
    case liveChat = "LiveChat"
    case myAccount = "MyAccount"
    case myWishList = "MyWishList"
    case myDiwan = "MyDiwan"
}

extension Menu{
    
    func getVc() -> BaseViewController{
        
        switch self{
            
        case .diwan:
            let controller = HomeViewController.loadVC()
            return controller
            
        case .celebrities:
             let controller = CelebritiesMainListController.loadVC()
             return controller
            
        case .bookPharmacy:
            let controller = PharmacyMainCategoryController.loadVC()
            
            return controller
            
        case .library:
            let controller = LibraryMainListController.loadVC()
            return controller
            
        case .authors:
            let controller = AuthorMainListController.loadVC()
            return controller
            
        case .store:
            
            let controller = StoreCategoryListController.loadVC()
            return controller
        case .brands:
            let controller = BrandsMainListController.loadVC()
            return controller
        case .TV:
            let controller = EhsanMainListController.loadVC()
            controller.videoType = .diwanTV
            return controller
            
        case .ehsanProject:
            let controller = EhsanMainListController.loadVC()
            return controller
            
        case .contactUS:
            let controller = ContactUsController.loadVC()
            return controller
            
        case .FAQs:
            let controller = FAQsController.loadVC()
            return controller
            
        case .liveChat:
            let controller = ChatMainController.loadVC()
            return controller
        case .myAccount:
            let controller = MyAccountController.loadVC()
            return controller
            
        case .myWishList:
            let controller = StoreProductListController.loadVC()
            controller.filledData = ( "My Wishlist".localized, .wishlist, .none)
            return controller
            
        case .myDiwan:
            let controller = MyDiwanController.loadVC()
            return controller
        default:
            return BaseViewController()
            
        }
    }
}
extension LeftMenuController : UIDocumentInteractionControllerDelegate {
    
       //MARK: UIDocumentInteractionController delegates
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
         return self//or use return self.navigationController for fetching app navigation bar colour
     }
 }

class LeftMenuController: BaseViewController, StoryBoardHandler{

    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.sideMenu.rawValue,nil)
    
    @IBOutlet weak var versionBtn: BaseUIButton!
    @IBOutlet weak var tblSideMenu: UITableView!
    
    @IBOutlet weak var backBtn: BaseUIButton!
    @IBOutlet weak var signInLogoutWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var SignInLogoutBtn: BounceButton!
    @IBOutlet weak var bgImageView: UIImageView!
    private let kTableHeaderHeight: CGFloat = DesignUtility.getValueFromRatio(80)
   // var menuArray = [Menu.home.rawValue, Menu.discover.rawValue, Menu.settings.rawValue, Menu.account.rawValue, Menu.faq.rawValue, Menu.logout.rawValue]
    var navigationList = [NavigationData]()
    var currencyList = [Currency]()
    var socialMediaList = [SocialMedia]()
    let getDispatchGroup = DispatchGroup()
    var currencyIndex = 0
    var socialMediaIndex = 0
    @IBAction func versionBtnPressed(_ sender: Any) {
        
        let path =  Bundle.main.path(forResource: "ReleaseNote1.0.0", ofType: ".pdf")!
           let dc = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
           dc.delegate = self
           dc.presentPreview(animated: true)
        
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        backBtn.setImage(backBtn.currentImage?.flipIfNeeded(), for: .normal)
        bgImageView.backgroundColor = ColorManager.color(forKey: "themeBlue")
        SignInLogoutBtn.setTitle(CurrentUser.data != nil ? "SIGNOUT".localized : "SIGN IN / SIGN UP".localized, for: .normal)
        SignInLogoutBtn.setImage(CurrentUser.data != nil ? UIImage(named: "sidelogout2icon")?.flipIfNeeded() : UIImage(named: "sidelogouticon")?.flipIfNeeded(), for: .normal)
        if (CurrentUser.languageId == .arabic) {
            SignInLogoutBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -20)
        }
        else {
            SignInLogoutBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
        }
        //SignInLogoutBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -20)
        signInLogoutWidthConstraint.constant = CurrentUser.data != nil ? 103 : 157
        SignInLogoutBtn.didTapOnBounceEnd = {[weak self] in
            if CurrentUser.userLoginType == .guest {
                router.goToLoginAsRoot()
            }
            else {
                self?.logoutAlert()
            }
        }
        tblSideMenu.registerCell(CurrenciesTableCell.self)
        tblSideMenu.registerCell(SocialMediaTableViewCell.self)
        getNavigationListApi()
        getCurrenciesListApi()
        getSocialListApi()
        getDispatchGroup.notify(queue: .main) {
            print("END CALLS BOTH")
            self.tblSideMenu.reloadData()
           // self.stopAnimating()
        }
        //NotificationCenter.default.addObserver(self, selector: #selector(self.setHome(notification:)), name: .setHomeOnLeftMenu, object: nil)
    }
    @IBAction func hideSideMenu(_ sender: Any) {
        sideMenuController?.hideLeftViewAnimated(sender)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @objc func setHome(notification:Notification){
        
        tblSideMenu.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)
        if let cell = tblSideMenu.cellForRow(at: IndexPath.init(row: 0, section: 0)) as? SideMenuTableViewCell{
            //cell.lblTitle.fontColorTheme = "sideMenuBlueText"
        }
    }
    
    
    func setRootViewController(_ vc: BaseNavigationController){
        sideMenuController?.rootViewController = vc
        print(vc)
        hideSideMenu()
        
    }
    func hideSideMenu() {
        if CurrentUser.languageId == .english {
            sideMenuController?.hideLeftViewAnimated()
        }
        else {
            sideMenuController?.hideRightViewAnimated()
        }
    }
    
    func changeControllers(_ menu: Menu){

        if (menu == .diwan  || menu == .celebrities  || menu == .store || menu == .bookPharmacy || menu == .library) {
            var index = 0
            switch menu {
                case .diwan:
                    index = 0
                case .bookPharmacy:
                    index = 1
                case .celebrities:
                    index = 2
                case .library:
                    index = 3
                case .store:
                    index = 4
                default:
                    index = 0
            }
            self.sideMenuController?.rootViewController = TabBarProvider.tabbarWithNavigationStyle(index: index)
            hideSideMenu()
        }
        else {
            setRootViewController(BaseNavigationController(rootViewController: menu.getVc()))
        }

        

    }
    

}

extension LeftMenuController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if (indexPath.row == navigationList.count + 1) {
            let count = Int(ceil(10/4))
            var height = (DesignUtility.getValueFromRatio(35) * CGFloat(count))
            height = height + CGFloat((8 * (count - 1)))
            return  height + 54
        }
            else if (indexPath.row == navigationList.count) {
                let count = Int(ceil(10/4))
                var height = (DesignUtility.getValueFromRatio(35) * CGFloat(count))
                //height = height + CGFloat((8 * (count - 1)))
                return  height
            }
        else {
            return  DesignUtility.getValueFromRatio(40)
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(indexPath.row == self.navigationList.count ||
            indexPath.row == self.navigationList.count + 1) {
            return
        }
        
        if (tableView.cellForRow(at: indexPath ) as? SideMenuTableViewCell) != nil{
            let data = self.navigationList[indexPath.row]
            //data.
        }
      //  let cellType = self.menuArray[indexPath.row]
        let cellType = self.navigationList[indexPath.row]
        
        if let menu = Menu(rawValue: cellType.menuClass){
            DispatchQueue.main.async {
                self.changeControllers(menu)
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        
    }
}


extension LeftMenuController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return navigationList.count + 2
        //return menuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if (indexPath.row == navigationList.count + 1) {
            let cell:SocialMediaTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SocialMediaTableViewCell") as! SocialMediaTableViewCell
            socialMediaIndex = indexPath.row
            //cell.lblTitle.text = navigationList[indexPath.row].title ?? ""
            //cell.sideShape.setImage(url: navigationList[indexPath.row].iconImage ?? "", isActivity: true)
            cell.selectionStyle = .none
            //cell.backgroundColor = .green
            return cell
        }
           else if (indexPath.row == navigationList.count) {
               let cell:CurrenciesTableCell = tableView.dequeueReusableCell(withIdentifier: "CurrenciesTableCell") as! CurrenciesTableCell
                currencyIndex = indexPath.row
            cell.selectionStyle = .none
               //cell.lblTitle.text = navigationList[indexPath.row].title ?? ""
               //cell.sideShape.setImage(url: navigationList[indexPath.row].iconImage ?? "", isActivity: true)
            //cell.backgroundColor = .orange
               return cell
           }
        else {
            let cell:SideMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as! SideMenuTableViewCell
            
            cell.lblTitle.text = navigationList[indexPath.row].title ?? ""
            cell.sideShape.setImage(url: navigationList[indexPath.row].iconImage ?? "", isActivity: true)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == navigationList.count + 1 {
            guard let tableViewCell = cell as? SocialMediaTableViewCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }
        else if indexPath.row == navigationList.count {
            guard let tableViewCell = cell as? CurrenciesTableCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }
        
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == navigationList.count + 1 {
            guard let tableViewCell = cell as? SocialMediaTableViewCell else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }
        else if indexPath.section == navigationList.count {
            guard let tableViewCell = cell as? CurrenciesTableCell else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }
        
    }
}

//MARK:- Collection Datasource and Delegate
extension LeftMenuController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView.tag {
        case socialMediaIndex:
            return self.socialMediaList.count
        case currencyIndex:
            return self.currencyList.count
        default:
            print("")
        }
        return 0
        
        //return self.categoryList[collectionView.tag].productlist.count
        //return model[collectionView.tag].count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView.tag {
        case socialMediaIndex:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SocialMediaCollectionCell.identifier(), for: indexPath) as! SocialMediaCollectionCell
             let data = self.socialMediaList[indexPath.item]
            cell.setData(data: data)
             return cell
        case currencyIndex:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CurrencyCollectionCell.identifier(), for: indexPath) as! CurrencyCollectionCell
             let data = self.currencyList[indexPath.item]
            cell.setData(data: data)
             return cell
        default:
            print("")
        }
        
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           //
        //DesignUtility.getValueFromRatio(162)
           return CGSize(width: DesignUtility.getValueFromRatio(35), height: DesignUtility.getValueFromRatio(35))
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
           return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
       }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 8
       }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        
    }
}
//MARK:-APIs
extension LeftMenuController {
    
        func getNavigationListApi() {
            self.getDispatchGroup.enter()
            UserModuleManager().getNavigationListApi(endPoint: ApiEndPoints.getNavigationList, param: nil, header: [:], completion: { (response,msg,errorMsg) in
                print(response)
                if let data = response {
                    self.navigationList.append(contentsOf: data)
                   // self.tblSideMenu.reloadData()
                }
                self.getDispatchGroup.leave()
            }){ (error) in
                self.getDispatchGroup.leave()
                    print("Error: ", error as Any)
                }
            
        }
    func getCurrenciesListApi() {
        
        self.getDispatchGroup.enter()
        CommonModuleManager().getCurrenciesApi(endPoint: ApiEndPoints.getCurrencies, param: nil, header: [:], completion: { (response,msg,errorMsg) in
            print(response)
            if let data = response {
                self.currencyList.append(contentsOf: data)
                //self.tblSideMenu.reloadData()
                
            }
            self.getDispatchGroup.leave()
        }){ (error) in
                print("Error: ", error as Any)
            self.getDispatchGroup.leave()
            }
        
    }
    func getSocialListApi() {
        
        self.getDispatchGroup.enter()
        CommonModuleManager().getSocialPagesApi(endPoint: ApiEndPoints.getSocialMediaPages, param: nil, header: [:], completion: { (response,msg,errorMsg) in
            print(response)
            if let data = response {
                self.socialMediaList.append(contentsOf: data)
                //self.tblSideMenu.reloadData()
                
            }
            self.getDispatchGroup.leave()
        }){ (error) in
                print("Error: ", error as Any)
            self.getDispatchGroup.leave()
            }
        
    }
}
