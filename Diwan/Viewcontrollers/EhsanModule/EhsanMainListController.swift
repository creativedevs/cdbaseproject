//
//  LibraryMainListController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 25/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class EhsanMainListController: BaseViewController , StoryBoardHandler {

    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.ehsanProject.rawValue , nil)
    var ehsanVideoList = [EhsanModel]()
    var videoType : VideoType = .ehsanProject
    @IBOutlet weak var collectionView: BaseUICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.isHidden = true
        
        setupAppDefaultNavigationBar()
        createNavSideMenuBtn()
        createBackBtn()
        
        switch videoType {
        case .ehsanProject:
            self.navigationItem.title = "Ehsan Projects".localized
            self.addBarButtonItemWithImage(UIImage(named: "topbarheart")!, .BarButtonItemPositionLeft, self, #selector(showWishListScreen))
            
            self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
            
            self.addBarButtonItemWithImage(UIImage(named: "topbarsearch")!, .BarButtonItemPositionRight, self, #selector(showSearchScreen))
        case .diwanTV:
            self.navigationItem.title = "Diwan TV".localized
            self.addBarButtonItemWithImage(UIImage(named: "topbarheart")!, .BarButtonItemPositionLeft, self, #selector(showWishListScreen))
            
            self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
            
            self.addBarButtonItemWithImage(UIImage(named: "topbarfilter")!, .BarButtonItemPositionRight, self, #selector(showSearchScreen))
        case .myTV:
            self.navigationItem.title = "My TV".localized
        default:
            print("")
        }
        
        
        //createRefreshView(tblView: categoryListTbl)
        setUpCollection()
        self.getEhsanVideoListApi()
        
        
    }
    
    func setUpCollection(){
        
        collectionView.registerCell(EhsanMainCollectionCell.self)
    }
    override func refreshData() {
        print("refresh")
        self.currentPage = 1
        self.ehsanVideoList.removeAll()
        self.getEhsanVideoListApi()
       // self.refreshView.endRefreshing()
    }

}
//MARK:- Collection Datasource and Delegate
extension EhsanMainListController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return 10
        if self.ehsanVideoList.count == 0 {
            UtilityEmptyListView.emptycollectionViewMessage(message: "test", image: "emptyproduct", collectionView: collectionView, listType: .author)
            if let emptyView = collectionView.backgroundView as? EmptyTableViewBackgroundView {
                emptyView.actionBtn.didTapOnBounceEnd = {
                    //self.show(AddAddressController.loadVC())
                }
            }
            return 0
        }
        UtilityEmptyListView.hideEmptyCollectionViewMessage(collectionView: collectionView)
        return ehsanVideoList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EhsanMainCollectionCell.identifier(), for: indexPath) as! EhsanMainCollectionCell
        let data = self.ehsanVideoList[indexPath.row]
        switch videoType {
            case .ehsanProject:
                cell.setData(data: data)
            case .diwanTV:
                if indexPath.row == 0 {
                    cell.setDetailData(data: data)
                }
                else {
                    cell.setData(data: data)
                }
            case .myTV:
                cell.setData(data: data)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
        let controller = EhsanDetailController.loadVC()
        controller.ehsanVideo = self.ehsanVideoList[indexPath.row]
        self.show(controller)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch videoType {
        case .ehsanProject:
            let width = collectionView.frame.size.width - 42
            return CGSize(width: width/2, height: DesignUtility.getValueFromRatio(135))
        case .diwanTV:
            if indexPath.row == 0 {
                let width = collectionView.frame.size.width - 32
                return CGSize(width: width, height: DesignUtility.getValueFromRatio(229))
            }
            let width = collectionView.frame.size.width - 42
            return CGSize(width: width/2, height: DesignUtility.getValueFromRatio(135))
        case .myTV:
            let width = collectionView.frame.size.width - 32
            return CGSize(width: width, height: DesignUtility.getValueFromRatio(229))
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 20, left: 15, bottom: 0, right: 15)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 12
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: collectionView.frame.width, height: DesignUtility.getValueFromRatio(156)) //add your height here
//    }
}
extension EhsanMainListController {
    
    func getEhsanVideoListApi() {
          let param:[String:Any] = ["page_number": currentPage,
                                    "page_size": 100000]
        var apiURL = ""
        switch videoType {
        case .ehsanProject:
            apiURL = ApiEndPoints.getEhsanVideos
        case .diwanTV:
            apiURL = ApiEndPoints.getVideos
        case .myTV:
            apiURL = ApiEndPoints.getVideos
        default:
            print("")
        }
            EhsanManager().getEhsanVideoListApi(endPoint: apiURL, param: param, header: [:], completion: { (response,msg,errorMsg) in
                //self.collectionView.isHidden = false
                print(response)
                //self.refreshView.endRefreshing()
                if let data = response {
                    self.ehsanVideoList.append(contentsOf: data)
                    let delay = self.isFirstTimeLoad == true ? 0.2 : 0.0
                    Utility.delay(seconds: delay) {
                        self.collectionView.isHidden = false
                        self.animateCollectionView(collectionView: self.collectionView)
                    }
                   // self.collectionView.reloadData()
                }

            }){ (error) in
                self.collectionView.isHidden = false
               // self.refreshView.endRefreshing()
                    print("Error: ", error as Any)
                }
            
        }
    
    
}
