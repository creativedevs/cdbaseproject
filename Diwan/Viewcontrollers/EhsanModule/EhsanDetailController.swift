//
//  EhsanDetailController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 28/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import BMPlayer
class EhsanDetailController: BaseViewController , StoryBoardHandler {

    @IBOutlet weak var containerTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var containerViewHieght: NSLayoutConstraint!
    @IBOutlet weak var testPlayer: BMCustomPlayer!
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.ehsanProject.rawValue , nil)
    var ehsanVideo : EhsanModel?
    @IBOutlet weak var collectionView: BaseUICollectionView!
    
    @IBOutlet weak var videoDescLbl: BaseUILabel!
    @IBOutlet weak var videoPlayer: BMCustomPlayer!
    @IBOutlet weak var playBtn: BounceButton!
    @IBOutlet weak var videoNameLbl: UILabel!
    @IBOutlet weak var videoThumbnailImg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Constants.kWINDOW_WIDTH)
        print(Constants.kWINDOW_HIEGHT)
        //testPlayer.removeFromSuperview()
        view.insetsLayoutMarginsFromSafeArea = false
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateFullScreen(notification:)), name: .updateVideoScreen, object: nil)
        
        //Constants.APP_DELEGATE.shouldRotate = true
        //collectionView.isHidden = true
        self.navigationItem.title = ehsanVideo?.title
        createBackBtn()
        //createRefreshView(tblView: categoryListTbl)
       // setUpCollection()
       // collectionView.reloadData()
//        self.getEhsanVideoListApi()
        setInitialData()
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarheart")!, .BarButtonItemPositionLeft, self, #selector(showWishListScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarshareicon")!, .BarButtonItemPositionRight, self, #selector(showSearchScreen))
        
    }
    func setInitialData() {
        videoPlayer.layer.cornerRadius = 10
        videoPlayer.layer.masksToBounds = true
        videoPlayer.isHidden = true
        videoThumbnailImg.image = UIImage()
        videoThumbnailImg.backgroundColor = ColorManager.color(forKey: "grayborder")
        
        if let img = self.ehsanVideo?.thumbnailUrl {
            videoThumbnailImg.setImage(url: img, placeholder: nil, isActivity: true, activityColor: ColorManager.color(forKey: "white"))
        }
        if let title = self.ehsanVideo?.title {
            videoNameLbl.text = title
        }
        if let title = self.ehsanVideo?.descriptionField {
            videoDescLbl.text = title
        }
        playBtn.didTapOnBounceEnd = {[weak self] in
            print(self)
            self?.playVideo()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewDidDisappear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
        super.viewDidDisappear(animated)
        
    }
    @objc func updateFullScreen(notification:Notification) {
        print(Constants.APP_DELEGATE.shouldRotate)
        if Constants.APP_DELEGATE.shouldRotate {
            //self.view.addSubview(videoPlayer)
           //collectionView.reloadData()
            containerTopConstraint.constant = 0
            containerLeadingConstraint.constant = 0
            containerTrailingConstraint.constant = 0
            
            imageViewHeight.constant = Constants.kWINDOW_WIDTH
            containerViewHieght.constant = Constants.kWINDOW_WIDTH
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
        else{
            //collectionView.reloadData()
            containerTopConstraint.constant = 16
            containerLeadingConstraint.constant = 16
            containerTrailingConstraint.constant = 16
            imageViewHeight.constant = DesignUtility.getValueFromRatio(182)
            containerViewHieght.constant = DesignUtility.getValueFromRatio(229)
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
        
    }
    override func popViewController() {
        Constants.APP_DELEGATE.shouldRotate = false
        if let player = videoPlayer {
            player.pause(allowAutoPlay: false)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    func setUpCollection(){
        
        collectionView.registerCell(EhsanMainCollectionCell.self)
    }
    func playVideo() {
        
        videoPlayer.isHidden = false
        //videoPlayer = cell.videoPlayer
        //videoPlayer.play()
        //videoPlayer.delegate = self
        videoPlayer.backBlock = { [unowned self] (isFullScreen) in
            if isFullScreen == true {
                return
            }
            let _ = self.navigationController?.popViewController(animated: true)
        }
        let asset = BMPlayerResource(url: URL(string: ehsanVideo!.videoUrl)!,
                                     name: "")
        videoPlayer.setVideo(resource: asset)
    }
    
    func playVideo2(cell : EhsanMainCollectionCell) {
        playVideo1()
        //return
        
        cell.videoPlayer.isHidden = false
        videoPlayer = cell.videoPlayer
        videoPlayer.play()
        videoPlayer.delegate = self
        cell.videoPlayer.backBlock = { [unowned self] (isFullScreen) in
            if isFullScreen == true {
                return
            }
            let _ = self.navigationController?.popViewController(animated: true)
        }
        let asset = BMPlayerResource(url: URL(string: ehsanVideo!.videoUrl)!,
                                     name: "")
        cell.videoPlayer.setVideo(resource: asset)
    }
    func playVideo1() {
       
        testPlayer.delegate = self
        testPlayer.backBlock = { [unowned self] (isFullScreen) in
            if isFullScreen == true {
                return
            }
            let _ = self.navigationController?.popViewController(animated: true)
        }
        let asset = BMPlayerResource(url: URL(string: ehsanVideo!.videoUrl)!,
                                     name: "")
        testPlayer.setVideo(resource: asset)
    }
    deinit {
        print("EhsanDetailController deinit")
    }

}
//MARK:- Collection Datasource and Delegate
extension EhsanDetailController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EhsanMainCollectionCell.identifier(), for: indexPath) as! EhsanMainCollectionCell
        cell.setDetailData(data: ehsanVideo!)
        cell.playBtn.didTapOnBounceEnd = {[weak self] in
            print(self)
            self?.playVideo2(cell: cell)
        }
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        var controller = EhsanDetailController.load()
//
//
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width - 32
        return CGSize(width: width, height: DesignUtility.getValueFromRatio(229))
        //return CGSize(width: DesignUtility.getValueFromRatio(188), height: DesignUtility.getValueFromRatio(198))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 16, bottom: 0, right: 16)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: collectionView.frame.width, height: DesignUtility.getValueFromRatio(156)) //add your height here
//    }
}
extension EhsanDetailController: BMPlayerDelegate {
    func bmPlayer(player: BMPlayer, playerStateDidChange state: BMPlayerState) {
        print("playStateDidChange \(state)")
    }
    
    func bmPlayer(player: BMPlayer, loadedTimeDidChange loadedDuration: TimeInterval, totalDuration: TimeInterval) {
       // print("loadedTimeDidChange loadedDuration: \(loadedDuration) totalDuration: \(totalDuration)")
    }
    
    func bmPlayer(player: BMPlayer, playTimeDidChange currentTime: TimeInterval, totalTime: TimeInterval) {
        //print("playTimeDidChange currentTime: \(currentTime) totalTime: \(totalTime)")
    }
    
    func bmPlayer(player: BMPlayer, playerIsPlaying playing: Bool) {
        print("playerIsPlaying: \(playing)")
    }
    
    func bmPlayer(player: BMPlayer, playerOrientChanged isFullscreen: Bool) {
        print("playerOrientChanged: \(isFullscreen)")
    }
    
    
}
