
//
//  ContactUsController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 15/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class ContactUsController: BaseViewController , StoryBoardHandler {

    @IBOutlet weak var fullNameStackView: UIStackView!
    @IBOutlet weak var emailStackView: UIStackView!
    @IBOutlet weak var titleStackView: UIStackView!
    
    @IBOutlet weak var fullNameTextField: BaseFloatingTextField!
    @IBOutlet weak var emailTextField: BaseFloatingTextField!
    @IBOutlet weak var titleTextField: BaseFloatingTextField!
    
    @IBOutlet weak var fullNameLabel: BaseUILabel!
    @IBOutlet weak var emailLabel: BaseUILabel!
    @IBOutlet weak var titleLabel: BaseUILabel!
    
    @IBOutlet weak var moreInfoLbl: BaseUILabel!
    @IBOutlet weak var moreInfoTxtView: BaseUITextView!
    @IBOutlet weak var characterCountLbl: BaseUILabel!
    @IBOutlet weak var viewContainerForm: BaseUIView!
    @IBOutlet weak var phoneBtn: BounceButton!
    var contactType : ContactType = .contactUs
    var phoneNumber = "+96588888888"
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.userModule.rawValue , nil)
    var isValidate = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
       phoneBtn.setTitle(phoneNumber, for: .normal)
        
        if (contactType == .contactUs) {
            self.navigationItem.title = "Contact us".localized
            setupAppDefaultNavigationBar()
            createNavSideMenuBtn()
        }
        else {
            self.navigationItem.title = "Support".localized
            createBackBtn()
        }
        
        phoneBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        setUpViews()
        setupButton()
        moreInfoTxtView.placeholderTxt = moreInfoTxtView.placeholderTxt?.localized
        // Do any additional setup after loading the view.
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
    }
    func setupButton() {
        phoneBtn.semanticContentAttribute = .forceLeftToRight
//        if (CurrentUser.languageId == .arabic) {
//            phoneBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -20)
//        }
//        else {
//            phoneBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
//        }
        
        phoneBtn.didTapOnBounceEnd = {
            Utility.callPhoneNumber(at: self.phoneNumber)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            // self.isHideNavigation(true)
        resetErrorViews()
    }
    func setContactData() {
        
        
    }
    func resetErrorViews() {
        
        moreInfoLbl.text = ""
        fullNameLabel.text = ""
        emailLabel.text = ""
        titleLabel.text = ""
        
    }
    func setUpViews(){
        
        fullNameTextField.delegate = self
        emailTextField.delegate = self
        titleTextField.delegate = self
        
        fullNameTextField.setInitialUI()
        emailTextField.setInitialUI()
        titleTextField.setInitialUI()
        
    }
    @IBAction func sendBtnPressed(_ sender: Any) {
        
        isValidate = true
        let fv = FormValidator()
        let fullName = fv.validateName(title: fullNameTextField.placeholder!, text: fullNameTextField.text!)
        let email = fv.validateEmail(title: emailTextField.placeholder!, text: emailTextField.text!)
        let title = fv.validateStreet(title: titleTextField.placeholder!, text: titleTextField.text!)
        let message = fv.validateStreet(title: moreInfoTxtView.placeholderTxt!, text: moreInfoTxtView.text!)
        
        if (fullName.0 == false) {
            fullNameLabel.text = fullName.1
            fullNameStackView.shake()
            isValidate = false
        }
        if (email.0 == false) {
            emailLabel.text = email.1
            emailStackView.shake()
            isValidate = false
        }
        if (title.0 == false) {
            titleLabel.text = title.1
            titleStackView.shake()
            isValidate = false
        }
        if (message.0 == false) {
            moreInfoLbl.text = message.1
            moreInfoTxtView.shake()
            isValidate = false
        }
        
        if isValidate {
            print("Validation sucess")
            self.view.endEditing(true)
            self.submitForm()
            // self.show(VerifyCodeController.loadVC())
            //            router.goToHomeAsRoot(from: self)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ContactUsController : UITextFieldDelegate {
    // MARK: - TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let currentCharacterCount = textField.text?.count ?? 0
        
        if (range.length + range.location > currentCharacterCount)
        {
            return false
        }
        
        let newLength = currentCharacterCount + string.count - range.length
        var maxLength = 50
        
        
        if (textField == fullNameTextField)
        {
            self.fullNameLabel.text = ""
            maxLength = 20
        }
        
        if (textField == emailTextField)
        {
            self.emailLabel.text = ""
            maxLength = 20
        }
        if (textField == titleTextField)
        {
            self.titleLabel.text = ""
            maxLength = 20
        }
        return newLength <= maxLength
        
    }
}
extension ContactUsController : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        moreInfoLbl.text = ""
        
        if numberOfChars <= 500 {
            self.characterCountLbl.text = "\(numberOfChars)/500"
        }
        
        return numberOfChars < 501
    }
}
//MARK:- APIS
extension ContactUsController {
      func submitForm() {
          self.view.endEditing(true)
        let param:[String:Any] = ["full_name":fullNameTextField.text!,
                                  "email_address":emailTextField.text!,
                                  "title": titleTextField.text!,
                                  "message": moreInfoTxtView.text!,
                                  "query_type": contactType.rawValue]
        print(param)
        CommonModuleManager().submitQueryApi(endPoint: ApiEndPoints.addQuery, param: param, header: [:], completion: { (response,msg,errorMsg) in

            var message = ""
            if let mesg = msg {
                message = mesg
            }
            self.showAlert(title: nil, subTitle: message, doneBtnTitle: "OK".localized) {
                
                if (self.contactType == .contactUs) {
                    let controller = BaseNavigationController(rootViewController: StoreCategoryListController.loadVC())
                    self.sideMenuController?.rootViewController = controller
                }
                else {
                    self.popViewController()
                }
                
                
            }

      }){ (error) in
              print("Error: ", error as Any)
          }
    }
}
