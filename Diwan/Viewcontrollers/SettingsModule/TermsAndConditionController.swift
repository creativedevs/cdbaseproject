//
//  TermsAndConditionController.swift
//  SalonDesBeaute
//
//  Created by Shujaatkhan on 26/11/2018.
//  Copyright © 2018 High- Sierra 13.6. All rights reserved.
//

import UIKit
import Alamofire

class TermsAndConditionController: BaseViewController ,StoryBoardHandler {
    
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.userModule.rawValue , nil)
    
    @IBOutlet weak var txtViewTerms: BaseUITextView!
    
    var type:CMSType = .none
    var isFromMenu = false
    var contentText = ""
    var titleText = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createBackBtn()
        callAboutService()
        
        txtViewTerms.text = ""
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
      
    }
    
    
    // MARK: - APIS
    func callAboutService() {
        
            CommonModuleManager().getCMSApi(endPoint: ApiEndPoints.getCMS, param: nil, header: [:], completion: { (response,msg,errorMsg) in
                
                print(response)
                if let data = response {
                    self.contentText = data[self.type.rawValue].descriptionField
                    self.titleText = data[self.type.rawValue].title
                    self.setCMSData()
//                    switch self.type {
//                    case .about:
//
//                    case .returnExchange:
//                    case .terms:
//                    case .privacy:
//                    default:
//                        print("")
//                    }
                }

            }){ (error) in
                    print("Error: ", error as Any)
                }
            
        }
    
    func setCMSData() {
        
        self.navigationItem.title = titleText
        //self.txtViewTerms.text  = contentText
        let htmlData = NSString(string: contentText).data(using: String.Encoding.unicode.rawValue)
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
        self.txtViewTerms.attributedText = attributedString
    }

}


extension UIView {
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
}
