//
//  FAQsController.swift
//  EducationUSA
//
//  Created by XEONCITY on 31/12/2017.
//  Copyright © 2017 Ingic. All rights reserved.
//

import UIKit
import ExpyTableView
//import LGRefreshView

class FAQsController: BaseViewController , StoryBoardHandler{
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.userModule.rawValue , nil)
    

  /*  var refreshView:LGRefreshView!
    var isRefreshing:Bool = false
    
    var faqData:[FAQFaq] = [FAQFaq]()
    var faqAllData:[FAQFaq] = [FAQFaq]()
    var currentPage = 1
    var shouldShowLoadingCell = false  */
    var faqList = [FAQ]()
    @IBOutlet weak var expendTableView: ExpyTableView!
    
    //SERVICE MANAGER
    var userDataManager = UserModuleManager()
    
    
    //VARIABLES
    var isExpand = false
    //var faqData:[FAQData] = [FAQData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createBackBtn()
        self.navigationItem.title = "FAQs".localized
        expendTableView.dataSource = self
        expendTableView.delegate = self
        expendTableView.rowHeight = UITableView.automaticDimension
        expendTableView.estimatedRowHeight = 64
        
        //Alter the animations as you want
        expendTableView.expandingAnimation = .fade
        expendTableView.collapsingAnimation = .fade
        createRefreshView(tblView: expendTableView)
        setupAppDefaultNavigationBar()
        createNavSideMenuBtn()
        self.callFAQService()
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setStatusBarStyle(.lightContent)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    
    }
    override func refreshData() {
        print("refresh")
        self.currentPage = 1
        self.faqList.removeAll()
        self.callFAQService()
       // self.refreshView.endRefreshing()
    }
    
    func callFAQService() {
        
        let param:[String:Any] = ["page_number": currentPage,
                                  "page_size": 100000]
        self.expendTableView.reloadData()
//        self.userDataManager.apiFAQ(param: params, headers: header, endPoint: ServiceEndPoints.FAQ) { (FAQData) in
//            self.faqData = FAQData.data!
//            self.expendTableView.reloadData()
//        }
        CommonModuleManager().getFAQApi(endPoint: ApiEndPoints.getFAQ, param: param, header: [:], completion: { (response,msg,errorMsg) in
            
            print(response)
            if let data = response {
                self.faqList.append(contentsOf: data)
                self.expendTableView.reloadData()
            }
           /// self.faqData = FAQData.data!
            //            self.expendTableView.reloadData()
            
//            if (response!.isEmailVerified) {
//
//                CurrentUser.data = response
//                try! Constants.realm.write(){
//                    if CurrentUser.data != nil {
//                        Constants.realm.add(CurrentUser.data!, update: .all)
//                    }
//                }
//                router.goToHomeAsRoot()
//            }else{
//                var message = ""
//                if let mesg = errorMsg {
//                    message = mesg
//                }
//                self.showAlert(title: nil, subTitle: message, doneBtnTitle: "OK".localized, completionAction: {
//                    let vc = VerifyCodeController.loadVC()
//                    vc.email = self.txtFieldEmail.text!
//                    vc.fromSignUp = true
//                    self.show(vc)
//                })
//
//            }

        }){ (error) in
                print("Error: ", error as Any)
            }
        
    }
    

   /* func createRefreshView() {
        
        refreshView = LGRefreshView.init(scrollView: expendTableView, refreshHandler: { ( ref ) in
            
            self.isRefreshing = true
            self.callFaqService(searchText: self.txtSearch?.text ?? "")
            
        })
        refreshView.tintColor = Utility.UIColorFromRGB(rgbValue: 0x0A3866)
        refreshView.backgroundColor = UIColor.clear
    }   */


}

// MARK: - Expending TableView Delegate
extension FAQsController: ExpyTableViewDataSource , ExpyTableViewDelegate {
    
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
        
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
     //   if section == faqData.count && shouldShowLoadingCell {
      //      let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell") as! LoadMoreCell
      //      return cell
     //   }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FAQSectionCell") as! FAQSectionCell
            
            cell.lblQuestion.text = self.faqList[section].question
//            if faqData[section].question == nil {
//                cell.canAnimate = false
//            }
            return cell
    //    }

    }
    
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
//        print(faqData[section].guidelineId)
//        let data = faqData[section]
//        if(data.guidelineId != 0) {
//            return false
//        }
        return true
    }
    
    func canExpand(section: Int, inTableView tableView: ExpyTableView) -> Bool {
//        print(faqData[section].guidelineId)
//        let data = faqData[section]
//        if(data.guidelineId != 0) {
//            return false
//        }
        return true
    }
    
   /* func expandableCell(forSection section: Int, inTableView tableView: ExpyTableView) -> UITableViewCell {
        
        if section == faqData.count && shouldShowLoadingCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell") as! LoadMoreCell
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FAQSectionCell") as! FAQSectionCell
            
            cell.lblQuestion.text = faqData[section].name
            cell.lblQuesNo.text = String(section+1)+"."
            return cell
        }

    }
    */
    func numberOfSections(in tableView: UITableView) -> Int {

        return self.faqList.count//shouldShowLoadingCell ? faqList.count + 1 : faqList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQExpendCell") as! FAQExpendCell
        cell.lblAns.text = self.faqList[indexPath.section].answer
        return cell


        
    }
    

//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//        if indexPath.section == faqData.count && shouldShowLoadingCell {
//            currentPage += 1
//            callFaqService(searchText: "")
//        }
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //If you don't deselect the row here, seperator of the above cell of the selected cell disappears.
        //Check here for detail: https://stackoverflow.com/questions/18924589/uitableviewcell-separator-disappearing-in-ios7
        
    /*    tableView.deselectRow(at: indexPath, animated: false)
        
        if faqData[indexPath.section].descriptionValue == nil {
            let eduGuideDetailVC = AppConstants.APP_STORYBOARD.HOME.instantiateViewController(withIdentifier: "EducationGuideDetailController") as! EducationGuideDetailController
            eduGuideDetailVC.eduGuidelineId = faqData[indexPath.section].guidelineId
            self.navigationController?.pushViewController(eduGuideDetailVC, animated:true)
        }
        //This solution obviously has side effects, you can implement your own solution from the given link.
        //This is not a bug of ExpyTableView hence, I think, you should solve it with the proper way for your implementation.
        //If you have a generic solution for this, please submit a pull request or open an issue.
        
        print("DID SELECT row: \(indexPath.row), section: \(indexPath.section)")  */
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
