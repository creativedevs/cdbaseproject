//
//  LibraryMainListController.swift
//  Diwan
//
//  Created by Muzamil Hassan on 25/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class AuthorMainListController: BaseViewController , StoryBoardHandler {

    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.author.rawValue , nil)
    @IBOutlet weak var filterTopView: HorizontalDynamicCollectionView!
    var authorList = [AuthorModel]()
    var sortCharacter = "All"
    @IBOutlet weak var collectionView: BaseUICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.isHidden = true
        self.navigationItem.title = "Authors".localized
        setupAppDefaultNavigationBar()
        createNavSideMenuBtn()
        //createRefreshView(tblView: categoryListTbl)
        setUpCollection()
        setupFilterView()
        self.getAuthorListApi()
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarheart")!, .BarButtonItemPositionLeft, self, #selector(showWishListScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarcart")!, .BarButtonItemPositionRight, self, #selector(showCartScreen))
        
        self.addBarButtonItemWithImage(UIImage(named: "topbarsearch")!, .BarButtonItemPositionRight, self, #selector(showSearchScreen))
    }
    
    func setUpCollection(){
        
        collectionView.registerCell(AuthorMainCollectionCell.self)
    }
    func setupFilterView() {
        
        filterTopView.didSelectItem = {[weak self] item in
            print(item)
            if (self?.sortCharacter != item) {
                self?.sortCharacter = item
                self?.refreshData()
            }
        }
        
    }
    override func refreshData() {
        print("refresh")
        self.currentPage = 1
        self.authorList.removeAll()
        self.getAuthorListApi()
       // self.refreshView.endRefreshing()
    }

}
//MARK:- Collection Datasource and Delegate
extension AuthorMainListController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return 10
        if self.authorList.count == 0 {
            UtilityEmptyListView.emptycollectionViewMessage(message: "test", image: "emptylist", collectionView: collectionView, listType: .author)
            if let emptyView = collectionView.backgroundView as? EmptyTableViewBackgroundView {
                emptyView.actionBtn.didTapOnBounceEnd = {
                    //self.show(AddAddressController.loadVC())
                }
            }
            return 0
        }
        UtilityEmptyListView.hideEmptyCollectionViewMessage(collectionView: collectionView)
        return authorList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AuthorMainCollectionCell.identifier(), for: indexPath) as! AuthorMainCollectionCell
        let data = self.authorList[indexPath.row]
//        let position : ProductCellPosition = indexPath.row % 2 == 0 ? .left : .right
        cell.setAuthorData(data: data)
//        cell.addToCartBtn.didTapOnBounceEnd = {
//            self.addToCartOrNotifyMe(data: data,cell: cell,indexPath: indexPath)
//        }
//        cell.addOrRemoveFavoriteBtn.didTapOnBounceEnd = {
//            self.addOrRemoveFavorite(data: data,cell: cell,indexPath: indexPath)
//        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let category = CategoryModel()
        let controller = StoreProductListController.loadVC()
        let data = self.authorList[indexPath.row]
        category.categoryId = data.Id
        category.title = data.title
        category.imageIcon = data.imageUrl
        controller.selectedMainCategory = category
        controller.selectedAuthor = data
        controller.filledData = ( category.title, .author, .author)
        self.show(controller)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width - 75
        return CGSize(width: width/2, height: DesignUtility.getValueFromRatio(184))
        //return CGSize(width: DesignUtility.getValueFromRatio(188), height: DesignUtility.getValueFromRatio(198))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 30, bottom: 0, right: 30)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: collectionView.frame.width, height: DesignUtility.getValueFromRatio(156)) //add your height here
//    }
}
extension AuthorMainListController {
    
    func getAuthorListApi() {
        let param:[String:Any] = ["page_number": currentPage,
                                  "page_size": 100000,
                                  //"search_keyword" : "",
                                "sort_start_with":sortCharacter]
        
            AuthorManager().getAuthorListApi(endPoint: ApiEndPoints.getAuthors, param: param, header: [:], completion: { (response,msg,errorMsg) in
              //  self.collectionView.isHidden = false
                print(response)
                //self.refreshView.endRefreshing()
                if let data = response {
                    self.authorList.append(contentsOf: data)
                    let delay = self.isFirstTimeLoad == true ? 0.2 : 0.0
                    Utility.delay(seconds: delay) {
                        self.collectionView.isHidden = false
                        self.animateCollectionView(collectionView: self.collectionView)
                    }
                   // self.collectionView.reloadData()
                }

            }){ (error) in
                self.collectionView.isHidden = false
               // self.refreshView.endRefreshing()
                    print("Error: ", error as Any)
                }
            
        }
    
    
}
