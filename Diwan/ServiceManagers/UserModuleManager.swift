
//  UserModuleManager


import Foundation

import Alamofire

class UserModuleManager: AFManagerProtocol {
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
    }
    
    
    
    var isSuccess = false
    var isShowAlert = false
    var htmlString = ""

    func apiGeneric<T : Decodable> (_ param: AFParam,isLoader:Bool , modelType : T.Type , completion: @escaping ([Any]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void)
    {

        print(modelType)
        
        self.isSuccess = false
        
        //request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: isLoader, success: { (response) in
            
            guard response != nil else { return }
            
            do
            {
                let dict = try JSONSerialization.jsonObject(with: response!, options:[]) as? Dictionary<String, Any>
                let arr = dict?["response_data"] as? [Any]
                let message = dict?["message_to_show"] as? String
                let erroMessage = dict?["error_internal"] as? String
                completion(arr,message,erroMessage)
            }
            catch let error
            {
                print("Error: ", error)
            }
            
            
        }) { (error) in
            print("Error: ", error as Any)
            failure(error)
        }
    }
    
    /*
    func apiRegister(param:[String:Any],completion: @escaping (UserModelBase) -> Void){

        let headers: [String : String] = [:]

        let param = AFParam(endpoint: SIGNUP, params: param, headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])

        self.apiGeneric(param, isLoader: true, modelType:UserModelBase.self ) { (res) in
            if res.status == true {
                completion(res)
            }else {
                let msg = MOLHLanguage.currentAppleLanguage() == "en" ? res.message:res.messageAr
                Alert.showAlertToast(msg: msg!)
            }

        }

    }
*/
    func postApi(endPoint:String,param:[String:Any],header:[String : String],completion: @escaping ([Any]?) -> Void){

        let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .post, parameterEncoding: JSONEncoding.default, images: [])

//        self.apiGeneric(param, isLoader: true, modelType:User.self ) { (res) in
//
//            //completion(res)
//            print(res)
//            //completion(res)
////            if res.status == true {
////                completion(res)
////            }else{
////                let msg = MOLHLanguage.currentAppleLanguage() == "en" ? res.message:res.messageAr
////                Alert.showAlertToast(msg: msg!)
////            }
//
//        }

    }
        func userLoginApi(endPoint:String,param:[String:Any],header:[String : String],completion: @escaping (User?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .post, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: User.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData[0])
                        let result = try decoder.decode(User.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func verifyOTPApi(endPoint:String,param:[String:Any],header:[String : String],completion: @escaping (User?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .post, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: User.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData[0])
                        let result = try decoder.decode(User.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func getApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping (User?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: User.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData[0])
                        let result = try decoder.decode(User.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func apiEditProfilePicture(endPoint:String,param:[String:Any]?,header:[String : String],image:[UIImage],completion: @escaping (User?,String?,String?) -> Void){

        //let headers: [String : String] = ["Authorization":(CurrentUser.data?.token)!]
        let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .post, parameterEncoding: JSONEncoding.default, images: image)

        AFNetwork.shared.apiRequestUpload(param, isSpinnerNeeded: true, success: { (response) in
            guard let data = response else { return }

                        guard response != nil else { return }
            
            do
            {
                let decoder = JSONDecoder()
                let dict = try JSONSerialization.jsonObject(with: response!, options:[]) as? Dictionary<String, Any>
                let arr = dict?["response_data"] as? [Any]
                let message = dict?["message_to_show"] as? String
                let erroMessage = dict?["error_internal"] as? String
                let jsonData = try? JSONSerialization.data(withJSONObject:arr![0])
                let result = try decoder.decode(User.self, from: jsonData!)
                completion(result,message,erroMessage)
            }
            catch let error
            {
                print("Error: ", error)
            }
        }) { (error) in
            print("Error: ", error)
        }


    }
    func getNavigationListApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([NavigationData]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: NavigationData.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let json = try JSONSerialization.jsonObject(with: jsonData!, options: []) as? [Any]
                        let dataArray = (json![0] as! [String:Any])["navigation_data"]
                        let jsonData1 = try? JSONSerialization.data(withJSONObject:dataArray!)
                        let result = try decoder.decode([NavigationData].self, from: jsonData1!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    
        func addAddressApi(endPoint:String,param:[String:Any],header:[String : String],completion: @escaping (AddressModel?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .post, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: User.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData[0])
                        let result = try decoder.decode(AddressModel.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    
    func getAddressListApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([AddressModel]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: AddressModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([AddressModel].self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func deleteAddressApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping (AddressModel?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: AddressModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        //let jsonData = try? JSONSerialization.data(withJSONObject:resData[0])
                        //let result = try decoder.decode(AddressModel.self, from: jsonData!)
                        completion(nil,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func defaultAddressApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping (AddressModel?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: AddressModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData[0])
                        let result = try decoder.decode(AddressModel.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
        
/*
    func getApi(endPoint:String,param:[String:Any],header:[String : String],completion: @escaping (UserModelBase) -> Void){

        let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: URLEncoding.default, images: [])

        self.apiGeneric(param, isLoader: true, modelType:UserModelBase.self ) { (res) in

            if res.status == true {
                completion(res)
            }else{
                print(res.message!)
                let msg = MOLHLanguage.currentAppleLanguage() == "en" ? res.message:res.messageAr
                Alert.showAlertToast(msg: msg!)
            }

        }

    }
    
    func getStaticContent(completion: @escaping (StaticContentBase) -> Void){
        
        let param = AFParam(endpoint: GENERAL_INFO, params: [:], headers:[:], method: .get, parameterEncoding: URLEncoding.default, images: [])
        
        self.apiGeneric(param, isLoader: true, modelType:StaticContentBase.self ) { (res) in
            
            if res.status == true {
                completion(res)
            }else{
                print(res.message!)
                Alert.showAlertToast(msg: res.message!)
            }
            
        }
        
    }
    
    func getCount(completion: @escaping (CountBase) -> Void){
        
        let header:[String:String] = CurrentUser.data != nil ? CurrentUser.headers:[:]
        
        let param = AFParam(endpoint: GET_COUNT, params: [:], headers:header, method: .get, parameterEncoding: URLEncoding.default, images: [])
        
        self.apiGeneric(param, isLoader: false, modelType:CountBase.self ) { (res) in
            
            if res.status == true {
                completion(res)
            }else{
                print(res.message!)
                Alert.showAlertToast(msg: res.message!)
            }
            
        }
        
    }

    func apiEditProfile(param:[String:Any],image:[UIImage],completion: @escaping (UserModelBase) -> Void){

        //let headers: [String : String] = ["Authorization":(CurrentUser.data?.token)!]

        let params = AFParam(endpoint: EDIT_PROFILE, params: param, headers: CurrentUser.headers, method: .post, parameterEncoding: JSONEncoding.default, images:image)

        AFNetwork.shared.apiRequestUpload(params, isSpinnerNeeded: true, success: { (response) in
            guard let data = response else { return }

            do
            {
                let decoder = JSONDecoder()
                let result = try decoder.decode(UserModelBase.self, from: data)

                if result.status == true {
                    completion(result)
                }else{
                    print(result.message!)
                    let msg = MOLHLanguage.currentAppleLanguage() == "en" ? result.message:result.messageAr
                    Alert.showAlertToast(msg: msg!)
                }

            }
            catch let error
            {
                print("Error: ", error)
            }
        }) { (error) in
            print("Error: ", error)
        }


    }
*/


    
}




