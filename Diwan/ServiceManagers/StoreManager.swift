
//  UserModuleManager


import Foundation

import Alamofire

class StoreManager: AFManagerProtocol {
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
    }
    
    
    
    var isSuccess = false
    var isShowAlert = false
    var htmlString = ""

    func apiGeneric<T : Decodable> (_ param: AFParam,isLoader:Bool , modelType : T.Type , completion: @escaping ([Any]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void)
    {

        print(modelType)
        
        self.isSuccess = false
        
        //request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: isLoader, success: { (response) in
            
            guard response != nil else { return }
            
            do
            {
                let dict = try JSONSerialization.jsonObject(with: response!, options:[]) as? Dictionary<String, Any>
                let arr = dict?["response_data"] as? [Any]
                let message = dict?["message_to_show"] as? String
                let erroMessage = dict?["error_internal"] as? String
                completion(arr,message,erroMessage)
            }
            catch let error
            {
                print("Error: ", error)
            }
            
            
        }) { (error) in
            print("Error: ", error as Any)
            failure(error)
        }
    }
    func getCategoryApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([CategoryModel]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: CategoryModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([CategoryModel].self, from: jsonData!)
                        //let result = try decoder.decode(FAQ.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    
    func getStoreProductListApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([ProductModel]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .post, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: ProductModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([ProductModel].self, from: jsonData!)
                        //let result = try decoder.decode(FAQ.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func getWishListApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([ProductModel]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: ProductModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([ProductModel].self, from: jsonData!)
                        //let result = try decoder.decode(FAQ.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func addOrRemoveFavoriteApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([ProductModel]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .post, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: ProductModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([ProductModel].self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func addToCartOrNotifyMeApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([ProductModel]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .post, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: ProductModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([ProductModel].self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    
    func getCategoryWiseroductListApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([CategoryWiseProductModel]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .post, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: ProductModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([CategoryWiseProductModel].self, from: jsonData!)
                        //let result = try decoder.decode(CategoryWiseProductModel.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func getFilterListApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([FilterModel]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

        let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: URLEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: FilterModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([FilterModel].self, from: jsonData!)
                        //let result = try decoder.decode(CategoryWiseProductModel.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
}

//MARK:- PRODUCT DETAIL API's
extension StoreManager {
    
    func getProductDetailApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping (ProductModel?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: ProductModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData[0])
                        let result = try decoder.decode(ProductModel.self, from: jsonData!)
                        //let result = try decoder.decode(FAQ.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    
    
    func getSimilarProductsApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([ProductModel]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: false, modelType: ProductModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([ProductModel].self, from: jsonData!)
                        //let result = try decoder.decode(FAQ.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
}

