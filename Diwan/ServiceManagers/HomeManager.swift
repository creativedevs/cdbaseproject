
//  UserModuleManager


import Foundation

import Alamofire

class HomeManager: AFManagerProtocol {
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
    }
    
    
    
    var isSuccess = false
    var isShowAlert = false
    var htmlString = ""

    func apiGeneric<T : Decodable> (_ param: AFParam,isLoader:Bool , modelType : T.Type , completion: @escaping ([Any]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void)
    {

        print(modelType)
        
        self.isSuccess = false
        
        //request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: isLoader, success: { (response) in
            
            guard response != nil else { return }
            
            do
            {
                let dict = try JSONSerialization.jsonObject(with: response!, options:[]) as? Dictionary<String, Any>
                let arr = dict?["response_data"] as? [Any]
                let message = dict?["message_to_show"] as? String
                let erroMessage = dict?["error_internal"] as? String
                completion(arr,message,erroMessage)
            }
            catch let error
            {
                print("Error: ", error)
            }
            
            
        }) { (error) in
            print("Error: ", error as Any)
            failure(error)
        }
    }
    func getHomeBannerApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping (([ProductMediaDetailModel]?,[ProductMediaDetailModel]?,[User]?),String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: CategoryModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let json = try JSONSerialization.jsonObject(with: jsonData!, options: []) as? [Any]
                        if json?.count ?? 0 > 0 {
                            let banners = (json![0] as! [String:Any])["banner_positions"] as? [Any]
                            var topBanners = [ProductMediaDetailModel]()
                            var middleBanners = [ProductMediaDetailModel]()
                            var usersList = [User]()
                            for d in banners! {
                                let data = d as! [String : Any]
                                let type = data["banner_position_id"] as! Int
                                print(type)
                                let list = data["banner_list"] as? [Any]
                                let listData = try? JSONSerialization.data(withJSONObject:list!)
                                if type == BannerPositionType.top.rawValue {
                                    topBanners.append(contentsOf: try decoder.decode([ProductMediaDetailModel].self, from: listData!))
                                }
                                else if type == BannerPositionType.middle1.rawValue {
                                    middleBanners.append(contentsOf: try decoder.decode([ProductMediaDetailModel].self, from: listData!))
                                }
                            }
                            let users = (json![0] as! [String:Any])["list_of_users"] as? [Any]
                            
                            let usersData = try? JSONSerialization.data(withJSONObject:users!)
                            usersList.append(contentsOf: try decoder.decode([User].self, from: usersData!))
                            print(usersList)
                            print(topBanners)
                            print(middleBanners)
                            completion((topBanners,middleBanners,usersList),msg,errorMsg)
                        }
                        else {
                            completion((nil,nil,nil),msg,errorMsg)
                        }
                    }
                    else {
                        completion((nil,nil,nil),msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    
    func getAttributeTagWiseProductListApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([AttributeWiseProductModel]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: AttributeWiseProductModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        var key = ""
                        if (endPoint == ApiEndPoints.getHomeAttributeData) {
                            key = "product_attributes_for_home_response"
                        }
                        else if (endPoint == ApiEndPoints.getHomeTagsData) {
                            key = "list_of_screen_tages"
                        }
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let json = try JSONSerialization.jsonObject(with: jsonData!, options: []) as? [Any]
                        if json?.count ?? 0 > 0 {
                            let dataList = (json![0] as! [String:Any])[key] as? [Any]
                            let listData = try? JSONSerialization.data(withJSONObject:dataList!)
                            let result = try decoder.decode([AttributeWiseProductModel].self, from: listData!)
                            print(result)
                            completion(result,msg,errorMsg)
                        }
                        else {
                            completion(nil,msg,errorMsg)
                        }
                        
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func getFilterListApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([FilterModel]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

        let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: URLEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: FilterModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([FilterModel].self, from: jsonData!)
                        //let result = try decoder.decode(CategoryWiseProductModel.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
}

//MARK:- PRODUCT DETAIL API's
extension HomeManager {
    
    func getProductDetailApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping (ProductModel?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: ProductModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData[0])
                        let result = try decoder.decode(ProductModel.self, from: jsonData!)
                        //let result = try decoder.decode(FAQ.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    
    
    func getSimilarProductsApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([ProductModel]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: false, modelType: ProductModel.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([ProductModel].self, from: jsonData!)
                        //let result = try decoder.decode(FAQ.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
}

