
//  UserModuleManager


import Foundation

import Alamofire

class CelebrityManager: AFManagerProtocol {
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
    }
    
    
    
    var isSuccess = false
    var isShowAlert = false
    var htmlString = ""

    func apiGeneric<T : Decodable> (_ param: AFParam,isLoader:Bool , modelType : T.Type , completion: @escaping ([Any]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void)
    {

        print(modelType)
        
        self.isSuccess = false
        
        //request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: isLoader, success: { (response) in
            
            guard response != nil else { return }
            
            do
            {
                let dict = try JSONSerialization.jsonObject(with: response!, options:[]) as? Dictionary<String, Any>
                let arr = dict?["response_data"] as? [Any]
                let message = dict?["message_to_show"] as? String
                let erroMessage = dict?["error_internal"] as? String
                completion(arr,message,erroMessage)
            }
            catch let error
            {
                print("Error: ", error)
            }
            
            
        }) { (error) in
            print("Error: ", error as Any)
            failure(error)
        }
    }
    
    func getCelebrityListApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([User]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .post, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: User.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([User].self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func getCelebrityDetailApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ((User?,[EhsanModel]?,[CategoryWiseProductModel]?),String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: User.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let json = try JSONSerialization.jsonObject(with: jsonData!, options: []) as? [Any]
                        if json?.count ?? 0 > 0 {
                            
                             let userObj = (json![0] as! [String:Any])
                            let userData = try? JSONSerialization.data(withJSONObject:userObj)
                            let user = try decoder.decode(User.self, from: userData!)
                            print(user)
                            var videosArr = [EhsanModel]()
                            let videos = (json![0] as! [String:Any])["videos"] as? [Any]
                            let videosData = try? JSONSerialization.data(withJSONObject:videos!)
                            videosArr.append(contentsOf: try decoder.decode([EhsanModel].self, from: videosData!))
                            print(videosArr)
                            
                            var productArr = [CategoryWiseProductModel]()
                            let products = (json![0] as! [String:Any])["celebrity_products"] as? [Any]
                            let productsData = try? JSONSerialization.data(withJSONObject:products!)
                            productArr.append(contentsOf: try decoder.decode([CategoryWiseProductModel].self, from: productsData!))
                            print(productArr)
                            
//                            let banners = (json![0] as! [String:Any])["banner_positions"] as? [Any]
//                            print(banners)
//                            var topBanners = [ProductMediaDetailModel]()
//                            for d in banners! {
//                                let data = d as! [String : Any]
//                                let type = data["banner_position_id"] as! Int
//                                print(type)
//                                let list = data["banner_list"] as? [Any]
//                                let listData = try? JSONSerialization.data(withJSONObject:list!)
//                                topBanners.append(contentsOf: try decoder.decode([ProductMediaDetailModel].self, from: listData!))
//
//                            }
                            completion((user,videosArr,productArr),msg,errorMsg)
                        }
                        else {
                            completion((nil,nil,nil),msg,errorMsg)
                        }
                    }
                    else {
                        completion((nil,nil,nil),msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
}




