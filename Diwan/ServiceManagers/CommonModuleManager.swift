
//  UserModuleManager


import Foundation

import Alamofire

class CommonModuleManager: AFManagerProtocol {
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
    }
    
    
    
    var isSuccess = false
    var isShowAlert = false
    var htmlString = ""

    func apiGeneric<T : Decodable> (_ param: AFParam,isLoader:Bool , modelType : T.Type , completion: @escaping ([Any]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void)
    {

        print(modelType)
        
        self.isSuccess = false
        
        //request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: isLoader, success: { (response) in
            
            guard response != nil else { return }
            
            do
            {
                let dict = try JSONSerialization.jsonObject(with: response!, options:[]) as? Dictionary<String, Any>
                let arr = dict?["response_data"] as? [Any]
                let message = dict?["message_to_show"] as? String
                let erroMessage = dict?["error_internal"] as? String
                completion(arr,message,erroMessage)
            }
            catch let error
            {
                print("Error: ", error)
            }
            
            
        }) { (error) in
            print("Error: ", error as Any)
            failure(error)
        }
    }
    func getFAQApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([FAQ]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .post, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: FAQ.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([FAQ].self, from: jsonData!)
                        //let result = try decoder.decode(FAQ.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func submitQueryApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping (User?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .post, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: User.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData[0])
                        let result = try decoder.decode(User.self, from: jsonData!)
                        //let result = try decoder.decode(FAQ.self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func getCMSApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([CMSData]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: CMSData.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([CMSData].self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    
    func getCountriesApi<T : Decodable>(endPoint:String,param:[String:Any]?,header:[String : String],modelType : T.Type ,completion: @escaping ([CountryStateCityProtocol]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){
            
            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: Country.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([T].self, from: jsonData!)
                        completion((result as! [CountryStateCityProtocol]),msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func getStateByCountryApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([State]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: State.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([State].self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func getCityByStateApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([City]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: City.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([City].self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func getCurrenciesApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([Currency]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: City.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([Currency].self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
    func getSocialPagesApi(endPoint:String,param:[String:Any]?,header:[String : String],completion: @escaping ([SocialMedia]?,String?,String?) -> Void, failure:@escaping (Error?) -> Void){

            let param = AFParam(endpoint: endPoint, params: param, headers: header, method: .get, parameterEncoding: JSONEncoding.default, images: [])
            
            self.apiGeneric(param, isLoader: true, modelType: City.self, completion:  { (data,msg,errorMsg) in
                
                do {
                    let decoder = JSONDecoder()
                    if let resData = data {
                        let jsonData = try? JSONSerialization.data(withJSONObject:resData)
                        let result = try decoder.decode([SocialMedia].self, from: jsonData!)
                        completion(result,msg,errorMsg)
                    }
                    else {
                        completion(nil,msg,errorMsg)
                    }
                    
                }catch{
                    print(error)
                    print("Error in Serialization")
                }
                
            }) { (error) in
                print("Error: ", error as Any)
                failure(error)
            }
    }
}




