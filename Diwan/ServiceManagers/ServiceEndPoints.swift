//
//  ServiceEndPoints.swift
//  Diwan
//
//  Created by Waqas Ali on 14/06/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import Foundation
enum ApiEndPoints{
    // MARK:-USER
    static let statusCode = "2000"
    static let signIn = "User/Login"
    static let socialSignIn = "User/SocialLogin"
    static let signUp = "User/AddUsers"
    static let editProfile = "User/EditProfile"
    static let verifyOTP = "User/VerifyUserOtp"
    static let resendOTP = "User/ResendOtp"
    static let forgotEmail = "User/ForgetPassword"
    static let resetPassword = "User/ForgetPasswordVerify"
    static let updatePassword = "User/ChangePassword"
    static let logout = "User/Logout"
    static let getProfile = "User/GetProfile"
    static let removeProfilePicture = "User/RemoveProfilePicture"
    static let updateProfilePicture = "User/UserProfilePicture"
    static let getNavigationList = "User/NavigationList"
    //MARK:-COMMON
    static let getFAQ = "Common/GetFaq"
    static let getCMS = "Common/GetTermandcondition"
    static let getCountries = "OG/GetCountries"
    static let getStateByCountry = "OG/GetStateByCountryId"
    static let getCityByState = "OG/GetCitiesByStateId"
    static let addQuery = "Common/AddQuery"
    static let getCurrencies = "OG/GetCurrencies"
    static let getSocialMediaPages = "OG/GetSocialMediaPages"
    //MARK:-Addresses
    static let addUserShippingAddress = "User/AddUserShippingAddress"
    static let deleteUserShippingAddress = "User/DeleteUserShippingAddress"//Append "Id"
    static let setDefaultShippingAddress = "User/SetDefaultShippingAddress"//Append "Id"
    static let getUserShippingAddress = "User/GetUserShippingAddress"
    static let editUserShippingAddress = "User/EditUserShippingAddress"
    //
//    static let updateProfile = "accounts/updateprofile"
//    static let forgetPwd = "accounts/forgetpassword"
//    static let changePwd = "accounts/changepassword"
//    static let getTerms = "accounts/getterms"
    //MARK:-Category
    static let getCategories = "Catalog/GetCategories"
    static let getCategoriesByParentId = "Catalog/GetCategoriesByParentId"//Append "Id"
    static let getProductsByCategoryId = "Catalog/GetProductsByCategoryId"//Append "Id"
    static let getProductDetail = "Catalog/GetProductDetail"
    //MARK:-Product
    static let getSubjectWiseProductsByCategory = "Catalog/GetSubjectWiseProductsByCategory"
    static let getStoreProductsByCategory = "Catalog/GetStoreProductsByCategory"
    static let getProducts = "Catalog/GetProducts"
    static let notifyMe = "Catalog/AddProductUserNotification" //Append "Id"
    static let addToFavorite = "Catalog/AddToFavorities" //Append "Id"
    static let getUserWishList = "Catalog/GetUserWishList" //Append "Id"
    static let getScreenFilterbyScreenId = "Common/GetScreenFilterbyScreenId" //Append "Id"
    //MARK:-Product Detail
    static let getSimilarProducts = "Catalog/GetSimilarProducts"//Append "Id"
    //MARK:-Library
    static let getSections = "Catalog/GetSections"
    static let getGenres = "Catalog/GetGenres"
    //MARK:-Brands
    static let getBrands = "Catalog/GetBrands"
    //MARK:-Author
    static let getAuthors = "Catalog/GetAuthors"
    //MARK:-Ehsan
    static let getEhsanVideos = "User/GetEhsanVideos"
    static let getVideos = "User/GetVideos"
    //MARK:-Celebrities
    static let getCelebrities = "User/GetCelebrities"
    static let getCelebritiesDetail = "User/GetCelebritiesDetail/"
    static let getHomeMainData = "Catalog/GetHomeMainData"
    static let getHomeAttributeData = "Catalog/GetHomeAttributeData"
    static let getHomeTagsData = "Catalog/GetHomeTagsData"
}


